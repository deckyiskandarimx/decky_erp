<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Swiftmailer {

    var $ci;

    function __construct() {
        $this->ci = & get_instance();
		require_once 'swiftmailer/swift_required.php';
    }

	function sendMail($to, $subject, $text){
		//Create the Transport 
		$transport = Swift_SmtpTransport::newInstance ($this->ci->config->item('email_server'), $this->ci->config->item('email_port'))
		->setUsername($this->ci->config->item('email_username'))
		->setPassword($this->ci->config->item('email_password')); 
		$mailer = Swift_Mailer::newInstance($transport); 

		//Create a message 
		$message = Swift_Message::newInstance($subject)
		->setFrom(array($this->ci->config->item('email_username') => $this->ci->config->item('email_from')))
        ->setFrom($this->ci->config->item('email_from'))
		->setTo($to)
		->setBody($text); 

		//Send the message 
		$result = $mailer->send($message);
		return $result;
	}


	function sendHTMLMail($to, $subject, $text){
		//Create the Transport 
		$transport = Swift_SmtpTransport::newInstance ($this->ci->config->item('email_server'), $this->ci->config->item('email_port'))
		->setUsername($this->ci->config->item('email_username'))
		->setPassword($this->ci->config->item('email_password')); 
		$mailer = Swift_Mailer::newInstance($transport); 

		//Create a message 
		$message = Swift_Message::newInstance($subject)
		->setFrom(array($this->ci->config->item('email_username') => $this->ci->config->item('email_from')))
        ->setFrom($this->ci->config->item('email_from'))
		->setTo($to)
		->setBody($text, 'text/html'); 

		//Send the message 
		$result = $mailer->send($message);
		return $result;
	}
}