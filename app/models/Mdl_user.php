<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mdl_user extends CI_model
{
    
    
    public function getuser(){
        
        $this->db->select("users.user_id,users.username,users.fullname, users.emp_id as emp_id, users.mobile_phone as mobile_phone, users.initial, ref_users_type.name as user_type,ref_status.name as status, parent.fullname as manager");
        $this->db->order_by("user_id", "DESC");
        $this->db->join("users parent","parent.user_id=users.parent_id","left");
        $this->db->join("ref_status","ref_status.status=users.status");
        $this->db->join("ref_users_type","ref_users_type.user_type=users.user_type");
        $data = $this->db->get("users")->result();
        
        return $data;
        
    }
    
}
