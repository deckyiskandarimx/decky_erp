<?php

class modul extends CI_Model {

    public function delete($table, $id, $idname) {
        if (!$id) {
            return FALSE;
        }
        $this->db->where($id, $idname);
        $this->db->delete($table);
    }

    function deleete($table, $id_name, $id) {
        $this->db->where($id_name, $id);
        $this->db->delete($table);
    }

    public function simpan($table, $kirim) {
        $this->db->insert($table, $kirim);
    }

    public function edit($table, $kirim, $id_name, $id) {
        $this->db->where($id_name, $id);
        $this->db->update($table, $kirim);
    }

    function save($table, $kirim, $f_name, $id) {
        if ($id == NULL) {
            $this->simpan($table, $kirim);
        } else {
            $this->edit($table, $kirim, $f_name, $id);
        }
    }


    function ss($table, $data) {
        $this->db->insert($table, $data);
    }

    function get_table($table) {
        return $this->db->get($table);
    }

    public function tabel_where($table, $cari = NULL, $like = NULL, $limit = NULL, $offset = NULL) {
        if ($cari != NULL) {
            $data = $this->db->where($cari);
        }
        if ($like != NULL) {
            $data = $this->db->like($like);
        }
        if ($limit != NULL) {
            if ($offset != NULL) {
                $data = $this->db->limit($limit, $offset);
            } else {
                $data = $this->db->limit($limit);
            }
        }
        $data = $this->db->get($table);
        return $data;
    }
    
    function getGroup($table, $f_id, $f_name){
        $data = $this->db->get($table);
        $send = array();
        $send['0'] = '';
        foreach($data->result() as $d){
            $send[$d->$f_id]= $d->$f_name;
        }
        return $send;
    }

}
