<?php

$config['protocol'] = 'smtp'; // kita menggunakan protocol SMTP (simple mail transfer protocol) untuk melakukan pengiriman email

$config['smtp_host'] = 'mail.mydomain.com'; // hostname dari SMTP kita
$config['smtp_port'] = '587';
$config['smtp_user'] = 'no-reply@mydomain.com';
$config['smtp_pass'] = 'mypassword';
$config['charset'] = 'iso-8859-1';
$config['mailtype'] = 'html';
$config['wordwrap'] = TRUE;
