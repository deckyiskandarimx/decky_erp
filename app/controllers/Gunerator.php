<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Gunerator extends Ci_Controller
{
    
    public function __construct() {
        parent::__construct();
    }

    public function invoice($id){
        define('FPDF_FONTPATH',$this->config->item('fonts_path'));
/*
        $this->db->select("invoice_generated.invoice_number, "
        				. "invoice_generated.created_date invoice_date, "
        				. "a.quotation_id as quotation_id, "
                        . "a.quotation_number as quotation_number, "
                        . "a.quotation_date as quotation_date, "
                        . "b.client_name as agency_name, "
                        . "(select city from ref_city where ref_city.id = b.address_city) as city_agency, "
                        . "b.address_zip as agency_zip, "
                        . "b.phone as agency_phone, "
                        . "b.finance_name as agency_finance_name, "
                        . "b.finance_phone as agency_finance_phone, "
                        . "b.finance_email as agency_finance_email, "
                        . "b.media_name as agency_media_name, "
                        . "b.media_phone as agency_media_phone, "
                        . "b.media_email as agency_media_email, "
                        . "b.npwp as agency_npwp, "
                        . "b.address_street as address_street, "
                        . "c.client_name as advertiser_name, "                        
                        . "(select category_name from ref_business_category where ref_business_category.business_category_id = c.business_category_id) as adv_category, "                        
                        . "d.brand_name as brand_name, "
                        . "a.campaign_name as campaign_name, "
                        . "e.name as payment_type, "
                        . "a.total as total,"
                        . "a.package_id,"
                        . "f.name as status,"
                        . "(select fullname from users where users.user_id = a.created_by) as sales_name,"
                        . "(select username from users where users.user_id = a.created_by) as sales_email,"
                        . "(select mobile_phone from users where users.user_id = a.created_by) as sales_phone");
        $this->db->join("quotation a", "a.quotation_id = invoice_generated.quotation_id", "left outer");
        $this->db->join("client b","a.agency_id = b.client_id", "left outer");
        $this->db->join("client c","a.advertiser_id = c.client_id", "left outer");
        $this->db->join("ref_brand d","a.brand_id = d.brand_id", "left outer");
        $this->db->join("ref_payment e","a.payment_type = e.payment_id", "left outer");
        $this->db->join("ref_quotation_status f","a.status = f.id", "left outer");
        $this->db->where("invoice_generated.id", $id);
        $data["invoice"] = $this->db->get("invoice_generated")->row();    
        
        if($data["invoice"]->package_id > 0){
            
            $this->db->select("name as product, price, 0 as diff, 1 as amount, 0 as diskon");
            $this->db->where("id", $data["invoice"]->package_id);
            $data["listitem"] = $this->db->get("package")->result();
            
        }else{
        
                $this->db->select("item_id,  product.name as product, "
                        . "product_category.name as type, "
                        . "quotation_item.price, "
                        . "quotation_item.discount, "
                        . "quotation_item.amount, "
                        . "quotation_item.target, "
                        . "quotation_item.unit,  "
                        . "ref_province.province, "
                        . "ref_city.city, "
                        . "quotation_item.area, "
                        . "quotation_item.start_date, "
                        . "quotation_item.end_date, "
                        . "quotation_item.price,"
                        . "quotation_item.amount,"
                        . "quotation_item.discount as diskon");
                    $this->db->join("product_partner_rel","product_partner_rel.pp_id = quotation_item.pp_id");
                    $this->db->join("product","product_partner_rel.product_id = product.product_id");
                    $this->db->join("product_category","product_category.id = product.category_id");
                    $this->db->join("ref_province","ref_province.id = quotation_item.province");
                    $this->db->join("ref_city","ref_city.id = quotation_item.city");
                    $this->db->where("quotation_id", $data['invoice']->quotation_id);
                    $data["listitem"] = $this->db->get("quotation_item")->result();
        }
*/

		$this->db->join("quotation_item b", "a.quotation_id = b.quotation_id and a.item_id = b.item_id");
		$this->db->join("quotation c", "a.quotation_id = c.quotation_id");
        $this->db->join("client d","c.advertiser_id = d.client_id");
        $this->db->join("ref_brand e","c.brand_id = e.brand_id");
        $this->db->join("ref_business_category f", "d.business_category_id = f.business_category_id");
        $this->db->join("ref_province","ref_province.id = d.address_province");
        $this->db->join("ref_city","ref_city.id = d.address_city");
        $this->db->where("a.id", $id);
        $data["invoice"] = $this->db->get("invoice_generated a")->row();    
		//var_dump($data);die;

		$amount = $data['invoice']->amount * $data['invoice']->price;
		$total = ($amount - ($amount * $data['invoice']->discount / 100)) * 1.1;
        $data["terbilang"] = $this->terbilang($total); //$this->terbilang("1100000");
        $this->load->view('template/pdf/invoice', $data);
        
    }
    
    function kekata($x=1100000) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = $this->kekata($x - 10). " belas";
        } else if ($x <100) {
            $temp = $this->kekata($x/10)." puluh". $this->kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . $this->kekata($x - 100);
        } else if ($x <1000) {
            $temp = $this->kekata($x/100) . " ratus" . $this->kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . $this->kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = $this->kekata($x/1000) . " ribu" . $this->kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = $this->kekata($x/1000000) . " juta" . $this->kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = $this->kekata($x/1000000000) . " milyar" . $this->kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = $this->kekata($x/1000000000000) . " trilyun" . $this->kekata(fmod($x,1000000000000));
        }     
            return $temp;
    }
    
    function terbilang($x, $style=3) {
        if($x<0) {
            $hasil = "minus ". trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil." Rupiah";
    }
	
	function io($idio){
		// header data
		$this->db->select("io.io_number,
							to_char(io.created_on,'dd-Mon-yyyy') io_date,
							quotation.campaign_name,
							advertiser.client_name advertiser,
							agency.client_name agency,
							quotation.quotation_number,
							quotation.campaign_name campaign,
							brand.brand_name,
							(select fullname from users where users.user_id = quotation.created_by) as sales_name,
							(select username from users where users.user_id = quotation.created_by) as sales_email,
							(select mobile_phone from users where users.user_id = quotation.created_by) as sales_phone");

		$this->db->join("quotation", "io.quotation_id = quotation.quotation_id", "left outer");
		$this->db->join("client as advertiser", "quotation.advertiser_id = advertiser.client_id", "left outer");
		$this->db->join("client as agency", "quotation.agency_id = agency.client_id", "left outer");
		$this->db->join("ref_brand as brand", "quotation.brand_id = brand.brand_id", "left outer");
		$this->db->where("io.io_id", $idio);
		$data['header'] = $this->db->get("io")->row();
		
		// item data
        $this->db->select("quotation_item.item_id,  "
                . "product.name as product, "
                . "product_category.name as type, "
                . "quotation_item.price, "
                . "quotation_item.discount, "
                . "quotation_item.amount, "
                . "quotation_item.target, "
                . "quotation_item.unit,  "
                . "province.province, "
                . "city.city, "
                . "quotation_item.area, "
                . "quotation_item.start_date, "
                . "quotation_item.end_date, "
                . "quotation_item.price,"
                . "quotation_item.amount,"
                . "quotation_item.discount as diskon,"
                . "partner.*");
		
		$this->db->join("quotation_item", "io.quotation_id = quotation_item.quotation_id", "left outer");
		$this->db->join("ref_province as province", "quotation_item.province = province.id", "left outer");
		$this->db->join("ref_city as city", "quotation_item.city = city.id", "left outer");
        $this->db->join("product_partner_rel","product_partner_rel.pp_id = quotation_item.pp_id", "left outer");
        $this->db->join("product","product_partner_rel.product_id = product.product_id", "left outer");
        $this->db->join("product_category","product_category.id = product.category_id", "left outer");
		$this->db->join("partner", "product_partner_rel.partner_id = partner.partner_id", "left_outer");
		$this->db->where("io.io_id", $idio);
		$data['item'] = $this->db->get("io")->result();

        define('FPDF_FONTPATH',$this->config->item('fonts_path'));
        $this->load->view('template/pdf/io', $data);
//		var_dump($data);die;
	}

}