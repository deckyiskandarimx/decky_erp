<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

        public $username;
        public $password;
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            
                $msg = $this->input->get_post("msg");
            
		$data['title'] = 'Login';
                $data['msg'] = $msg;
		$this->template->load('auth', 'auth/login', $data);
		//		$this->load->view('auth/login');
	}

        public function checklogin(){
            
            $this->username = $this->input->post("username");
            $this->password = $this->input->post("password");
            
            $query = $this->db->query("select user_login('".$this->username."','".$this->password."')")->row();
            $result = explode(";", $query->user_login);    
            //var_dump($result[2]); exit();
            if($result[0] == "00"){  
                $this->db->select("ref_users_type.name as tipeakun");
                $this->db->join("ref_users_type", "users.user_type = ref_users_type.user_type");
                $datauser = $this->db->get_where("users", array("user_id"=> $result[1]))->row();
                $data["code"] = $result[0];
                $data["user_id"] = $result[1];
                $data["username"] = $result[2];
                $data["account_type"] = $result[3];
                $data["role"] = $datauser->tipeakun;
                $data["fullname"] = $result[4];
                $data["LOGEDIN"] = true;
                $data["login_status"] = "success";
                
                $this->session->set_userdata($data);
              
                
                echo json_encode($data); 
                exit();

            }else{
                $data["token"] =  $this->security->get_csrf_hash(); 
                $data["LOGEDIN"] = false;
                $data["login_status"] = "invalid";
                
                echo json_encode($data);
                exit();
                
            }
            
        }
        
	public function register()
	{
		$data['title'] = 'register';
		$this->template->load('default', 'auth/register', $data);
		//		$this->load->view('auth/login');
	}

	public function forget()
	{
                $this->form_validation->set_rules('email', 'email', 'valid_email');
                $send = $this->input->get_post("send");
            
                if ($this->form_validation->run() == FALSE) {
            
                    $data['title'] = 'forget';
                    $data['send'] = $send;
                    $this->template->load('auth', 'auth/forget', $data);
                    //		$this->load->view('auth/login');
                }else{
                    
                    $email = $this->input->post("email");
                    
                    $q = $this->db->query("select user_forget_password('".$email."');")->row();
                    
                    $qexp = explode(";", $q->user_forget_password);
                    
                    if($qexp[0] == "00"){
                   
                        $subject = "Forget Password Notification";

                        $msg = "<html>"
                                . "<body>"
                                . "<p>Hi,</p>"
                                . "<p>You have just request to change your password, please click link bellow</p>"
                                . "<p></p>"
                                . "<p><a href='".base_url()."auth/changepswd/?c=".$qexp[1]."' mc:disable-tracking>".base_url()."auth/changepswd/?c=".$qexp[1]."</a></p>"
                                . "<p></p>"
                                . "<p>Cheers</p>"
                                . "</body>"
                                . "</html>";

                        
                        
                        $this->swiftmailer->sendHTMLMail($email, $subject, $msg);
                        redirect(base_url()."auth/forget?send=success");
                    }
                    
                }
	}
        
        public function changepswd(){
           
            
            $this->form_validation->set_rules('password', 'New Password', 'trim|required|strip_tags|min_length[6]');
            $this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|strip_tags|matches[password]');
            
            $kodehidden = $this->input->post("kodehidden");
            $kode = $this->input->get_post("c");
            $msg = $this->input->get_post("msg");
            $password = $this->input->post("password");            
            
            if ($this->form_validation->run() == FALSE) {              
                $data = array();
                $data['title'] = 'Change Password';
                $data['kode'] = $kode;
                $data['msg'] = $msg;
                $this->template->load('auth', 'auth/changepswd', $data);

            
            }else{
                $query = "select user_activate('".$kodehidden."', '".$password."');";                
                $q = $this->db->query($query)->row();                   
                    $qexp = explode(";", $q->user_activate);                    
                    if($qexp[0] == "00"){
                        redirect(base_url());
                    }  else {
                        redirect(base_url()."auth/changepswd/?msg=invalidcode");
                    }
            }
            
            
        }
        
        function logout(){
        
        
        $this->session->sess_destroy();
        redirect(base_url()."auth");
        
    }
}
