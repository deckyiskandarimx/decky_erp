<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reminder extends CI_controller
{
    
    
    public function __construct() {
        parent::__construct();
        
    }
    
    function content($idquotation = null){
        
        
        $this->db->where("quotation_id", $idquotation);
        $dataquotation = $this->db->get("quotation")->row();
        
        //die(var_dump($dataquotation));
        
        $this->db->where_in("user_type", array("AC03","AC09"));
        $userdata = $this->db->get("users")->result();
        $subject = "Upcoming Campaign";
        foreach ($userdata as $dataemail) {
            $msg = "<html>"
                    . "<body>"
                    . "<p>Dear " . $dataemail->fullname . ",</p>"
                    . "<p></p>"
                    . "<p>There is upcoming campaign as follows: </p>"
                    . "<p></p>"
                    . "<p>Quotation Number: ".$dataquotation->quotation_number."</p>"
                    . "<p>Campaign Name: ".$dataquotation->campaign_name."</p>";
            
            
            $this->db->where("quotation_id", $dataquotation->quotation_id);
            $this->db->join("product_partner_rel b","a.pp_id = b.pp_id");
            $this->db->join("product c","b.product_id = c.product_id");
            $item = $this->db->get("quotation_item a")->result(); 
          
            $nom = 1;
            foreach ($item as $dataitem){                
                $msg .= "<p>".$nom++." Product Name: ".$dataitem->name."</p>";
                $msg .= "<ul>";
                $msg .= "<p><li>Start date: ".$dataitem->start_date."</li></p>";
                $msg .= "<p><li>End date: ".$dataitem->end_date."</li></p>";
                $msg .= "<p><li>Amount: ".$dataitem->amount."</li></p>";
                $msg .= "</ul>";
            }
            
                    $msg .= "<p></p>"
                            ."<p>Please ensure the campaign material is ready.</p>"
                    . "<p></p>"
                    . "<p></p>"
                    . "<p>Cheers</p>"
                    . "</body>"
                    . "</html>";

            $this->swiftmailer->sendHTMLMail($dataemail->username, $subject, $msg);
            
        }        
        echo "Sukses";
        exit();
    }
    
    
    function quotation($idquotation = null){
        
         
        $this->db->where("quotation_id", $idquotation);
        $dataquotation = $this->db->get("quotation")->row();
        
        //die(var_dump($dataquotation));
        
        $this->db->where_in("user_type", array("AC03"));
        $userdata = $this->db->get("users")->result();
        $subject = "Quotation to be processed";
        foreach ($userdata as $dataemail) {
            $msg = "<html>"
                    . "<body>"
                    . "<p>Dear " . $dataemail->fullname . ",</p>"
                    . "<p></p>"
                    . "<p>You have quotation to be processed: ".$dataquotation->quotation_number.".</p>";
           
                    $numberOfWeeks = 2;                    
                    $date = $dataquotation->quotation_date;                    
                    $newdate = strtotime ( '+2 weeks' , strtotime ( $date ) ) ;
                    $newdate = date ( 'd-m-Y' , $newdate );

                    $msg .= "<p></p>"
                            ."<p>Please ensure the signed quotation is uploaded before ".$newdate."</p>"
                    . "<p></p>"
                    . "<p></p>"
                    . "<p>Cheers</p>"
                    . "</body>"
                    . "</html>";
            
            $this->swiftmailer->sendHTMLMail($dataemail->username, $subject, $msg);
            
        }
        
        echo "Sukses";
        exit();
    }
    
    
    function sendhtml(){
        
        $subject = "Surve Cipikabook mate";
        $this->swiftmailer->setContentType("text/html");
        $this->swiftmailer->setBody($this->getPartial('view/template/mail/receipt.html'), 'text/html');
        $this->swiftmailer->sendHTMLMail("miftahsteven@gmail.com", $subject, $msg);
        
        echo "Send";
        exit();
    }
    
}

