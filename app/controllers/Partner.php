<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Partner extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $cekid = $this->session->userdata("user_id");
        if ($cekid == "") {
            redirect(base_url() . "auth");
        }
    }

    public function index() {

        $data['title'] = 'Partner Management';
        $this->db->select("partner_id, "
                . "partner.name as name, "
                . "partner.other_street as other_street, "
                . "partner.other_country as other_country, "
                . "address_street, partner.status as status, "
                . "ref_doc_status.name as docstatusname");
        $this->db->join("ref_doc_status", "ref_doc_status.status = partner.doc_status");
        $this->db->where("remove_flag", 0);
        $this->db->order_by("partner_id", "DESC");
        $data["partner"] = $this->db->get("partner")->result();
        $this->template->load('default', 'partner/index', $data);
    }

    public function add() {
        // invalid permission
        if ($this->session->userdata("account_type") != "AC06" &&
                $this->session->userdata("account_type") != "AC08" &&
                $this->session->userdata("account_type") != "AC07")
            redirect(base_url("partner"));

        //datacompany
        $name = $this->input->post("name");
        $countrylist = $this->input->post("countrylist");
        $othercountry = $this->input->post("othercountry");
        $province = $this->input->post("province");
        $city = $this->input->post("city");
        $district = $this->input->post("district");
        $subdistrict = $this->input->post("subdistrict");
        $street = $this->input->post("street");
        $blok = $this->input->post("blok");
        $homenumber = $this->input->post("homenumber");
        $rt = $this->input->post("rt");
        $rw = $this->input->post("rw");
        $zip = $this->input->post("zip");

        //othercountry on company info
        $provinceot = $this->input->post("provinceot");
        $cityot = $this->input->post("cityot");
        $streetot = $this->input->post("streetot");
        $zipot = $this->input->post("zipot");

        //financeinfo
        $accountname = $this->input->post("accountname");
        $bankacc = $this->input->post("bankacc");
        $bankname = $this->input->post("bankname");
        $swiftid =  $this->input->post("swiftid");
        $npwp = $this->input->post("npwp");
        $npwpaddress = $this->input->post("npwpaddress");

        //datanpwpother
        //indonesia
        $countrylistnpwp = $this->input->post("countrylistnpwp");
        $provincenpwp = $this->input->post("provincenpwp");
        $citynpwp = $this->input->post("citynpwp");
        $districtnpwp = $this->input->post("districtnpwp");
        $subdistrictnpwp  = $this->input->post("subdistrictnpwp");
        $streetnpwp = $this->input->post("streetnpwp");
        $bloknpwp = $this->input->post("bloknpwp");
        $homenumbernpwp = $this->input->post("homenumbernpwp");
        $rtnpwp = $this->input->post("rtnpwp");
        $rwnpwp = $this->input->post("rwnpwp");
        $zipnpwp = $this->input->post("zipnpwp");

        //othercountry
        $othercountrynpwp = $this->input->post("othercountrynpwp");
        $provincenpwpot = $this->input->post("provincenpwpot");
        $citynpwpot = $this->input->post("citynpwpot");
        $streetnpwpot = $this->input->post("streetnpwpot");
        $zipnpwpot = $this->input->post("zipnpwpot");

        //salesinfo
        $s_name = $this->input->post("s_name");
        $s_phone = $this->input->post("s_phone");
        $s_email = $this->input->post("s_email");

        //finance info
        $o_name = $this->input->post("o_name");
        $o_phone = $this->input->post("o_phone");
        $o_email = $this->input->post("o_email");


        $this->form_validation->set_rules('name', 'Name', 'trim|required');

        if($countrylist == 1){
            $this->form_validation->set_rules('province', 'Province', 'callback_check_select');
            $this->form_validation->set_rules('city', 'City', 'callback_check_select');
            $this->form_validation->set_rules('street', 'Street', 'trim|required');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required');
        }else{
            $this->form_validation->set_rules('othercountry', 'Other Country', 'trim|required');
            //$this->form_validation->set_rules('provinceot', 'Province', 'trim|required');
            //$this->form_validation->set_rules('cityot', 'City', 'trim|required');
            $this->form_validation->set_rules('streetot', 'Street', 'trim|required');
            $this->form_validation->set_rules('zipot', 'Zip', 'trim|required');
        }

        $this->form_validation->set_rules('accountname', 'Account Name', 'trim|required');
        $this->form_validation->set_rules('bankacc', 'Account Bank', 'trim|required');
        $this->form_validation->set_rules('bankname', 'Bank Name', 'trim|required');
        $this->form_validation->set_rules('npwp', 'NPWP', 'trim|required');

        //other npwp address
        //$this->form_validation->set_rules('othercountrynpwp', 'NPWP Country', 'trim|required');


        $this->form_validation->set_rules('s_name', 'Sales Name', 'trim|required');
        $this->form_validation->set_rules('s_phone', 'Sales Phone', 'trim|required');
        $this->form_validation->set_rules('s_email', 'Sales Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('o_name', 'Finance Name', 'trim|required');
        $this->form_validation->set_rules('o_phone', 'Finance Phone', 'trim|required');
        $this->form_validation->set_rules('o_email', 'Finance Email', 'trim|required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $data["companyname"] = $name;
            $data["countrylist"] = $countrylist;
            $data["othercountry"] = $othercountry;
            $data["province"] = $province;
            $data["city"] = $city;
            $data["district"] = $district;
            $data["subdistrict"] = $subdistrict;
            $data["listcity"] = $this->db->get("ref_city",array("province"=>$province))->result();

            $data["street"] = $street;
            $data["blok"] = $blok;
            $data["homenumber"] = $homenumber == 0 ? 0:$homenumber;
            $data["rt"] = $rt == 0 ? 0:$rt;
            $data["rw"] = $rw == 0 ? 0:$rw;
            $data["zip"] = $zip == 0 ? 0:$zip;

            $data["accountname"] = $accountname;
            $data["bankacc"] = $bankacc;
            $data["bankname"] = $bankname;
            $data["swiftid"] = $swiftid;
            $data["npwp"] = $npwp;
            $data["countrylistnpwp"] = $countrylistnpwp;
            $data["othercountrynpwp"] = $othercountrynpwp;
            $data["othercitynpwp"] = $othercitynpwp;
            $data["streetnpwp"] = $streetnpwp;
            $data["districtnpwp"] = $districtnpwp;
            $data["subdistrictnpwp"] = $subdistrictnpwp;
            $data["bloknpwp"] = $bloknpwp;
            $data["homenumbernpwp"] = $homenumbernpwp;
            $data["rtnpwp"] = $rtnpwp;
            $data["rwnpwp"] = $rwnpwp;
            $data["zipnpwp"] = $zipnpwp;

            $data["s_name"] = $s_name;
            $data["s_phone"] = $s_phone;
            $data["s_email"] = $s_email;
            $data["o_name"] = $o_name;
            $data["o_phone"] = $o_phone;
            $data["o_email"] = $o_email;
            $data["id"] = "";
            $data["mod"] = "add";


            $data['title'] = 'Add Partner';


            $this->db->order_by("id", "asc");
            $data["province"] = $this->db->get("ref_province")->result();

            //$data["city"] = $this->db->get("ref_city")->result();
            //$data["district"] = $this->db->get("ref_district")->result();
            //$data["subdistrict"] = $this->db->get("ref_subdistrict")->result();
            $data["tokenstart"] = $this->security->get_csrf_hash();


//            $this->db->select("item_id, product.name, quotation_item.price, quotation_item.discount, quotation_item.amount, "
//                    . " quotation_item.target, quotation_item.unit,  ref_province.province, ref_city.city, quotation_item.area, "
//                    . " quotation_item.start_date, quotation_item.end_date");
//            $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
//            $this->db->join("product", "product_partner_rel.product_id = product.product_id");
//            $this->db->join("ref_province", "ref_province.id = quotation_item.province");
//            $this->db->join("ref_city", "ref_city.id = quotation_item.city");
//            //$this->db->where("quotation_id", $idquo);
//            $data["listitem"] = $this->db->get("quotation_item")->result();


            $this->template->load('default', 'partner/form', $data);
        } else {
            $this->db->where('name', $name);
            $this->db->select('partner.partner_id');
            $datapartner = $this->db->get('partner')->result();


            if (sizeof($datapartner) != 0) {
                $this->session->set_flashdata('validation_partner_name', '<div class="alert alert-danger">
                <strong>Danger!</strong> Partner name already exists.
                </div>');
                redirect(base_url("partner/add"));
            } else {
                $record["name"] = $name;
            }

            $record["country"] = $countrylist;
            $record["address_province"] = $province;
            $record["address_city"] = $city;
            $record["address_district2"] = $district;
            $record["address_subdistrict2"] = $subdistrict;
            $record["address_street"] = $street;
            $record["blok"] = $blok;
            $record["homenumber"] = $homenumber;
            $record["rt"] = $rt;
            $record["rw"] = $rw;
            $record["zip"] = $zip;
            $record["other_country"] = $othercountry;
            //$record["other_province"] = $provinceot;
            //$record["other_city"] = $cityot;
            $record["other_street"] = $streetot;
            $record["other_zip"] = $zipot;
            $record["account_name"] = $accountname;
            $record["bank_acc"] = $bankacc;
            $record["bank_name"] = $bankname;
            $record["swiftid"] = $swiftid;
            $record["npwp"] = $npwp;

            if($npwpaddress == 1){
                $record["address_npwp"] = 1;
                $record["province_npwp"] = 0;
                $record["city_npwp"] = 0;

            }elseif($npwpaddress == 2){
                $record["address_npwp"] = 2;
                if($countrylistnpwp == 1){
                    $record["country_npwp"] = $countrylistnpwp;
                    $record["province_npwp"] = $provincenpwp;
                    $record["city_npwp"] = $citynpwp;
                    $record["district_npwp2"] = $districtnpwp;
                    $record["subdistrict_npwp2"] = $subdistrictnpwp;
                    $record["street_npwp"] = $streetnpwp;
                    $record["blok_npwp"] = $bloknpwp;
                    $record["homenumber_npwp"] = $homenumbernpwp;
                    $record["rt_npwp"] = $rtnpwp;
                    $record["rw_npwp"] = $rwnpwp;
                    $record["zip_npwp"] = $zipnpwp;
                }else if($countrylistnpwp == 2){
                    $record["country_npwp"] = $countrylistnpwp;
                    $record["country_other_npwp"] = $othercountrynpwp;
                    $record["street_npwp"] = $streetnpwpot;
                    $record["zip_npwp"] = $zipnpwpot;
                }
            }


            $record["othercountry_npwp"] = $othercountrynpwp == 0 ? 0:$othercountrynpwp;
            $record["otherprovince_npwp"] = $provincenpwpot;
            $record["othercity_npwp"] = $citynpwpot;
            $record["otherstreet_npwp"] = $streetnpwpot;
            $record["otherzip_npwp"] = $zipnpwpot;

            $record["sales_name"] = $s_name;
            $record["sales_phone"] = $s_phone;
            $record["sales_email"] = $s_email;

            $record["finance_name"] = $o_name;
            $record["finance_phone"] = $o_phone;
            $record["finance_email"] = $o_email;

            if ($this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC06") {
                $record["status"] = 1;
                $record["doc_status"] = 2;
            } else {
                $record["status"] = 0;
            }
            $record["created_by"] = $this->session->userdata("user_id");

            $this->db->insert("partner", $record);
            $idpartner = $this->db->insert_id();

            if ($this->session->userdata("account_type") != "AC08" && $this->session->userdata("account_type") != "AC06") {

                $this->db->where("user_type", "AC06");
                $userdata = $this->db->get("users")->result();
                $subject = "New Partner Notification";
                foreach ($userdata as $dataemail) {
                    $msg = "<html>"
                            . "<body>"
                            . "<p>Hi " . $dataemail->fullname . ",</p>"
                            . "<p>" . $this->session->userdata("fullname") . " has creating a partner. Please click link bellow to approve it.</p>"
                            . "<p></p>"
                            . "<p><a href='" . base_url("partner/approve/" . $idpartner) . "' mc:disable-tracking>" . base_url("partner/approve/" . $idpartner) . "</a></p>"
                            . "<p></p>"
                            . "<p>Cheers</p>"
                            . "</body>"
                            . "</html>";

                    $this->swiftmailer->sendHTMLMail($dataemail->username, $subject, $msg);
                }
            }
            $this->session->set_flashdata('add_partner', '<div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> New partner has been added.
                </div>');
            redirect(base_url("partner"));
        }
    }

    public function edit($id = NULL) {
        // invalid permission
        if ($this->session->userdata("account_type") != "AC06" &&
                $this->session->userdata("account_type") != "AC08" &&
                $this->session->userdata("account_type") != "AC07")
            redirect(base_url("partner"));

        $name = $this->input->post("name");
        $countrylist = $this->input->post("countrylist");
        $othercountry = $this->input->post("othercountry");
        $province = $this->input->post("province");
        $city = $this->input->post("city");
        $district = $this->input->post("district");
        $subdistrict = $this->input->post("subdistrict");
        $street = $this->input->post("street");
        $blok = $this->input->post("blok");
        $homenumber = $this->input->post("homenumber");
        $rt = $this->input->post("rt");
        $rw = $this->input->post("rw");
        $zip = $this->input->post("zip");

        //othercountry on company info
        $provinceot = $this->input->post("provinceot");
        $cityot = $this->input->post("cityot");
        $streetot = $this->input->post("streetot");
        $zipot = $this->input->post("zipot");

        //financeinfo
        $accountname = $this->input->post("accountname");
        $bankacc = $this->input->post("bankacc");
        $bankname = $this->input->post("bankname");
        $swiftid =  $this->input->post("swiftid");
        $npwp = $this->input->post("npwp");
        $npwpaddress = $this->input->post("npwpaddress");

        //datanpwpother
        //indonesia
        $countrylistnpwp = $this->input->post("countrylistnpwp");
        $provincenpwp = $this->input->post("provincenpwp");
        $citynpwp = $this->input->post("citynpwp");
        $districtnpwp = $this->input->post("districtnpwp");
        $subdistrictnpwp  = $this->input->post("subdistrictnpwp");
        $streetnpwp = $this->input->post("streetnpwp");
        $bloknpwp = $this->input->post("bloknpwp");
        $homenumbernpwp = $this->input->post("homenumbernpwp");
        $rtnpwp = $this->input->post("rtnpwp");
        $rwnpwp = $this->input->post("rwnpwp");
        $zipnpwp = $this->input->post("zipnpwp");

        //othercountry
        $othercountrynpwp = $this->input->post("othercountrynpwp");
        $provincenpwpot = $this->input->post("provincenpwpot");
        $citynpwpot = $this->input->post("citynpwpot");
        $streetnpwpot = $this->input->post("streetnpwpot");
        $zipnpwpot = $this->input->post("zipnpwpot");

        //salesinfo
        $s_name = $this->input->post("s_name");
        $s_phone = $this->input->post("s_phone");
        $s_email = $this->input->post("s_email");

        //finance info
        $o_name = $this->input->post("o_name");
        $o_phone = $this->input->post("o_phone");
        $o_email = $this->input->post("o_email");
        $idhidden = $this->input->post("idhidden");

        $this->form_validation->set_rules('name', 'Name', 'trim|required');

        if($countrylist == 1){
            $this->form_validation->set_rules('province', 'Province', 'callback_check_select');
            $this->form_validation->set_rules('city', 'City', 'callback_check_select');
            $this->form_validation->set_rules('street', 'Street', 'trim|required');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required');
        }else{
            $this->form_validation->set_rules('othercountry', 'Other Country', 'trim|required');
            //$this->form_validation->set_rules('provinceot', 'Province', 'trim|required');
            //$this->form_validation->set_rules('cityot', 'City', 'trim|required');
            $this->form_validation->set_rules('streetot', 'Street', 'trim|required');
            $this->form_validation->set_rules('zipot', 'Zip', 'trim|required');
        }

        $this->form_validation->set_rules('accountname', 'Account Name', 'trim|required');
        $this->form_validation->set_rules('bankacc', 'Account Bank', 'trim|required');
        $this->form_validation->set_rules('bankname', 'Bank Name', 'trim|required');
        $this->form_validation->set_rules('npwp', 'NPWP', 'trim|required');

        //other npwp address
        //$this->form_validation->set_rules('othercountrynpwp', 'NPWP Country', 'trim|required');


        $this->form_validation->set_rules('s_name', 'Sales Name', 'trim|required');
        $this->form_validation->set_rules('s_phone', 'Sales Phone', 'trim|required');
        $this->form_validation->set_rules('s_email', 'Sales Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('o_name', 'Finance Name', 'trim|required');
        $this->form_validation->set_rules('o_phone', 'Finance Phone', 'trim|required');
        $this->form_validation->set_rules('o_email', 'Finance Email', 'trim|required|valid_email');


        $dataedit = $this->db->get_where("partner", array("partner_id" => $id))->row();

        if ($this->form_validation->run() == FALSE) {
            $data["name"] = $dataedit->name;
            $data["country"] = $dataedit->country;
            $data["address_province"] = $dataedit->address_province;

            $this->db->where("province", $dataedit->address_province);
            $data["citylist"] = $this->db->get("ref_city")->result();
            $data["address_city"] = $dataedit->address_city;

            $data["address_district"] = $dataedit->address_district2; //data belum ada
            $data["address_subdistrict"] = $dataedit->address_subdistrict2; //data belum ada

            $data["address_street"] = $dataedit->address_street;
            $data["blok"] = $dataedit->blok;
            $data["homenumber"] = $dataedit->homenumber;
            $data["rt"] = $dataedit->rt;
            $data["rw"] = $dataedit->rw;
            $data["zip"] = $dataedit->zip;
            $data["other_country"] = $dataedit->other_country;
            $data["other_province"] = $dataedit->other_province;
            $data["other_city"] = $dataedit->other_city;
            $data["other_street"] = $dataedit->other_street;
            $data["other_zip"] = $dataedit->other_zip;
            $data["account_name"] = $dataedit->account_name;
            $data["bank_acc"] = $dataedit->bank_acc;
            $data["bank_name"] = $dataedit->bank_name;
            $data["swiftid"] = $dataedit->swiftid;
            $data["npwp"] = $dataedit->npwp;
            $data["address_npwp"] = $dataedit->address_npwp;
            $data["country_npwp"] = $dataedit->country_npwp;
            $data["country_other_npwp"] = $dataedit->country_other_npwp;
            $data["province_npwp"] = $dataedit->province_npwp;
            $this->db->where("province", $dataedit->province_npwp);
            $data["citylist2"] = $this->db->get("ref_city")->result();
            $data["city_npwp"] = $dataedit->city_npwp;
            $data["district_npwp"] = $dataedit->district_npwp;
            $data["subdistrict_npwp"] = $dataedit->subdistrict_npwp;
            $data["street_npwp"] = $dataedit->street_npwp;
            $data["blok_npwp"] = $dataedit->blok_npwp;
            $data["homenumber_npwp"] = $dataedit->homenumber_npwp;
            $data["rt_npwp"] = $dataedit->rt_npwp;
            $data["rw_npwp"] = $dataedit->rw_npwp;
            $data["zip_npwp"] = $dataedit->zip_npwp;
            $data["othercountry_npwp"] = $dataedit->othercountry_npwp;
            $data["otherstreet_npwp"] = $dataedit->otherstreet_npwp;
            $data["otherzip_npwp"] = $dataedit->otherzip_npwp;
            $data["s_name"] = $dataedit->sales_name;
            $data["s_phone"] = $dataedit->sales_phone;
            $data["s_email"] = $dataedit->sales_email;
            $data["o_name"] = $dataedit->finance_name;
            $data["o_phone"] = $dataedit->finance_phone;
            $data["o_email"] = $dataedit->finance_email;
            $data["district"] = $dataedit->address_district2;
            $data["subdistrict"] = $dataedit->address_subdistrict2;
            $data["districtnpwp"] = $dataedit->district_npwp2;
            $data["subdistrictnpwp"] = $dataedit->subdistrict_npwp2;

            $data["id"] = $dataedit->partner_id;
            $data["mod"] = "edit";
            $this->db->where("id <>", 0);
            $data["province"] = $this->db->get_where("ref_province")->result();
            //$data["city"] = $this->db->get_where("ref_city")->result();
            $data['title'] = 'Edit Partner';
            $this->template->load('default', 'partner/form', $data);
        } else {
            $this->db->where('name', $name);
            $this->db->select('partner.partner_id');
            $datapartner = $this->db->get('partner')->row();


            if (sizeof($datapartner) != 0 && $datapartner->partner_id != $idhidden) {
                $this->session->set_flashdata('validation_partner_name', '<div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Danger!</strong> Partner name already exists.
                </div>');
                redirect(base_url("partner/edit/".$idhidden));
            } else {
                $record["name"] = $name;
            }

            $record["country"] = $countrylist;
            $record["address_province"] = $province;
            $record["address_city"] = $city;
            $record["address_district2"] = $district;
            $record["address_subdistrict2"] = $subdistrict;
            $record["address_street"] = $street;
            $record["blok"] = $blok;
            $record["homenumber"] = $homenumber;
            $record["rt"] = $rt;
            $record["rw"] = $rw;
            $record["zip"] = $zip;
            $record["other_country"] = $othercountry;
            //$record["other_province"] = $provinceot;
            //$record["other_city"] = $cityot;
            $record["other_street"] = $streetot;
            $record["other_zip"] = $zipot;
            $record["account_name"] = $accountname;
            $record["bank_acc"] = $bankacc;
            $record["bank_name"] = $bankname;
            $record["swiftid"] = $swiftid;
            $record["npwp"] = $npwp;

            if($npwpaddress == 1){
                $record["address_npwp"] = 1;
                $record["province_npwp"] = 0;
                $record["city_npwp"] = 0;

            }elseif($npwpaddress == 2){
                $record["address_npwp"] = 2;
                if($countrylistnpwp == 1){
                    $record["country_npwp"] = $countrylistnpwp;
                    $record["province_npwp"] = $provincenpwp;
                    $record["city_npwp"] = $citynpwp;
                    $record["district_npwp2"] = $districtnpwp;
                    $record["subdistrict_npwp2"] = $subdistrictnpwp;
                    $record["street_npwp"] = $streetnpwp;
                    $record["blok_npwp"] = $bloknpwp;
                    $record["homenumber_npwp"] = $homenumbernpwp;
                    $record["rt_npwp"] = $rtnpwp;
                    $record["rw_npwp"] = $rwnpwp;
                    $record["zip_npwp"] = $zipnpwp;
                }else if($countrylistnpwp == 2){
                    $record["country_npwp"] = $countrylistnpwp;
                    $record["country_other_npwp"] = $othercountrynpwp;
                    $record["street_npwp"] = $streetnpwpot;
                    $record["zip_npwp"] = $zipnpwpot;
                }


            }


            $record["othercountry_npwp"] = $othercountrynpwp == 0 ? 0:$othercountrynpwp;
            $record["otherprovince_npwp"] = $provincenpwpot;
            $record["othercity_npwp"] = $citynpwpot;
            $record["otherstreet_npwp"] = $streetnpwpot;
            $record["otherzip_npwp"] = $zipnpwpot;

            $record["sales_name"] = $s_name;
            $record["sales_phone"] = $s_phone;
            $record["sales_email"] = $s_email;

            $record["finance_name"] = $o_name;
            $record["finance_phone"] = $o_phone;
            $record["finance_email"] = $o_email;
            if ($this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC06") {
                $record["status"] = 1;
                $record["doc_status"] = 2;
            } else {
                $record["status"] = 0;
            }

            $record["modified_by"] = $this->session->userdata("user_id");
            $where["partner_id"] = $this->input->post("idhidden");
            $this->db->update("partner", $record, $where);

            if ($this->session->userdata("account_type") != "AC06" && $this->session->userdata("account_type") != "AC08") {

                $this->db->where("user_type", "AC06");
                $userdata = $this->db->get("users")->result();
                $subject = "Partner Update Notification";
                foreach ($userdata as $dataemail) {
                    $msg = "<html>"
                            . "<body>"
                            . "<p>Hi " . $dataemail->fullname . ",</p>"
                            . "<p>" . $this->session->userdata("fullname") . " has updating a partner. Please click link bellow to view it.</p>"
                            . "<p></p>"
                            . "<p><a href='" . base_url("partner/approve/" . $id) . "' mc:disable-tracking>" . base_url("partner/approve/" . $id) . "</a></p>"
                            . "<p></p>"
                            . "<p>Cheers</p>"
                            . "</body>"
                            . "</html>";

                    $this->swiftmailer->sendHTMLMail($dataemail->username, $subject, $msg);
                }
            }
            $this->session->set_flashdata('edit_partner', '<div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> Partner has been edited.
            </div>');

            redirect(base_url("partner"));
        }
    }

    function check_select($post_string) {

        if ($post_string > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function check_partner_name() {
        $this->db->where("name", $this->input->post("name"));
        if ($this->input->post("idhidden") > 0) {
            $this->db->where("partner_id !=", $this->input->post("idhidden"));
        }
        $namecheck = $this->db->get("partner")->row();
        $this->form_validation->set_message('check_partner_name', 'Partner "<b>' . $this->input->post("name") . '</b>" already used. Please choose another name');
        return false;
    }

    function actpartner($id = NULL, $status = NULL) {
        // invalid permission
        if ($this->session->userdata("account_type") != "AC06" &&
                $this->session->userdata("account_type") != "AC08")
            redirect(base_url("partner"));

        $record["status"] = $status == 1 ? 0 : 1;
        $record["modified_by"] = $this->session->userdata("user_id");
        $where["partner_id"] = $id;
        if ($id != NULL && $status != NULL) {
            $this->db->update("partner", $record, $where);
        }

        redirect(base_url("partner"));
    }

    public function relation() {
        $this->db->select("pp_id, product.name, partner.name as partner, partner_status, price, discount, product_partner_rel.status as status, ref_doc_status.name as docstatusname");
        $this->db->join("ref_doc_status", "ref_doc_status.status = product_partner_rel.doc_status");
        $this->db->join("product", "product.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");

        $data['title'] = 'Product Partner Relation';
        $data["pp_rel"] = $this->db->get("product_partner_rel")->result();
        $this->template->load('default', 'partner/relation', $data);
    }

    public function add_relation() {
        // invalid permission
        if ($this->session->userdata("account_type") != "AC06" &&
                $this->session->userdata("account_type") != "AC08" &&
                $this->session->userdata("account_type") != "AC07")
            redirect(base_url("partner/relation"));

        $this->form_validation->set_rules('product', 'Product', 'trim|required');
        $this->form_validation->set_rules('partner', 'Partner', 'trim|required');
        $this->form_validation->set_rules('partnerstatus', 'Partner Status', 'trim|required');
        $this->form_validation->set_rules('partnertype', 'Partner Type', 'trim|required');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required|integer');
        $this->form_validation->set_rules('discount', 'Discount', 'trim|required|integer');

        $product = $this->input->post("product");
        $partner = $this->input->post("partner");
        $partnerstatus = $this->input->post("partnerstatus");
        $partnertype = $this->input->post("partnertype");
        $amount = $this->input->post("amount");
        $discount = $this->input->post("discount");

        if ($this->form_validation->run() == FALSE) {
            $data["partner_sel"] = $partner;
            $data["product_sel"] = $product;
            $data["partnerstat_sel"] = $partnerstatus;
            $data["partnertype_sel"] = $partnertype;
            $data["amount"] = $amount;
            $data["discount"] = $discount;
            $data["id"] = "";
            $data["mod"] = "edit_relation";
            $data["partner"] = $this->db->get_where("partner", array('status' => 1, 'doc_status' => 2))->result();
            $data["product"] = $this->db->get_where("product", array('status' => 1, 'doc_status' => 2))->result();
            $data["partnerstatus"] = $this->db->get_where("ref_partner_status")->result();
            $data["partnertype"] = $this->db->get_where("ref_partnership_type")->result();
            $data['title'] = 'Add Relation';
            $this->template->load('default', 'partner/form_relation', $data);
        } else {
            $record["partner_id"] = $partner;
            $record["product_id"] = $product;
            $record["partner_status"] = $partnerstatus;
            $record["partner_type"] = $partnertype;
            $record["price"] = $amount;
            $record["discount"] = $discount;
            $record["created_by"] = $this->session->userdata("user_id");
            $this->db->insert("product_partner_rel", $record);
            redirect(base_url("partner/relation"));
        }
    }

    public function edit_relation($id) {
        // invalid permission
        if ($this->session->userdata("account_type") != "AC06" &&
                $this->session->userdata("account_type") != "AC08" &&
                $this->session->userdata("account_type") != "AC07")
            redirect(base_url("partner/relation"));

        $this->form_validation->set_rules('product', 'Product', 'trim|required');
        $this->form_validation->set_rules('partner', 'Partner', 'trim|required');
        $this->form_validation->set_rules('partnerstatus', 'Partner Status', 'trim|required');
        $this->form_validation->set_rules('partnertype', 'Partner Type', 'trim|required');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required|integer');
        $this->form_validation->set_rules('discount', 'Discount', 'trim|required|integer');

        $product = $this->input->post("product");
        $partner = $this->input->post("partner");
        $partnerstatus = $this->input->post("partnerstatus");
        $partnertype = $this->input->post("partnertype");
        $amount = $this->input->post("amount");
        $discount = $this->input->post("discount");

        $dataedit = $this->db->get_where("product_partner_rel", array("pp_id" => $id))->row();

        if ($this->form_validation->run() == FALSE) {
            $data["partner_sel"] = $dataedit->partner_id;
            $data["product_sel"] = $dataedit->product_id;
            $data["partnerstat_sel"] = $dataedit->partner_status;
            $data["partnertype_sel"] = $dataedit->partner_type;
            $data["amount"] = $dataedit->price;
            $data["discount"] = $dataedit->discount;
            $data["pp_id"] = $dataedit->pp_id;
            $data["mod"] = "edit_relation";
            $data["partner"] = $this->db->get_where("partner", array('status' => 1, 'doc_status' => 2))->result();
            $data["product"] = $this->db->get_where("product", array('status' => 1, 'doc_status' => 2))->result();
            $data["partnerstatus"] = $this->db->get_where("ref_partner_status")->result();
            $data["partnertype"] = $this->db->get_where("ref_partnership_type")->result();
            $data['title'] = 'Edit Relation';
            $this->template->load('default', 'partner/form_relation', $data);
        } else {
            $record["partner_id"] = $partner;
            $record["product_id"] = $product;
            $record["partner_status"] = $partnerstatus;
            $record["partner_type"] = $partnertype;
            $record["price"] = $amount;
            $record["discount"] = $discount;
            if ($this->session->userdata("account_type") == "AC07") {
                $record["status"] = 0;
            }
            $record["modified_by"] = $this->session->userdata("user_id");
            $where["pp_id"] = $this->input->post("pp_id");
            $this->db->update("product_partner_rel", $record, $where);

            if ($this->session->userdata("account_type") != "AC06" && $this->session->userdata("account_type") != "AC08") {

                $this->db->where("user_type", "AC06");
                $userdata = $this->db->get("users")->result();
                $subject = "Product Partner Relation Update Notification";
                foreach ($userdata as $dataemail) {
                    $msg = "<html>"
                            . "<body>"
                            . "<p>Hi " . $dataemail->fullname . ",</p>"
                            . "<p>" . $this->session->userdata("fullname") . " has updating a product partner relation. Please click link bellow to view it.</p>"
                            . "<p></p>"
                            . "<p><a href='" . base_url("partner/relation/approve/" . $pp_id) . "' mc:disable-tracking>" . base_url("partner/relation/approve/" . $pp_id) . "</a></p>"
                            . "<p></p>"
                            . "<p>Cheers</p>"
                            . "</body>"
                            . "</html>";

                    $this->swiftmailer->sendHTMLMail($dataemail->username, $subject, $msg);
                }
            }
            redirect(base_url("partner/relation"));
        }
    }

    public function ajax($mod = null) {

        if ($mod == "country") {
            $countryid = $this->input->post("countryid");
            $prov = $this->db->get("ref_province")->result();
            $opt = "<option value=''> ==Select Province== </option>";
            foreach ($prov as $dataprovince) {
                $opt .= "<option value='" . $dataprovince->id . "'> " . $dataprovince->province . " </option>";
            }

            $ret = '<script>';
            $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
            $ret .= '$("#csrf_city").attr("value","' . $this->security->get_csrf_hash() . '");';
            if (sizeof($prov) > 0) {
                $ret.= '$("#province").attr("disabled",false);';
                $ret.= '$("#province").html("' . $opt . '");';
            } else {
                $ret.= '$("#province").attr("disabled",true);';
            }
            $ret .= '</script>';
            echo $ret;
            exit();
        } elseif ($mod == "province") {
            $provinceid = $this->input->post("provinceid");
            $this->db->where("province", $provinceid);
            $city = $this->db->get("ref_city")->result();
            $opt = "<option value='0'> --Select City-- </option>";
            foreach ($city as $datacity) {
                $opt .= "<option value='" . $datacity->id . "' > " . $datacity->city . " </option>";
            }

            $ret = '<script>';
            $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
            $ret .= '$("#csrf_city").attr("value","' . $this->security->get_csrf_hash() . '");';
            if (sizeof($city) > 0) {
                $ret.= '$("#city").attr("disabled",false);';
                $ret.= '$("#city").html("' . $opt . '");';
            } else {
                $ret.= '$("#city").attr("disabled",true);';
            }
            $ret .= '</script>';
            echo $ret;
            exit();
        }elseif ($mod == "provincenpwp") {
            $provinceid = $this->input->post("provinceid");
            $this->db->where("province", $provinceid);
            $city = $this->db->get("ref_city")->result();
            $opt = "<option value='0'> --Select City-- </option>";
            foreach ($city as $datacity) {
                $opt .= "<option value='" . $datacity->id . "'> " . $datacity->city . " </option>";
            }

            $ret = '<script>';
            $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
            $ret .= '$("#csrf_city").attr("value","' . $this->security->get_csrf_hash() . '");';
            if (sizeof($city) > 0) {
                $ret.= '$("#citynpwp").attr("disabled",false);';
                $ret.= '$("#citynpwp").html("' . $opt . '");';
            } else {
                $ret.= '$("#citynpwp").attr("disabled",true);';
            }
            $ret .= '</script>';
            echo $ret;
            exit();
        }else if($mod == "removeitem"){

            $item_id = $this->input->post("item_id");
            $datarem["remove_flag"] = 1;
            $datarem["remove_by"] = $this->session->userdata("user_id");
            $whereclause["partner_id"] = $item_id;
            $this->db->update("partner", $datarem, $whereclause);

            $ret["partner_id"] = $item_id;
            $ret["message"] = "OK";
            echo json_encode($ret);
            exit();

        }else if($mod == "productbyid"){

            $id = urldecode($this->input->post("item_id"));
            //$this->db->where("category_id", $id);

            $this->db->where("product_family", $id);
            $product = $this->db->get("product2")->result();

            $opt = "<option value=0>-- Select Product --</option>";
            foreach ($product as $dataproduct){
                $opt .= "<option value='".$dataproduct->product_id."'>".$dataproduct->product_name."</option>";
            }

            echo '<script type="text/javascript">';
            echo '$("select#productlist").html("'.$opt.'");';
            echo '$("#tokenxyz").attr("value","'.$this->security->get_csrf_hash().'");';
            echo '$("#tokenxyz2").attr("value","'.$this->security->get_csrf_hash().'");';
            echo '</script>';
            exit();
        }else if($mod == "codebyid"){
            $id = $this->input->post("item_id");
            $this->db->where("product_id", $id);
            $prod= $this->db->get("product2")->row();

            echo '<script type="text/javascript">';
            echo '$("input#code").attr("value","'.$prod->product_code.'");';
            echo '$("#tokenxyz").attr("value","'.$this->security->get_csrf_hash().'");';
            echo '$("#tokenxyz2").attr("value","'.$this->security->get_csrf_hash().'");';
            echo '</script>';
            exit();
        }else if($mod == "getdatapropart"){
            $id = $this->input->post("item_id");

            $this->db->select("product2.product_family, product2.product_id, product2.product_code, product_partner_rel.partner_type, product_partner_rel.pshare, product_partner_rel.price, product_partner_rel.pp_id");
            $this->db->join("product2","product2.product_id = product_partner_rel.product_id");
            $this->db->where("pp_id", $id);
            $datapropart = $this->db->get("product_partner_rel")->row();

            $this->db->where("product_family", $datapropart->product_family);
            $product = $this->db->get("product2")->result();

            $opt = "<option value=0>-- Select Product --</option>";
            foreach ($product as $dataproduct){
                $opt .= "<option value='".$dataproduct->product_id."'>".$dataproduct->product_name."</option>";
            }

            echo '<script type="text/javascript">';
            echo '$("select#family").val("'.$datapropart->product_family.'");';
            echo '$("select#productlist").html("'.$opt.'");';
            echo '$("select#productlist").val("'.$datapropart->product_id.'");';
            echo '$("input#code").attr("value","'.$datapropart->product_code.'");';
            echo '$("input[name=bm][value='.$datapropart->partner_type.']").prop("checked",true);';
            echo '$("input#price").attr("value","'.$datapropart->price.'");';
            if($datapropart->partner_type == 1){
                echo '$("#modal-1").find(\'div[id="sharep"]\').css("display","block");';
                echo '$("#modal-1").find(\'div[id="pricep"]\').css("display","none");';
                echo '$("input#price").attr("readonly",true);';
                echo '$("input#pshare").attr("readonly",false);';
            }else if($datapropart->partner_type == 2){
                echo '$("#modal-1").find(\'div[id="sharep"]\').css("display","none");';
                echo '$("#modal-1").find(\'div[id="pricep"]\').css("display","block");';
                echo '$("input#price").attr("readonly",false);';
                echo '$("input#pshare").attr("readonly",true);';
            }
            echo '$("input#pshare").attr("value","'.$datapropart->pshare.'");';
            echo '$("input#idpp").attr("value","'.$datapropart->pp_id.'");';
            echo '$("#tokenxyz2").attr("value","'.$this->security->get_csrf_hash().'");';
            echo '$("#tokenxyz").attr("value","'.$this->security->get_csrf_hash().'");';
            //echo '$("#modal-1").find(\'input[id="tokenxyz2"]\').attr("value","'.$this->security->get_csrf_hash().'");';
            echo '</script>';
            exit();
        }else if($mod == "productremoveitem"){

            $id = $this->input->post("item_id");
            $data["remove_flag"] = 1;
            $data["remove_by"] = $this->session->userdata("user_id");
            $where["pp_id"] = $id;

            $this->db->update("product_partner_rel", $data, $where);
            $dr["id"] = $id;
            $dr["msg"] = "sukses";

            echo json_encode($dr);
            exit();
        }else if($mod == "activatepp"){

            $id = $this->input->post("item_id");
            $st = $this->input->post("st");
            $datapp["status"] = $st == 0 ? 1:0;
            $wherepp["pp_id"] = $id;

            $this->db->update("product_partner_rel", $datapp, $wherepp);

            $dr["id"] = $id;
            $dr["msg"] = "sukses";
            echo json_encode($dr);
            exit();
        }
    }

    function getproducts($id = null){

        $data = array();
        $data["title"] = "Manage Product Partner";
        $this->db->select("a.pp_id,b.product_code as code, b.product_name as name, b.product_family as category, a.status as status, a.base_cost, a.floor_price, a.isat_rev_share_percent, a.isat_rev_share_fixed, a.third_party_rev_share_percent, a.third_party_rev_share_fixed");
        $this->db->join("product2 b", "b.product_id = a.product_id");
        //$this->db->join("product_category c", "c.id = b.category_id");
        $this->db->where("a.partner_id", $id);
        $this->db->where("a.remove_flag", 0);
        // $this->db->order_by('a.pp_id', 'DESC');
        $data["prodpart"] = $this->db->get("product_partner_rel a")->result();
        $data["id"] = $id;

        $this->db->select("name");
        $this->db->where("partner_id", $id);
        $data["partner_name"] = $this->db->get("partner")->row()->name;

        //$data["family"] = $this->db->get("product_category")->result();
        $this->db->select("product_family");
        $this->db->group_by("product_family");
        $data["family"] = $this->db->get("product2")->result();

        $this->template->load('default', 'partner/products', $data);
    }

    function saveproducts(){

        $product = $this->input->post("product");
        $partner = $this->input->post("idpartner");
        $bm = $this->input->post("bm");
        $price = $this->input->post("price");
        $pshare = $this->input->post("pshare");
        $idpp = $this->input->post("idpp");

        $data["product_id"] = $product;
        $data["partner_id"] = $partner;
        $data["partner_type"] = $bm;
        $data["created_by"] = $this->session->userdata("user_id");

        if($bm == 1){
            $data["price"] = 0;
            $data["pshare"] = $pshare;
        }elseif ($bm == 2) {
            $data["price"] = $price;
            $data["pshare"] = 0;
        }

        if($idpp > 0){
            $wherep["pp_id"] = $idpp;
            $this->db->update("product_partner_rel", $data, $wherep);
        }else{
            $this->db->insert("product_partner_rel", $data);
        }

    }

    function manage_partner_account($partner_id = null) {
        $this->db->where("partner_id", $partner_id);
        $get_partners = $this->db->get("partner");
        $partner = $get_partners->row();
        $sales_pic_name = $partner->sales_name;
        $sales_pic_position = "Partner PIC";
        $sales_pic_phone = $partner->sales_phone;
        $sales_pic_email = $partner->sales_email;
        $finance_pic_name = $partner->finance_name;
        $finance_pic_position = "Finance PIC";
        $finance_pic_phone = $partner->finance_phone;
        $finance_pic_email = $partner->finance_email;


        $this->db->where("partner_id", $partner_id);
        $this->db->order_by("id", "DESC");
        $get_partner_accounts = $this->db->get("partner_account");
        $partner_accounts = $get_partner_accounts->result();

        // echo '<pre>';
        // var_dump($partner_accounts); die();
        // echo '</pre>';

        $data["title"] = "Manage Partner Accounts";
        $data["partner_id"] = $partner->partner_id;
        $data["partner_name"] = $partner->name;
        $data["partner_accounts"] = $partner_accounts;
        $data["sales_pic_name"] = $sales_pic_name;
        $data["sales_pic_position"] = $sales_pic_position;
        $data["sales_pic_email"] = $sales_pic_email;
        $data["sales_pic_phone"] = $sales_pic_phone;
        $data["finance_pic_name"] = $finance_pic_name;
        $data["finance_pic_position"] = $finance_pic_position;
        $data["finance_pic_email"] = $finance_pic_email;
        $data["finance_pic_phone"] = $finance_pic_phone;

        $this->template->load('default', 'partner/manage_partner_account', $data);
    }

    function add_partner_account($partner_id = null) {
        $this->db->where("partner_id", $partner_id);
        $get_partners = $this->db->get("partner");
        $partner = $get_partners->row();

        $this->db->where("partner_id", $partner_id);
        $partner_accounts = $this->db->get("partner_account");
        $partner_accounts = $get_partners->result();

        $data["title"] = "Add Partner Account";
        $data["partner_id"] = $partner->partner_id;
        $data["partner_name"] = $partner->name;
        $data["partner_accounts"] = $partner_accounts;

        // echo '<pre>';
        // print_r($data); die();
        // echo '</pre>';

        $this->template->load('default', 'partner/add_partner_account', $data);
    }

    function insert_partner_account() {
        // echo "Insert Partner Account";
        // die();
        $data["title"] = "Add Partner Account";

        $name = $this->input->post("name");
        $position = $this->input->post("position");
        $email = $this->input->post("email");
        $phone = $this->input->post("phone");
        $partner_id = $this->input->post("partner_id");
        $partner_name = $this->input->post("partner_name");
        $created_on = date("Y-m-d H:i:s");
        // $created_by = 163;
        $created_by = $this->session->userdata("user_id");

        $data["partner_id"] = $partner_id;
        $data["partner_name"] = $partner_name;
        // var_dump($name); die();
        // $modified_on

        $inserted = false;

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
        $this->form_validation->set_rules("name", "Name", 'trim|required');
        $this->form_validation->set_rules("position", "Position", 'trim|required');
        $this->form_validation->set_rules("email", "Email", 'trim|required');
        $this->form_validation->set_rules("phone", "Phone Number", 'trim|required');

        if($this->form_validation->run() == FALSE) {
            $this->template->load('default', 'partner/add_partner_account', $data);
        } else {
            $records = array();
            $records["name"] = $name;
            $records["position"] = $position;
            $records["email"] = $email;
            $records["phone"] = $phone;
            $records["partner_id"] = $partner_id;
            $records["created_by"] = $created_by;
            $records["created_on"] = $created_on;

            $inserted = $this->db->insert("partner_account", $records);
            if($inserted) {
                $partner_account_id = $this->db->insert_id();
            } else {
                $partner_account_id = 0;
            }

            if(!$inserted) {
                $this->session->set_flashdata('add_partner_account_alert',
                '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;</button>
                    <strong>Oops, Add Partner Account Failed! You can try again.</strong>.
                </div>');
                redirect(base_url("partner/manage_partner_account/" . $partner_id));
            } else {
                $this->session->set_flashdata('add_partner_account_alert',
                '<div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;</button>
                    <strong>Add Partner Account Success!</strong> ' . $name . ' is added to ' . $partner_name .
                ' account list.</div>');
                redirect(base_url("partner/manage_partner_account/" . $partner_id));
            }
        }
    }

    function edit_partner_account($partner_account_id = null) {
        $this->db->where("id", $partner_account_id);
        $get_partner_account = $this->db->get("partner_account");
        $partner_account = $get_partner_account->row();

        $data["title"] = "Edit Partner Account";
        $data["partner_account_id"] = $partner_account->id;
        $data["name"] = $partner_account->name;
        $data["position"] = $partner_account->position;
        $data["email"] = $partner_account->email;
        $data["phone"] = $partner_account->phone;
        $partner_id = $partner_account->partner_id;
        $data["partner_id"] = $partner_id;

        $this->db->where("partner_id", $partner_id);
        $get_partner = $this->db->get("partner");
        $partner = $get_partner->row();
        $partner_name = $partner->name;

        $data["partner_name"] = $partner_name;


        // echo '<pre>';
        // print_r($data); die();
        // echo '</pre>';

        $this->template->load('default', 'partner/edit_partner_account', $data);
    }

    function update_partner_account($partner_account_id = null) {
        $data["title"] = "Update Partner Account";

        $partner_account_id = $partner_account_id;
        $name = $this->input->post("name");
        $position = $this->input->post("position");
        $email = $this->input->post("email");
        $phone = $this->input->post("phone");
        $partner_id = $this->input->post("partner_id");
        $partner_name = $this->input->post("partner_name");
        // $modified_by = 163;
        $modified_by = $this->session->userdata("user_id");
        $modified_on = date("Y-m-d H:i:s");

        $data["partner_account_id"] = $partner_account_id;
        $data["partner_id"] = $partner_id;
        $data["partner_name"] = $partner_name;

        $updated = false;

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
        $this->form_validation->set_rules('name', 'Account Name', 'trim|required');
        $this->form_validation->set_rules('position', 'Position', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');

        if($this->form_validation->run() == FALSE) {
            $data["name"] = $name;
            $data["position"] = $position;
            $data["email"] = $email;
            $data["phone"] = $phone;

            $this->template->load('default', 'partner/edit_partner_account', $data);
        } else {
            $where["id"] = $partner_account_id;
            $record["name"] = $name;
            $record["position"] = $position;
            $record["email"] = $email;
            $record["phone"] = $phone;
            $record["modified_by"] = $modified_by;
            $record["modified_on"] = $modified_on;

            $updated = $this->db->update("partner_account", $record, $where);

            if($updated) {
                $this->session->set_flashdata('edit_partner_account_alert', '<div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Update Partner Account Success!</strong> ' . $name . 
                ' updated on ' . $partner_name . ' account list.</div>');
                redirect(base_url("partner/manage_partner_account/" . $partner_id));
            } else {
                $this->session->set_flashdata('edit_partner_account_alert', '<div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Oops, Update ' . $name . ' from ' . $partner_name . ' Account List Failed!</strong> You can try again.</div>');
                redirect(base_url("partner/manage_partner_account/" . $partner_id));
            }
        }

    }

    function delete_partner_account($partner_account_id = null) {
        $this->db->where("id", $partner_account_id);
        $get_partner_account = $this->db->get("partner_account");
        $partner_account = $get_partner_account->row();
        $partner_id = $partner_account->partner_id;
        $name = $partner_account->name;

        $deleted = false;

        $this->db->where("id", $partner_account_id);
        $deleted = $this->db->delete("partner_account");

        // echo '<pre>';
        // var_dump($deleted);
        // die();
        // echo '</pre>';

        if($deleted) {
            $this->session->set_flashdata('delete_partner_account_alert',
                '<div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;</button>
                    <strong>Delete Partner Account Success!</strong> ' . $name . ' is deleted from account list.
                </div>');
                redirect(base_url("partner/manage_partner_account/" . $partner_id));
        } else {
            $this->session->set_flashdata('delete_partner_account_alert',
            '<div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                &times;</button>
                <strong>Oops, Delete Partner Account Failed! You can try again.</strong>.
            </div>');
            redirect(base_url("partner/manage_partner_account/" . $partner_id));
        }
    }

    function edit_partner_pic($mod = null, $partner_id = null) {
        $this->db->where("partner_id", $partner_id);
        $get_partner = $this->db->get("partner");
        $partner = $get_partner->row();
        $partner_name = $partner->name;

        $data["title"] = "Edit Partner Account PIC";
        $data["partner_id"] = $partner_id;
        $data["partner_name"] = $partner_name;
        if(strcmp($mod, "sales") == 0) {
            $data["mod"] = $mod;
            $data["name"] = $partner->sales_name;
            $data["position"] = "Partner PIC";
            $data["email"] = $partner->sales_email;
            $data["phone"] = $partner->sales_phone;
        } else if(strcmp($mod, "finance") == 0) {
            $data["mod"] = $mod;
            $data["name"] = $partner->finance_name;
            $data["position"] = "Finance PIC";
            $data["email"] = $partner->finance_email;
            $data["phone"] = $partner->finance_phone;
        }

        $this->template->load('default', 'partner/edit_partner_pic', $data);
    }

    function update_partner_pic($mod = null, $partner_id = null) {
        $data["title"] = "Update " . ucfirst($mod) . " PIC Account";

        $partner_id = $partner_id;
        $name = $this->input->post("name");
        $position = $this->input->post("position");
        $email = $this->input->post("email");
        $phone = $this->input->post("phone");
        $partner_name = $this->input->post("partner_name");
        // $modified_by = 163;
        $modified_by = $this->session->userdata("user_id");
        $modified_on = date("Y-m-d H:i:s");
        
        $data["mod"] = $mod;
        $data["partner_id"] = $partner_id;
        $data["partner_name"] = $partner_name;

        $updated = false;

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
        $this->form_validation->set_rules('name', 'Account Name', 'trim|required');
        $this->form_validation->set_rules('position', 'Position', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');

        if($this->form_validation->run() == FALSE) {
            $data["name"] = $name;
            $data["position"] = $position;
            $data["email"] = $email;
            $data["phone"] = $phone;

            $this->template->load('default', 'partner/edit_partner_pic', $data);
        } else {
            $where["partner_id"] = $partner_id;
            if(strcmp($mod, "sales") == 0) {
                $record["sales_name"] = $name;
                // $record["position"] = $position;
                $record["sales_email"] = $email;
                $record["sales_phone"] = $phone;
                $record["modified_by"] = $modified_by;
                $record["modified_on"] = $modified_on;
            } else if(strcmp($mod, "finance") == 0) {
                $record["finance_name"] = $name;
                // $record["position"] = $position;
                $record["finance_email"] = $email;
                $record["finance_phone"] = $phone;
                $record["modified_by"] = $modified_by;
                $record["modified_on"] = $modified_on;
            }

            $updated = $this->db->update("partner", $record, $where);

            if($updated) {
                $this->session->set_flashdata('edit_partner_pic_alert', '<div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Update Partner Account Success!</strong> ' . $name . 
                ' updated on ' . $partner_name . ' account list.</div>');
                redirect(base_url("partner/manage_partner_account/" . $partner_id));
            } else {
                $this->session->set_flashdata('edit_partner_pic_alert', '<div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Oops, Update ' . $name . ' from ' . $partner_name . ' Account List Failed!</strong> You can try again.</div>');
                redirect(base_url("partner/manage_partner_account/" . $partner_id));
            }
        }
    }

    function add_partner_product($partner_id = null) {
        $data['title'] = "Add Product Partner";

        $this->db->where("partner_id", $partner_id);
        $get_partners = $this->db->get("partner");
        $partner = $get_partners->row();

        $this->db->where("partner_id", $partner_id);
        $get_product_partner_rel = $this->db->get("product_partner_rel");
        $product_partner_rels = $get_product_partner_rel->result();
        if($get_product_partner_rel->num_rows() > 0) {
            $existing_products = array();
            foreach($product_partner_rels as $product_partner_rel) {
                array_push($existing_products, $product_partner_rel->product_id);
            }
            $this->db->where_not_in("product_id", $existing_products);
            // var_dump($existing_products); die();
        }

        $this->db->where("status", 1);
        $this->db->order_by('product_name', 'ASC');
        $get_products = $this->db->get("product2");
        $products = $get_products->result();

        $data["title"] = "Add Partner Product";
        $data["partner_id"] = $partner->partner_id;
        $data["partner_name"] = $partner->name;
        $data["products"] = $products;

        // echo "<pre>";
        // foreach($products as $product) {
        //     echo $product->product_id;
        //     echo ", " . $product->product_name;
        //     echo '<br />';
        // }
        // echo "</pre>";
        // die();

        $this->template->load('default', 'partner/add_partner_product', $data);
    }

    function insert_partner_product() {
        $data["title"] = "Add Partner Product";

        $partner_id = $this->input->post("partner_id");
        $partner_name = $this->input->post("partner_name");
        // $product_name = $this->input->post("product_name");
        $product_id = $this->input->post("product_id");
        $partner_type = $this->input->post("partner_type"); // 1. fixed_price 2. rev_share
        $price = $this->input->post("price");
        $discount = $this->input->post("discount");
        $status = $this->input->post("status"); // 1. active 2. deactive
        // $doc_status = $this->input->post("doc_status");
        $doc_status = 2;
        $created_on = date("Y-m-d H:i:s");
        // $created_by = 163;
        $created_by = $this->session->userdata("user_id");
        $remove_flag = 0;
        $telco_unit = $this->input->post("telco_unit");
        $base_cost = $this->input->post("base_cost");
        $floor_price = $this->input->post("floor_price");
        $isat_rev_share_percent = $this->input->post("isat_rev_share_percent");
        $isat_rev_share_fixed = $this->input->post("isat_rev_share_fixed");
        $third_party_rev_share_percent = $this->input->post("third_party_rev_share_percent");
        $third_party_rev_share_fixed = $this->input->post("third_party_rev_share_fixed");

        $data["partner_id"] = $partner_id;
        $data["id"] = $partner_id;
        $data["partner_name"] = $partner_name;

        // $data["product_name"] = $product_name;
        // echo "<pre>";
        // var_dump($data);
        // echo "</pre>";
        // die();

        $inserted = false;

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
        $this->form_validation->set_rules('price', 'Price (IDR)', 'trim|required|numeric');
        $this->form_validation->set_rules('discount', 'Discount (%)', 'trim|numeric');
        $this->form_validation->set_rules('base_cost', 'Base Cost (IDR)', 'trim|numeric');
        $this->form_validation->set_rules('floor_price', 'Floor Price (IDR)', 'trim|numeric');
        $this->form_validation->set_rules('isat_rev_share_percent', 'Indosat Revenue Share Percent (%)', 'trim|numeric');
        $this->form_validation->set_rules('isat_rev_share_fixed', 'Indosat Revenue Share Fixed (IDR)', 'trim|numeric');
        $this->form_validation->set_rules('third_party_rev_share_percent', 'Third Party Revenue Share Percent (%)', 'trim|numeric');
        $this->form_validation->set_rules('third_party_rev_share_fixed', 'Indosat Revenue Share Fixed (IDR)', 'trim|numeric');

        if($this->form_validation->run() == false) {
            $this->db->where("status", 1);
            $this->db->order_by('product_name', 'ASC');
            $get_products = $this->db->get("product2");
            $products = $get_products->result();

            $data["products"] = $products;
            $this->template->load("default", "partner/add_partner_product", $data);
            // redirect(base_url("partner/getproducts/" . $partner_id));
        } else {
            $this->db->where("product_id", $product_id);
            $this->db->where("partner_id", $partner_id);
            $get_product_partner_rel = $this->db->get("product_partner_rel");
            $product_partner_rel_count = $get_product_partner_rel->num_rows();

            if($product_partner_rel_count > 0) {
                $product_partner_rel = $get_product_partner_rel->row();
                $pp_id = $product_partner_rel->pp_id;
                $modified_on = date("Y-m-d H:i:s");
                // $modified_by = 163;
                $modified_by = $this->session->userdata("user_id");

                $where["pp_id"] = $pp_id;
                $records = array();
                $records["partner_type"] = $partner_type;
                $records["price"] = $price;
                $records["discount"] = $discount;
                $records["status"] = $status;
                $records["doc_status"] = $doc_status;
                $records["modified_on"] = $modified_on;
                $records["modified_by"] = $modified_by;
                $records["remove_flag"] = $remove_flag;
                $records["telco_unit"] = $telco_unit;
                $records["base_cost"] = (float) $base_cost;
                $records["floor_price"] = (float) $floor_price;
                $records["isat_rev_share_percent"] = empty($isat_rev_share_percent) ? 0 : (float) $isat_rev_share_percent;
                $records["isat_rev_share_fixed"] = empty($isat_rev_share_fixed) ? 0 : (float) $isat_rev_share_fixed;
                $records["third_party_rev_share_percent"] = empty($third_party_rev_share_percent) ? 0 : (float) $third_party_rev_share_percent;
                $records["third_party_rev_share_fixed"] = empty($third_party_rev_share_fixed) ? 0 : (float) $third_party_rev_share_fixed;

                $inserted = $this->db->update("product_partner_rel", $records, $where);
            } else {
                $records = array();
                $records["product_id"] = $product_id;
                $records["partner_id"] = $partner_id;
                $records["partner_type"] = $partner_type;
                $records["price"] = $price;
                $records["discount"] = $discount;
                $records["status"] = $status;
                $records["doc_status"] = $doc_status;
                $records["created_on"] = $created_on;
                $records["created_by"] = $created_by;
                $records["remove_flag"] = $remove_flag;
                $records["telco_unit"] = $telco_unit;
                $records["base_cost"] = (float) $base_cost;
                $records["floor_price"] = (float) $floor_price;
                $records["isat_rev_share_percent"] = empty($isat_rev_share_percent) ? 0 : (float) $isat_rev_share_percent;
                $records["isat_rev_share_fixed"] = empty($isat_rev_share_fixed) ? 0 : (float) $isat_rev_share_fixed;
                $records["third_party_rev_share_percent"] = empty($third_party_rev_share_percent) ? 0 : (float) $third_party_rev_share_percent;
                $records["third_party_rev_share_fixed"] = empty($third_party_rev_share_fixed) ? 0 : (float) $third_party_rev_share_fixed;

                // echo '<pre>';
                // var_dump($records);
                // echo '</pre>';
                // die();
                $inserted = $this->db->insert("product_partner_rel", $records);
            }

            if(!$inserted) {
                $this->session->set_flashdata('add_partner_product_alert',
                '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;</button>
                    <strong>Oops, Add Partner Product Failed! You can try again.</strong>.
                </div>');
                redirect(base_url("partner/add_partner_product/" . $partner_id));
            } else {
                $this->db->where("product_id", $product_id);
                $get_product = $this->db->get("product2");
                $product = $get_product->row();
                $product_name = $product->product_name;

                $this->session->set_flashdata('add_partner_product_alert',
                '<div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;</button>
                    <strong>Add Partner Product Success!</strong> ' . $product_name . ' is added to ' . $partner_name .
                ' product list.</div>');
                redirect(base_url("partner/getproducts/" . $partner_id));
            }
        }
    }

    function edit_partner_product($pp_id = null) {
        $this->db->where("pp_id", $pp_id);
        $get_product_partner_rel = $this->db->get("product_partner_rel");
        $product_partner_rel = $get_product_partner_rel->row();

        $partner_id = $product_partner_rel->partner_id;
        $product_id = $product_partner_rel->product_id;
        $partner_type = $product_partner_rel->partner_type;
        $price = $product_partner_rel->price;
        $discount = $product_partner_rel->discount;
        $status = $product_partner_rel->status;
        $telco_unit = $product_partner_rel->telco_unit;
        $base_cost = $product_partner_rel->base_cost;
        $floor_price = $product_partner_rel->floor_price;
        $isat_rev_share_percent = $product_partner_rel->isat_rev_share_percent;
        $isat_rev_share_fixed = $product_partner_rel->isat_rev_share_fixed;
        $third_party_rev_share_percent = $product_partner_rel->third_party_rev_share_percent;
        $third_party_rev_share_fixed = $product_partner_rel->third_party_rev_share_fixed;
        // $modified_by = 163;
        $modified_by = $this->session->userdata("user_id");
        $modified_on = date("Y-m-d H:i:s");

        $data["title"] = "Edit Partner Product";
        $data["pp_id"] = $pp_id;
        $data["partner_id"] = $partner_id;
        $data["product_id"] = $product_id;
        $data["partner_type"] = $partner_type;
        $data["price"] = $price;
        $data["discount"] = $discount;
        $data["status"] = $status;
        $data["telco_unit"] = $telco_unit;
        $data["base_cost"] = $base_cost;
        $data["floor_price"] = $floor_price;
        $data["isat_rev_share_percent"] = $isat_rev_share_percent;
        $data["isat_rev_share_fixed"] = $isat_rev_share_fixed;
        $data["third_party_rev_share_percent"] = $third_party_rev_share_percent;
        $data["third_party_rev_share_fixed"] = $third_party_rev_share_fixed;
        $data["modified_by"] = $modified_by;
        $data["modified_on"] = $modified_on;

        $this->db->where("partner_id", $partner_id);
        $get_partner = $this->db->get("partner");
        $partner = $get_partner->row();
        $partner_name = $partner->name;
        $data["partner_name"] = $partner_name;

        $this->db->where("status", 1);
        $this->db->order_by('product_name', 'ASC');
        $get_products = $this->db->get("product2");
        $products = $get_products->result();
        $data["products"] = $products;

        $this->template->load('default', 'partner/edit_partner_product', $data);
    }

    function update_partner_product($pp_id = null) {
        $data['title'] = "Edit Partner Product";

        $this->db->where("pp_id", $pp_id);
        $get_product_partner_rel = $this->db->get("product_partner_rel");
        $product_partner_rel = $get_product_partner_rel->row();
        $product_id = $product_partner_rel->product_id;

        $pp_id = $pp_id;

        // hidden input
        $partner_id = $this->input->post("partner_id");
        $partner_name = $this->input->post("partner_name");

        $partner_type = $this->input->post("partner_type");
        $price = $this->input->post("price");
        $discount = $this->input->post("discount");
        $status = $this->input->post("status");
        $doc_status = 2;
        $modified_on = date("Y-m-d H:i:s");
        // $modified_by = 163;
        $modified_by = $this->session->userdata("user_id");
        $telco_unit = $this->input->post("telco_unit");
        $base_cost = $this->input->post("base_cost");
        $floor_price = $this->input->post("floor_price");
        $isat_rev_share_percent = $this->input->post("isat_rev_share_percent");
        $isat_rev_share_fixed = $this->input->post("isat_rev_share_fixed");
        $third_party_rev_share_percent = $this->input->post("third_party_rev_share_percent");
        $third_party_rev_share_fixed = $this->input->post("third_party_rev_share_fixed");

        $data["partner_id"] = $partner_id;
        $data["partner_name"] = $partner_name;

        $updated = false;

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
        $this->form_validation->set_rules('price', 'Price (IDR)', 'trim|required|numeric');
        $this->form_validation->set_rules('discount', 'Discount (%)', 'trim|numeric');
        $this->form_validation->set_rules('base_cost', 'Base Cost (IDR)', 'trim|numeric');
        $this->form_validation->set_rules('floor_price', 'Floor Price (IDR)', 'trim|numeric');
        $this->form_validation->set_rules('isat_rev_share_percent', 'Indosat Revenue Share Percent (%)', 'trim|numeric');
        $this->form_validation->set_rules('isat_rev_share_fixed', 'Indosat Revenue Share Fixed (IDR)', 'trim|numeric');
        $this->form_validation->set_rules('third_party_rev_share_percent', 'Third Party Revenue Share Percent (%)', 'trim|numeric');
        $this->form_validation->set_rules('third_party_rev_share_fixed', 'Indosat Revenue Share Fixed (IDR)', 'trim|numeric');

        if($this->form_validation->run() == false) {
            $data["pp_id"] = $pp_id;
            $data["partner_id"] = $partner_id;
            $data["product_id"] = $product_id;
            $data["partner_type"] = $partner_type;
            $data["price"] = $price;
            $data["discount"] = $discount;
            $data["status"] = $status;
            $data["telco_unit"] = $telco_unit;
            $data["base_cost"] = $base_cost;
            $data["floor_price"] = $floor_price;
            $data["isat_rev_share_percent"] = $isat_rev_share_percent;
            $data["isat_rev_share_fixed"] = $isat_rev_share_fixed;
            $data["third_party_rev_share_percent"] = $third_party_rev_share_percent;
            $data["third_party_rev_share_fixed"] = $third_party_rev_share_fixed;
            $data["modified_by"] = $modified_by;
            $data["modified_on"] = $modified_on;

            $this->db->where("partner_id", $partner_id);
            $get_partner = $this->db->get("partner");
            $partner = $get_partner->row();
            $partner_name = $partner->name;
            $data["partner_name"] = $partner_name;

            $this->db->where("status", 1);
            $this->db->order_by('product_name', 'ASC');
            $get_products = $this->db->get("product2");
            $products = $get_products->result();
            $data["products"] = $products;
            $this->template->load("default", "partner/edit_partner_product", $data);
        } else {

            $where["pp_id"] = $pp_id;
            $records = array();
            $records["product_id"] = $product_id;
            $records["partner_type"] = $partner_type;
            $records["price"] = $price;
            $records["discount"] = $discount;
            $records["status"] = $status;
            $records["modified_on"] = $modified_on;
            $records["modified_by"] = $modified_by;
            $records["telco_unit"] = $telco_unit;
            $records["base_cost"] = (float) $base_cost;
            $records["floor_price"] = (float) $floor_price;
            $records["isat_rev_share_percent"] = empty($isat_rev_share_percent) ? 0 : (float) $isat_rev_share_percent;
            $records["isat_rev_share_fixed"] = empty($isat_rev_share_fixed) ? 0 : (float) $isat_rev_share_fixed;
            $records["third_party_rev_share_percent"] = empty($third_party_rev_share_percent) ? 0 : (float) $third_party_rev_share_percent;
            $records["third_party_rev_share_fixed"] = empty($third_party_rev_share_fixed) ? 0 : (float) $third_party_rev_share_fixed;

            $inserted = $this->db->update("product_partner_rel", $records, $where); 

            if(!$inserted) {
                $this->session->set_flashdata('edit_partner_product_alert',
                '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;</button>
                    <strong>Oops, Edit Partner Product Failed! You can try again.</strong>.
                </div>');
                redirect(base_url("partner/add_partner_product/" . $partner_id));
            } else {
                $this->db->where("product_id", $product_id);
                $get_product = $this->db->get("product2");
                $product = $get_product->row();
                $product_name = $product->product_name;

                $this->session->set_flashdata('edit_partner_product_alert',
                '<div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;</button>
                    <strong>Edit Partner Product Success!</strong> ' . $product_name . ' is edited.
                </div>');
                redirect(base_url("partner/getproducts/" . $partner_id));
            }
        }

    }

    function delete_partner_product($pp_id = null) {
        $this->db->where("pp_id", $pp_id);
        $get_product_partner_rel = $this->db->get("product_partner_rel");
        $product_partner_rel = $get_product_partner_rel->row();
        $product_id = $product_partner_rel->product_id;
        $partner_id = $product_partner_rel->partner_id;

        $this->db->where("product_id", $product_id);
        $get_product = $this->db->get("product2");
        $product = $get_product->row();
        $product_name = $product->product_name;

        $deleted = false;

        // Permanent Delete
        // $this->db->where("pp_id", $pp_id);
        // $deleted = $this->db->delete("product_partner_rel");

        // Update Delete
        $where["pp_id"] = $pp_id;
        $record["remove_flag"] = 1;
        // $record["remove_by"] = 163;
        $record["remove_by"] = $this->session->userdata("user_id");
        $deleted = $this->db->update("product_partner_rel", $record, $where);

        if($deleted) {
            $this->session->set_flashdata('delete_partner_product_alert',
            '<div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                &times;</button>
                <strong>Delete Partner Product Success!</strong> ' . $product_name . ' is deleted from product list.
            </div>');
            redirect(base_url("partner/getproducts/" . $partner_id));
        } else {
            $this->session->set_flashdata('delete_partner_product_alert',
            '<div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                &times;</button>
                <strong>Oops, Delete Partner Product Failed! You can try again.</strong>.
            </div>');
            redirect(base_url("partner/getproducts/" . $partner_id));
        }
    }

}
