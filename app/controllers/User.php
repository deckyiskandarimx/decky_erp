<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User extends Ci_controller {

    function __construct() {
        parent::__construct();
        /* $cekid = $this->session->userdata("user_id");
          if($cekid == ""){
          redirect(base_url("auth"));
          } */
    }

    function index() {

        $cekid = $this->session->userdata("user_id");

        if ($cekid == "") {
            redirect(base_url("auth"));
        } else {
            $this->load->model("mdl_user");
            $data["title"] = "User Management";
            $data["userdata"] = $this->mdl_user->getuser();
            $this->template->load('default', 'user/admin', $data);
        }
    }

    function add() {

        $cekid = $this->session->userdata("user_id");
        $type = $this->session->userdata("account_type");

        if ($cekid == "" || $type != "AC08") {
            redirect(base_url("auth"));
            $data["heading"] = "Oops SORRY.. This is NOT An Authorized Page";
            $this->session->set_flashdata('salah', '<div class="alert alert-danger">
  <strong>Sorry!!!</strong> this in not an authorized page.
</div>');
            $data["message"] = "Please Choose Another Menu";
            $this->template->load('default', 'errors/html/error_auth', $data);
        } else {

            $data["title"] = "Add User";
            
            $this->db->where("status",1);
            $data["type"] = $this->db->get("ref_users_type")->result();
            $this->template->load('default', 'user/form', $data);
        }
    }

    function edit($id = NULL) {

        $cekid = $this->session->userdata("user_id");
        $type = $this->session->userdata("account_type");

        if ($cekid == "" || $type != "AC08") {
            //redirect(base_url("auth"));
            $data["heading"] = "Oops SORRY.. This is NOT An Authorized Page";
            $data["message"] = "Please Choose Another Menu";
            $this->template->load('default', 'errors/html/error_auth', $data);
        } else {

            if ($id > 0) {
                $data["title"] = "Edit User";
                $data["idx"] = $id;
                $data["uactivate"] = $this->db->get("ref_status")->result();
                $data["type"] = $this->db->get("ref_users_type")->result();
                $data["parent"] = $this->selectboxparent($id);
                $datauser = $this->db->get_where("users", array("user_id" => $id))->row();
                $data["uname"] = $datauser->username;
                $data["fname"] = $datauser->fullname;
                $data["utype"] = $datauser->user_type;
                $data["ustatus"] = $datauser->status;
                $data["nik"] = $datauser->emp_id;
                $data["mobile"] = $datauser->mobile_phone;
                $data["idrecorddb"] = $datauser->user_id;
                $data["initial"] = $datauser->initial;
            } else {
                redirect(base_url("user"));
            }
        }

        $this->template->load('default', 'user/form', $data);
    }

    private function selectboxparent($id) {

        $this->db->select("user_type, parent_id");
        $q = $this->db->get_where("users", array("user_id" => $id))->row();

        $whereclause["user_type"] = $q->user_type;
        $cektype = $this->db->get_where("ref_users_type", $whereclause)->row();

        $dataparent = $this->db->get_where("users", array("user_type" => $cektype->report_to))->result();

        $opt = "";
        foreach ($dataparent as $usertype):
            if ($usertype->user_id == $q->parent_id) {
                $selected = "selected='selected'";
            } else {
                $selected = "";
            }
            $opt .= "<option value='" . $usertype->user_id . "' " . $selected . ">" . $usertype->fullname . "</option>";
        endforeach;

        if ($dataparent != "") {
            return $opt;
        } else {
            return 0;
        }
    }

    function store($idcallback = null) {

        $this->form_validation->set_rules('fname', 'Fullname', 'trim|required');
        $this->form_validation->set_rules('utype', 'User Type', 'required|callback_check_default');
        $this->form_validation->set_rules('mobile', 'Mobile Number ', 'required|numeric|min_length[10]');
        $this->form_validation->set_rules('initial', 'Initial ', 'required|min_length[3]');
        if ($idcallback > 0) {
            $this->form_validation->set_rules('nik', 'NIK', 'required|callback_check_nik[' . $idcallback . ']');
        } else {
            $this->form_validation->set_rules('uname', 'Username', 'trim|required|valid_email');
            $this->form_validation->set_rules('nik', 'NIK', 'required|callback_check_nik');
        }
        $this->form_validation->set_message('check_default', 'You need to select something other than the default');
        $this->form_validation->set_message('check_nik', 'NIK already used. Please choose another name');

        $uname = $this->input->post("uname");
        $fname = $this->input->post("fname");
        $utype = $this->input->post("utype");
        $initial = $this->input->post("initial");
        $ustts = $this->input->post("uactivate");
        $mobile = $this->input->post("mobile");
        $nik = $this->input->post("nik");
        $parent = $this->input->post("parent");
        $id_hidden = $this->input->post("id_hidden");

        if ($this->form_validation->run() == FALSE) {

            if ($idcallback > 0) {

                $data["title"] = "Edit User";
                $data["idx"] = $idcallback;
                $data["uactivate"] = $this->db->get("ref_status")->result();
                $data["type"] = $this->db->get("ref_users_type")->result();
                $data["parent"] = $this->selectboxparent($idcallback);
                $datauser = $this->db->get_where("users", array("user_id" => $idcallback))->row();
                $data["uname"] = $datauser->username;
                $data["fname"] = $datauser->fullname;
                $data["utype"] = $datauser->user_type;
                $data["initial"] = $datauser->initial;
                $data["ustatus"] = $datauser->status;
                $data["nik"] = $datauser->emp_id;
                $data["mobile"] = $datauser->mobile_phone;
                $this->template->load('default', 'user/form', $data);
            } else {

                $data["title"] = "Add User";
                $data["uname"] = $uname;
                $data["fname"] = $fname;
                $data["nik"] = $nik;
                $data["initial"] = $initial;
                $data["mobile"] = $mobile;
                $data["utype"] = $utype;
                $data["type"] = $this->db->get("ref_users_type")->result();
                $this->template->load('default', 'user/form', $data);
            }
        } else {

            if (!$parent) {
                $dataparent = 0;
            } else {
                $dataparent = $parent;
            }

            if ($id_hidden > 0) {


                $dataedit["fullname"] = $fname;
                $dataedit["user_type"] = $utype;
                $dataedit["initial"] = $initial;
                $dataedit["parent_id"] = $dataparent;
                $dataedit["status"] = $ustts;
                $dataedit["emp_id"] = $nik;
                $dataedit["mobile_phone"] = $mobile;
                $where["user_id"] = $id_hidden;

                $upddata = $this->db->update("users", $dataedit, $where);
   
                if (!$upddata) {
                    $error["title"] = "Add User";
                    $error["uname"] = $uname;
                    $error["fname"] = $fname;
                    $error["initial"] = $initial;
                    $error["mobile"] = $mobile;
                    $error["nik"] = $nik;
                    $error["utype"] = $utype;
                    $error["msg"] = $dataex[1];
                    $error["type"] = $this->db->get("ref_users_type")->result();
                    $this->template->load('default', 'user/form', $error);
                } else {
                    redirect(base_url("user"));
                }
            } else {

                $this->form_validation->set_rules('uname', 'Username', 'trim|required|valid_email');

                //$query = "select user_register(?,?,?,?,?);";  

                $query = "select user_register(?, ?, ?, ?, ?, ?, ?, ?);";

                $data = $this->db->query($query, array($uname, $fname, $nik, $mobile, $utype, $dataparent, $initial, $this->session->userdata('user_id')))->row();

                $dataex = explode(";", $data->user_register);

                if ($dataex[0] == "00") {
                    $subject = "New User Notification";

                    $msg = "<html>"
                            . "<body>"
                            . "<p>Hi,</p>"
                            . "<p>You have just registered in ERP, for confirmation please click link bellow</p>"
                            . "<p></p>"
                            . "<p><a href='" . base_url() . "user/newpassword/?c=" . $dataex[2] . "' mc:disable-tracking>" . base_url() . "user/newpassword/?c=" . $dataex[2] . "</a></p>"
                            . "<p></p>"
                            . "<p>Cheers</p>"
                            . "</body>"
                            . "</html>";

                    $this->swiftmailer->sendHTMLMail($dataex[1], $subject, $msg);
                    redirect(base_url("user"));
                } else {
                    $error["title"] = "Add User";
                    $error["uname"] = $uname;
                    $error["fname"] = $fname;
                    $error["initial"] = $initial;
                    $error["mobile"] = $mobile;
                    $error["nik"] = $nik;
                    $error["utype"] = $utype;
                    $error["msg"] = $dataex[1];
                    $error["type"] = $this->db->get("ref_users_type")->result();
                    $this->template->load('default', 'user/form', $error);
                }
            }
        }
    }

    public function newpassword() {


        $this->form_validation->set_rules('password', 'New Password', 'trim|required|strip_tags|min_length[6]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|strip_tags|matches[password]');

        $kodehidden = $this->input->post("kodehidden");
        $kode = $this->input->get_post("c");
        $msg = $this->input->get_post("msg");
        $password = $this->input->post("password");

        if ($this->form_validation->run() == FALSE) {
            $data = array();
            $data['title'] = 'Input Your Password';
            $data['kode'] = $kode;
            $data['msg'] = $msg;
            $this->template->load('auth', 'user/inputpass', $data);
        } else {
            $query = "select user_activate('" . $kodehidden . "', '" . $password . "');";
            $q = $this->db->query($query)->row();
            $qexp = explode(";", $q->user_activate);
            if ($qexp[0] == "00") {
                redirect(base_url("auth/index/?msg=success"));
            } else {
                redirect(base_url("auth/changepswd/?msg=invalidcode"));
            }
        }
    }

    function check_default($post_string) {
        return $post_string == '0' ? FALSE : TRUE;
    }

    function check_nik($post_string, $idcallback=0) {

        $this->db->where("emp_id", $post_string);
        if ($idcallback > 0) {
            $this->db->where("user_id != ", $idcallback);
        }
        $nikcheck = $this->db->get("users")->row();

        return $nikcheck->user_id > 0 ? FALSE : TRUE;
    }

    public function ajax($action = NULL) {

        if ($action == "getparent"):
            $type = $this->input->get_post("type_id");
            $selfid = $this->input->get_post("selfid");
            $whereclause["user_type"] = $type;
            $cektype = $this->db->get_where("ref_users_type", $whereclause)->row();
            if ($cektype->report_to != "") {
                $this->db->where("user_type", $cektype->report_to);
                if ($selfid > 0) {
                    $this->db->where("user_id !=", $selfid);
                }
                $parents = $this->db->get("users")->result();
                $datatables = "";
                $datatables .= "<option value='0'>-- Select Parent --</option>";
                foreach ($parents as $users):
                    $datatables .="<option value='" . $users->user_id . "'>" . $users->fullname . "</option>";
                endforeach;
                $ret = '<script>';
                $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
                if ($datatables != "") {
                    $ret.= '$("#parent").attr("disabled",false);';
                    $ret.= '$("#parent").html("' . $datatables . '");';
                } else {
                    $ret.= '$("#parent").attr("disabled",true);';
                }
                $ret .= '</script>';
                echo $ret;
                exit();
            } else {
                $datatables = "";
                $datatables .= "<option value='0'>-- Select Parent --</option>";
                $ret = '<script>';
                $ret.= '$("#parent").html("' . $datatables . '");';
                $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
                $ret.= '$("#parent").attr("disabled",true);';
                $ret .= '</script>';
                echo $ret;
                exit();
            } elseif ($action == "chstate"):
            $id = $this->input->post("user_id");
            $status = $this->input->post("status");
            $where["user_id"] = $id;
            if ($status == 1) {
                $data["status"] = 0;
            } else {
                $data["status"] = 1;
            }

            if ($this->db->update("users", $data, $where)) {
                $res["msg"] = "Update status success!";
                $res["status"] = true;
            } else {
                $res["msg"] = "Update status success!";
                $res["status"] = true;
            }
            echo json_encode($res);
            exit();
        endif;
    }

    public function changepswd() {

        $this->form_validation->set_rules('password', 'New Password', 'trim|required|strip_tags|min_length[6]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|strip_tags|matches[password]');

        $kodehidden = $this->input->post("kodehidden");
        $msg = $this->input->get_post("msg");
        $oldpassword = $this->input->post("oldpassword");
        $password = $this->input->post("password");
        $oldpassword = $this->input->post("oldpassword");

        if ($this->form_validation->run() == FALSE) {
            $data = array();
            $data['title'] = 'Change Password';
            $data['kode'] = $kode;
            $data['msg'] = $msg;
            $this->template->load('default', 'user/changepass', $data);
        } else {
            $query = "select user_change_password('" . $this->session->userdata("username") . "','" . $oldpassword . "', '" . $password . "');";
            $q = $this->db->query($query)->row();
            $qexp = explode(";", $q->user_change_password);
            if ($qexp[0] == "00") {
                $data = array();
                $data['title'] = 'Change Password';
                $data['kode'] = $kode;
                $data['msg'] = "Success";
                $this->template->load('default', 'user/changepass', $data);
            } else {
                $msg = "invalid old password";
                redirect(base_url("user/changepswd/?msg=" . urlencode($msg)));
            }
        }
    }
    
    
    
    function profile(){
        
        $fullname = $this->input->post("fullname");
        $nik = $this->input->post("nik");
        
        $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('nik', 'NIK', 'trim|required|callback_check_nik[' . $this->session->userdata()["user_id"] . ']');
        
        $this->form_validation->set_message('check_nik', 'NIK already used. Please choose another name');
        
        if ($this->form_validation->run() == FALSE) {
            
            $this->db->select("a.user_id, a.fullname as fullname, a.emp_id, a.username,c.name as role, c.user_type, b.fullname as atasan  ");
            $this->db->where("a.user_id", $this->session->userdata()["user_id"]);
            $this->db->join("ref_users_type c", "a.user_type = c.user_type");
            $this->db->join("users b", "a.user_id = b.parent_id","left");
            $data["profile"] = $this->db->get("users a")->row();
            
            $this->template->load('default', 'user/profile', $data);
            
        }else{
            
            $record["fullname"] = $fullname;
            $record["emp_id"] = $nik;
            
            $where["user_id"] = $this->session->userdata()["user_id"];
            
            $this->db->update("users", $record, $where);
            
            redirect(base_url("user/profile"));
        }
        
    }

}
