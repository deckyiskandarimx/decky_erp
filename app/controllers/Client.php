<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Client extends Ci_Controller {

    function __construct() {
        parent::__construct();
        $cekid = $this->session->userdata("user_id");
        if ($cekid == "") {
            redirect(base_url("auth"));
        }
    }

    public function index() {
        $this->db->select("client_id,"
                . "client_name,"
                . "category_name,"
                . "ref_city.city,"
                . "country_name,"
                . "phone,"
                . "ref_province.province");
        $this->db->join("ref_business_category", "ref_business_category.business_category_id = client.business_category_id");
        $this->db->join("ref_country", "address_country=ref_country.id");
        $this->db->join("ref_province", "address_province=ref_province.id");
        $this->db->join("ref_city", "address_city=ref_city.id");
        $data["title"] = "Client Management";
        $data["client"] = $this->db->get("client")->result();


        $this->template->load('default', 'client/index', $data);
    }
	
	public function account() {
        ini_set('max_execution_time', 3600);
        // $this->db->select("client.client_id,"
        //         . "client.client_name,"
        //         . "ref_business_category.category_name,"
        //         . "iklanstore_web_new.user.payment_type");
        $this->db->select("client.client_id,"
                . "client.client_name,"
                . "client.npwp,"
                . "client.address_street,"
                . "client.address_country,"
                . "client.payment_type,"
                . "ref_city.city,"
                . "ref_province.province,"
                . "ref_business_category.category_name");
		$this->db->from("client");
        $this->db->join("ref_city", "ref_city.id = client.address_city");
        $this->db->join("ref_province", "ref_province.id = client.address_province");
        $this->db->join("ref_business_category", "ref_business_category.business_category_id = client.business_category_id");
        // $this->db->join("iklanstore_web_new.public.user_erp_mapping", "iklanstore_web_new.public.user_erp_mapping.client_id_erp=client.client_id");
        // $this->db->join("iklanstore_user_erp_mapping", "iklanstore_user_erp_mapping.client_id_erp=client.client_id");
        // $this->db->join("iklanstore_web_new.user", "iklanstore_web_new.user.id=iklanstore_web_new.user_erp_mapping.user_id_iklan_store");
        $this->db->order_by('client.client_id', 'DESC');
        $data["title"] = "Account Management";
        $data["client"] = $this->db->get()->result();
        // echo "<pre>";

        $index = 0;
        foreach($data["client"] as $client) {
            // var_dump($client);
            // echo $client->client_id;
            $client_id = $client->client_id;
            $this->db->where("advertiser_id", $client_id);
            $this->db->or_where("agency_id", $client_id);
            $get_quotations = $this->db->get("quotation");
            $total_quotations = $get_quotations->num_rows();
            if($total_quotations > 0) {
                $quotations = $get_quotations->result();
                // var_dump($quotations);
                $total_campaign = 0;
                foreach($quotations as $quotation) {
                    $quotation_id = $quotation->quotation_id;
                    $this->db->where("quotation_id", $quotation_id);
                    $get_quotation_items = $this->db->get("quotation_item");
                    $total_quotation_items = $get_quotation_items->num_rows();
                    $total_campaign = $total_campaign + $total_quotation_items;
                }
            } else {
                $quotation_id = 0; // DEBUG
                $total_campaign = 0;
            }

            // echo "client: ". $client_id . ", total quotation: " . $total_quotations . ", total campaign: " . $total_campaign . "<br />";

            $temp_client = (array) $client;
            $temp_client["total_campaign"] = $total_campaign;
            $client = (object) $temp_client;
            $data["client"][$index] = $client;

            $index++;
            // echo "<br />";
        }
        // var_dump($data["client"][0]);
        // var_dump($data["client"][1]);
        // var_dump($data["client"]);
        // echo "</pre>";
        // die();

        $this->template->load('default', 'client/account', $data);
    }

    public function contact() {
        $this->db->order_by('client.client_id', 'DESC');
        $get_clients = $this->db->get("client");
        $data["title"] = "Contact Mangement";
        $data["client"] = $get_clients->result();

        // echo "<pre>";
        // var_dump($data["accounts"]);
        // echo "</pre>";
        // die();

        $this->template->load('default', 'client/contact', $data);
    }

    public function edit_contact($mod = null, $client_id = null) {
        $this->db->where("client_id", $client_id);
        $get_clients = $this->db->get("client");
        $client = $get_clients->row();

        $data["client_id"] = $client_id;
        $data["mod"] = $mod;

        if(strcmp($mod, "finance") == 0) {
            // Finance
            $finance_name = $client->finance_name;
            $finance_phone = $client->finance_phone;
            $finance_email = $client->finance_email;
            
            $data["title"] = "Update Contact - " . ucfirst($mod);
            $data["finance_name"] = $finance_name;
            $data["finance_phone"] = $finance_phone;
            $data["finance_email"] = $finance_email;
        } else if(strcmp($mod, "media") == 0) {
            // Media Planner
            $media_name = $client->media_name;
            $media_phone = $client->media_phone;
            $media_email = $client->media_email;

            $data["title"] = "Update Contact - " . ucfirst($mod);
            $data["media_name"] = $media_name;
            $data["media_phone"] = $media_phone;
            $data["media_email"] = $media_email;
        }

        // echo '<pre>';
        // echo $finance_name;
        // echo '<br />';
        // echo $finance_phone;
        // echo '<br />';
        // echo $finance_email;
        // echo '<br />';
        // echo '</pre>';

        // die();

        $this->template->load('default', 'client/form_edit_contact', $data);
    }

    public function update_contact($mod, $client_id) {
        // $client_id = $this->input->post("client_id");
        $name = $this->input->post("name");
        $phone = $this->input->post("phone");
        $email = $this->input->post("email");
        $modified_on = date("Y-m-d H:i:s");
        $inserted = false;

        $this->db->where("client_id", $client_id);
        $client = $this->db->get("client")->row();
        $client_name = $client->client_name;

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
        $this->form_validation->set_rules('name', ucfirst($mod) . ' Name', 'required');
        $this->form_validation->set_rules('phone', ucfirst($mod) . ' Phone', 'required');
        $this->form_validation->set_rules('email', ucfirst($mod) . ' Email', 'required');

        if($this->form_validation->run() == FALSE) {
            $data["client_id"] = $client_id;
            $data["mod"] = $mod;
            $data["title"] = "Update Contact - " . ucfirst($mod);

            if(strcmp($mod, "finance") == 0) {
                $data["finance_name"] = $client->finance_name;
                $data["finance_phone"] = $client->finance_phone;
                $data["finance_email"] = $client->finance_email;
            } else if(strcmp($mod, "media") == 0) {
                $data["media_name"] = $client->media_name;
                $data["media_phone"] = $client->media_phone;
                $data["media_email"] = $client->media_email;
            }

            $this->template->load('default', 'client/form_edit_contact', $data);
        } else {
            if(strcmp($mod, "finance") == 0) {
                $where["client_id"] = $client_id;
                $record["finance_name"] = $name;
                $record["finance_phone"] = $phone;
                $record["finance_email"] = $email;
                $record["modified_on"] = $modified_on;

                $inserted = $this->db->update("client", $record, $where);
            } else if(strcmp($mod, "media") == 0) {
                $where["client_id"] = $client_id;
                $record["media_name"] = $name;
                $record["media_phone"] = $phone;
                $record["media_email"] = $email;
                $record["modified_on"] = $modified_on;

                $inserted = $this->db->update("client", $record, $where);
            } else {
                redirect(base_url("client/contact"));
            }

            if($inserted) {
                $this->session->set_flashdata('edit_contact', '<div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Update Contact Success!</strong> ' . $client_name . ' <b>' . ucfirst($mod) . 
                ' Contact</b> Updated.</div>');
                redirect(base_url("client/contact"));
            } else {
                $this->session->set_flashdata('edit_contact', '<div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Oops, Update ' . ucfirst($mod) . ' Contact Failed!</strong> You can try again.</div>');
                redirect(base_url("client/contact"));
            }
        }

        // die();
    }

    public function export() {
        $this->load->library('excel');

        $this->db->order_by("client_id", "DESC");
        $get_clients = $this->db->get("client");
        $total_clients = $get_clients->num_rows();

        $clients = $get_clients->result();

        $objPHPExcel = new PHPExcel();
        $currentTime = date("Ymd");
        $objPHPExcel->getActiveSheet()->setTitle("Client_" . $currentTime);

        $objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
        $objWriter->setDelimiter(',');
        $objWriter->setEnclosure('');
        $objWriter->setLineEnding("\r\n");
        $objWriter->setSheetIndex(0);
        $objWriter->save('client_' . $currentTime . '.csv');

        $row_number = 1;
        $headings = array('client_id', 
                        'client_name',
                        'business_category_id',
                        'address_street',
                        'address_country',
                        'address_province',
                        'address_city',
                        'address_zip',
                        'phone',
                        'npwp',
                        'finance_name',
                        'finance_phone',
                        'finance_email',
                        'media_name',
                        'media_phone',
                        'media_email',
                        'deposit',
                        'client_type',
                        'created_on',
                        'modified_on',
                        'created_by',
                        'modified_by',
                        'address_blok',
                        'address_nomor',
                        'address_rt',
                        'address_kota',
                        'address_rw',
                        'address_kecamatan',
                        'address_kelurahan',
                        'address_negara',
                        'address_npwp',
                        'address_status',
                        'is_completed',
                        'is_deleted',
                        'due_date',
                        'is_storex',
                        'bank_account_number',
                        'bank_name',
                        'bank_account',
                        'fax',
                        'payment_type'
                    );
        $objPHPExcel->getActiveSheet()->fromArray(array($headings), NULL, 'A' . $row_number);
        $row_number++;
        // $col = 'B';
        foreach($clients as $client) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row_number, '"' . $client->client_id . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row_number, '"' . $client->client_name . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $row_number, '"' . $client->business_category_id . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $row_number, '"' . str_replace(array("\r", "\n"), ' ', $client->address_street) . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $row_number, '"' . $client->address_country . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $row_number, '"' . $client->address_province . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $row_number, '"' . $client->address_city . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $row_number, '"' . $client->address_zip . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('I' . $row_number, '"' . $client->phone . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('J' . $row_number, '"' . $client->npwp . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('K' . $row_number, '"' . $client->finance_name . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('L' . $row_number, '"' . $client->finance_phone . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('M' . $row_number, '"' . $client->finance_email . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('O' . $row_number, '"' . $client->media_name . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('P' . $row_number, '"' . $client->media_phone . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $row_number, '"' . $client->media_email . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('R' . $row_number, '"' . $client->deposit . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('S' . $row_number, '"' . $client->client_type . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('T' . $row_number, '"' . $client->created_on . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('U' . $row_number, '"' . $client->modified_on . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('V' . $row_number, '"' . $client->created_by . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('W' . $row_number, '"' . $client->modified_by . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('X' . $row_number, '"' . $client->address_blok . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('Y' . $row_number, '"' . $client->address_nomor . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('Z' . $row_number, '"' . $client->address_rt . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AA' . $row_number, '"' . $client->address_kota . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AB' . $row_number, '"' . $client->address_rw . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AC' . $row_number, '"' . $client->address_kecamatan . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AD' . $row_number, '"' . $client->address_kelurahan . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AE' . $row_number, '"' . $client->address_negara . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AF' . $row_number, '"' . str_replace(array("\r", "\n"), ' ', $client->address_npwp) . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AG' . $row_number, '"' . $client->address_status . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AH' . $row_number, '"' . $client->is_completed . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AI' . $row_number, '"' . $client->is_deleted . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AJ' . $row_number, '"' . $client->due_date . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AK' . $row_number, '"' . $client->is_storex . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AL' . $row_number, '"' . $client->bank_account_number . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AM' . $row_number, '"' . $client->bank_name . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AN' . $row_number, '"' . $client->bank_account . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AO' . $row_number, '"' . $client->fax . '"');
            $objPHPExcel->getActiveSheet()->setCellValue('AP' . $row_number, '"' . $client->payment_type . '"');

            $row_number++;
        }

        header("Content-Type: text/csv");
        header('Content-Disposition: attachment; filename="client_' . $currentTime . '.csv"');
        header('Cache-Control: max-age=0');

        $objWriter->save("php://output");

        // var_dump($total_clients);
        // die();
    }

    public function add() {

        $name = $this->input->post("name");
        $category = $this->input->post("category");
        $phone = $this->input->post("phone");
        $npwp = $this->input->post("npwp");
        $street = $this->input->post("street");
        $country = $this->input->post("country");
        $province = $this->input->post("province");
        $city = $this->input->post("city");
        $zip = $this->input->post("zip");
        $fname = $this->input->post("fname");
        $fphone = $this->input->post("fphone");
        $femail = $this->input->post("femail");
        $mname = $this->input->post("mname");
        $mphone = $this->input->post("mphone");
        $memail = $this->input->post("memail");
        $package = $this->input->post("package");
        $deposit = $this->input->post("deposit");
        $blok = $this->input->post("blok");
        $nomor = $this->input->post("nomor");
        $rt = $this->input->post("rt");
        $rw = $this->input->post("rw");
        $kab = $this->input->post("kab");
        $kec = $this->input->post("kec");
        $kel = $this->input->post("kel");
        $neg = $this->input->post("neg");
        $status = $this->input->post("status");
        $npwpdress = $this->input->post("npwpdress");
        $payment_type = $this->input->post("payment_type");

        $this->form_validation->set_rules('name', 'Name', 'trim|required|callback_check_client_name');
        $this->form_validation->set_rules('category', 'Business Category', 'callback_check_select');
        $this->form_validation->set_rules('package', 'Client Type', 'callback_check_select');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[10]');
        $this->form_validation->set_rules('npwp', 'NPWP Number', 'required|numeric');
        $this->form_validation->set_rules('zip', 'Zip Code', 'numeric|max_length[5]');
        $this->form_validation->set_rules('street', 'Street Address', 'trim|required');
        $this->form_validation->set_rules('country', 'Country', 'callback_check_select');
        //$this->form_validation->set_rules('province', 'Province', 'callback_check_select');
        //$this->form_validation->set_rules('city', 'City', 'callback_check_select');


        $this->form_validation->set_rules('blok', 'Blok');
        $this->form_validation->set_rules('nomor', 'Nomor Rumah');
        $this->form_validation->set_rules('rt', 'RT');
        $this->form_validation->set_rules('rw', 'RW');
        $this->form_validation->set_rules('kab', 'Kabupaten');
        $this->form_validation->set_rules('kec', 'Kecamatan');
        $this->form_validation->set_rules('neg', 'Country');
        $this->form_validation->set_rules('npwpdress', 'NPWP Address');

        $this->form_validation->set_rules('fname', 'Finance Name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('mname', 'Media Name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('fphone', 'Finance Phone Number', 'required|numeric|min_length[10]');
        $this->form_validation->set_rules('mphone', 'Media Phone Number', 'required|numeric|min_length[10]');
        //$this->form_validation->set_rules('femail', 'Finance Email', 'trim|required|valid_email');
        //$this->form_validation->set_rules('memail', 'Media Email', 'trim|required|valid_email');
        //$this->form_validation->set_rules('deposit', 'Deposit', 'trim|integer');

        $this->form_validation->set_message('check_client_name', 'Name "<b>' . $name . '</b>" already used. Please choose another name');
        $this->form_validation->set_message('check_select', 'Please select %s');


        if ($this->form_validation->run() == FALSE) {


            $data["name"] = $name;
            $data["id"] = "";
            $data["selectcat"] = $category;
            $data["phone"] = $phone;
            $data["npwp"] = $npwp;
            $data["address_street"] = $street;
            $data["category"] = $this->db->get("ref_business_category")->result();
            $data["selectcountry"] = $country;
            $this->db->order_by("country_name", "ASC");
            $data["country"] = $this->db->get("ref_country")->result();
            $data["conter"] = $country;
            $data["selectprovince"] = $province;
            $names = array('NULL');

            $this->db->where_not_in('province', $names);
            $this->db->order_by("province", "ASC");
            $data["province"] = $this->db->get("ref_province")->result();


            $data["selectcity"] = $city;
            $this->db->order_by("id", "ASC");
            $data["city"] = $this->db->get("ref_city")->result();

            $data["package"] = $this->db->get("ref_client_type")->result();
            $data["zip"] = $zip;
            $data["fname"] = $fname;
            $data["femail"] = $femail;
            $data["fphone"] = $fphone;
            $data["mname"] = $mname;
            $data["memail"] = $memail;
            $data["deposit"] = $deposit;
            $data["packageselect"] = $package;
            $data["mphone"] = $mphone;
            $data["blok"] = $blok;
            $data["nomor"] = $nomor;
            $data["rt"] = $rt;
            $data["rw"] = $rw;
            $data["kab"] = $kab;
            $data["kec"] = $kec;
            $data["kel"] = $kel;
            $data["neg"] = $neg;
            $data["status"] = $status;
            $data["npwpdress"] = $npwpdress;

            $data["selected_payment_type"] = $dataedit->payment_type;
            $payment_types = array("Postpaid", "Prepaid");
            $data["payment_type"] = $payment_types;

            $data["mod"] = "add";
            $data["title"] = "Add New Client";
            $this->template->load('default', 'client/form', $data);
        } else {
            $record["client_name"] = $name;
            $record["business_category_id"] = $category;
            $record["address_street"] = $street;
            $record["address_country"] = $country;
            if ($country == 1) {
                $record["address_province"] = $province;
            } else {
                $record["address_province"] = 34;
            }
            if ($country == 1 ) {
                $record['address_city'] = $city ;
            } else {
                $record["address_city"] = 435;
            }
            $record["address_zip"] = $zip;
            $record["phone"] = $phone;
            $record["npwp"] = $npwp;
            $record["finance_name"] = $fname;
            $record["finance_phone"] = $fphone;
            $record["finance_email"] = $femail;
            $record["media_name"] = $mname;
            $record["media_phone"] = $mphone;
            $record["media_email"] = $memail;
            $record["deposit"] = $deposit;
            $record["client_type"] = $package;
            $record["created_by"] = $this->session->userdata("user_id");
            $record["address_blok"] = $blok;
            $record["address_nomor"] = $nomor;
            $record["address_rt"] = $rt;
            $record["address_rw"] = $rw;
            $record["address_kota"] = $kab;
            $record["address_kecamatan"] = $kec;
            $record["address_kelurahan"] = $kel;
            $record["address_negara"] = $neg;
            $record["address_npwp"] = $npwpdress;
            $record["address_status"] = $status;
            $record["payment_type"] = $payment_type;

            $this->db->insert("client", $record);
            $idclient = $this->db->insert_id();
            //var_dump(add);

            $this->session->set_flashdata('add_client', '<div class="alert alert-success">
  <strong>Success!</strong> Sukses Add Client.
</div>');
            redirect(base_url("client/account"));
        }
    }

    public function edit($id = null) {

        $name = $this->input->post("name");
        $category = $this->input->post("category");
        $phone = $this->input->post("phone");
        $npwp = $this->input->post("npwp");
        $street = $this->input->post("street");
        $country = $this->input->post("country");
        $province = $this->input->post("province");
        $city = $this->input->post("city");
        $zip = $this->input->post("zip");
        $fname = $this->input->post("fname");
        $fphone = $this->input->post("fphone");
        $femail = $this->input->post("femail");
        $mname = $this->input->post("mname");
        $mphone = $this->input->post("mphone");
        $memail = $this->input->post("memail");
        $idhidden = $this->input->post("idhidden");
        $package = $this->input->post("package");
        $deposit = $this->input->post("deposit");
        $blok = $this->input->post("blok");
        $nomor = $this->input->post("nomor");
        $rt = $this->input->post("rt");
        $rw = $this->input->post("rw");
        $kab = $this->input->post("kab");
        $kec = $this->input->post("kec");
        $kel = $this->input->post("kel");
        $neg = $this->input->post("neg");
        $status = $this->input->post("status");
        $npwpdress = $this->input->post("npwpdress");
        $payment_type = $this->input->post("payment_type");

        $this->form_validation->set_rules('name', 'Name', 'trim|required|callback_check_client_name[' . $id . ']');
        $this->form_validation->set_rules('category', 'Business Category', 'callback_check_select');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[10]');
        $this->form_validation->set_rules('npwp', 'NPWP Number', 'required');
        $this->form_validation->set_rules('zip', 'Zip Code', 'required|numeric|max_length[5]');
        $this->form_validation->set_rules('street', 'Street Address', 'trim|required');
        //$this->form_validation->set_rules('country', 'Country', 'callback_check_select');
        //$this->form_validation->set_rules('province', 'Province', 'callback_check_select');
        //$this->form_validation->set_rules('city', 'City', 'callback_check_select');

        $this->form_validation->set_rules('fname', 'Finance Name', 'trim|required');
        $this->form_validation->set_rules('mname', 'Media Name', 'trim|required');
        $this->form_validation->set_rules('fphone', 'Finance Phone Number', 'required|numeric|min_length[10]');
        $this->form_validation->set_rules('mphone', 'Media Phone Number', 'required|numeric|min_length[10]');
        $this->form_validation->set_rules('femail', 'Finance Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('memail', 'Media Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('blok');
        $this->form_validation->set_rules('nomor');
        $this->form_validation->set_rules('rt');
        $this->form_validation->set_rules('rw');
        $this->form_validation->set_rules('kab');
        $this->form_validation->set_rules('kec');
        $this->form_validation->set_rules('neg');
        $this->form_validation->set_rules('npwpdress');
        // $this->form_validation->set_rules('deposit', 'Deposit', 'trim|required|integer');

        $this->form_validation->set_message('check_client_name', 'Name "<b>' . $name . '</b>" already used. Please choose another name');
        $this->form_validation->set_message('check_select', 'Please select %s');

        $this->db->where("client_id", $id);
        $dataedit = $this->db->get("client")->row();

        if ($this->form_validation->run() == FALSE) {

            $data["name"] = $dataedit->client_name;
            $data["id"] = $id;
            $data["selectcat"] = $dataedit->business_category_id;
            $data["phone"] = $dataedit->phone;
            $data["npwp"] = $dataedit->npwp;
            $data["address_street"] = $dataedit->address_street;
            $data["category"] = $this->db->get("ref_business_category")->result();
            $data["selectcountry"] = $dataedit->address_country;
            $this->db->order_by("country_name", "ASC");
            $data["country"] = $this->db->get("ref_country")->result();
            $data["selectprovince"] = $dataedit->address_province;
            $this->db->order_by("province", "ASC");
            $data["province"] = $this->db->get("ref_province")->result();
            $data["selectcity"] = $dataedit->address_city;
            $this->db->order_by("city", "ASC");
            $data["city"] = $this->db->get("ref_city")->result();
            $data["zip"] = $dataedit->address_zip;
            $data["fname"] = $dataedit->finance_name;
            $data["femail"] = $dataedit->finance_email;
            $data["fphone"] = $dataedit->finance_phone;
            $data["mname"] = $dataedit->media_name;
            $data["memail"] = $dataedit->media_email;
            $data["mphone"] = $dataedit->media_phone;

            $data["blok"] = $dataedit->address_blok;
            $data["nomor"] = $dataedit->address_nomor;
            $data["rt"] = $dataedit->address_rt;
            $data["rw"] = $dataedit->address_rw;
            $data["kab"] = $dataedit->address_kota;
            $data["kec"] = $dataedit->address_kecamatan;
            $data["kel"] = $dataedit->address_kelurahan;
            $data["neg"] = $dataedit->address_negara;
            $data["npwpdress"] = $dataedit->address_npwp;
            $data["status"] = $dataedit->address_status;

            $data["deposit"] = $dataedit->deposit;

            $data["packageselect"] = $dataedit->client_type;

            $data["package"] = $this->db->get("ref_client_type")->result();

            $data["selected_payment_type"] = $dataedit->payment_type;
            $payment_types = array("Postpaid", "Prepaid");
            $data["payment_type"] = $payment_types;

            $this->db->where("client_id", $id);
            $this->db->join("ref_brand", "ref_brand.brand_id=client_brand_rel.brand_id");
            $data["brandlist"] = $this->db->get("client_brand_rel")->result();


            $data["mod"] = "edit";
            $data["title"] = "Update Client";
            $data["tokenstart"] = $this->security->get_csrf_hash();
            $this->template->load('default', 'client/formedit', $data);
        } else {
            $record["client_name"] = $name;
            $record["business_category_id"] = $category;
            $record["address_street"] = $street;
            $record["address_country"] = $country;
            if ($country == 1) {
                $record["address_province"] = $province;
            } else {
                $record["address_province"] = 34;
            }
            if ($country == 1 ) {
                $record['address_city'] = $city ;
            } else {
                $record["address_city"] = 435;
            }
            $record["address_zip"] = $zip;
            $record["phone"] = $phone;
            $record["npwp"] = $npwp;
            $record["finance_name"] = $fname;
            $record["finance_phone"] = $fphone;
            $record["finance_email"] = $femail;
            $record["media_name"] = $mname;
            $record["media_phone"] = $mphone;
            $record["media_email"] = $memail;
            $record["deposit"] = $deposit;
            $record["client_type"] = $package;
            $record["created_by"] = $this->session->userdata("user_id");

            $record["address_blok"] = $blok;
            $record["address_nomor"] = $nomor;
            $record["address_rt"] = $rt;
            $record["address_rw"] = $rw;
            $record["address_kota"] = $kab;
            $record["address_kecamatan"] = $kec;
            $record["address_kelurahan"] = $kel;
            $record["address_negara"] = $neg;
            $record["address_npwp"] = $npwpdress;
            $record["address_status"] = $status;

            $record["payment_type"] = $payment_type;

            $where["client_id"] = $idhidden;

            $this->db->update("client", $record, $where);
            $this->session->set_flashdata('edit_client', '<div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> <b>' . $name . 
            '</b> Updated.</div>');
            redirect(base_url("client/account"));
        }
    }

    function check_client_name($post_string, $idcallback) {

        $this->db->where("client_name", $post_string);
        if ($idcallback > 0) {
            $this->db->where("client_id != ", $idcallback);
        }
        $namecheck = $this->db->get("client")->row();

        return $namecheck->client_id > 0 ? FALSE : TRUE;
    }

    function check_select($post_string) {

        if ($post_string > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function ajax($mod = null) {

        if ($mod == "country") {
            $countryid = $this->input->post("countryid");
            $prov = $this->db->get("ref_province")->result();
            $opt = "<option value=''> ==Select Province== </option>";
            foreach ($prov as $dataprovince) {
                $opt .= "<option value='" . $dataprovince->id . "'> " . $dataprovince->province . " </option>";
            }

            $ret = '<script>';
            $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
            $ret .= '$("#csrf_city").attr("value","' . $this->security->get_csrf_hash() . '");';
            if (sizeof($prov) > 0) {
                $ret.= '$("#province").attr("disabled",false);';
                $ret.= '$("#province").html("' . $opt . '");';
            } else {
                $ret.= '$("#province").attr("disabled",true);';
            }
            $ret .= '</script>';
            echo $ret;
            exit();
        } elseif ($mod == "province") {
            $provinceid = $this->input->post("provinceid");
            $this->db->where("province", $provinceid);
            $city = $this->db->get("ref_city")->result();
            $opt = "<option value=''> ==Select City== </option>";
            foreach ($city as $datacity) {
                $opt .= "<option value='" . $datacity->id . "'> " . $datacity->city . " </option>";
            }

            $ret = '<script>';
            $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
            $ret .= '$("#csrf_city").attr("value","' . $this->security->get_csrf_hash() . '");';
            if (sizeof($city) > 0) {
                $ret.= '$("#city").attr("disabled",false);';
                $ret.= '$("#city").html("' . $opt . '");';
            } else {
                $ret.= '$("#city").attr("disabled",true);';
            }
            $ret .= '</script>';
            echo $ret;
            exit();
        }
    }

    public function brand() {

        $data["brandlist"] = $this->db->get("ref_brand")->result();
        $data["title"] = "Brand List";
        $this->template->load('default', 'client/brand', $data);
    }

    public function addbrandlist($id = null) {


        $this->db->where("client_id", $id);
        $dataedit = $this->db->get("client")->row();

        $data["name"] = $dataedit->client_name;
        $data["id"] = $id;
        $data["selectcat"] = $dataedit->business_category_id;
        $data["category"] = $this->db->get("ref_business_category")->result();
        $data["phone"] = $dataedit->phone;
        $data["npwp"] = $dataedit->npwp;
        $this->db->where("client_id", $id);
        $this->db->join("ref_brand", "ref_brand.brand_id=client_brand_rel.brand_id");
        $data["brandlist"] = $this->db->get("client_brand_rel")->result();

        $data["title"] = "Add Brand to Client";
        $data["tokenstart"] = $this->security->get_csrf_hash();
        $this->template->load('default', 'client/addbrand', $data);
    }

    public function addbrandclient($id = null) {


        $brand = $this->input->post("brand");
        $idhidden = $this->input->post("idhidden");

        $this->form_validation->set_rules('brand', 'Brand', 'trim|required|callback_check_brand_name');
        $this->form_validation->set_message('check_brand_name', 'Name "<b>' . $brand . '</b>" already used. Please choose another name');

        if ($this->form_validation->run() == TRUE) {


            $rec["brand_name"] = $brand;
            $this->db->insert("ref_brand", $rec);
            $idbrand = $this->db->insert_id();

            $record["client_id"] = $idhidden;
            $record["brand_id"] = $idbrand;
            $this->db->insert("client_brand_rel", $record);

            //update script
            redirect(base_url("client/addbrandlist/" . $id));
        } else {

            //$this->db->select('brand_id')->from('client_brand_rel');
            //$subQuery =  $this->db->get_compiled_select();
            // Main Query
            /* $data["brandlist"] =
              $this->db->select('*')
              ->from('ref_brand')
              ->where("brand_id NOT IN ($subQuery)", NULL, FALSE)
              ->get()
              ->result();

             */

            $data["id"] = $id;
            $data["mod"] = "addbrandclient";
            $this->template->load('default', 'client/addbrandclient', $data);
        }
    }

    public function editbrandclient($id = null) {

        $brand = $this->input->post("brand");
        $idhidden = $this->input->post("idhiddenn");

        $iden = $this->input->post("iden");
        //$oke = $this->input->post("idiikut");
        //$idhidden = $this->input->post("idhidden");
        //$this->db->where("client_id", $idhidden);
        //$dataedit = $this->db->get("client")->row();
        //$oke = $dataedit->client_id;
        //$oke = $this->db->get("client_brand_rel");
        //$this->db->where("brand_id", array('brand_id' => $id));
        //$bb = $oke->client_id;

        $this->form_validation->set_rules('brand', 'Brand', 'trim|required|callback_check_brand_name');
        $this->form_validation->set_message('check_brand_name', 'Name "<b>' . $brand . '</b>" already used. Please choose another name');


        $this->db->where("brand_id", $id);
        $dataedit = $this->db->get("ref_brand")->row();

        //$this->db->where("brand_id", array('brand_id' => $id));
        //$dataid = $this->db->get("client_brand_rel")->row();
        //$bb = $dataid->client_id;

        if ($this->form_validation->run() == TRUE) {


            $data["brand_name"] = $brand;
            //$data["idikut"] = $bb;


            $this->db->where("brand_id", $id);


            //$data["title"] = "Update Client";
            //$data["tokenstart"] = $this->security->get_csrf_hash();

            $where["brand_id"] = $idhidden;

            $this->db->update("ref_brand", $data, $where);
            redirect(base_url("./client"));

            //redirect(base_url("./client/edit/".$bb));
        } else {

            //$this->db->select('brand_id')->from('client_brand_rel');
            //$subQuery =  $this->db->get_compiled_select();
            // Main Query
            /* $data["brandlist"] =
              $this->db->select('*')
              ->from('ref_brand')
              ->where("brand_id NOT IN ($subQuery)", NULL, FALSE)
              ->get()
              ->result();

             */

            $data["id"] = $id;
            $data["brand"] = $dataedit->brand_name;

            //$data["mod"] = "editbrandclient";
            $this->template->load('default', 'client/editbrandclient', $data);
        }
    }

    public function deletebrandclient() {

        $id = $this->input->post("brand_id");
        $this->db->delete("client_brand_rel", array("brand_id" => $id));
        echo "OK";
        exit();
    }

    public function addbrandref() {

        $name = $this->input->post("name");

        $this->form_validation->set_rules('name', 'Name', 'trim|required|callback_check_brand_name');
        $this->form_validation->set_message('check_brand_name', 'Name "<b>' . $name . '</b>" already used. Please choose another name');

        if ($this->form_validation->run() == FALSE) {

            $data["name"] = $name;
            $data["id"] = "";
            $data["title"] = "Add Brand";
            $data["mod"] = "addbrandref";
            $this->template->load('default', 'client/formbrandref', $data);
        } else {

            $record["brand_name"] = $name;
            $this->db->insert("ref_brand", $record);

            //update script
            redirect(base_url("client/brand"));
        }
    }

    public function editbrandref($id = NULL) {

        $name = $this->input->post("name");
        $idhidden = $this->input->post("idhidden");

        $this->form_validation->set_rules('name', 'Name', 'trim|required|callback_check_brand_name[' . $id . ']');
        $this->form_validation->set_message('check_brand_name', 'Name "<b>' . $name . '</b>" already used. Please choose another name');

        $dataedit = $this->db->get_where("ref_brand", array("brand_id" => $id))->row();

        if ($this->form_validation->run() == FALSE) {

            $data["name"] = $dataedit->brand_name;
            $data["id"] = $dataedit->brand_id;
            $data["title"] = "Update Brand";
            $data["mod"] = "editbrandref";
            $this->template->load('default', 'client/formbrandref', $data);
        } else {

            $record["brand_name"] = $name;
            $where["brand_id"] = $idhidden;
            $this->db->update("ref_brand", $record, $where);

            //update script
            redirect(base_url("client/brand"));
        }
    }

    function check_brand_name($post_string, $idcallback) {

        $this->db->where("brand_name", $post_string);
        if ($idcallback > 0) {
            $this->db->where("brand_id != ", $idcallback);
        }
        $namecheck = $this->db->get("ref_brand")->row();

        return $namecheck->brand_id > 0 ? FALSE : TRUE;
    }

    public function category() {

        $data["category"] = $this->db->get("ref_business_category")->result();

        $this->template->load('default', 'client/category', $data);
    }

    public function addcategory() {

        $name = $this->input->post("name");
        $description = $this->input->post("description");

        $this->form_validation->set_rules('name', 'Name', 'trim|required|callback_check_name');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');

        $this->form_validation->set_message('check_name', 'Name "<b>' . $name . '</b>" already used. Please choose another name');

        if ($this->form_validation->run() == FALSE) {
            $data["name"] = $name;
            $data["id"] = "";
            $data["mod"] = "addcategory";
            $data["description"] = $description;
            $data["title"] = "Create Business Category";
            $this->template->load('default', 'client/formcategory', $data);
        } else {
            $record["category_name"] = $name;
            $record["description"] = $description;
            $this->db->insert("ref_business_category", $record);
            $idcategory = $this->db->insert_id();


            redirect(base_url("client/category"));
        }
    }

    public function editcategory($id = null) {

        $name = $this->input->post("name");
        $description = $this->input->post("description");
        $idhidden = $this->input->post("idhidden");

        $this->form_validation->set_rules('name', 'Name', 'trim|required|callback_check_name[' . $id . ']');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');

        $this->form_validation->set_message('check_name', 'Name "<b>' . $name . '</b>" already used. Please choose another name');

        $dataedit = $this->db->get_where("ref_business_category", array("business_category_id" => $id))->row();

        if ($this->form_validation->run() == FALSE) {
            $data["name"] = $dataedit->category_name;
            $data["id"] = $id;
            $data["mod"] = "editcategory";
            $data["description"] = $dataedit->description;
            $data["title"] = "Edit Business Category";
            $this->template->load('default', 'client/formcategory', $data);
        } else {
            $record["category_name"] = $name;
            $record["description"] = $description;
            $where["business_category_id"] = $idhidden;
            $this->db->update("ref_business_category", $record, $where);

            redirect(base_url("client/category"));
        }
    }

    function check_name($post_string, $idcallback) {

        $this->db->where("category_name", $post_string);
        if ($idcallback > 0) {
            $this->db->where("business_category_id != ", $idcallback);
        }
        $namecheck = $this->db->get("ref_business_category")->row();

        return $namecheck->business_category_id > 0 ? FALSE : TRUE;
    }

}

