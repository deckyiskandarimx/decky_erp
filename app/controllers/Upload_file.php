<?php

class Upload_file extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $cekid = $this->session->userdata("user_id");
        if ($cekid == "") {
            redirect(base_url("auth"));
        }
    }

    function index() {
        $this->load->view('form_upload', array('error' => ' '));
    }

    function do_upload() {
        $config['upload_path'] = 'upload/';
        $config['allowed_types'] = 'pdf|txt';
        $config['max_size'] = '5000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('payment/form_upload', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $this->load->view('payment/sukses', $data);

            $upload_data = $this->upload->data();
            $status = '1';
            $insert_data = array(
                'file_name' => $upload_data['file_name'],
                'file_type' => $upload_data['file_type'],
                'status' => $status,
            );

            $this->db->insert('tb_file', $insert_data);
        }
    }

}
