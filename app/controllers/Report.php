<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Report extends Ci_Controller {

    function __construct() {
        parent::__construct();
        $cekid = $this->session->userdata("user_id");
        if ($cekid == "") {
            redirect(base_url("auth"));
        }
    }

    public function ar() {
        /*$this->db->select("a.quotation_id as quotation_id, "
                . "a.quotation_number as quotation_number, "
                . "a.quotation_date as quotation_date, "
                . "b.client_name as agency_name, "
                . "c.client_name as advertiser_name, "
                . "d.brand_name as brand_name, "
                . "a.campaign_name as campaign_name, "
                . "e.name as payment_type, "
                . "a.total as total,"
                . "g.name as package_name,"
                . "a.package_id,"
                . "f.name as status, "
                . "case when date_part('day', now() - a.created_on) < 30 then '<30'
                           when date_part('day', now() - a.created_on) < 60 then '30-60'
                           when date_part('day', now() - a.created_on) < 90 then '60-90'
                           when date_part('day', now() - a.created_on) < 180 then '90-180'
                           else '>180' end aging");
        $this->db->join("client b", "a.agency_id = b.client_id");
        $this->db->join("client c", "a.advertiser_id = c.client_id");
        $this->db->join("ref_brand d", "a.brand_id = d.brand_id");
        $this->db->join("ref_payment e", "a.payment_type = e.payment_id");
        $this->db->join("ref_quotation_status f", "a.status = f.id");
        $this->db->join("package g", "a.package_id = g.id", "left");
        $this->db->order_by("quotation_id", "DESC");
        $data["quotation"] = $this->db->get("quotation a")->result(); */
        
        $this->db->select("view_io1.quotation_number, "
                . "view_io1.quotation_id, "
                . "view_io1.campaign_name, "
                . "payment_invoice.total_quotation,"
                . "payment_invoice.pinv_total");
                //. 'view_io1.io_number, '
                //. 'product.name as product_name, '
                //. 'partner.name as partner_name, '
                //. 'view_io1.io_status, '
                //. 'view_io1.invoice_number, view_io1.quotation_id');

        //$this->db->join('product', 'product.product_id = view_io1.product_id');
        //$this->db->join('partner', 'partner.partner_id = view_io1.partner_id');
        $this->db->join('payment_invoice', 'view_io1.quotation_id = payment_invoice.quotation_id');
        //$this->db->where("view_io1.quotation_id in (select quotation_id from payment_invoice_item)");        
        $this->db->order_by("view_io1.quotation_id");               
        $this->db->group_by("quotation_number,view_io1.quotation_id,campaign_name,total_quotation,pinv_total");
        $data['quotation'] = $this->db->get("view_io1")->result();

        $data["title"] = "Accounts Receivable";
        $this->template->load('default', 'reports/ar', $data);
    }
    
    
    public function getquotationtotal($id = null){
        
        $this->db->select("quotation_item.amount as amount, quotation_item.price as price, quotation_item.discount as discount");             
        $this->db->join("product_partner_rel", "quotation_item.pp_id = product_partner_rel.pp_id");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");   
        $this->db->where("quotation_item.quotation_id", $id);        
        $ppinv = $this->db->get("quotation_item")->result();
        
        foreach ($ppinv as $data){
            $discounts = 100-$data->discount;
            $tdisk = ($discounts/100)*$data->price*$data->amount;
            $amountprince = $amountprince + ($tdisk); 
        }
        
        return $amountprince;
        
    }
    
    public function productdetailcpay($quotation_id = null){
        
        $this->db->select("product2.product_name as product_name, partner.name as partner_name, amount, quotation_item.price, quotation_item.discount, io_number, amount_invoice");              
        $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");            
        $this->db->join("io", "quotation_item.item_id = io.campaign_item_id");            
        $this->db->join("payment_invoice_item", "payment_invoice_item.quotation_item = quotation_item.item_id", "left");            
        $this->db->where("quotation_item.quotation_id", $quotation_id);        
        $datadetail = $this->db->get("quotation_item")->result();     
        
        //4*gapok*(((1/2)150%)+(1/2(3)))*budgetpoll 
        return $datadetail;
    }
    
    
    public function productdetailcpay_report($quotation_id = null){
        
        $this->db->select("product2.product_name as product_name, partner.name as partner_name, amount, quotation_item.price, quotation_item.discount, io_number, amount_invoice");              
        $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");            
        $this->db->join("io", "quotation_item.item_id = io.campaign_item_id");            
        $this->db->join("payment_invoice_item", "payment_invoice_item.quotation_item = quotation_item.item_id", "left");            
        $this->db->where("quotation_item.quotation_id", $quotation_id);        
        $datadetail = $this->db->get("quotation_item")->result();     
        
        $text = "";
        foreach ($datadetail as $data){
            $text .= "<".$data->io_number."|".$data->product_name."|".$data->partner_name.">,";
        }
        
        return $text;
    }
    
    
    public function productdetailcpay_report_beta($quotation_id = null){
        
        $this->db->select("quotation.quotation_id, quotation.quotation_number, quotation.campaign_name, "
                        . "payment_invoice.pinv_total, "
                        . "product2.product_name as product_name, "
                        . "partner.name as partner_name, "
                        . "amount, quotation_item.price, "
                        . "quotation_item.discount, io_number, "
                        . "amount_invoice");              
        $this->db->join("quotation", "quotation_item.quotation_id = quotation.quotation_id");
        $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");            
        $this->db->join("io", "quotation_item.item_id = io.campaign_item_id");            
        $this->db->join("payment_invoice", "payment_invoice.quotation_id = quotation_item.quotation_id", "left");            
        $this->db->join("payment_invoice_item", "payment_invoice_item.quotation_item = quotation_item.item_id", "left");            
        $this->db->where("quotation_item.quotation_id", $quotation_id);        
        $datadetail = $this->db->get("quotation_item")->result();     
        
        $text = "";
        foreach ($datadetail as $data){
            $text .= "<".$data->io_number."|".$data->product_name."|".$data->partner_name.">,";
        }
        
        return $text;
    }
    
    
    public function getpaymentpaid($id=null){
                
        $this->db->where("quotation_id", $id);        
        $data = $this->db->get("payment_invoice")->row();
        
        return $data->paid;
    }
 
    
    function getstatuspayment($quotation_id = null){
        
        $this->db->select("count(payinvo_item_id) as jml");
        $this->db->where("quotation_id", $quotation_id);
        $data = $this->db->get("payment_invoice_item")->row();
        
        return $data->jml;
    }
    
    
    function agingdays($id=NULL){
        
        $this->db->where("quotation_id", $id);        
        $data = $this->db->get("payment_invoice")->row();
        $paid = $data->paid;
        $dateinv = $data->pinv_date;
        $datepaid = $data->paid_date;
        $now = date("Y-m-d");
        
        if($paid > 0){
            $tgl = Report::dateDiffcustom($datepaid, $dateinv);
        }else{
            $tgl = Report::dateDiffcustom($now, $dateinv);
            //$tgl = "";
        }
        
        return $tgl;
    }

    
    function qamount($id = null){
        
        //$this->db->select("");
        $this->db->where("quotation_id", $id);
        $data = $this->db->get("quotation_item")->result();
        
        $discounts = 100-$data->discount;
        $tdisk = ($discounts/100)*$data->price;
        
    }
    
    
    public function dateDiffcustom ($d1, $d2) {
      return round(abs(strtotime($d1)-strtotime($d2))/86400);
    }  
    
    
    public function ap() {
        $this->db->select('view_io1.quotation_id, view_io1.io_id,view_io1.io_number,io.campaign_item_id,'
                . 'view_io1.quotation_number, '
                . 'view_io1.campaign_name,'                
                . 'product2.product_name as product_name,'
                . 'partner.name as partner_name,'
                . 'view_io1.grand_total,'
                . 'view_io1.io_status,'
                . '(select total_quotation as iojml from payment_invoice where payment_invoice.quotation_id = view_io1.quotation_id) as iojml, '
                //. '(select sum(amount_invoice) as jml from payment_invoice_item where payment_invoice_item.quotation_id = view_io1.quotation_id) as jml, '
                . 'invoice_.transfer_amount,'
                . 'invoice_.paid_status,'
                . 'invoice_.paid_date,'
                . 'invoice_.invoice_date,'
                //. 'invoice_.adjusted,'
                //. 'invoice_.tax_ppn,'
                //. 'invoice_.invoice_date, '
                . 'users.fullname');
        $this->db->join('io', 'io.io_id = view_io1.io_id');
        $this->db->join('product2', 'product2.product_id = view_io1.product_id');
        $this->db->join('partner', 'partner.partner_id = view_io1.partner_id');        
        $this->db->join('users', 'users.user_id = view_io1.created_by');
        $this->db->join('invoice_', 'invoice_.id_invoice = view_io1.id_invoice','left');
        //$this->db->group_by('view_io1.quotation_id,view_io1.quotation_number,view_io1.campaign_name,view_io1.io_status,users.fullname,pinv_date,paid,paid_date');
        $data['io_report'] = $this->db->get('view_io1')->result();
        
        $data['datenow'] = '20'.date('y-m-d');
        $data["title"] = "Accounts Payable";
        $this->template->load('default', 'reports/ap', $data);
    }

    
    public function getIOAmount($id = null){
        
        $this->db->select("b.amount as amount, "
                . "(case when b.partner_type = 2 then b.fixed_price  else c.product_tariff_idr_total end) as priced,"
                . "(case when b.partner_type = 2 then '0'  else (100 - b.pshare)  end) as disc");
        $this->db->join("view_item b", "b.item_id = a.campaign_item_id");        
        $this->db->join("product2 c", "b.product_id = c.product_id");
        $this->db->join("partner d", "b.partner_id = d.partner_id");
        
        $this->db->where("a.campaign_item_id", $id);
        $this->db->order_by("a.io_id", "DESC");      
        $data = $this->db->get("io a")->row();
        
        return (((100- $data->disc)/100)*($data->priced)*$data->amount) * 1.1;
        
        
    }
    
    
    public function revenue() {
        $this->db->select("a.amount, a.price, a.discount, b.price, b.discount, c.selling_price, c.bearer_cost, x.status, x.advertiser_id, x.agency_id");
        $this->db->join("quotation_item a", "x.quotation_id = a.quotation_id", "left");
        $this->db->join("product_partner_rel b", "a.pp_id = b.pp_id", "left");
        $this->db->join("product2 c", "b.product_id = c.product_id", "left");
        $this->db->where("x.agency_id <> x.advertiser_id");
        $data['revenue'] = $this->db->get("quotation x")->result();

        var_dump($data);
        die;
    }

    
    function rev(){
        
        
        $data = array();
        
        $this->db->select("c.pinv_number as number, c.pinv_date as invdate, c.paid as paid, c.paid_date as paid_date, a.bearer_costdb as bearer_costdb,"
                . "a.rev_share_isat as shareisat, a.cogs as cogs, a.total as total, a.partner_share as partner_share,"
                . "campaign_name, quotation_number, io_number");
        $this->db->join("quotation b","a.quotation_id = b.quotation_id");
        $this->db->join("payment_invoice c","b.quotation_id = c.quotation_id", "left");
        //$this->db->join("payment_invoice_item d","d.payinvoice_id = c.pinv_id");
        $this->db->group_by("io_id,pinv_number,pinv_date,paid,paid_date,a.bearer_costdb,a.rev_share_isat,a.cogs,a.total,campaign_name, quotation_number, io_number");
        $data["datarev"] = $this->db->get("io a")->result();
        
        $data["title"] = "Report Revenue";
        $this->template->load('default', 'reports/revenue', $data);
        
    }
    
    
    /*
     * select  from quotation x left outer join quotation_item a on (x.quotation_id = a.quotation_id) 
     * left outer join product_partner_rel b on (a.pp_id = b.pp_id)
     * left outer join product c on (b.product_id = c.product_id) where x.agency_id <> x.advertiser_id;
     */
    
    
    function ardownload(){
        
        
        $this->load->library('excel');
        
        $this->excel->setActiveSheetIndex(0);
        
        $this->excel->getActiveSheet()->setTitle('Accounts Receivable');
        
        
        $this->db->select("view_io1.quotation_number, "
                . "view_io1.quotation_id, "
                . "view_io1.campaign_name, "
                . "payment_invoice.total_quotation,"
                . "payment_invoice.pinv_total");
        $this->db->join('payment_invoice', 'view_io1.quotation_id = payment_invoice.quotation_id');        
        $this->db->order_by("view_io1.quotation_id");               
        $this->db->group_by("quotation_number,view_io1.quotation_id,campaign_name,total_quotation,pinv_total");
        $quotation = $this->db->get("view_io1")->result();
        
        
        $this->excel->getActiveSheet()->setCellValue('A1', "No."); 
            
            $this->excel->getActiveSheet()->setCellValue('B1', "Quotation Number"); 
            $this->excel->getActiveSheet()->setCellValue('C1', "Campaign Name"); 
            
            //$this->excel->getActiveSheet()->getStyle('D1:D1000')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('D1', "Detail Item"); 
            $this->excel->getActiveSheet()->setCellValue('E1', "Quotation Amount"); 
            $this->excel->getActiveSheet()->setCellValue('F1', "Invoiced Amount"); 
            $this->excel->getActiveSheet()->setCellValue('G1', "Variance"); 
            $this->excel->getActiveSheet()->setCellValue('H1', "Aging Days"); 
            $this->excel->getActiveSheet()->setCellValue('I1', "Aging"); 
            $this->excel->getActiveSheet()->setCellValue('J1', "Status"); 
            
        $no = 1;
        $num = 2;
        
        foreach($quotation as $datar){
            $this->excel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('A'.$num, $no++); 
            
            $this->excel->getActiveSheet()->getStyle('B')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $this->excel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('B'.$num, $datar->quotation_number); 
            
            $this->excel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('C'.$num, $datar->campaign_name); 
            
            
            $this->excel->getActiveSheet()->getColumnDimension('D'.$num)->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle('D'.$num.':D'.$this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('D'.$num, $this->productdetailcpay_report($datar->quotation_id));            
            
            $this->excel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('E')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValue('E'.$num, $datar->total_quotation); 
            
            $this->excel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('F')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValue('F'.$num, $datar->pinv_total); 
            
            $this->excel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('G')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValue('G'.$num, $datar->total_quotation-$datar->pinv_total);     
            
            $this->excel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('H'.$num, $datar->total_quotation == 0 ? 0 : $this->agingdays($datar->quotation_id)); 
            
            
            if ($this->agingdays($datar->quotation_id) < 30) {
                $q = "0 - 29";
            }  else if($this->agingdays($datar->quotation_id) >= 30 && Report::agingdays($datar->quotation_id) <= 60){
                $q = "30 - 60";
            } else if($this->agingdays($datar->quotation_id) >= 61 && Report::agingdays($datar->quotation_id) <= 90){
                $q = "61 - 90";
            } else if($this->agingdays($datar->quotation_id) > 90 && $datar->total_quotation != 0){
                $q = "> 90";
            } 
            
            $this->excel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $this->excel->getActiveSheet()->getStyle('I')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->setCellValue('I'.$num, $q); 
            
            if($this->getpaymentpaid($datar->quotation_id) > 0){
                $q2 = "PAID";
            }else{
                if($this->getstatuspayment($datar->quotation_id) > 0):  
                    $q2 = "Invoice Sent";
                else :
                   $q2 = "Invoicing Process"; 
                endif;
            }
            
            $this->excel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('J'.$num, $q2); 
         
            $num++;
        }
        
        
        $filename='Accounts_Receivable.xls'; 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output');

        
    }
    
    function ardownload2(){
        
        $this->load->library('excel');        
        $this->excel->setActiveSheetIndex(0);        
        $this->excel->getActiveSheet()->setTitle('Accounts Receivable');
        
        
        $this->db->select("quotation.quotation_id, quotation.quotation_number, quotation.campaign_name, payment_invoice.total_quotation, "
                        . "payment_invoice.pinv_total, "
                        . "product2.product_name as product_name, "
                        . "partner.name as partner_name, "
                        . "amount, quotation_item.price, "
                        . "quotation_item.discount, io_number, "
                        . "amount_invoice");              
        $this->db->join("quotation", "quotation_item.quotation_id = quotation.quotation_id");
        $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");            
        $this->db->join("io", "quotation_item.item_id = io.campaign_item_id");            
        $this->db->join("payment_invoice", "payment_invoice.quotation_id = quotation_item.quotation_id");            
        $this->db->join("payment_invoice_item", "payment_invoice_item.quotation_item = quotation_item.item_id", "left");            
        //$this->db->where("quotation_item.quotation_id", $quotation_id);       
        $this->db->order_by("quotation_id", "DESC");       
        $datadetail = $this->db->get("quotation_item")->result(); 
        
        
        
        $this->excel->getActiveSheet()->setCellValue('A1', "No."); 
            
        $this->excel->getActiveSheet()->setCellValue('B1', "Quotation Number"); 
        $this->excel->getActiveSheet()->setCellValue('C1', "Campaign Name"); 

        //$this->excel->getActiveSheet()->getStyle('D1:D1000')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
        $this->excel->getActiveSheet()->setCellValue('D1', "IO Number"); 
        $this->excel->getActiveSheet()->setCellValue('E1', "Product"); 
        $this->excel->getActiveSheet()->setCellValue('F1', "Partner"); 
        $this->excel->getActiveSheet()->setCellValue('G1', "Quotation Amount"); 
        $this->excel->getActiveSheet()->setCellValue('H1', "Invoiced Amount"); 
        $this->excel->getActiveSheet()->setCellValue('I1', "Variance"); 
        $this->excel->getActiveSheet()->setCellValue('J1', "Aging Days"); 
        $this->excel->getActiveSheet()->setCellValue('K1', "Aging"); 
        $this->excel->getActiveSheet()->setCellValue('L1', "Status"); 
            
        $no = 1;
        $num = 2;
        foreach ($datadetail as $datar){
            $this->excel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('A'.$num, $no++); 
            
            
            $this->excel->getActiveSheet()->getStyle('B')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $this->excel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('B'.$num, $datar->quotation_number); 
            
            
            $this->excel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('C'.$num, $datar->campaign_name); 
            
            $this->excel->getActiveSheet()->getStyle('D')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $this->excel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('D'.$num, $datar->io_number); 
            
            $this->excel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('E'.$num, $datar->product_name);
            
            $this->excel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('F'.$num, $datar->partner_name);
            
            
            $this->excel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('G')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValue('G'.$num, $datar->total_quotation); 
            
            $this->excel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('H')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValue('H'.$num, $datar->pinv_total); 
            
            $this->excel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValue('I'.$num, $datar->total_quotation-$datar->pinv_total);     
            
            $this->excel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('J'.$num, $datar->total_quotation == 0 ? 0 : $this->agingdays($datar->quotation_id)); 
            
            
            if ($this->agingdays($datar->quotation_id) < 30) {
                $q = "0 - 29";
            }  else if($this->agingdays($datar->quotation_id) >= 30 && $this->agingdays($datar->quotation_id) <= 60){
                $q = "30 - 60";
            } else if($this->agingdays($datar->quotation_id) >= 61 && $this->agingdays($datar->quotation_id) <= 90){
                $q = "61 - 90";
            } else if($this->agingdays($datar->quotation_id) > 90 && $datar->total_quotation != 0){
                $q = "> 90";
            } 
            
            $this->excel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('K')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $this->excel->getActiveSheet()->getStyle('K')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->excel->getActiveSheet()->setCellValue('K'.$num, $q); 
            
            if($this->getpaymentpaid($datar->quotation_id) > 0){
                $q2 = "PAID";
            }else{
                if($this->getstatuspayment($datar->quotation_id) > 0):  
                    $q2 = "Invoice Sent";
                else :
                   $q2 = "Invoicing Process"; 
                endif;
            }
            
            $this->excel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->setCellValue('L'.$num, $q2); 
            
            $num++;
        }
        
        
        $filename='Accounts_Receivable.xls'; 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output');
        
        
        
    }
    
    
    function apdownload(){
        
        
        $this->load->library('excel');
        
        $this->excel->setActiveSheetIndex(0);
        
        $this->excel->getActiveSheet()->setTitle('Accounts Payable');
        
        $this->db->select('view_io1.quotation_id, view_io1.io_id,view_io1.io_number,io.campaign_item_id,'
                . 'view_io1.quotation_number, '
                . 'view_io1.campaign_name,'                
                . 'product2.product_name as product_name,'
                . 'partner.name as partner_name,'
                . 'view_io1.grand_total,'
                . 'view_io1.io_status,'
                . '(select total_quotation as iojml from payment_invoice where payment_invoice.quotation_id = view_io1.quotation_id) as iojml, '
                //. '(select sum(amount_invoice) as jml from payment_invoice_item where payment_invoice_item.quotation_id = view_io1.quotation_id) as jml, '
                . 'invoice_.transfer_amount,'
                . 'invoice_.paid_status,'
                . 'invoice_.paid_date,'
                . 'invoice_.invoice_date,'
                //. 'invoice_.adjusted,'
                //. 'invoice_.tax_ppn,'
                //. 'invoice_.invoice_date, '
                . 'users.fullname');
        $this->db->join('io', 'io.io_id = view_io1.io_id');
        $this->db->join('product2', 'product2.product_id = view_io1.product_id');
        $this->db->join('partner', 'partner.partner_id = view_io1.partner_id');        
        $this->db->join('users', 'users.user_id = view_io1.created_by');
        $this->db->join('invoice_', 'invoice_.id_invoice = view_io1.id_invoice','left');
        //$this->db->group_by('view_io1.quotation_id,view_io1.quotation_number,view_io1.campaign_name,view_io1.io_status,users.fullname,pinv_date,paid,paid_date');
        $dap = $this->db->get('view_io1')->result();
        
        
        $this->excel->getActiveSheet()->setCellValue('A1', "No."); 
            
            $this->excel->getActiveSheet()->setCellValue('B1', "Quotation Number"); 
            $this->excel->getActiveSheet()->setCellValue('C1', "Campaign Name"); 
            
            $this->excel->getActiveSheet()->setCellValue('D1', "IO Number"); 
            $this->excel->getActiveSheet()->setCellValue('E1', "Product"); 
            $this->excel->getActiveSheet()->setCellValue('F1', "Partner"); 
            $this->excel->getActiveSheet()->setCellValue('G1', "IO Amount"); 
            $this->excel->getActiveSheet()->setCellValue('H1', "Invoice Amount"); 
            $this->excel->getActiveSheet()->setCellValue('I1', "Adjusment"); 
            $this->excel->getActiveSheet()->setCellValue('J1', "Status"); 
            $this->excel->getActiveSheet()->setCellValue('K1', "Aging(days)"); 
            $this->excel->getActiveSheet()->setCellValue('L1', "Aging(Category)"); 
            $this->excel->getActiveSheet()->setCellValue('M1', "Sales"); 
            
            $num = 2;
            $no = 1;
            
            foreach($dap as $datar){
                $this->excel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('A'.$num, $no++); 
                
                $this->excel->getActiveSheet()->getStyle('B')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('B'.$num, $datar->quotation_number); 

                $this->excel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('C'.$num, $datar->campaign_name); 
                
                $this->excel->getActiveSheet()->getStyle('D')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle(D.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('D'.$num, $datar->io_number); 
                
                $this->excel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('E'.$num, $datar->product_name); 
                
                $this->excel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('F'.$num, $datar->partner_name); 
                
            
                $this->excel->getActiveSheet()->getStyle('G')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('G'.$num, $this->getIOAmount($datar->campaign_item_id)); 
                
                $this->excel->getActiveSheet()->getStyle('H')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('H'.$num, $datar->transfer_amount); 
                
                $ioamount = $this->getIOAmount($datar->campaign_item_id);
                $transferamount= $datar->transfer_amount;
                $adj = (int)$ioamount - (int)$transferamount;
                
                $this->excel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('I'.$num, $adj); 
                
                
                if ($datar->paid_status == 2) {
                    $status = 'paid';
                } else {
                    $status = 'unpaid';
                }
                

                $this->excel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('J'.$num, $status); 
                
                $invdate = $datar->invoice_date;
                $paiddate = $datar->paid_date;
                $nowdate = date("Y-m-d");

                if ($datar->paid_status == 2) {

                    $dt1 = strtotime($invdate);
                    $dt2 = strtotime($paiddate);
                    $diff = abs($dt2-$dt1);
                    $selisih = $diff/86400;



                } else {

                    $dt1 = strtotime($invdate);
                    $dt2 = strtotime($nowdate);
                    $diff = abs($dt2-$dt1);
                    $selisih = $diff/86400;

                }
                
                $this->excel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('K'.$num, $selisih); 
                
                if($selisih < 30){
                    $agingkat = "0 - 29";
                }else if($selisih >= 30 && $selisih <= 59 ){
                    $agingkat = "30 - 59";
                }else if($selisih >= 60 && $selisih <= 90 ){
                    $agingkat = "60 - 90";
                }else if($selisih > 90){
                    $agingkat = "90";
                }
                
                $this->excel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('L'.$num, $agingkat); 
                
                
                
                $this->excel->getActiveSheet()->getStyle('M'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('M'.$num, $datar->fullname); 
            
            $num++;
        }
        
        
        $filename='Accounts_Payable.xls'; 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output');
        
    }
    
    
    function revdownload(){
        
        
        $this->load->library('excel');
        
        $this->excel->setActiveSheetIndex(0);
        
        $this->excel->getActiveSheet()->setTitle('Revenue');
        
        $this->db->select("c.pinv_number as number, c.pinv_date as invdate, c.paid as paid, c.paid_date as paid_date, a.bearer_costdb as bearer_costdb,"
                . "a.rev_share_isat as shareisat, a.cogs as cogs, a.total as total, a.partner_share as partner_share,"
                . "campaign_name, quotation_number, io_number");
        $this->db->join("quotation b","a.quotation_id = b.quotation_id");
        $this->db->join("payment_invoice c","b.quotation_id = c.quotation_id");
        $this->db->join("payment_invoice_item d","d.payinvoice_id = c.pinv_id");
        $this->db->group_by("io_id,pinv_number,pinv_date,paid,paid_date,a.bearer_costdb,a.rev_share_isat,a.cogs,a.total,campaign_name, quotation_number, io_number");
        $revd = $this->db->get("io a")->result();
        
        
        $this->excel->getActiveSheet()->setCellValue('A1', "No."); 
            
            $this->excel->getActiveSheet()->setCellValue('B1', "Quotation Number"); 
            $this->excel->getActiveSheet()->setCellValue('C1', "Campaign Name"); 
            
            $this->excel->getActiveSheet()->setCellValue('D1', "IO Number"); 
            $this->excel->getActiveSheet()->setCellValue('E1', "Invoice Number"); 
            $this->excel->getActiveSheet()->setCellValue('F1', "Invoice Date"); 
            $this->excel->getActiveSheet()->setCellValue('G1', "Payment Status"); 
            $this->excel->getActiveSheet()->setCellValue('H1', "Payment Date"); 
            $this->excel->getActiveSheet()->setCellValue('I1', "Cost Base"); 
            $this->excel->getActiveSheet()->setCellValue('J1', "Thirdparty Cost"); 
            $this->excel->getActiveSheet()->setCellValue('K1', "Rev Share Indosat"); 
            $this->excel->getActiveSheet()->setCellValue('L1', "COGS"); 
            $this->excel->getActiveSheet()->setCellValue('M1', "Gross Margin(rev nett)"); 
            
            $num = 2;
            $no = 1;
            
            foreach($revd as $datar){
                $this->excel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('A'.$num, $no++); 
                
                $this->excel->getActiveSheet()->getStyle('B')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('B'.$num, $datar->quotation_number); 

                $this->excel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('C'.$num, $datar->campaign_name); 
                
                $this->excel->getActiveSheet()->getStyle('D')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle(D.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('D'.$num, $datar->io_number); 
                
                $this->excel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('E'.$num, $datar->number); 
                
                $this->excel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('F'.$num, $datar->invdate); 
                
                if($datar->paid) {$s= "Paid"; }else{ $s="Not Paid"; }
                
                $this->excel->getActiveSheet()->getStyle('G')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('G'.$num, $s); 
            
                $this->excel->getActiveSheet()->getStyle('H')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('H'.$num, date("d-m-Y", strtotime($datar->paid_date))); 
                
                
                
                $this->excel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('I'.$num, $datar->bearer_costdb); 
               
                
                $this->excel->getActiveSheet()->getStyle('J')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('J'.$num, $datar->partner_share); 
                
               
                $this->excel->getActiveSheet()->getStyle('K')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('K'.$num, $datar->shareisat); 
               
                $this->excel->getActiveSheet()->getStyle('L')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('L'.$num, $datar->cogs); 
               
                $this->excel->getActiveSheet()->getStyle('M')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $this->excel->getActiveSheet()->getStyle('M'.$num)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->setCellValue('M'.$num, number_format((int)$datar->total-(int)$datar->cogs)); 
                
                
            
            $num++;
        }
        
        
        $filename='Accounts_Payable.xls'; 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output');
        
    }
    
    
}
