<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Campaign extends Ci_Controller {

    public function __construct() {
        parent::__construct();
        $cekid = $this->session->userdata("user_id");
        if ($cekid == "") {
            redirect(base_url("auth"));
        }
    }

    public function quotation() {
        ini_set('max_execution_time', 300);
        $data["title"] = "Quotation";

        $this->db->select("a.quotation_id as quotation_id, "
                . "a.quotation_number as quotation_number, "
                . "a.quotation_date as quotation_date, "
                . "b.client_name as agency_name, "
                . "c.client_name as advertiser_name, "
                . "d.brand_name as brand_name, "
                . "a.campaign_name as campaign_name, "
                . "e.name as payment_type, "
                . "a.total as total,"
                . "g.package_name as package_name,"
                . "a.signed_quotation_file as signed_quotation_file,"
                . "a.package_id,"
                . "a.created_by,"
                . "f.name as status,"
                . "a.termpayment");
        $this->db->join("client b", "a.agency_id = b.client_id", "left");
        $this->db->join("client c", "a.advertiser_id = c.client_id");
        $this->db->join("ref_brand d", "a.brand_id = d.brand_id", "left");
        $this->db->join("ref_payment e", "a.payment_type = e.payment_id");
        $this->db->join("ref_quotation_status f", "a.status = f.id");
        $this->db->join("package g", "a.package_id = g.package_id", "left");
        $this->db->order_by("quotation_id", "DESC");
        $data["quotation"] = $this->db->get("quotation a")->result();

        $this->db->where("id < 4", NULL, FALSE);
        $data["status"] = $this->db->get("ref_quotation_status")->result();

        $this->template->load('default', 'campaign/quotation', $data);
    }
    
    public function totalitemquotation($idq){
        
        $query = "select amount,price,discount from quotation_item where quotation_id = '".$idq."'";
        
        $datatotal = $this->db->query($query)->result();
        $grandtotal = 0;   
        foreach ($datatotal as $total){                     
            $price = $total->price;
            $amount = $total->amount;
            $discount = 100 - $total->discount;            
            $semitotal = ($price*$amount)*($discount/100);            
            $grandtotal = $semitotal+$grandtotal;
        }
        
        return $grandtotal;
        
    }

    public function addquotation() {

        $name = $this->input->post("campaign");
        $qno = $this->input->post("qno");
        $qdate = $this->input->post("qdate");
        $advertiser = $this->input->post("advertiser");
        $agency = $this->input->post("agency");
        $brand = $this->input->post("brand");
        $payment = $this->input->post("payment");
        $busscat = $this->input->post("busscat");
        $package = $this->input->post("package");
        $q_payment = $this->input->post("q_payment");
        $termpayment = $this->input->post("termpayment");

        $datepick = explode("/", $qdate);
        $dateformat = $datepick[2] . "-" . $datepick[1] . "-" . $datepick[0];

        $this->form_validation->set_rules('qno', 'Quotation Number', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('qdate', 'Quotation Date', 'trim|required');
        $this->form_validation->set_rules('advertiser', 'Advertiser', 'callback_check_value');
        //$this->form_validation->set_rules('brand', 'Brand', 'callback_check_value'); <-- package depending
        $this->form_validation->set_rules('agency', 'Agency', 'callback_check_value');
        $this->form_validation->set_rules('payment', 'Payment Type', 'callback_check_value');
        if($package > 0){
            $this->form_validation->set_rules('termpayment', 'Term Payment', 'callback_check_value');
        }

        $this->form_validation->set_message('check_value', 'You need to select %s something other than the default');

        if ($this->form_validation->run() == FALSE) {
            $this->db->select("ref_brand.brand_id, ref_brand.brand_name");
            $this->db->where("client_brand_rel.client_id", $advertiser);
            $this->db->join("client_brand_rel", "client_brand_rel.brand_id = ref_brand.brand_id");
            $data["brandid"] = $this->db->get("ref_brand")->result();

            $data["title"] = "Add Quotation";
            $data["client"] = $this->db->get("client")->result();
            $data["payment"] = $this->db->get("ref_payment")->result();
            $data["package"] = $this->db->get("package")->result();
            $data["full_payment"] = $this->db->get("ref_quotation_payment")->result();
            $data["mod"] = "addquotation";
            $data["qno"] = $qno;
            $data["qdate"] = $qdate;
            $data["advertiserid"] = $advertiser;
            $data["busscat"] = $busscat;
            $data["brands"] = $brand;
            $data["campaign"] = $name;
            $data["agency"] = $agency;
            $data["paymentid"] = $payment;
            $data["packageselect"] = $package;
            $data["fpaymentselect"] = $q_payment;
            $data["termpayment"] = $termpayment;
            $data["id"] = 0;


            $this->template->load('default', 'campaign/formquotation', $data);
        } else {
            $this->db->where('quotation_number', $qno);
            $this->db->select('quotation_number');
            $dataquotation = $this->db->get('quotation')->result();

            if (sizeof($dataquotation) != 0) {
                $this->session->set_flashdata('val_qno', '<div class="alert alert-danger">
  <strong>Danger!</strong> quotation number already exists.
</div>');
                redirect(base_url("campaign/addquotation"));
            } else {
                $record["quotation_number"] = $qno;
            }
            $record["quotation_date"] = $dateformat;
            $record["agency_id"] = $agency;
            $record["advertiser_id"] = $advertiser;
            $record["brand_id"] = $brand;
            $record["campaign_name"] = $name;
            $record["payment_type"] = $payment;
            
            if($package > 0){
                $this->db->select("product_tariff_idr_total as totalpackage");
                $this->db->where("package_id",$package);
                $totalpackage = $this->db->get("package")->row();
                $totalpackage_idr = $totalpackage->totalpackage;
            }else{
                $totalpackage_idr = 0;
            }
            
                $record["total"] = $totalpackage_idr;
            $record["tax"] = 0;
            $record["grand_total"] = 0;
            $record["termpayment"] = $termpayment;
//$record["package_id"]= null;
            if ($package == 0) {
                $this->session->set_flashdata('quotation_np', '<div class="alert alert-success">
  <strong>Success!</strong> Quotation non Package has been created.
</div>');
                echo $package;
                $record["package_id"] = null;
            } else {
                $this->session->set_flashdata('quotation_p', '<div class="alert alert-success">
  <strong>Success!</strong> Quotation with Package has been created.
</div>');
                echo $package;
                $record["package_id"] = $package;
            }

            $record["full_payment"] = null;
            $record["created_by"] = $this->session->userdata("user_id");

            $this->db->insert("quotation", $record);

            redirect(base_url("campaign/quotation"));
        }
    }

    public function editquotation($id = null) {

        $name = $this->input->post("campaign");
        $qno = $this->input->post("qno");
        $qdate = $this->input->post("qdate");
        $advertiser = $this->input->post("advertiser");
        $agency = $this->input->post("agency");
        $brand = $this->input->post("brand");
        $payment = $this->input->post("payment");
        $busscat = $this->input->post("busscat");
        $package = $this->input->post("package");
        $q_payment = $this->input->post("q_payment");
        $idhidden = $this->input->post("idhidden");
        $termpayment = $this->input->post("termpayment");

        $datepick = explode("/", $qdate);
        $dateformat = $datepick[2] . "-" . $datepick[1] . "-" . $datepick[0];

//$this->form_validation->set_rules('qno', 'Quotation Number', 'trim|required|min_length[5]|callback_check_number['.$id.']');
        $this->form_validation->set_rules('qdate', 'Quotation Date', 'trim|required');
        $this->form_validation->set_rules('advertiser', 'Advertiser', 'callback_check_value');
        $this->form_validation->set_rules('brand', 'Brand', 'callback_check_value');
        $this->form_validation->set_rules('agency', 'Agency', 'callback_check_value');
        $this->form_validation->set_rules('payment', 'Payment Type', 'callback_check_value');
        if($package > 0){
            $this->form_validation->set_rules('termpayment', 'Term Payment', 'callback_check_value');
        }

        $this->form_validation->set_message('check_value', 'You need to select %s something other than the default');

        $this->db->select("count(*) as jumlah_item");
        $dataitem = $this->db->get_where("quotation_item", array("quotation_id" => $id))->row();

        $dataselect = $this->db->get_where("quotation", array("quotation_id" => $id))->row();


        $date = explode("-", $dataselect->quotation_date);
        $formatform = $date[2] . "/" . $date[1] . "/" . $date[0];

        if ($this->form_validation->run() == FALSE) {

            $data["title"] = "Update Quotation";
            $data["client"] = $this->db->get("client")->result();
            $data["payment"] = $this->db->get("ref_payment")->result();
            $data["package"] = $this->db->get("package")->result();
            $data["full_payment"] = $this->db->get("ref_quotation_payment")->result();
            $data["mod"] = "editquotation";
            $data["qno"] = $dataselect->quotation_number;
            $data["campaign_name"] = $dataselect->campaign_name;
            $data["qdate"] = $formatform;
            $data["advertiserid"] = $dataselect->advertiser_id;
            $data["cancel_reason"] = $dataselect->cancel_reason;
            $data["status_quotation"] = $dataselect->status;
            $data["jumlah_item"] = $dataitem->jumlah_item;

            $this->db->select("category_name");
            $this->db->join("ref_business_category", "ref_business_category.business_category_id = client.business_category_id");
            $this->db->where("client_id", $dataselect->advertiser_id);
            $getclient = $this->db->get("client")->row();

            $data["busscat"] = $getclient->category_name;
            $data["brands"] = $dataselect->brand_id;
            $data["campaign"] = $dataselect->campaign_name;
            $data["agency"] = $dataselect->agency_id;
            $data["paymentid"] = $dataselect->payment_type;
            $data["packageselect"] = $dataselect->package_id;
            $data["fpaymentselect"] = $dataselect->full_payment;
            $data["termpayment"] = $dataselect->termpayment;
            $data["id"] = $dataselect->quotation_id;

            $this->db->select("ref_brand.brand_id, ref_brand.brand_name");
            $this->db->where("client_brand_rel.client_id", $dataselect->advertiser_id);
            $this->db->join("client_brand_rel", "client_brand_rel.brand_id = ref_brand.brand_id");
            $data["brandid"] = $this->db->get("ref_brand")->result();

            $this->template->load('default', 'campaign/formquotation', $data);
        } else {

//$record["quotation_number"] = $qno;
            $record["quotation_date"] = $dateformat;
            $record["agency_id"] = $agency;
            $record["advertiser_id"] = $advertiser;
            $record["brand_id"] = $brand;
            $record["campaign_name"] = $name;
            $record["payment_type"] = $payment;
            $record["total"] = 0;
            $record["tax"] = 0;
            $record["grand_total"] = 0;
            $record["package_id"] = $package;
            $record["termpayment"] = $termpayment;
            $record["full_payment"] = $q_payment;
            $record["modified_by"] = $this->session->userdata("user_id");

            $where["quotation_id"] = $idhidden;

            $this->db->update("quotation", $record, $where);

            redirect(base_url("campaign/quotation"));
        }
    }

    function check_number($post_string, $idcallback) {

        $this->db->where("quotation_number", $post_string);
        if ($idcallback > 0) {
            $this->db->where("quotation_id != ", $idcallback);
        }
        $namecheck = $this->db->get("quotation")->row();

        return $namecheck->quotation_id > 0 ? FALSE : TRUE;
    }

    function check_value($string_val) {

        return ($string_val > 0) ? TRUE : FALSE;
    }

    public function additemquotation($idquo = null) {

        $idquotation = $this->input->post("idquotation");
        $ppid = $this->input->post("ppid");
        $price = $this->input->post("price");
        $discount = $this->input->post("discount");
        $amount = $this->input->post("amount");
        $target = $this->input->post("target");
        $unit = $this->input->post("unit");
        $province = $this->input->post("province");
        $city = $this->input->post("city");
        $area = $this->input->post("area");
        $startdate = $this->input->post("startdate");
        $enddate = $this->input->post("enddate");

        $datepick_s = explode("/", $startdate);
        $dateformat_start = $datepick_s[2] . "-" . $datepick_s[1] . "-" . $datepick_s[0];

        $datepick_e = explode("/", $enddate);
        $dateformat_end = $datepick_e[2] . "-" . $datepick_e[1] . "-" . $datepick_e[0];

        $this->form_validation->set_rules('ppid', 'Product Partner', 'callback_check_value');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|integer');
        $this->form_validation->set_rules('discount', 'Discount', 'trim|required|integer');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required|integer');
        $this->form_validation->set_rules('target', 'Target', 'trim|required');
        $this->form_validation->set_rules('unit', 'Unit', 'trim|required');
        $this->form_validation->set_rules('province', 'Province', 'callback_check_value');
        $this->form_validation->set_rules('city', 'City', 'callback_check_value');
        $this->form_validation->set_rules('area', 'Area', 'trim|required');
        $this->form_validation->set_rules('startdate', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('enddate', 'End Date', 'trim|required');
        $validation = array(
            array('field' => 'startdate', 'label' => 'Start Date', 'rules' => 'required'),
            array('field' => 'enddate', 'label' => 'End Date', 'rules' => 'required|callback_compareDate'),
        );

        $this->form_validation->set_rules($validation);

        $this->form_validation->set_message('check_value', 'You need to select %s something other than the default');

        $this->db->where("quotation_id", $idquo);
        $curr = $this->db->get("quotation")->row();

        if ($this->form_validation->run() == FALSE) {

            $data["title"] = "Add Item Quotation - " . $curr->campaign_name . " (No : " . $curr->quotation_number . ")";
            $data["mod"] = "additemquotation";
            $data["id"] = 0;
            $data["idquotation"] = $idquo;
            $data["ppid"] = $ppid;
            $data["price"] = $price;
            $data["discount"] = $discount;
            $data["amount"] = $amount;
            $data["target"] = $target;
            $data["unit"] = $unit;
            $data["selectprovince"] = $province;
            $data["selectcity"] = $city;
            $data["area"] = $area;
            $data["startdate"] = $startdate;
            $data["enddate"] = $enddate;

            $this->db->select("pp_id, product2.product_id, product2.product_name as name, product2.product_code as code, partner.name as partner");
            $this->db->join("ref_doc_status", "ref_doc_status.status = product_partner_rel.doc_status");
            $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
            $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");
            $this->db->order_by("pp_id","DESC");
            //$this->db->where("product_partner_rel.doc_status", 2); <-- di unset 15 juni 2016
            //$this->db->where("product_partner_rel.status", 1); <-- di unset 15 juni 2016
            $data["pp_rel"] = $this->db->get("product_partner_rel")->result();

            $this->db->order_by("id", "asc");
            $data["province"] = $this->db->get("ref_province")->result();

            $data["city"] = $this->db->get("ref_city")->result();
            $data["tokenstart"] = $this->security->get_csrf_hash();

            $this->db->select("item_id, product2.product_name as name, quotation_item.price, quotation_item.discount, quotation_item.amount, "
                    . " quotation_item.target, quotation_item.unit,  ref_province.province, ref_city.city, quotation_item.area, "
                    . " quotation_item.start_date, quotation_item.end_date");
            $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
            $this->db->join("product2", "product_partner_rel.product_id = product2.product_id");
            $this->db->join("ref_province", "ref_province.id = quotation_item.province");
            $this->db->join("ref_city", "ref_city.id = quotation_item.city");
            $this->db->where("quotation_id", $idquo);
            $data["listitem"] = $this->db->get("quotation_item")->result();
//die($this->db->last_query());
            $this->template->load('default', 'campaign/formitemquotation', $data);
        } else {

            $record["quotation_id"] = $idquotation;
            $record["pp_id"] = $ppid;
            $record["province"] = $province;
            $record["city"] = $city;
            $record["area"] = $area;
            $record["start_date"] = $dateformat_start;
            $record["end_date"] = $dateformat_end;
            $record["price"] = $price;
            $record["amount"] = $amount;
            $record["discount"] = $discount;
            $record["target"] = $target;
            $record["unit"] = $unit;
            
            $totalperitem = ($price*$amount)*(100-$discount)/100;
            $record["total_peritem"] = $totalperitem;

            $this->db->insert("quotation_item", $record);

            $this->updatetotal($idquotation);
            $this->session->set_flashdata('sukses_add_item', '<div class="alert alert-success">
  <strong>Sukses!</strong> quotation item has been added.
</div>');
            redirect(base_url("campaign/additemquotation/" . $idquotation));
        }
    }

    function updatetotal($id) {
        $this->db->select("sum(price) as total");
        $datatotal = $this->db->get_where("quotation_item", array("quotation_id" => $id))->row();

        if ($datatotal->total > 0) {
            $currenttotal = $datatotal->total;
            $updatetotal = $currenttotal;
            $grandtotal = $updatetotal + ((10 / 100) * $updatetotal);

            $data["total"] = $updatetotal;
            $data["grand_total"] = $grandtotal;
            $where["quotation_id"] = $id;
            $this->db->update("quotation", $data, $where);
            return true;
        } else {
            $data["total"] = 0;
            $data["grand_total"] = 0;
            $where["quotation_id"] = $id;
            $this->db->update("quotation", $data, $where);
            return true;
        }
    }

    public function exportpdf() {


        define('FPDF_FONTPATH', $this->config->item('fonts_path'));

        $id = $this->input->get_post("quotation_id");

        $this->db->select("a.quotation_id as quotation_id, "
                . "a.quotation_number as quotation_number, "
                . "a.quotation_date as quotation_date, "
                //. "b.client_name as agency_name, "
                //. "(select city from ref_city where ref_city.id = b.address_city) as city_agency, "
                //. "b.address_zip as agency_zip, "
                //. "b.phone as agency_phone, "
                //. "b.finance_name as agency_finance_name, "
                //. "b.finance_phone as agency_finance_phone, "
                //. "b.finance_email as agency_finance_email, "
                //. "b.media_name as agency_media_name, "
                //. "b.media_phone as agency_media_phone, "
                //. "b.media_email as agency_media_email, "
                //. "b.npwp as agency_npwp, "
                //. "b.address_street as address_street, "
				
				. "c.client_name as agency_name, "
                . "(select city from ref_city where ref_city.id = c.address_city) as city_agency, "
				. "(select province from ref_province where ref_province.id = c.address_province) as province_agency, "
                . "c.address_zip as agency_zip, "
                . "c.phone as agency_phone, "
                . "c.finance_name as agency_finance_name, "
                . "c.finance_phone as agency_finance_phone, "
                . "c.finance_email as agency_finance_email, "
                . "c.media_name as agency_media_name, "
                . "c.media_phone as agency_media_phone, "
                . "c.media_email as agency_media_email, "
                . "c.npwp as agency_npwp, "
                . "c.address_street as address_street, "
				
                . "c.client_name as advertiser_name, "
                . "(select category_name from ref_business_category where ref_business_category.business_category_id = c.business_category_id) as adv_category, "
                . "d.brand_name as brand_name, "
                . "a.campaign_name as campaign_name, "
                . "e.name as payment_type, "
                . "a.total as total,"
                . "a.package_id,"
                . "f.name as status,"
                . "(select fullname from users where users.user_id = a.created_by) as sales_name,"
                . "(select username from users where users.user_id = a.created_by) as sales_email,"
                . "(select mobile_phone from users where users.user_id = a.created_by) as sales_phone");
        //$this->db->join("client b", "a.agency_id = b.client_id");
        $this->db->join("client c", "a.advertiser_id = c.client_id");
        $this->db->join("ref_brand d", "a.brand_id = d.brand_id");
        $this->db->join("ref_payment e", "a.payment_type = e.payment_id");
        $this->db->join("ref_quotation_status f", "a.status = f.id");
        $this->db->where("a.quotation_id", $id);
        $data["quotation"] = $this->db->get("quotation a")->row();

        if ($data["quotation"]->package_id > 0) {

            $this->db->select("package_name as product, product_tariff_idr_total as price, 0 as diff, 1 as amount, 0 as diskon");
            $this->db->where("package_id", $data["quotation"]->package_id);
            $data["listitem"] = $this->db->get("package")->result();
        } else {

            $this->db->select("item_id,  product2.product_name as product, "
                    //. "product_category.name as type, "
                    . "product2.product_family as type, "
                    . "quotation_item.price, "
                    . "quotation_item.discount, "
                    . "quotation_item.amount, "
                    . " quotation_item.target, "
                    . "quotation_item.unit,  "
                    . "ref_province.province, "
                    . "ref_city.city, "
                    . "quotation_item.area, "
                    . " quotation_item.start_date, "
                    . "quotation_item.end_date, "
                    . "quotation_item.price,"
                    . " quotation_item.amount,"
                    . " quotation_item.discount as diskon");
            $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
            $this->db->join("product2", "product_partner_rel.product_id = product2.product_id");
            //$this->db->join("product_category", "product_category.id = product.category_id");
            $this->db->join("ref_province", "ref_province.id = quotation_item.province");
            $this->db->join("ref_city", "ref_city.id = quotation_item.city");
            $this->db->where("quotation_id", $id);
            $data["listitem"] = $this->db->get("quotation_item")->result();
        }

        $this->load->view('template/pdf/quotation', $data);
    }

    public function generateinvoice() {


        define('FPDF_FONTPATH', $this->config->item('fonts_path'));

        $data = array();
        $data["terbilang"] = $this->terbilang(1100000); //$this->terbilang("1100000");
        $this->load->view('template/pdf/invoice', $data);
    }

    function kekata($x = 1100000) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    function terbilang($x, $style = 3) {
        if ($x < 0) {
            $hasil = "minus " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil . " Rupiah";
    }

    function compareDate() {

        $startdate = $this->input->post("startdate");
        $enddate = $this->input->post("enddate");

        $datepick_s = explode("/", $startdate);
        $dateformat_start = $datepick_s[2] . "-" . $datepick_s[1] . "-" . $datepick_s[0];

        $datepick_e = explode("/", $enddate);
        $dateformat_end = $datepick_e[2] . "-" . $datepick_e[1] . "-" . $datepick_e[0];

        $startDate = strtotime($dateformat_start);
        $endDate = strtotime($dateformat_end);

        if ($endDate >= $startDate)
            return True;
        else {
            $this->form_validation->set_message('compareDate', '%s should be greater than Start Date.');
            return False;
        }
    }

    public function ajax($tag = null) {
        if ($tag == "quotation") {
            $id = $this->input->post("idclient");

            $this->db->select("category_name");
            $this->db->join("ref_business_category", "ref_business_category.business_category_id = client.business_category_id");
            $this->db->where("client_id", $id);
            $getclient = $this->db->get("client")->row();

            $this->db->select("ref_brand.brand_id, ref_brand.brand_name");
            $this->db->where("client_brand_rel.client_id", $id);
            $this->db->join("client_brand_rel", "client_brand_rel.brand_id = ref_brand.brand_id");
            $getdata = $this->db->get("ref_brand")->result();

            $datatables = "";
            $datatables .= "<option value='0'>-- Select Brand --</option>";
            foreach ($getdata as $brands):
                $datatables .="<option value='" . $brands->brand_id . "'>" . $brands->brand_name . "</option>";
            endforeach;
            $ret = '<script>';
            $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
            if ($datatables != "") {
                $ret.= '$("#busscat").attr("value","' . $getclient->category_name . '");';
                $ret.= '$("#brand").attr("disabled",false);';
                $ret.= '$("#brand").html("' . $datatables . '");';
            } else {
                $ret.= '$("#brand").attr("disabled",true);';
            }
            $ret .= '</script>';
            echo $ret;
            exit();
        } elseif ($tag == "ppid") {

            $ppid = $this->input->post("ppid");
            $this->db->select("product_tariff_idr_total");
            $this->db->where("product_partner_rel.pp_id = '" . $ppid . "'");
            $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
            $selectpid = $this->db->get("product_partner_rel")->row();

            $ret = '<script>';
            $ret .= '$("#xyztoken").attr("value","' . $this->security->get_csrf_hash() . '");';
            if ($selectpid != "") {
                //$ret.= '$("#sellingprice").attr("value","' . $selectpid->selling_price . '");';
                $ret.= '$("#price").attr("value","' . (int)$selectpid->product_tariff_idr_total . '");';
                //$ret.= '$("#discount").attr("value","' . $selectpid->discount . '");';
                $ret.= '$("#csrf_city").attr("value","' . $this->security->get_csrf_hash() . '");';
            }
            $ret .= '</script>';
            echo $ret;
            exit();
        } elseif ($tag == "removeitem") {

            $id = $this->input->post("item_id");
            $quotid = $this->input->post("quotid");
            $this->db->delete("quotation_item", array("item_id" => $id));
            $this->updatetotal($quotid);
            $res["msg"] = "OK";
            $res["quotid"] = $quotid;
            echo json_encode($res);
            exit();
        } elseif ($tag == "modal") {

            $quotationid = $this->input->post("quotation_id");
            
            $this->db->select("count(item_id) as jml");
            $this->db->where("quotation_id", $quotationid);
            $cekitem = $this->db->get("quotation_item")->row();
            
            $currentst = $this->db->get_where("quotation", array('quotation_id' => $quotationid))->row();
            
            if($cekitem->jml > 0){
                $data["status"] = $currentst->status;
                $data["newtoken"] = $this->security->get_csrf_hash();
                $data["qid"] = $currentst->quotation_id;
                $data["pid"] = $currentst->package_id;
                $data["item"] = $cekitem->jml;
                echo json_encode($data);
                exit();
            }else{
                $data["status"] = $currentst->status;
                $data["newtoken"] = $this->security->get_csrf_hash();
                $data["qid"] = $quotationid;
                $data["pid"] = $currentst->package_id;
                $data["item"] = 0;
                echo json_encode($data);
                exit();
            }

                
        } elseif ($tag == "saveio") {

            $refnum = $this->input->post("refnumber");
            $ioid = $this->input->post("ioid");

            $data["reference_id"] = $refnum;
            $where["io_id"] = $ioid;

            $this->db->update("io", $data, $where);

            echo "OK";
            exit();
        } elseif ($tag == "copyquoation") {
            $quotation_id = $this->input->post("quotation_id");

            $querycopy = $this->db->query("select copy_quotation(" . $quotation_id . ")")->row();

            $xpl = explode(";", $querycopy->copy_quotation);

            $idlast = end($xpl);

            echo $idlast;

            exit();
        } else if($tag == "gettoken"){
            $data["newtoken"] = $this->security->get_csrf_hash();
            echo json_encode($data);
            exit();
        }
    }

    function exportio($idio) {
// header data
        $this->db->select("io.io_number,
							  to_char(io.created_on,'dd-Mon-yyyy') io_date,
							  quotation.campaign_name,
							  advertiser.client_name advertiser,
							  agency.client_name agency");

        $this->db->join("quotation", "io.quotation_id = quotation.quotation_id", "left outer");
        $this->db->join("client as advertiser", "quotation.advertiser_id = advertiser.client_id", "left outer");
        $this->db->join("client as agency", "quotation.agency_id = agency.client_id", "left outer");
        $this->db->where("io.io_id", $idio);
        $data['header'] = $this->db->get("io")->result();

// item data
        $this->db->join("quotation_item", "io.quotation_id = quotation.quotation_id", "left outer");
        $this->db->join("ref_province as province", "quotation_item.province = province.id", "left outer");
        $this->db->join("ref_city as city", "quotation_item.city = city.id", "left outer");
        $this->db->where("io.io_id", $idio);
        $data['item'] = $this->db->get("io")->result();

        var_dump($data);
        die;
    }

    /*
      function exportio($idio){

      define('FPDF_FONTPATH',$this->config->item('fonts_path'));

      $idio = $this->input->get_post("io");

      $this->db->select("item_id,  "
      . "product.name as product, "
      . "product_category.name as type, "
      . "quotation_item.price, "
      . "quotation_item.discount, "
      . "quotation_item.amount, "
      . "quotation_item.target, "
      . "quotation_item.unit,  "
      . "ref_province.province, "
      . "ref_city.city, "
      . "quotation_item.area, "
      . "quotation_item.start_date, "
      . "quotation_item.end_date, "
      . "quotation_item.price,"
      . "quotation_item.amount,"
      . "quotation_item.discount as diskon");
      $this->db->join("product_partner_rel","product_partner_rel.pp_id = quotation_item.pp_id");
      $this->db->join("product","product_partner_rel.product_id = product.product_id");
      $this->db->join("product_category","product_category.id = product.category_id");
      $this->db->join("ref_province","ref_province.id = quotation_item.province");
      $this->db->join("ref_city","ref_city.id = quotation_item.city");
      $this->db->where("item_id", $idio);

      $data["listitem"] = $this->db->get("quotation_item")->result();

      $this->db->where("quotation_id", $data["listitem"]->quotation_id);
      $data["itemio"] = $this->db->get("io")->row();

      $this->load->view('template/pdf/io', $data);

      } */

    function setstatus() {

        $idquotationhidden = $this->input->post("idquotationhidden");
        $status = $this->input->post("status_id");
        if ($status != 3) {
            $data["status"] = $status;
            $where["quotation_id"] = $idquotationhidden;

            $this->db->update("quotation", $data, $where);

            redirect(base_url("campaign/quotation"));
        } else {

            $data["quotation_id"] = $idquotationhidden;

            $this->template->load('default', 'campaign/cancelation', $data);
        }
    }

    function cancelprocess() {

        $id = $this->input->post("idhidden");
        $reason = $this->input->post("reason");

        $update["cancel_reason"] = $reason;
        $update["status"] = 3;
        $where["quotation_id"] = $id;

        $this->db->update("quotation", $update, $where);

        redirect(base_url("campaign/quotation"));
    }

    function uploadquotation($idquotation = null) {

        $config['upload_path'] = './assets/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 2000;
        $path = "assets/";

        $this->db->select("campaign_name, quotation_number, signed_quotation_file");
        $this->db->where("quotation_id", $idquotation);
        $dataquotation = $this->db->get("quotation")->row();

        $filename = str_replace(" ", "_", $dataquotation->campaign_name);

        $config["file_name"] = $filename;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $data["quotation_id"] = $idquotation;
            $data["filename"] = $dataquotation->signed_quotation_file;
            $data["download_label"] = "<a href='" . base_url($path . $dataquotation->signed_quotation_file) . "' target='_blank' id='files'>" . $dataquotation->signed_quotation_file . "</a>";
            $data["download_label"] .= "&nbsp;&nbsp;<a href='#' onClick='remfile(" . $idquotation . ");' id='remfile' title='Delete File And Re-Upload'><i class='entypo-erase'></i></a>";
            $data["label"] = $dataquotation->campaign_name . " (" . $dataquotation->quotation_number . ")";
            if ($this->input->post()) {
                $failed = array('error' => $this->upload->display_errors());
                $data["msg"] = $failed["error"];
            }
            $this->template->load('default', 'campaign/uploaddata', $data);
        } else {
            $idhidden = $this->input->post("idhidden");
            $this->db->select("count(io_id) as count");
            $dataio = $this->db->get_where("io", array("quotation_id" => $idhidden))->row();

            if ($dataio->count == 0) {
//insert "IO" per item

                $this->db->where('quotation_id', $idhidden);
                $this->db->select("view_item.item_id, "
                        . "view_item.amount,"
                        . "view_item.price, "
                        . "view_item.discount, "
                        . "view_item.fixed_price, "
                        . "view_item.rev_share, "
                        . "view_item.pp_id,"
                        . "view_item.partner_type, "
                        . "view_item.total_peritem, "
                        . "view_item.pshare,"
                        . "view_item.partner_id,"                        
                        . "product2.product_id, "
                        . "product2.product_tariff_idr_total, "
                        . "product2.product_tariff_idr_status, "
                        . "product2.product_tariff_usd_total,"
                        . "product2.product_tariff_usd_status,"
                        . "product2.bearer_cost, "
                        . "product2.partner_id,"
                        . "product2.cost_per_unit,"
                        . "product2.isindosat_product,"
                        . "product2.iskloc_product");
                $this->db->join("product2", "product2.product_id = view_item.product_id");
                $dataitem = $this->db->get('view_item')->result();
                
                foreach ($dataitem as $item) {

                    $rec2io["io_number"] = $this->generateRandomString();
                    
                    $rec2io["quotation_id"] = $idhidden;
                    $rec2io["campaign_item_id"] = $item->item_id;
                    
                    $REVENUE = 0; //$item->total_peritem;
                    
                    $COGS = 0;
                    
                    if($item->partner_type == 1){
                        if($item->product_tariff_idr_status == 1){
                            $PARTNER_SHARE = $item->amount*($item->pshare/100)*$item->product_tariff_idr_total;
                        }else{
                            $PARTNER_SHARE = $item->amount*($item->pshare/100)*$item->product_tariff_usd_total;
                        }
                            
                    }else if($item->partner_type == 2){
                        $PARTNER_SHARE = $item->amount*$item->price;
                    }
                    
                    $BEARER_COST = $item->amout*$item->cost_per_unit;
                    
                    //if($item->isindosat_product == 1){
                    //    $REVSHISAT = (30/100) * ($REVENUE - $PARTNER_SHARE - $BEARER_COST);
                    //}
                    
                    //if($item->iskloc_product == 1){
                    //    $REVSHKLOC = (5/100) * ($REVENUE - $PARTNER_SHARE - $BEARER_COST);
                    //}
                    
                    //$REVSHTOTAL = $REVSHISAT + $REVSHKLOC;
                    
                    //$COGS = $PARTNER_SHARE + $BEARER_COST + $REVSHTOTAL;
                    
                    $rec2io["cogs"] = 0; //$COGS;
                    $rec2io["total"] = 0; //$REVENUE;        
                    $rec2io["bearer_cost"] = $BEARER_COST;        
                    $rec2io["bearer_costdb"] = $item->cost_per_item;                            
                    $rec2io["partner_id_bearercost"] = $item->partner_id;
                    $rec2io["partner_share"] = $PARTNER_SHARE;
                    $rec2io["partner_share_item"] = $this->pshare;
                    $rec2io["rev_share_isat"] = 0; //$REVSHISAT;
                    $rec2io["rev_share_kloc"] = 0; //$REVSHKLOC;
                    
                    $rec2io["product_id"] = $item->product_id;
                    $rec2io["reference_id"] = 0;
                    $rec2io["tax"] = 10;
                    $rec2io["status"] = 1;
                    $rec2io["grand_total"] = 0;
                    
                    $rec2io["created_by"] = $this->session->userdata("user_id");

                    $this->db->insert("io", $rec2io);
                   
                   
                }
            }
//end off insert IO perItem

            $dataupload = array('upload_data' => $this->upload->data());

            $record["signed_quotation_file"] = $dataupload["upload_data"]["file_name"];
            $where["quotation_id"] = $idhidden;
            $this->db->update("quotation", $record, $where);

            $data["download_label"] = "<a href='" . base_url($path . $dataupload["upload_data"]["file_name"]) . "' target='_blank' id='files'>" . $dataupload["upload_data"]["file_name"] . "</a>";
            $data["download_label"] .= "&nbsp;&nbsp;<a href='#' onClick='remfile(" . $idhidden . ");' id='remfile' title='Delete File And Re-Upload'><i class='entypo-erase'></i></a>";
            $data["filename"] = $dataupload["upload_data"]["file_name"];

            $this->template->load('default', 'campaign/uploaddata', $data);
        }
    }

    function viewio($quotation_id = null) {

        $this->db->select("a.io_id, c.product_name as product_name,d.name as name, b.amount as amount, "
                . " b.partner_type as type, a.reference_id,"
                . "(case when b.partner_type = 2 then b.fixed_price  else c.product_tariff_idr_total end) as priced,"
                . "(case when b.partner_type = 2 then '0'  else (100 - b.pshare)  end) as disc,"
                . "b.start_date, b.end_date");
        $this->db->join("view_item b", "b.item_id = a.campaign_item_id");        
        $this->db->join("product2 c", "b.product_id = c.product_id");
        $this->db->join("partner d", "b.partner_id = d.partner_id");
            
        $this->db->where("a.quotation_id", $quotation_id);
        $this->db->order_by("a.io_id", "DESC");      
        $data["listio"] = $this->db->get("io a")->result();
        
        $data["quotation_id"] = $quotation_id;

        $this->template->load('default', 'campaign/viewio', $data);
    }
    
    
    function saveallitem(){
        
        $ioid = $this->input->post("ref_number");
        $qid = $this->input->post("quotation_id");
        
        foreach ($ioid as $key => $value) {
           
            $data["reference_id"] = $value;
            $where["io_id"] = $key;

            $this->db->update("io", $data, $where);
            
        }
        
        redirect(base_url("campaign/viewio/".$qid));
        
    }

    function sendIoToEmail($id) {

        $config['email_server'] = 'smtp.mandrillapp.com';
        $config['email_port'] = 587;
        $config['email_protocol'] = 'ssl';
        $config['email_username'] = 'andyajadeh@gmail.com';
        $config['email_password'] = 'oG6fgw6jJFmbM341Vf-Sdg';
        $config['email_from'] = 'no-reply@imx-erp.co.id';
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function removeupload() {

        $idquotation = $this->input->post("idquotation");
        $qdata = $this->db->get_where("quotation", array("quotation_id" => $idquotation))->row();
        unlink(pdf_path . $qdata->signed_quotation_file);        //hapus existing file
        $record["signed_quotation_file"] = null;
        $this->db->update("quotation", $record, array("quotation_id" => $idquotation));
        echo $qdata->quotation_id;
        exit();
    }
    
    
    
    public function termpayment($qid = null){
        
        
        $start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $amount = $this->input->post("amount");
        $idtp = $this->input->post("idtp");
        $data["title"] = "Term Payment Quotation";
        
        $this->form_validation->set_rules('start_date[]', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date[]', 'End Date', 'trim|required');
        $this->form_validation->set_rules('amount[]', 'Amount', 'trim|required');
        
        $this->db->where("quotation_id", $qid);        
        $quotationtp = $this->db->get("quotation")->row();
        
        if ($this->form_validation->run() == FALSE) {
        
            $this->db->where("quotation_id", $qid);        
            $quotationtp = $this->db->get("quotation")->row();
            $data["qid"] = $qid;
            
            $data["total"] = $quotationtp->total;
            $data["jmltp"] = $quotationtp->termpayment;
            
            $this->db->select("sum(payment_amount) as sumjml");
            $this->db->where("quotation_id", $qid);
            $this->db->group_by("quotation_id");
            $sumtotal = $this->db->get("quotation_payment_period")->row();
            
            $data["selisih"] = $quotationtp->total - $sumtotal->sumjml;
            
            $this->db->where("quotation_id", $qid);
            $datapayment = $this->db->get("quotation_payment_period")->result();
            if(sizeof($datapayment) > 0){
                for($x=0;$x<sizeof($datapayment);$x++){
                    $data["idtp"][$x] = $datapayment[$x]->id;
                    $data["start_date"][$x] = $datapayment[$x]->start_date;
                    $data["end_date"][$x] = $datapayment[$x]->end_date;
                    $data["payment_amount"][$x] = $datapayment[$x]->payment_amount;
                }
            }
//die(var_dump($data["start_date"][0]));
            $this->template->load('default', 'campaign/termpayment', $data);
            
        }else{
            
//            echo var_dump($start_date)."<br/>";
//            echo var_dump($end_date)."<br/>";
//            echo var_dump($amount)."<br/>";
//            
            
            
            for($x=1;$x<=$quotationtp->termpayment;$x++){  
                $record["quotation_id"] = $qid;
                $record["start_date"] = $this->reformatdate($start_date[$x]);
                $record["end_date"] = $this->reformatdate($end_date[$x]);
                $record["payment_amount"] = $amount[$x];
                $record["date_created"] = date("Y-m-d");
                $record["payment_status"] = 0;
                if($idtp[$x] == ""){
                    $this->db->insert("quotation_payment_period", $record);
                }else{
                    $where["id"] = $idtp[$x]; 
                    $this->db->update("quotation_payment_period", $record, $where);
                }
                unset($record);
            }
            
            
            redirect(base_url("campaign/quotation"));
            
        }
        
    }
    
    public function reformatdate($date = null){
        
        $dateex = explode("-", $date);
        
        $tgl = $dateex[2]."-".$dateex[1]."-".$dateex[0];
        
        return $tgl;
        
    }

    public function synccampaign($id = null) {
        ini_set('max_execution_time', 300);
        $erp_db = $this->load->database('default', TRUE);
        $iklanstore_web_new_db = $this->load->database('iklanstore_web_new', TRUE);

        $erp_db->where("quotation_id", $id);
        $quotation = $erp_db->get("quotation")->row();

        // GET campaign_id
        $iklanstore_web_new_db->where("quotation_id", $id);
        $campaign_quotation = $iklanstore_web_new_db->get("campaign_quotation")->row();
        $campaign_id = $campaign_quotation->campaign_id;
        $ExtCampaignid = $campaign_id;
        $quotation_item_id = $campaign_quotation->quotation_item_id;

        $Quoteid = $quotation->quotation_id;
        $CampaignName = $quotation->campaign_name;
        $QuoteNo = $quotation->quotation_number;

        // SALES
        $created_by = $quotation->created_by; // ADD PROTECTION
        $erp_db->where("user_id", $created_by);
        $Sales = ucwords(strtolower($erp_db->get("users")->row()->fullname));

        // AGENCY
        $agency_id = $quotation->agency_id;
        if($agency_id != 0) {
            $erp_db->where("client_id", $agency_id);
            $Agency = $erp_db->get("client")->row()->client_name;
        } else {
            $Agency = "";
        }

        // BUDGET
        $iklanstore_web_new_db->where("campaign_id", $campaign_id);
        $campaign_report =  $iklanstore_web_new_db->get("campaign_report")->row();
        $Budget = $campaign_report->plan_amount;
        
        // ADVERTISER
        $advertiser_id = $quotation->advertiser_id;
        $erp_db->where("client_id", $advertiser_id);
        $Advertiser = $erp_db->get("client")->row()->client_name;
        $Maskingid = "";

        // QUOTATION IS THE KEY
        // $erp_db->where("quotation_id", $id);
        // $quotation_items = $erp_db->get("quotation_item")->result();

        // CAMPAIGN IS THE KEY
        $iklanstore_web_new_db->where("id", $ExtCampaignid);
        $campaign = $iklanstore_web_new_db->get("campaign")->row();
        $CampaignNo = $campaign->campaign_number;

        // PRODUCT CATEGORY
        $erp_db->where("item_id", $quotation_item_id);
        $quotation_item = $erp_db->get("quotation_item")->row();
        $pp_id = $quotation_item->pp_id;

        $erp_db->where("pp_id", $pp_id);
        $product_partner_rel = $erp_db->get("product_partner_rel")->row();
        $product_id = $product_partner_rel->product_id;

        $erp_db->where("product_id", $product_id);
        $product2 = $erp_db->get("product2")->row();
        $ProductCategory = $product2->product_family;
        
        $StartDate_C = date('m/d/Y', strtotime($campaign->start_date));
        $EndDate_C = date('m/d/Y', strtotime($campaign->end_date));

        // + CAMPAIGN PRODUCT
        // $ExtProductid = $product2->product_id; // ZUL
        $ExtProductid = $quotation_item_id; // DAVID
        $Channel = $product2->product_name;
        // $Success_CP // total Success_PM
        // $Used_CP // total Success_PM * Price_PM
        // $Plan_CP // total Plan_PM
        $Price = $quotation_item->price; // Final Price After Discount

        // ++ PRODUCT MESSAGE
        $iklanstore_web_new_db->where("campaign_id", $campaign_id);
        $campaign_report = $iklanstore_web_new_db->get("campaign_report")->row();
        $campaign_report_id = $campaign_report->id;
        $Used_C = $campaign_report->used_amount;
        $Plan_C = $campaign_report->plan_amount;

        $Success_C = 0;
        $ProductMessage = array();

        $iklanstore_web_new_db->where("campaign_report_id", $campaign_report_id);
        // $campaign_report_telco_detail = $iklanstore_web_new_db->get("campaign_report_telco_detail")->row();
        // $ExtMessageid = $campaign_report_telco_detail->id;

        // $ExtProductid = $product2->product_id; // ZUL
        // $ExtProductid = $quotation_item_id; // DAVID

        // $Description = $campaign_report_telco_detail->message;
        // $StartDate_PM = $campaign_report_telco_detail->start_date;
        // $EndDate_PM = $campaign_report_telco_detail->end_date;
        // $Plan_PM = $campaign_report_telco_detail->plan;
        // $Success_PM = $campaign_report_telco_detail->success;
        // $Location = $campaign_report_telco_detail->profiling;
        // $Clicks_PM = "";
        // $CTR_PM = "";

        // $ProductMessage = array(
        //         array(
        //             'ExtMessageid' => $ExtMessageid,
        //             'ExtProductid' => $ExtProductid,
        //             'Description' => $Description,
        //             'StartDate' => $StartDate_PM,
        //             'EndDate' => $EndDate_PM,
        //             'Plan' => $Plan_PM,
        //             'Success' => $Success_PM,
        //             'Location' => $Location,
        //             'Clicks' => $Clicks_PM,
        //             'CTR' => $CTR_PM
        //             )
        //     );

        // ++ PRODUCT IMPRESSION
        // $ExtImpressionid
        // $ExtProductid
        // $ImpressionDate
        // $Impressions
        // $Clicks_PI
        // $Spent_PI
        // $CPM_PI
        // $CTR_PI

        // $iklanstore_web_new = $this->db->db_select('iklanstore_web_new');

        // echo '<pre>';
        $campaign_report_telco_details_q = $iklanstore_web_new_db->get("campaign_report_telco_detail");
        $total_campaign_report_telco_details = $campaign_report_telco_details_q->num_rows();
        if($total_campaign_report_telco_details > 0) {
            $campaign_report_telco_details = $campaign_report_telco_details_q->result();
            foreach($campaign_report_telco_details as $campaign_report_telco_detail) {
                $ExtMessageid = $campaign_report_telco_detail->id;

            //     // $ExtProductid = $product2->product_id; // ZUL
            //     // $ExtProductid = $quotation_item_id; // DAVID

                $Description = $campaign_report_telco_detail->message;
                $StartDate_PM = date('m/d/Y', strtotime($campaign_report_telco_detail->start_date));
                $EndDate_PM = date('m/d/Y', strtotime($campaign_report_telco_detail->end_date));
                $Plan_PM = $campaign_report_telco_detail->plan;
                $Success_PM = $campaign_report_telco_detail->success;
                $Location = $campaign_report_telco_detail->profiling;
                $Clicks_PM = "";
                $CTR_PM = "";

                $ProductMessageDetail = array(
                        'ExtMessageid' => $ExtMessageid,
                        'ExtProductid' => $ExtProductid,
                        'Description' => $Description,
                        'StartDate' => $StartDate_PM,
                        'EndDate' => $EndDate_PM,
                        'Plan' => $Plan_PM,
                        'Success' => $Success_PM,
                        'Location' => $Location,
                        'Clicks' => $Clicks_PM,
                        'CTR' => $CTR_PM
                        );

                // print_r($ProductMessageDetail);
                // echo '<br />';
                $Success_C = $Success_C + $Success_PM;
                array_push($ProductMessage, $ProductMessageDetail);
            }
            // echo '</pre>';
        }

        $ProductImpression = array(
                // array()
            );

        $CampaignProduct = array(
                array(
                    'ExtProductid' => $ExtProductid,
                    'Channel' => $Channel,
                    'Success' => $Success_C,
                    'Used' => $Used_C,
                    'Plan' => $Plan_C,
                    'Price' => $Price,
                    'ProductMessage' => $ProductMessage,
                    'ProductImpression' => $ProductImpression
                    )
            );

        $CampaignReport = array(
                'ExtCampaignid' => $ExtCampaignid,
                'CampaignNo' => $CampaignNo,
                'CampaignName' => $CampaignName,
                'QuoteNo' => $QuoteNo,
                'Sales' => $Sales,
                'Agency' => $Agency,
                'Budget' => $Budget,
                'Advertiser' => $Advertiser,
                'ProductCategory' => $ProductCategory,
                'Maskingid' => $Maskingid,
                'StartDate' => $StartDate_C,
                'EndDate' => $EndDate_C,
                'CampaignProduct' => $CampaignProduct
            );
        
        // $this->db->close();
        // $erp_db->close();
        // $iklanstore_web_new_db->close();
        $CampaignReportPost = json_encode($CampaignReport);
        // echo '<pre>';
        // var_dump($CampaignReport); die();
        // var_dump($CampaignReportPost); die();
        // echo '<pre />';

        // Request SalesForce Token
        $token_response = $this->get_salesforce_token_response();
        $token_response_decoded = json_decode($token_response);
        // var_dump($token_response_decoded); die();
        $access_token = $token_response_decoded->access_token;

        $authorization = 'Bearer ' . $access_token;

        // echo '<pre>';
        // var_dump($quotation); die();
        // echo '=== QUOTATION ===';
        // echo '<br />';
        // echo 'Quoteid: ' . $Quoteid;
        // echo '<br />';
        // echo 'CampaignName: ' . $CampaignName;
        // echo '<br />';
        // echo 'QuoteNo: ' . $QuoteNo;
        // echo '<br />';
        // echo 'created_by: ' . $created_by;
        // echo '<br />';
        // echo 'agency_id: ' . $agency_id;
        // echo '<br />';
        // echo 'advertiser_id: ' . $advertiser_id;
        // echo '<br />';
        // echo 'Sales: ' . $Sales;
        // echo '<br/ >';
        // echo 'Agency: ' . $Agency;
        // echo '<br />';
        // echo '<br />';
        // echo '=== CAMPAIGN ===';
        // echo '<br />';
        // echo 'ExtCampaignid: ' . $ExtCampaignid;
        // echo '<br />';
        // echo 'Budget: ' . $Budget;
        // echo '<br />';
        // echo 'Advertiser: ' . $Advertiser;
        // echo '<br />';
        // echo 'ProductCategory: ' . $ProductCategory;
        // echo '<br />';
        // echo 'Maskingid: ' . $Maskingid;
        // echo '<br />';
        // echo 'StartDate: ' . $StartDate_C;
        // echo '<br />';
        // echo 'EndDate: ' . $EndDate_C;
        // echo '<br />';
        // echo '<br />';
        // echo '==== CAMPAIGN PRODUCT ===';
        // echo '<br />';
        // echo 'ExtProductid: ' . $ExtProductid;
        // echo '<br />';
        // echo 'Channel: ' . $Channel;
        // echo '<br />';
        // echo 'Success: ' . $Channel;
        // echo '<br />';
        // echo 'Used: ' . $Channel;
        // echo '<br />';
        // echo 'Plan: ' . $Channel;
        // echo '<br />';
        // echo 'Price: ' . $Price;
        // echo '<br />';
        // echo '<br />';
        // echo '===== PRODUCT MESSAGE ===';
        // echo '<br />';

        // var_dump($campaign_quotation);
        // var_dump($quotation_items);
        // var_dump($campaign);
        // var_dump($quotation_item);
        // var_dump($product2);
        // var_dump($campaign_report);
        // var_dump($campaign_report_telco_detail);

        // echo 'ExtMessageid: ' . $ExtMessageid;
        // echo '<br />';
        // echo 'ExtProductid: ' . $ExtProductid;
        // echo '<br />';
        // echo 'Description: ' . $Description;
        // echo '<br />';
        // echo 'StartDate: ' . $StartDate_PM;
        // echo '<br />';
        // echo 'EndaDate: ' . $EndDate_PM;
        // echo '<br />';
        // echo 'Plan: ' . $Plan_PM;
        // echo '<br />';
        // echo 'Success: ' . $Success_PM;
        // echo '<br />';
        // echo 'Location: ' . $Location;
        // echo '<br />';
        // echo 'Clicks: ' . $Clicks_PM;
        // echo '<br />';
        // echo 'CTR: ' . $CTR_PM;
        // echo '<br />';

        // var_dump($CampaignReport);
        // print_r($CampaignReport);
        // var_dump($total_campaign_report_telco_details);

        // var_dump($CampaignReportPost);
        // var_dump($campaign_report_telco_details);
        // var_dump($ProductMessage);
        // print_r($ProductMessage);
        // print_r($CampaignProduct);
        // echo '<pre />';
        // die();

        // SEND REQUEST BY CURL to sf-x
        $curl = curl_init();

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "http://localhost/sf-x/send-campaign-report",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => $CampaignReportPost,
        //   CURLOPT_HTTPHEADER => array(
        //     "Content-Type: application/json",
        //     "authorization: Basic aW14X3NmeDppbXhfc2Z4MjAxNyM=",
        //     "cache-control: no-cache",
        //     "postman-token: 3724beaf-a02e-9511-3aaf-c797b5b09ebb"
        //   ),
        // ));

        // SEND REQUEST BY CURL to Sandbox
        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://cs5.salesforce.com/services/apexrest/sendCampaignReport",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => $CampaignReportPost,
        //   CURLOPT_HTTPHEADER => array(
        //     "Content-Type: application/json",
        //     "authorization: Bearer 00DO000000531aG!ARQAQI9xduvaAiEUVfmwhg7RkHgJYK7jRwSsq0jfxyCBlXw.B7OfXBWahosbY6LEC71RYZt6QbY25reV4VrK_uaIKwf2WSvC",
        //     "cache-control: no-cache",
        //     "postman-token: d6be100e-096c-18c6-b9d9-13cbd0a28e08"
        //   ),
        // ));

        // SEND REQUEST BY CURL to SalesForce
        curl_setopt_array($curl, array(
          // CURLOPT_URL => "https://cs5.salesforce.com/services/apexrest/sendCampaignReport",
          CURLOPT_URL => "https://ap2.salesforce.com/services/apexrest/sendCampaignReport",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $CampaignReportPost,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "authorization: " . $authorization,
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err; die();
          $this->session->set_flashdata('synccampaign_alert', 
            '<div class="alert alert-danger">
                <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
                &times;</button>
                <strong>Failed!</strong> Error HTTP request on synchronizing <b>' . $CampaignName  .
            '</b></div>');
            redirect(base_url("campaign/quotation"));
        } else {
          // echo $response; die();
            $decoded_response = json_decode($response);
            $response_status = $decoded_response->Status;
            $response_message = (isset($decoded_response->Message)) ? $decoded_response->Message : $decoded_response->message;
            $response_time = $decoded_response->ResponseTime;

            if(strcasecmp('SUCCESS', $response_status) === 0) {
                $this->session->set_flashdata('synccampaign_alert', 
                '<div class="alert alert-success">
                    <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
                    &times;</button>
                    <strong>Success!</strong> <b>' . $CampaignName . '</b> Has Been Synchronized. ' . $response_message .
                '.</div>');
                redirect(base_url("campaign/quotation"));
            } else {
                $this->session->set_flashdata('synccampaign_alert', 
                '<div class="alert alert-danger">
                    <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
                    &times;</button>
                    <strong>Failed!</strong> Error on synchronizing campaign <b>' . $CampaignName  .
                '</b> '. $response_message . '.</div>');
                redirect(base_url("campaign/quotation"));
            }
        }
    }

    public function hitcurl() {
        $CampaignReportPost = '{"ExtCampaignid":"5107","CampaignNo":"100068-1010817014","CampaignName":"UBER 1 Agustus","QuoteNo":"Q\/IMS\/INTEGRA\/UBER\/20170731\/0142448","Sales":"Mario Benedictus","Agency":"PT Integra Mitra Sejati","Budget":"3900000","Advertiser":"Integra Mitra Sejati","ProductCategory":"LBA","Maskingid":"","StartDate":"2017-08-01 00:00:00+07","EndDate":"2017-08-01 00:00:00+07","CampaignProduct":[{"ExtProductid":"2472","Channel":"LBA SMS ISAT","Success":6000,"Used":"3900000","Plan":"3900000","Price":"650","ProductMessage":[{"ExtMessageid":"2608","ExtProductid":"2472","Description":"Dapatkan diskon 50% s\/d Rp 25.000 utk 3 perjalanan khusus pengguna baru. Download dan gunakan kode promo AGUSTUS50 utk pesan Uber sebelum 6 Agustus","StartDate":"2017-08-01 00:00:00+07","EndDate":"2017-08-01 00:00:00+07","Plan":"2000","Success":"2000","Location":"malangsurabaya - Kantor Pos - Universitas Airlangga Outdoor SBY","Clicks":"","CTR":""},{"ExtMessageid":"2609","ExtProductid":"2472","Description":"Dapatkan diskon 50% s\/d Rp 25.000 utk 3 perjalanan khusus pengguna baru. Download dan gunakan kode promo AGUSTUS50 utk pesan Uber sebelum 6 Agustus","StartDate":"2017-08-01 00:00:00+07","EndDate":"2017-08-01 00:00:00+07","Plan":"2000","Success":"2000","Location":"malangsurabaya - Universitas Petra In\/Out SBY_Cola","Clicks":"","CTR":""},{"ExtMessageid":"2610","ExtProductid":"2472","Description":"Dapatkan diskon 50% s\/d Rp 25.000 utk 3 perjalanan khusus pengguna baru. Download dan gunakan kode promo AGUSTUS50 utk pesan Uber sebelum 6 Agustus","StartDate":"2017-08-01 00:00:00+07","EndDate":"2017-08-01 00:00:00+07","Plan":"2000","Success":"2000","Location":"malangsurabaya - Jalan Basuki Rahmat 98","Clicks":"","CTR":""}],"ProductImpression":[]}]}';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://localhost/sf-x/send-campaign-report",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          // CURLOPT_POSTFIELDS => "{\r\n\"ExtCampaignid\":\"5107\",\r\n\"CampaignNo\":\"100068-1010817014\",\r\n\"CampaignName\":\"UBER 1 Agustus\",\r\n\"QuoteNo\":\"Q\\/IMS\\/INTEGRA\\/UBER\\/20170731\\/0142448\",\r\n\"Sales\":\"Mario Benedictus\",\r\n\"Agency\":\"PT Integra Mitra Sejati\",\r\n\"Budget\":\"3900000\",\r\n\"Advertiser\":\"Integra Mitra Sejati\",\r\n\"ProductCategory\":\"LBA\",\r\n\"Maskingid\":\"\",\r\n\"StartDate\":\"2017-08-01 00:00:00+07\",\r\n\"EndDate\":\"2017-08-01 00:00:00+07\",\r\n\"CampaignProduct\":[\r\n{\r\n\"ExtProductid\":\"2472\",\r\n\"Channel\":\"LBA SMS ISAT\",\r\n\"Success\":6000,\r\n\"Used\":\"3900000\",\r\n\"Plan\":\"3900000\",\r\n\"Price\":\"650\",\r\n\"ProductMessage\":[\r\n{\r\n\"ExtMessageid\":\"2608\",\r\n\"ExtProductid\":\"2472\",\r\n\"Description\":\"Dapatkan diskon 50% s\\/d Rp 25.000 utk 3 perjalanan khusus pengguna baru. Download dan gunakan kode promo AGUSTUS50 utk pesan Uber sebelum 6 Agustus\",\r\n\"StartDate\":\"2017-08-01 00:00:00+07\",\r\n\"EndDate\":\"2017-08-01 00:00:00+07\",\r\n\"Plan\":\"2000\",\r\n\"Success\":\"2000\",\r\n\"Location\":\"malangsurabaya - Kantor Pos - Universitas Airlangga Outdoor SBY\",\r\n\"Clicks\":\"\",\r\n\"CTR\":\"\"\r\n},\r\n{\r\n\"ExtMessageid\":\"2609\",\r\n\"ExtProductid\":\"2472\",\r\n\"Description\":\"Dapatkan diskon 50% s\\/d Rp 25.000 utk 3 perjalanan khusus pengguna baru. Download dan gunakan kode promo AGUSTUS50 utk pesan Uber sebelum 6 Agustus\",\r\n\"StartDate\":\"2017-08-01 00:00:00+07\",\r\n\"EndDate\":\"2017-08-01 00:00:00+07\",\r\n\"Plan\":\"2000\",\r\n\"Success\":\"2000\",\r\n\"Location\":\"malangsurabaya - Universitas Petra In\\/Out SBY_Cola\",\r\n\"Clicks\":\"\",\r\n\"CTR\":\"\"\r\n},\r\n{\r\n\"ExtMessageid\":\"2610\",\r\n\"ExtProductid\":\"2472\",\r\n\"Description\":\"Dapatkan diskon 50% s\\/d Rp 25.000 utk 3 perjalanan khusus pengguna baru. Download dan gunakan kode promo AGUSTUS50 utk pesan Uber sebelum 6 Agustus\",\r\n\"StartDate\":\"2017-08-01 00:00:00+07\",\r\n\"EndDate\":\"2017-08-01 00:00:00+07\",\r\n\"Plan\":\"2000\",\r\n\"Success\":\"2000\",\r\n\"Location\":\"malangsurabaya - Jalan Basuki Rahmat 98\",\r\n\"Clicks\":\"\",\r\n\"CTR\":\"\"\r\n}\r\n],\r\n\"ProductImpression\":[\r\n]\r\n}\r\n]\r\n}",
          CURLOPT_POSTFIELDS => $CampaignReportPost,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "authorization: Basic aW14X3NmeDppbXhfc2Z4MjAxNyM=",
            "cache-control: no-cache",
            "postman-token: 3724beaf-a02e-9511-3aaf-c797b5b09ebb"
          ),
        ));

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "http://localhost/sf-x/send-pricebook-product",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => "{\r\n\t\"Productid\":\"\",\r\n\t\"ExternalPrdId\":\"801\",\r\n\t\"ProductName\":\"Integration Testing 0108201701\",\r\n\t\"ProductCode\":\"801-STD01082017\",\r\n\t\"ProductFamily\":\"LBA\",\r\n\t\"Description\":\"STDU TESTING 01082017\",\r\n\t\"IsActive\":\"true\",\r\n\t\"StandardPrice\":[\r\n\t\t{\r\n\t\t\"Productid\":\"\",\r\n\t\t\"StandardPriceCurrency\":\"IDR\",\r\n\t\t\"StandardPrice\":\"2000000\"\r\n\t\t}\r\n\t],\r\n\t\"PricebookEntry\":[\r\n\t\t{\r\n\t\t\"Productid\":\"\",\r\n\t\t\"PricebookEntryid\":\"\",\r\n\t\t\"PriceBookId\":\"01sO00000005SZZ\",\r\n\t\t\"PricebookCurrency\":\"IDR\",\r\n\t\t\"ListPrice\":20000\r\n\t\t}\r\n\t]\r\n}",
        //   CURLOPT_HTTPHEADER => array(
        //     "authorization: Basic aW14X3NmeDppbXhfc2Z4MjAxNyM=",
        //     "cache-control: no-cache",
        //     "postman-token: 26bc8ebc-e4ef-e3c9-697d-67d07a2088f5"
        //   ),
        // ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err; die();
        } else {
          echo $response; die();
        }
    }

    function get_salesforce_token_response() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9ZL0ppGP5UrBTE48zUGkWo7kS6fy.t0nZX0Tv2TZLnE.O7_SHbILUw29NyRbI3nMrRWuTZQ7IcZOsvaV_&client_secret=430217730338650741&username=julius.ns%40imx.co.id&password=force1mx",
          // CURLOPT_URL => "https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Nvmjd9lcjRnBUlKcqSpSd3opFzoRK2bxDYjFCBWP9UfHHam5ZgANwR6Hs0R_vL8N.8ToR4XTYVaFXgxT&client_secret=442904999547975622&username=julius.ns%40imx.co.id.imxdev&password=imxdev%402017",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
          // die();
          $this->session->set_flashdata('syncproduct_alert', 
            '<div class="alert alert-danger">
                <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
                &times;</button>
                <strong>Failed!</strong> Error on requesting Access Token to SalesForce.</div>');
            redirect(base_url("product/listproduct"));
        } else {
          // echo $response;
          // die();

          // $token_response_decoded = json_decode($response);
          // $access_token = $token_response_decoded->access_token;
          // echo '<pre>';
          // var_dump($access_token);
          // echo '</pre>';
          // die();

            return $response;
        }
    }

}
