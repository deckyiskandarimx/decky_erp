<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends Ci_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $cekid = $this->session->userdata("user_id");


        if ($cekid == "") {
            redirect(base_url("auth"));
        }
    }

//for io
    public function index() {

        $this->db->select('view_io1.io_id, '
                . 'view_io1.quotation_number, '
                . 'view_io1.campaign_name, '
                . 'view_io1.io_number, '
                . 'product2.product_name as product_name, '
                . 'partner.name as partner_name, '
                . 'view_io1.io_status, '
                . 'invoice_.paid_status,'
                . 'view_io1.invoice_number,'
                . 'invoice_.transfer_amount');

        $this->db->join('product2', 'product2.product_id = view_io1.product_id');
        $this->db->join('partner', 'partner.partner_id = view_io1.partner_id');
        $this->db->join('invoice_', 'invoice_.io_id = view_io1.io_id','left');
        $data['io'] = $this->db->get("view_io1")->result();
        
        $data["title"] = "IO";
        $this->template->load('default', 'payment/io', $data);
    }

//for invoice
    public function proses($id = null) {
        
        $this->db->where('io_id', $id);
        $this->db->select('io_id');
        $dataedit = $this->db->get("io")->row();

        $data["io_id"] = $dataedit->io_id;
        
        
        $data["title"] = "Invoice";
        $data["tokenstart"] = $this->security->get_csrf_hash();
        $this->template->load('default', 'payment/form_invoice', $data);
    }

    public function tax_upload($id = null) {

        $this->db->where("io_id", $id);
        $this->db->select("io.io_id, io.io_number");
        $dataedit = $this->db->get("io")->row();
        $data["io_id"] = $dataedit->io_id;
        $data["io_number"] = $dataedit->io_number;
        $this->session->set_userdata($data);

        $this->db->where('io_id', $this->session->userdata("io_id"));
        $this->db->select('tax_.id_tax, tax_.name_file, tax_.io_id, users.fullname');
        $this->db->join("users", "tax_.uploaded_by = users.user_id ", "inner");
        $data['tax_'] = $this->db->get("tax_")->result();

        $data["title"] = "Upload Tax Document";
        $this->template->load('default', 'payment/form_tax', $data);
    }

    public function remove_invoice($id = null) {
        $io_id = $this->session->userdata("io_id");
        $this->db->where('id_invoice', $id);
        $this->db->delete('invoice_');
        $this->session->set_flashdata('hapus_invoice', '<div class="alert alert-success">
  <strong>Success!</strong> Invoice has been deleted.
</div>');

        redirect(base_url('payment/proses/' . $io_id));
    }

    public function remove_tax($id = null) {
        $io_id = $this->session->userdata("io_id");
        $this->db->where('id_tax', $id);
        $this->db->delete('tax_');
        $this->session->set_flashdata('hapus_tax_document', '<div class="alert alert-success">
      <strong>Success!</strong> Tax Document has been deleted.
      </div>');

        redirect(base_url('payment/tax_upload/' . $io_id));
    }

    public function detail($id = null) {
        $data["title"] = "Detail IO";

        $this->db->where("io.io_id", $id);
        $this->db->select("io.io_id, io.io_number, quotation.campaign_name, product.description, io.campaign_item_id, io.reference_id,io.total, io.tax, io.grand_total, io.created_on, users.fullname");
        $this->db->join("quotation", "io.quotation_id = quotation.quotation_id ", "inner");
        $this->db->join("product", "io.campaign_item_id = product.product_id ", "inner");
        $this->db->join("users", "io.created_by = users.user_id ", "inner");
        //$this->db->join("tax_", "io.io_id = tax_.io_id ", "inner");
        $dataedit = $this->db->get("io")->row();

        $data["io_id"] = $dataedit->io_id;
        $data["io_number"] = $dataedit->io_number;
        $data["campaign_name"] = $dataedit->campaign_name;
        $data["description"] = $dataedit->description;
        $data["total"] = $dataedit->total;
        $data["tax"] = $dataedit->tax;
        $data["grand_total"] = $dataedit->grand_total;
        $data["created_on"] = $dataedit->created_on;
        $data["fullname"] = $dataedit->fullname;

        $this->template->load('default', 'payment/detail_io', $data);
    }

    public function detail_quotation($id = null) {
        $data["title"] = "Detail Quotation";

        $this->db->where("io.io_id", $id);
        $this->db->select("io.io_id, io.io_number, quotation.campaign_name, product.description, io.campaign_item_id, io.reference_id,io.total, io.tax, io.grand_total, io.created_on, users.fullname");
        $this->db->join("quotation", "io.quotation_id = quotation.quotation_id ", "inner");
        $this->db->join("product", "io.campaign_item_id = product.product_id ", "inner");
        $this->db->join("users", "io.created_by = users.user_id ", "inner");
        //$this->db->join("tax_", "io.io_id = tax_.io_id ", "inner");
        $dataedit = $this->db->get("io")->row();

        $data["io_id"] = $dataedit->io_id;
        $data["io_number"] = $dataedit->io_number;
        $data["campaign_name"] = $dataedit->campaign_name;
        $data["description"] = $dataedit->description;
        $data["total"] = $dataedit->total;
        $data["tax"] = $dataedit->tax;
        $data["grand_total"] = $dataedit->grand_total;
        $data["created_on"] = $dataedit->created_on;
        $data["fullname"] = $dataedit->fullname;

        $this->template->load('default', 'payment/detail_io', $data);
    }

    public function approval($id_invoice = null) {

        $cekid = $this->session->userdata("user_id");
        $cek_user = $this->session->userdata("account_type");

        if ($cek_user == "AC08" || $cek_user == "AC04") {

            $status_app = '2';

            $data = array(
                'id_status' => $status_app,
                'approved_by' => $cekid
            );

            $io_id = $this->session->userdata("io_id");
            $this->db->where("user_type", "AC05");
            $userdata = $this->db->get("users")->result();
            $subject = "Finance Controller Approved Notification";
            foreach ($userdata as $dataemail) {
                $msg = "<html>"
                        . "<body>"
                        . "<p>Hi " . $dataemail->fullname . ",</p>"
                        . "<p>" . $this->session->userdata("fullname") . " has approved a payment io. Please click link bellow to see it.</p>"
                        . "<p></p>"
                        . "<p><a href='" . base_url("payment/proses/" . $io_id) . "' mc:disable-tracking>" . base_url("payment/proses" . $io_id) . "</a></p>"
                        . "<p></p>"
                        . "<p>Cheers</p>"
                        . "</body>"
                        . "</html>";

                $this->swiftmailer->sendHTMLMail($dataemail->username, $subject, $msg);
            }

            $this->db->where("user_type", "AC04");
            $userdata_ = $this->db->get("users")->result();
            $subject_ = "Success Approved Notification";
            foreach ($userdata_ as $dataemail) {
                $msg = "<html>"
                        . "<body>"
                        . "<p>Hi " . $dataemail->fullname . ",</p>"
                        . "<p>" . $this->session->userdata("fullname") . " success to approved payment. Please click link bellow to see it.</p>"
                        . "<p></p>"
                        . "<p><a href='" . base_url("payment/proses/" . $io_id) . "' mc:disable-tracking>" . base_url("payment/proses" . $io_id) . "</a></p>"
                        . "<p></p>"
                        . "<p>Cheers</p>"
                        . "</body>"
                        . "</html>";

                $this->swiftmailer->sendHTMLMail($dataemail->username, $subject_, $msg);
            }

            $this->db->where("id_invoice", $id_invoice);
            $this->db->update('invoice_', $data);
            $this->session->set_flashdata('approve_invoice', '<div class="alert alert-success">
  <strong>Success!</strong> Invoice has been Approved.
</div>');
            redirect(base_url('payment/proses/' . $io_id));
        } else {
            $this->session->set_flashdata('rejected', 'you dont have privileges only finance controller have privileges to approve!!!');
            redirect(base_url('payment/'));
        }
    }

    public function download_invoice($id = null) {

        $this->db->where("invoice_number", $id);
        $this->db->select("invoice_.id_invoice, invoice_.name_file, invoice_.full_path");
        $datadown = $this->db->get("invoice_")->row();

        $data["full_path"] = $datadown->full_path;
        $data["name_file"] = $datadown->name_file;

        $path = file_get_contents($data["full_path"]);
        force_download($data["name_file"], $path);

        redirect(base_url('payment/proses'));
    }

    public function client_payment() {

//        $this->db->select("io.io_id, "
//                . "io.io_number, "
//                . "quotation.quotation_number ,"
//                . "quotation.campaign_name, "
//                . "invoice_.id_invoice, "
//                . "invoice_.name_file, "
//                . "ref_io_status.status");
//        $this->db->join("quotation", "io.quotation_id = quotation.quotation_id ", "inner");
//        $this->db->join("invoice_", "io.io_id = invoice_.io_id ", "inner");
//        $this->db->join("ref_io_status", "ref_io_status.id_status = io.status ", "inner");
//        $this->db->order_by("io.io_id", "asc");
      /*  $this->db->select('view_io1.quotation_number, '
                . 'view_io1.quotation_id, '
                . 'view_io1.campaign_name ');
                //. 'view_io1.io_number, '
                //. 'product.name as product_name, '
                //. 'partner.name as partner_name, '
                //. 'view_io1.io_status, '
                //. 'view_io1.invoice_number, view_io1.quotation_id');

        //$this->db->join('product', 'product.product_id = view_io1.product_id');
        //$this->db->join('partner', 'partner.partner_id = view_io1.partner_id');
        $this->db->order_by("quotation_id");        
        $this->db->group_by("quotation_number,quotation_id,campaign_name");
        $data['io'] = $this->db->get("view_io1")->result();  */
        
        //query2
        $data["io"] = $this->db->query("select view_io1.quotation_number, view_io1.quotation_id, view_io1.campaign_name, view_io1.package_id from view_io1 "
                . " where view_io1.payment_type <> 2  group by quotation_number,quotation_id,campaign_name ,package_id"
        //        . " UNION select a.quotation_number, a.quotation_id, a.campaign_name, a.package_id from quotation a, quotation_payment_period b where a.quotation_id = b.quotation_id and "
				. " UNION select quotation_number, quotation_id, campaign_name, package_id from quotation where payment_type = 2 and status = 5")->result();   //end_date  > '".date("Y-m-d")."' //status 'done'
        
        
        $datasession = $this->db->get("io")->row();

        $data["io_id"] = $datasession->io_id;
        $this->session->set_userdata($data);

        $data["title"] = "client payment";
        $this->template->load('default', 'payment/client_pay', $data);
    }
    
    public function productdetailcpay($quotation_id = null){
        
        $this->db->select("product2.product_name as product_name, partner.name as partner_name");              
        $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");            

        $this->db->where("quotation_item.quotation_id", $quotation_id);
        //$this->db->where("quotation_item.item_id not in (select quotation_item from payment_invoice_item)", null, false);
        
        $data["io"] = $this->db->get("quotation_item")->result();     
        //echo $this->db->last_query();
        
        return $data;
    }
    
    public function packagedetailcpay($p_id = null){
        
        $this->db->select("package_name");              
        $this->db->where("package_id", $p_id);
        $data["io"] = $this->db->get("package")->result();     
        //echo $this->db->last_query();
        
        return $data;
    }

    public function download_tax($id = null) {

        $this->db->where("id_tax", $id);
        $this->db->select("tax_.id_tax, tax_.name_file, tax_.full_path");
        $datadown = $this->db->get("tax_")->row();

        $data["full_path"] = $datadown->full_path;
        $data["name_file"] = $datadown->name_file;

        $path = file_get_contents($data["full_path"]);
        force_download($data["name_file"], $path);

        redirect(base_url('payment/form_tax'));
    }

    public function close_pay($id = null) {
        $pay_status = '2';
        
        $data = array(
            'paid_status' => $pay_status,
            'paid_date' => date("Y-m-d")
        );

        $io_id = $this->session->userdata("io_id");

        $this->db->where('io_id', $id);
        $this->db->update('invoice_', $data);

        $this->session->set_flashdata('close_pay', '<div class="alert alert-success">
  <strong>Success!</strong> Payment has been paid.
</div>');
        redirect(base_url('payment/'));
    }

    public function do_upload($id = null) {

        //configuration for upload file
        $config['upload_path'] = 'upload_file/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '5000';
        $this->load->library('upload', $config);

        $cek_user = $this->session->userdata("account_type");

        //get data from form
        $io_id = $this->input->post("idhidden");
        $invoice_num = $this->input->post("invoice_num");
        $invoice_date = $this->input->post("invoice_date");
        $amount = $this->input->post("amount");
        $ppn = $this->input->post("ppn");
        $tax = $this->input->post("tax");
        $transfer = $this->input->post("transfer");
        $adjusted = $this->input->post("adjusted");

        //create validation for form
        $this->form_validation->set_rules('invoice_num', 'Invoice_num', 'trim|required');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required|numeric');
        $this->form_validation->set_rules('ppn', 'Ppn', 'trim|required|integer');
        $this->form_validation->set_rules('tax', 'Tax', 'trim|required|integer');
        $this->form_validation->set_rules('adjusted', 'Adjusted', 'trim|required|integer');


        //cek for one io one invoice
        $this->db->where('io_id', $io_id);
        $this->db->select('io_id');
        $data_io_id = $this->db->get('invoice_')->result();

        if (sizeof($data_io_id > 1)) {
            $this->db->where('io_id', $io_id);
            $this->db->delete('invoice_');
        }
        //end of check

        if ($cek_user == "AC08" || $cek_user == "AC05") {
            if (!$this->upload->do_upload() or $this->form_validation->run() == FALSE) {
                $error = array('error' => $this->upload->display_errors());
                $data["invoice_number"] = $invoice_num;
                $data["invoice_date"] = $invoice_date;
                $data["amount"] = $amount;
                $data["tax_ppn"] = $ppn;
                $data["holding_tax"] = $tax;
                $data["transfer_amount"] = $transfer;
                $data["adjusted"] = $adjusted;
                $data["io_id"] = $io_id;
                $data["title"] = "Form Invoice";
                $this->template->load('default', 'payment/form_invoice', $error, $data);
            } else {
                $data = array('upload_data' => $this->upload->data());
                $data["title"] = "Upload Invoice";

                $upload_data = $this->upload->data();
                $status = '1';
                $status_a = '2';
                $record['name_file'] = $upload_data['file_name'];
                $record['tipe_file'] = $upload_data['file_type'];
                if ($this->session->userdata("account_type") != "AC04" && $this->session->userdata("account_type") != "AC08") {
                    $record['id_status'] = $status;
                } else {
                    $record['id_status'] = $status_a;
                }
                $record['io_id'] = $io_id;
                $record['uploaded_by'] = $this->session->userdata("user_id");
                $record['full_path'] = $upload_data['full_path'];
                $record["invoice_number"] = $invoice_num;
                $record["invoice_date"] = $invoice_date;
                $record["amount"] = $amount;
                $record["tax_ppn"] = $ppn;
                $record["holding_tax"] = $tax;
                $record["transfer_amount"] = $amount + $ppn - $tax;
                $record["adjusted"] = $adjusted;

                $this->db->insert('invoice_', $record);
                
                //die($this->db->last_query());

                //insert id_invoice into io and update status to 3
                $this->db->where('io_id', $io_id);
                $this->db->select('id_invoice');
                $invoice_id = $this->db->get('invoice_')->row();

                $this->db->where('io_id', $io_id);
                $dataupdate = array(
                    'status' => 3,
                    'invoice_id' => $invoice_id->id_invoice
                );
                $this->db->update('io', $dataupdate);
                //end update

                if ($this->session->userdata("account_type") != "AC04" && $this->session->userdata("account_type") != "AC08") {

                    $io_id = $this->session->userdata("io_id");
                    $this->db->where("user_type", "AC04");
                    $userdata = $this->db->get("users")->result();
                    $subject = "New Payment Notification";
                    foreach ($userdata as $dataemail) {
                        $msg = "<html>"
                                . "<body>"
                                . "<p>Hi " . $dataemail->fullname . ",</p>"
                                . "<p>" . $this->session->userdata("fullname") . " has creating a payment io. Please click link bellow to approve it.</p>"
                                . "<p></p>"
                                . "<p><a href='" . base_url("payment/proses/" . $io_id) . "' mc:disable-tracking>" . base_url("payment/proses" . $io_id) . "</a></p>"
                                . "<p></p>"
                                . "<p>Cheers</p>"
                                . "</body>"
                                . "</html>";

                        $this->swiftmailer->sendHTMLMail($dataemail->username, $subject, $msg);
                    }
                }
                $this->session->set_flashdata('sukses_upload_invoice', '<div class="alert alert-success">
  <strong>Success!</strong> Invoice has been uploaded
  <br>
</div>');

                redirect(base_url('payment'));
            }
        } else {
            $this->session->set_flashdata('gagal_upload_invoice', '<div class="alert alert-danger">
  <strong>Danger!</strong> Only finance can do upload.
</div>');

            redirect(base_url('payment/proses/' . $io_id));
        }
    }
    
    public function cekio($id = null){
        
        $this->db->select("count(id_invoice) as jml");
        $this->db->where("io_id", $id);
        $io = $this->db->get("invoice_")->row();
        
        return $io->jml;
        
    }

    public function tax_do_upload($id = null) {

        $config['upload_path'] = 'upload_file/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '5000';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $data["title"] = "Upload Invoice";
            $this->template->load('default', 'payment/form_tax', $error, $data);
        } else {

            $data = array('upload_data' => $this->upload->data());
            $data["title"] = "Upload Tax Document";

            $upload_data = $this->upload->data();
            $update_data = array(
                'name_file' => $upload_data['file_name'],
                'tipe_file' => $upload_data['file_type'],
                'io_id' => $this->session->userdata("io_id"),
                'uploaded_by' => $this->session->userdata("user_id"),
                'full_path' => $upload_data['full_path']
            );

            $this->db->insert('tax_', $update_data);

            $this->db->where('io_id', $this->session->userdata("io_id"));
            $this->db->select('name_file');
            $query_tax = $this->db->get('tax_')->row();

            $data["name_file"] = $query_tax->name_file;
            $cek_name_file = $data["name_file"];

            $up_date = array(
                'tax_file' => $cek_name_file
            );

            $this->db->where('io_id', $this->session->userdata("io_id"));
            $this->db->update('io', $up_date);

            $this->session->set_flashdata('upload_tax_document', '<div class="alert alert-success">
  <strong>Success!</strong> Tax Document has been uploaded.
</div>');

            redirect(base_url('payment/tax_upload/' . $this->session->userdata('io_id')));
        }
    }
    
    
    public function invform($id = null,$idpinv=null){
        
        $data = array();
        $this->db->select("pp_id, product2.product_name as name, partner.name as partner");
        $this->db->join("ref_doc_status", "ref_doc_status.status = product_partner_rel.doc_status");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");
        $this->db->where("product_partner_rel.doc_status", 2);
        $this->db->where("product_partner_rel.status", 1);
        $data["item"] = $this->db->get("product_partner_rel")->result();
        
        if($idpinv > 0){
            $this->db->where("pinv_id", $idpinv);
            $getppinv = $this->db->get("payment_invoice")->row();
            $data["invoice_number"] = $getppinv->pinv_number;
            $data["invoice_date"] = $this->formatdate($getppinv->pinv_date);          
            $data["invoice_total"] = $getppinv->pinv_total;          
            $data["invoice_tax"] = $getppinv->pinv_tax;
            $data["invoice_variance"] = $getppinv->pinv_variance;          
            $this->db->select("quotation_item.pp_id, product2.product_name as name, quotation_item.amount as amount");              
            $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
            $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
            $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");            
            
            $this->db->where("quotation_item.quotation_id", $id);
            $data["item"] = $this->db->get("quotation_item")->result();
           
            $this->db->select("product2.product_name as name, quotation_item.amount as amount, quotation_item.price as price, quotation_item.discount as discount, quotation_item.item_id, payment_invoice_item.payinvo_item_id, payment_invoice_item.amount_invoice,  payment_invoice_item.file");     
            $this->db->join("quotation_item", "quotation_item.item_id = payment_invoice_item.quotation_item");   
            $this->db->join("product_partner_rel", "quotation_item.pp_id = product_partner_rel.pp_id");
            $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
            $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");   
            $this->db->where("quotation_item.quotation_id", $id);
            $this->db->order_by("payinvo_item_id","DESC");
            $data["ppinv_item"] = $this->db->get("payment_invoice_item")->result();
        }
        
        $data["io_id_hidden"] = $id;
        $data["idpinv"] = $idpinv;
        $this->template->load('default', 'payment/invform', $data);
        
    }
    
    
    function cekpaymenttype($id=null){ $this->db->select("a.name as type"); $this->db->join("ref_payment a", "a.payment_id = b.payment_type"); $data = $this->db->get("quotation b")->row(); return $data->type; }
    
    public  function paymentitem($id=null,$idinv=null){
        
        $this->db->select("quotation_item.item_id, product2.product_name as name, quotation_item.amount as amount, partner.name as partner");              
        $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
        $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
        $this->db->join("partner", "partner.partner_id = product_partner_rel.partner_id");            

        $this->db->where("quotation_item.quotation_id", $id);
        $this->db->where("quotation_item.item_id not in (select quotation_item from payment_invoice_item)", null, false);
        
        $data["item"] = $this->db->get("quotation_item")->result();
        $data["io_id_hidden"] = $id;
        $data["idpinv"] = $idinv;
        
        $this->template->load('default', 'payment/paymentitem', $data);
    }
    
    public function generateinvoice($id=null) {


        define('FPDF_FONTPATH', $this->config->item('fonts_path'));

        $data = array();
        
        //$this->db->join("client","agency_id = client_id");
		$this->db->join("client","advertiser_id = client_id");
		$this->db->join("ref_city","ref_city.id = client.address_city");
		$this->db->join("ref_province","ref_province.id = client.address_province");
        $this->db->join("payment_invoice","payment_invoice.quotation_id = quotation.quotation_id");
        
        $this->db->where("quotation.quotation_id", $id);
        $data["invoice"] = $this->db->get("quotation")->row();
        
        $this->db->join("product_partner_rel b","b.pp_id = a.pp_id");
        $this->db->join("product2 c","c.product_id = b.product_id");
        $this->db->where("a.quotation_id", $id);
        $data["invoice_item"] = $this->db->get("payment_invoice_item a")->result();
        
        $data["terbilang"] = $this->terbilang(1100000); //$this->terbilang("1100000");
        $this->load->view('template/pdf/invoice_payment', $data);
    }
    
    
    public function generateinvoicepackage($id=null) {


        define('FPDF_FONTPATH', $this->config->item('fonts_path'));

        $data = array();
        $this->db->join("client","agency_id = client_id");
        $this->db->join("package c","c.package_id = quotation.package_id");
        $this->db->where("quotation.quotation_id", $id);
        $data["invoice"] = $this->db->get("quotation")->row();
        
        $data["terbilang"] = $this->terbilang(1100000); //$this->terbilang("1100000");
        $this->load->view('template/pdf/invoice_payment_package', $data);
    }


    public function saveinvoicegeneral(){
        
        $idpinv = $this->input->post("idpinv");
        $number = $this->input->post("invoice_number");
        $date = $this->input->post("invoice_date");
        $io_id = $this->input->post("io_id_hidden");
        
        $data["pinv_number"] = $number;
        $data["pinv_date"] = $this->reformatedatedb($date);
        $data["quotation_id"] = $io_id;
        
        if($idpinv > 0){
            $where["pinv_id"] = $idpinv;
            $this->db->update("payment_invoice", $data, $where);
            redirect(base_url("payment/invform/".$io_id."/".$idpinv));
        }else{
            $this->db->insert("payment_invoice", $data);
            $idlast = $this->db->insert_id();
            $this->saveqitem($idlast,$io_id);
            redirect(base_url("payment/invform/".$io_id."/".$idlast));
        }
        
        
    }
    
    
    public function saveinvprepaid($number = null, $qid = 0){
        
        
        $data["pinv_number"] = $number;
        $data["pinv_date"] = date("Y-m-d");
        $data["quotation_id"] = $qid;
        
        $this->db->insert("payment_invoice", $data);
        $idlast = $this->db->insert_id();
        
        
        $this->db->select("quotation_item.pp_id as pp_id, quotation_item.item_id as item_id");                      
        $this->db->where("quotation_item.quotation_id", $qid);
        $dataresitem = $this->db->get("quotation_item")->result();     
        //die($this->db->last_query());
        
        foreach ($dataresitem as $item){
            $data["payinvoice_id"] = $payinvoice_id;
            $data["quotation_id"] = $quotation_id;
            $data["quotation_item"] = $item->item_id;
            $data["pp_id"] = $item->pp_id;
            $this->db->insert("payment_invoice_item", $data);
            
            unset($data);
        }
        
    }
    
    
    
    public function saveqitem($payinvoice_id = null, $quotation_id = null){
        
        //$quotation_id = $this->input->post("io_id_hidden");
        //$payinvoice_id = $this->input->post("idpinv");
        //$pp_id = $this->input->post("pp_id");
        
        $this->db->select("quotation_item.pp_id as pp_id, quotation_item.item_id as item_id");                      
        $this->db->where("quotation_item.quotation_id", $quotation_id);
        $dataresitem = $this->db->get("quotation_item")->result();     
        //die($this->db->last_query());
        
        foreach ($dataresitem as $item){
            $data["payinvoice_id"] = $payinvoice_id;
            $data["quotation_id"] = $quotation_id;
            $data["quotation_item"] = $item->item_id;
            $data["pp_id"] = $item->pp_id;
            $this->db->insert("payment_invoice_item", $data);
            
            unset($data);
        }
        
        redirect(base_url("payment/invform/".$quotation_id."/".$payinvoice_id));
        
    }
    

    
    function reformatedatedb($date=NULL){
        
        $explode = explode("-", $date);
        return $explode[2]."-".$explode[1]."-".$explode[0];
        
    }
    
    function formatdate($date = null){
        $explode = explode("-", $date);
        return $explode[2]."-".$explode[1]."-".$explode[0];        
    }
    
    
    public function getpaymentinvo($id=null){
        
        $this->db->where("quotation_id", $id);
        $data = $this->db->get("payment_invoice")->row();
        
        if($data->pinv_id > 0){
            $link = base_url("payment/invform/" . $id."/".$data->pinv_id);
        }else{
            $link = base_url("payment/invform/" . $id);
        }
        
        return $link;
    }
    
    
    public function getpaymentpaid($id=null){
        
        $this->db->where("quotation_id", $id);
        $data = $this->db->get("payment_invoice")->row();
        
        return $data->paid;
    }
    
    public function getpaymentinvoitem($id=null){
        
        $this->db->select("count(pinv_id) as jml");
        $this->db->where("quotation_id", $id);
        $data = $this->db->get("payment_invoice")->row();
        
        return $data->jml;
    }
    
    function savedetailpaymentinv(){
        
        //die(var_dump($this->input->post()));
        $total = $this->input->post("total");
        $tax = $this->input->post("tax");
        $variance = $this->input->post("variance");
        $idpinv = $this->input->post("idpinv");
        $io_id_hidden = $this->input->post("io_id_hidden");
        $hidden_quotation_amount = $this->input->post("hidden_quotation_amount");
        
        $amount_invoice = $this->input->post("amount_invoice");
        foreach($amount_invoice as $id => $aminv){
            
            $data["amount_invoice"] = $aminv;
            $where["payinvo_item_id"] = $id;
            $this->db->update("payment_invoice_item", $data, $where);
            
            
            //save invoice :
        $this->db->select("count(io_id) as count");
        $dataio = $this->db->get_where("io", array("quotation_id" => $io_id_hidden))->row();
        //die($this->db->last_query());
        if ($dataio->count > 0) {
        //insert "IO" per item

            
            $this->db->select("io.io_id,partner_share,io.bearer_cost,isindosat_product,iskloc_product");
            $this->db->join("quotation_item", "campaign_item_id = item_id");
            $this->db->join("product_partner_rel", "product_partner_rel.pp_id = quotation_item.pp_id");
            $this->db->join("product2", "product2.product_id = product_partner_rel.product_id");
            $this->db->where('io.quotation_id', $io_id_hidden);
            $dataitem = $this->db->get('io')->result();
//die($this->db->last_query());
            foreach ($dataitem as $item) {

                //$REVENUE = $item->total_peritem;
                $REVENUE = $aminv;

                $COGS = 0;
                
                $PARTNER_SHARE = $item->partner_share;
                $BEARER_COST = $item->bearer_cost;
                
               // die($PARTNER_SHARE." -- ".$BEARER_COST);

                if($item->isindosat_product == 1){
                    $REVSHISAT = (0.3) * ($REVENUE - $PARTNER_SHARE - $BEARER_COST);
                    $rec2io["rev_share_isat"] = $REVSHISAT;
                }

                if($item->iskloc_product == 1){
                    $REVSHKLOC = (0.05) * ($REVENUE - $PARTNER_SHARE - $BEARER_COST);
                    $rec2io["rev_share_kloc"] = $REVSHKLOC;
                }

                $REVSHTOTAL = $REVSHISAT + $REVSHKLOC;

                $COGS = $PARTNER_SHARE + $BEARER_COST + $REVSHTOTAL;

                $rec2io["cogs"] = $COGS;
                $rec2io["total"] = $REVENUE;        
               
                $wherec["io_id"] = $item->io_id;
                $this->db->update("io", $rec2io, $wherec);
//die($this->db->last_query());

            }
        }
        
        //end of save invoice 
            
        }
        
        $data2["pinv_total"] = $total;
        $data2["pinv_tax"] = $tax;
        $data2["pinv_variance"] = $variance;
        $data2["total_quotation"] = $hidden_quotation_amount;
        $wherepinv["pinv_id"] = $idpinv;
        $this->db->update("payment_invoice", $data2, $wherepinv);
        
        
        
        
        redirect(base_url("payment/invform/".$io_id_hidden."/".$idpinv));
        
    }
    
    
    function uploadreportitem($id = null, $iditem = null, $idinv = null){
        
        $config['upload_path'] = './assets/invoicereport/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 2000;
        $path = "assets/invoicereport/";
        
        $idqitem = $this->input->post("idqitem");
        $payinv_id = $this->input->post("payinv_id");
        
        
        $filename = date("ymdhis_".$id);
        $config["file_name"] = $filename;
        $data = array();
        $data["payinv_item_id"] = $id;
        $data["idqitem"] = $iditem;
        $data["payinv_id"] = $idinv;
                
        $this->load->library('upload', $config);
        
        
        if (!$this->upload->do_upload('reportitem')) {
            if ($this->input->post()) {
                $failed = array('error' => $this->upload->display_errors());
                $data["error"] = $failed["error"];
            }
            $this->template->load('default', 'payment/uploadreportitem', $data);
        }else{   
            $data2["file"] = $filename.".pdf";
            $where["payinvo_item_id"] = $id;
            $this->db->update("payment_invoice_item", $data2, $where);
            redirect(base_url('payment/invform/'.$idqitem.'/'.$payinv_id));
        }
        
        
        
    }
    
    function markpaid($qid = null){
        
        $quotation_id = $this->input->post("quotation_id");
        $data["quotation_id"] = $qid;        
        
        if($this->input->post()){
            $data2["paid"] = 1;
            $data2["paid_date"] = date("Y-m-d");
            $where["quotation_id"] = $quotation_id;
            $this->db->update("payment_invoice", $data2, $where);            
            redirect(base_url("payment/client_payment"));
        }else{
            $data["action"] = "markpaid";
            $this->template->load('default', 'payment/markpaid', $data);
        }
        
        
    }
    
    
    function markpaidpackage($qid = null){
        
        $quotation_id = $this->input->post("quotation_id");
        $data["quotation_id"] = $qid;   
        
        if($this->input->post()){
            
            $this->db->where("quotation_id", $quotation_id);
            $this->db->join("package","package.package_id = quotation.package_id");
            $get = $this->db->get("quotation")->row();
            
            
            $mark["pinv_date"] = date("Y-m-d");
            $mark["pinv_total"] = $get->product_tariff_idr_total;
            $mark["pinv_tax"] = 10/100*$get->product_tariff_idr_total;
            $mark["pinv_number"] = $get->quotation_number;
            $mark["quotation_id"] = $get->quotation_id;
            $mark["paid"] = 1;
            $mark["paid_date"] = date("Y-m-d");
            $mark["total_quotation"] = $get->product_tariff_idr_total + (10/100*$get->product_tariff_idr_total);
            $mark["ispackage"] = 1;
            
            $this->db->insert("payment_invoice", $mark);   
            
            redirect(base_url("payment/client_payment"));
            
        }else{
            $data["action"] = "markpaidpackage/".$qid;
            $this->template->load('default', 'payment/markpaid', $data);
        }
        
        
    }
    
    function getstatuspayment($quotation_id = null){
        
        $this->db->select("count(payinvo_item_id) as jml");
        $this->db->where("quotation_id", $quotation_id);
        $data = $this->db->get("payment_invoice_item")->row();
        
        return $data->jml;
    }
    
    
    public function kekata($x = 1100000) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = Payment::kekata($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = Payment::kekata($x / 10) . " puluh" . Payment::kekata($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . Payment::kekata($x - 100);
        } else if ($x < 1000) {
            $temp = Payment::kekata($x / 100) . " ratus" . Payment::kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . Payment::kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = Payment::kekata($x / 1000) . " ribu" . Payment::kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = Payment::kekata($x / 1000000) . " juta" . Payment::kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = Payment::kekata($x / 1000000000) . " milyar" . Payment::kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = Payment::kekata($x / 1000000000000) . " trilyun" . Payment::kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    public function terbilang($x, $style = 3) {
        if ($x < 0) {
            $hasil = "minus " . trim(Payment::kekata($x));
        } else {
            $hasil = trim(Payment::kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil . " Rupiah";
    }
    

}
