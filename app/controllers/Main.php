<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Main extends CI_Controller
{
    
    public function __construct() {
        parent::__construct();
        $cekid = $this->session->userdata("user_id");
        if($cekid == ""){
            redirect(base_url()."auth");
        }
        
    }
    
    public function index(){
        
        $data['title'] = 'Main Page';
		$this->template->load('default', 'main/index', $data);
    }
    
}

