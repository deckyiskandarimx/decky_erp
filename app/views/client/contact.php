<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('client'); ?>">Client</a>
    </li>
    <li class="active">
        <strong>Contact</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('edit_contact'); ?>
<h1><?php echo $title; ?></h1>
<br />
<form action="<?php echo base_url("client/edit/"); ?>" method="post" id="formcategory">
    <table class="table table-hover datatable" id="table-contact">
        <thead>
            <tr>
                <th style="text-align: center;"><b>#</b></th>
                <th style="width: 20%;"><b>Company</b></th>
                <th style="text-align: center;"><b>Contacts</b></th>
            </tr>
        </thead>
        <?php if (sizeof($client) > 0): ?>
        <tbody>
            <?php
                $num = 1;
                foreach($client as $data):
            ?>
                <tr class="odd gradeX">
                    <td align="center"><?=$num++?></td>
                    <td><?=$data->client_name?></td>
                    <td>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><b>Name</b></th>
                                    <th><b>Position</b></th>
                                    <th><b>Phone</b></th>
                                    <th><b>Email</b></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?=$data->finance_name?></td>
                                    <td>Finance</td>
                                    <td><?=$data->finance_phone?></td>
                                    <td><?=$data->finance_email?></td>
                                    <td align="right">
                                        <a href="<?php echo base_url('client/edit_contact/finance/' . $data->client_id);?>" class="btn btn-default btn-sm" data-toggle="tooltip" title="Edit Finance"><span class="entypo-pencil"></span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?=$data->media_name?></td>
                                    <td>Media Planner</td>
                                    <td><?=$data->media_phone?></td>
                                    <td><?=$data->media_email?></td>
                                    <td align="right">
                                        <a href="<?php echo base_url('client/edit_contact/media/' . $data->client_id);?>" class="btn btn-default btn-sm" data-toggle="tooltip" title="Edit Media"><span class="entypo-pencil"></span></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>                    
                    </td>
                </tr>
            <?php 
                endforeach;
            ?>
        </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>
                <th style="text-align:center; width: 10%;"><b>#</b></th>
                <th style="width: 25%;"><b>Company</b></th>
                <th style="text-align: center;"><b>Contacts</b></th>
            </tr>
        </tfoot>
    </table>
</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>

<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-contact").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
            // "aoColumnDefs": [
            //     {
            //         "aTargets": [0],
            //         "bVisible": false,
            //         "bSearchable": false
            //     },
            // ],
            "bStateSave": true,
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-plus hidden" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/add") ?>\'"> Add Contact</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
        // $("div.dataTables_filter").append('<button type="button" class="btn btn-success"> Alert</button>');
        // $("div.dataTables_filter").css("");
    });
</script>