<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('client');?>">Contact</a>
	</li>
	<li class="active">
		<strong>Form</strong>
	</li>
</ol>
<?php echo $this->session->flashdata('edit_contact'); ?>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
	<?php
        if (validation_errors()) {
            echo validation_errors();
        }
        echo $msg;
	?>
</div>
<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary panel-card" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<span style="border-left: 3px solid #31708F;"></span><span class="entypo-doc-text"></span>Form Edit Contact - <?=ucfirst($mod)?>
				</div>
			</div>
			<?php
				if(strcmp($mod, "finance") == 0) {
					$name = $finance_name;
					$phone = $finance_phone;
					$email = $finance_email;
				} else if(strcmp($mod, "media") == 0) {
					$name = $media_name;
					$phone = $media_phone;
					$email = $media_email;
				}
			?>
			<div class="panel-body" style="padding-left: 250px; padding-right: 250px;">
				<form class="form-horizontal" method="post" action="<?php echo base_url("client/update_contact/" . $mod . "/" . $client_id)?>">
					<div class="form-group">
						<label for="name" class="col-sm-3 control-label">Name</label>
						
						<div class="col-sm-5">
	                       <input type="text" class="form-control" value="<?=$name; ?>" name="name" id="name" placeholder="Name" required="required">
						</div>
					</div>

					<div class="form-group">
						<label for="phone" class="col-sm-3 control-label">Phone</label>
						
						<div class="col-sm-5">
	                       <input type="text" class="form-control" value="<?=$phone; ?>" name="phone" id="phone" placeholder="Phone" required="required">
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-sm-3 control-label">Email</label>
						
						<div class="col-sm-5">
	                       <input type="text" class="form-control" value="<?=$email; ?>" name="email" id="email" placeholder="Email" required="required">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<input type="hidden" name="client_id" value="<?=$client_id;?>">
							<input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                
	                        <button type="submit" class="btn btn-success"><span class="entypo-floppy"></span> Update</button>
	                        <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>client/contact';"><span class="entypo-cancel-squared"></span> Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>