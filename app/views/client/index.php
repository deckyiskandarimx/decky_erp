<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('client'); ?>">Client</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('add_client'); ?>
<?php echo $this->session->flashdata('add_client'); ?>
<h1><?php echo $title; ?></h1>
<br />
<form action="<?php echo base_url("client/edit/"); ?>" method="post" id="formcategory">
    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Category</th>
                <th>City</th>
                <th>Province</th>
                <th>Country</th>
                <th>Phone</th>
                <th>Actions</th>

            </tr>
        </thead>
        <?php if (sizeof($client) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($client as $data):
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td style="width: 15px;"><input type="checkbox" name="clientid[]" value="<?php echo $data->client_id ?>"></td>
                        <td><?php echo $data->client_name; ?></td>
                        <td><?php echo $data->category_name; ?></td>
                        <td><?php
                            echo $data->city;
                            ?>
                        </td>
                        <td><?php echo $data->province;
                            ?></td>
                        <td><?php echo $data->country_name; ?></td>
                        <td><?php echo $data->phone; ?></td>
                        <td>

                            <a href="<?php echo base_url("client/edit/" . $data->client_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                <i class="entypo-pencil"></i>
                                Edit
                            </a>          
                            <a href="<?php echo base_url("client/addbrandlist/" . $data->client_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                <i class="entypo-globe"></i>
                                Add Brand
                            </a>    


                        </td>
                    </tr>		
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>    
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Category</th>
                <th>City</th>
                <th>Province</th>
                <th>Country</th>
                <th>Phone</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/add") ?>\'"> Add Client</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });



</script>