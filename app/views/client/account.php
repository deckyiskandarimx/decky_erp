<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('client'); ?>">Client</a>
    </li>
    <li class="active">
        <strong>Account</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('add_client'); ?>
<?php echo $this->session->flashdata('add_client'); ?>
<?php echo $this->session->flashdata('edit_client'); ?>
<h1><?php echo $title; ?></h1>
<br />
<form action="<?php echo base_url("client/edit/"); ?>" method="post" id="formcategory">
    <table class="table table-hover datatable" id="table-4">
        <thead>
            <tr>
                <th>#</th>
                <!-- <th>&nbsp;</th> -->
                <th>Client Name</th>
                <th>Address</th>
                <th>NPWP</th>
                <th>Business Category</th>
                <th>Payment Type</th>
                <!-- <th>City</th>
                <th>Province</th>
                <th>Country</th>
                <th>Phone</th> -->
                <th>Total Campaign</th>
                <th style="text-align: center;"></th>

            </tr>
        </thead>
        <?php if (sizeof($client) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($client as $data):
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <!-- <td style="width: 15px;"><input type="checkbox" name="clientid[]" value="<?php echo $data->client_id ?>"></td> -->
                        <td><?php echo $data->client_name; ?></td>
                        <td>
                            <?php 
                                echo $data->address_street . '<br />';
                                $city = empty($data->city) ? '' : ucwords(strtolower($data->city));
                                $province = ucwords(strtolower($data->province));
                                if(strcmp('Dki Jakarta') == 0) {
                                    $province = 'DKI Jakarta';
                                } else if(strcmp('Diy Djogjakarta') == 0) {
                                    $province = 'DIY Djogjakarta';
                                }
                                echo((empty($city) OR ($data->city == " ")) ? $province . '<br />' : $city . ', ' . $province . '<br />');
                                if($data->address_country == 1) {
                                    echo "Indonesia";
                                }
                            ?>
                        </td>
                        <td><?php echo $data->npwp; ?></td>
                        <td><?php echo $data->category_name; ?></td>
                        <td><?php echo $data->payment_type; ?></td>
                        <!-- <td><?php
                            //echo $data->city;
                            ?>
                        </td>
                        <td><?php //echo $data->province;
                            ?></td>
                        <td><?php //echo $data->country_name; ?></td>
                        <td><?php //echo $data->phone; ?></td> -->
                        <td align="center"><?=$data->total_campaign?></td>
                        <td align="center">
                            <a href="<?php echo base_url("client/edit/" . $data->client_id) ?>" class="btn btn-default btn-sm" data-toggle="tooltip" title="Edit Account">
                                <span class="entypo-pencil"></span>
                            </a>          
                            <a href="<?php echo base_url("client/addbrandlist/" . $data->client_id) ?>" class="btn btn-default btn-sm" data-toggle="tooltip" title="Add Brand">
                                <span class="entypo-plus"></span>
                            </a>    
                        </td>
                    </tr>		
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>    
                <th>#</th>
                <!-- <th>&nbsp;</th> -->
                <th>Client Name</th>
                <th>Address</th>
                <th>NPWP</th>
                <th>Business Category</th>
                <th>Payment Type</th>
                <!-- <th>City</th>
                <th>Province</th>
                <th>Country</th>
                <th>Phone</th> -->
                <th>Total Campaign</th>
                <th style="text-align: center;"></th>
            </tr>
        </tfoot>
    </table>

</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
            "bStateSave": true,
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/add") ?>\'"> Add Client</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>