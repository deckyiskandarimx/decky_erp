<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('client');?>">Client</a>
	</li>
        
        <li>
		<a href="<?php echo base_url('client/brand');?>">Brand</a>
	</li>
	<li class="active">
		<strong>Form</strong>
	</li>
</ol>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
                            <?php
                                    if (validation_errors()) {
                                        echo validation_errors();
                                    }
                                    echo $msg;
                            ?>
                            </div>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Form Brand
				</div>
				
				
			</div>
			<form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("client/".$mod."/".$id)?>">
			<div class="panel-body">
                                <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Brand Name</label>

                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="field-1" placeholder="Brand Name" required="required">
                                        </div>
                                </div>
                                <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <input type="hidden" name="idhidden" value="<?php echo $id;?>">
                                                    <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                    
                                                    <button type="submit" class="btn btn-default">Save</button>
                                                    <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url("client/brand")?>';">Cancel</button>
                                            </div>
                                    </div>
                        </div>
                            
                            </form>
		
		</div>
            
	
	</div>
</div>



<link rel="stylesheet" href="<?php echo assets;?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2.css">


<script src="<?php echo assets;?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets;?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets;?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets;?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets;?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets;?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets;?>js/select2/select2.min.js"></script>


<script type="text/javascript">
    
    <?php if($id>0){
        echo '$("#province").attr("disabled",false); ';
        echo '$("#city").attr("disabled",false);';
    }?>
    
    $("#country").change(function(){
       $.post("<?php echo base_url();?>client/ajax/country",{countryid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'}, function(data){  
                    $("#ajax_responses").html(data);
                    $("#ajax_responses").find("script").each(function(){
                      eval($(this).text());
                    });
                });
    });
    
    $("#province").change(function(){
        var token2 = $("#csrf_city").val()
       $.post("<?php echo base_url();?>client/ajax/province",{provinceid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>':token2}, function(data){  
                    $("#ajax_responses2").html(data);
                    $("#ajax_responses2").find("script").each(function(){
                      eval($(this).text());
                    });
                });
    });
    
        jQuery(document).ready(function($)
	{
		var table = $("#table-4").dataTable({
			"sPaginationType": "bootstrap",
			"oTableTools": {
			},
			
		});
                $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/add")?>\'"> Add Client</button>');                
                $(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
    
</script>

