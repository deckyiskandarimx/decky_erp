<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />


<div style="color: red">
                            <?php
                                    if (validation_errors()) {
                                        echo validation_errors();
                                    }
                                    echo $msg;
                            ?>
                            </div>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Form Business Category
				</div>
				
				
			</div>
			
			<div class="panel-body">
                            
                            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("client/".$mod."/".$id)?>">
	
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Name</label>
						
						<div class="col-sm-5">
                                                    <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="field-1" placeholder="Category Name" required="required">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-3" class="col-sm-3 control-label">Description</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" value="<?php echo $description; ?>"  name="description" id="field-1" placeholder="description" required="required">
						</div>
					</div>
                                        
                                    	
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
                                                    <input type="hidden" name="idhidden" value="<?php echo $id;?>">
                                                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							<button type="submit" class="btn btn-default">Save</button>
                                                        <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url("client/category")?>';">Cancel</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

