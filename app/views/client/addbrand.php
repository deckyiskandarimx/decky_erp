<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('client'); ?>">Client</a>
    </li>
    <li class="active">
        <strong>Form</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('add_brand'); ?>
<?php echo $this->session->flashdata('sukses_hapus_brand'); ?>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Form Client
                </div>


            </div>
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("client/" . $mod . "/" . $id) ?>">
                <div class="panel-body">



                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Client Name</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="field-1" placeholder="Client Name" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Business Category</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="category" id="category" disabled="disabled">
                                <option value="0">-- select category --</option>
                                <?php foreach ($category as $data): ?>
                                    <option value="<?php echo $data->business_category_id; ?>" <?php echo $selectcat == $data->business_category_id ? "selected='selected'" : ""; ?>><?php echo $data->category_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Phone</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $phone; ?>"  name="phone" id="field-1" placeholder="Phone Number" disabled="disabled">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">NPWP</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $npwp; ?>"  name="npwp" id="field-1" placeholder="NPWP Number" disabled="disabled">
                        </div>
                    </div>

                </div>


                <hr>
                <div class="panel-body">

                    <table class="table table-bordered datatable" id="table-4">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>&nbsp;</th>
                                <th>Brand Name</th>                                                       
                                <th>Actions</th>

                            </tr>
                        </thead>
                        <?php if (sizeof($brandlist) > 0): ?>
                            <tbody>
                                <?php
                                $num = 1;
                                foreach ($brandlist as $data):
                                    ?>
                                    <tr class="odd gradeX">
                                        <td style="width: 15px;"><?php echo $num++; ?></td>
                                        <td style="width: 15px;"><input type="checkbox" name="clientid[]" value="<?php echo $data->client_id ?>"></td>
                                        <td><?php echo $data->brand_name; ?></td>
                                        <td>
                                            <a href = "<?php echo base_url("client/remove_brand/" . $data->brand_id) ?>" 
                                               onclick="return confirmDelete();"
                                               class = "btn btn-default btn-sm btn-icon icon-left" >

                                                <i class = "entypo-pencil"></i>
                                                Remove
                                            </a>
                                            <a href="<?php echo base_url("client/editbrandclient/" . $data->brand_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                                <i class="entypo-erase"></i>
                                                Edit
                                            </a>  
                                            <script type="text/javascript">
                                                function confirmDelete() {
                                                    return confirm('Are you sure you want to delete this brand?');
                                                }

                                                function confirmApprove() {
                                                    return confirm('Are you sure you want to approve this product relation?');
                                                }
                                            </script>
                                        </td>
                                    </tr>		
                            <?php endforeach; ?>
                            </tbody>
<?php endif; ?>
                        <tfoot>
                            <tr>    
                                <th>No.</th>
                                <th>&nbsp;</th>
                                <th>Brand Name</th>                                                    
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>

                </div>

            </form>

        </div>


    </div>
</div>
<div id="ajax_responses" style="display:none;"></div>
<div id="ajax_responses2" style="display:none;"></div>


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>


<script type="text/javascript">


                                                jQuery(document).ready(function ($)
                                                {
                                                    var table = $("#table-4").dataTable({
                                                        "sPaginationType": "bootstrap",
                                                        "oTableTools": {
                                                        },
                                                    });
                                                    $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/addbrandclient/" . $id) ?>\'"> Add Brand</button>');
                                                    $(".dataTables_wrapper select").select2({
                                                        minimumResultsForSearch: -1
                                                    });
                                                });

                                                function removebrand(id) {

                                                    var confirms = confirm("Are You Sure ?");

                                                    if (confirms == true) {
                                                        $.post("<?php echo base_url(); ?>client/deletebrandclient", {brand_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function () {
                                                            location.href = '<?php echo base_url(); ?>client/addbrandlist/<?php echo $id; ?>';
                                                                        });
                                                                    } else {
                                                                        ;
                                                                    }

                                                                }



</script>

