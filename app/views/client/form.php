<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('client'); ?>">Client</a>
    </li>
    <li class="active">
        <strong>Form</strong>
    </li>  
</ol>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary panel-card" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Form Client
                </div>


            </div>
            <form role="form" id="myform" name="myform" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("client/" . $mod . "/" . $id) ?>">
                <div class="panel-body">



                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Client Name</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="field-1" placeholder="Client Name" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Business Category</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="category" id="category" required="required">
                                <option value="0">-- select category --</option>
                                <?php foreach ($category as $data): ?>
                                    <option value="<?php echo $data->business_category_id; ?>" <?php echo $selectcat == $data->business_category_id ? "selected='selected'" : ""; ?>><?php echo $data->category_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client Type</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="package" id="package"  <?php echo sizeof($package) == 0 ? "disabled='disabled'" : ""; ?>> 
                                <option value="0">-- Select Type --</option>
                                <?php foreach ($package as $datapackage): ?>
                                    <option value="<?php echo $datapackage->id; ?>" <?php echo $packageselect == $datapackage->id ? "selected='selected'" : ""; ?>><?php echo $datapackage->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Phone</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $phone; ?>"  name="phone" id="field-1" placeholder="Phone Number" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="payment_type" class="col-sm-3 control-label">Payment Type</label>
                        
                        <div class="col-sm-5">

                            <select class="form-control" name="payment_type" id="payment_type">
                                <option value="0">-- Select Payment Type --</option>
                                <?php foreach($payment_type as $data): ?>
                                    <option value="<?=$data;?>" <?php echo $selected_payment_type == $data ? "selected='selected'":""; ?>><?=$data;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="panel-body">


                    <div class="form-group ngumpet">
                        <label class="col-sm-3 control-label">Country</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="country" id="country" required="required" onchange="run()">
                                <option value="0">-- select country --</option>
                                <?php foreach ($country as $datacountry): ?>
                                    <option value="<?php echo $datacountry->id; ?>" <?php echo $selectcountry == $datacountry->id ? "selected='selected'" : ""; ?>><?php echo $datacountry->country_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div id="negara" class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Country</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $neg; ?>"  name="neg" id="field-1" placeholder="Please Input Your Country" >
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Street</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $address_street; ?>"  name="street" id="field-1" placeholder="Street Address" required="required">
                        </div>
                    </div>


                    <div class="form-group ngumpet">
                        <label for="field-3" class="col-sm-3 control-label">Blok/Kavling</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="blok" value="<?php echo $blok; ?>" placeholder="Blok/Kavling" id="field-1" >
                        </div>
                    </div>

                    <div class="form-group ngumpet">
                        <label for="field-3" class="col-sm-3 control-label">Home Number</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control"  name="nomor" value="<?php echo $nomor; ?>" placeholder="Home Number" id="field-1" >
                        </div>
                    </div>

                    <div class="form-group ngumpet">
                        <label for="field-3" class="col-sm-3 control-label">RT</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $rt; ?>"  name="rt"  placeholder="RT" id="field-1" >
                        </div>
                    </div>

                    <div class="form-group ngumpet">
                        <label for="field-3" class="col-sm-3 control-label">RW</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control"  name="rw" value="<?php echo $rw; ?>" placeholder="RW" id="field-1" >
                        </div>
                    </div>

                    <div class="form-group ngumpet">
                        <label class="col-sm-3 control-label">Province</label>

                        <div class="col-sm-5">
                        <select class="form-control" name="province" id="province">
                                <option value="34">-- Select Province --</option>
                                <?php foreach ($province as $dataprovince): ?>
                                    <option value="<?php echo $dataprovince->id; ?>" <?php echo $selectprovince == $dataprovince->id ? "selected='selected'" : ""; ?>><?php echo $dataprovince->province; ?></option>
                                <?php endforeach; ?>
                            </select>
                            
                        </div>
                    </div>

                    <div class="form-group ngumpet">
                        <label class="col-sm-3 control-label">City</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="city" id="city">
                                <option value="435">-- Select City --</option>
                                <?php foreach ($city as $datacity): ?>
                                    <option value="<?php echo $datacity->id; ?>" <?php echo $selectcity == $datacity->id ? "selected='selected'" : ""; ?>><?php echo $datacity->city; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>




                    <div class="form-group hidden">
                        <label for="field-3" class="col-sm-3 control-label">Kabupaten</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control"  name="kab" value="<?php echo $kab; ?>" placeholder="Kabupaten" id="field-1" >
                        </div>
                    </div>

                    <div class="form-group ngumpet">
                        <label for="field-3" class="col-sm-3 control-label">Kecamatan</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control"  name="kec" value="<?php echo $kec; ?>" placeholder="Kecamatan" id="field-1"  >
                        </div>
                    </div>

                    <div class="form-group ngumpet">
                        <label for="field-3" class="col-sm-3 control-label">Kelurahan</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control"  name="kel" value="<?php echo $kel; ?>" placeholder="Kelurahan" id="field-1"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Zip Code</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $zip; ?>" name="zip" id="field-1" placeholder="Zip Code" >
                        </div>
                    </div>

                </div>



                <hr>
                <div class="panel-body">


                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">NPWP</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $npwp; ?>"  name="npwp" id="field-1" placeholder="NPWP Number" required="required">
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Input Address</label>

                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1"> Similiar With Company Address
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="2"> Other
                            </label>
                        </div>
                    </div>

                    <div id="sek" class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">NPWP Address</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value=""  name="npwpdress" id="field-1" placeholder="NPWP Address">
                        </div>
                    </div>



                </div>


                <hr>
                <div class="panel-body">

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Finance Name</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $fname; ?>" name="fname" id="field-1" placeholder="Finance Name" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Finance Phone</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $fphone; ?>" name="fphone" id="field-1" placeholder="Finance Phone" required="required">
                        </div>
                    </div>  

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Finance Email</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $femail; ?>" name="femail" id="field-1" placeholder="Finance Email" required="required">
                        </div>
                    </div>  

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Media Name</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $mname; ?>" name="mname" id="field-1" placeholder="Media Name" required="required">
                        </div>
                    </div>  

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Media Phone</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $mphone; ?>" name="mphone" id="field-1" placeholder="Media Phone" required="required">
                        </div>
                    </div>  

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Media Email</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $memail; ?>" name="memail" id="field-1" placeholder="Media Email" required="required">
                        </div>
                    </div> 

                    <div class="form-group hidden">
                        <label for="field-1" class="col-sm-3 control-label">Deposit</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="1" id="field-1"  name="deposit"  >
                        </div>
                    </div> 


                </div>
                <hr>
                <div class="panel-body">

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="idhidden" value="<?php echo $id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="text" value="<?php echo $id > 0 ? $tokenstart : ""; ?>" id="csrf_city" style="display: none;">
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url("client/account") ?>';">Cancel</button>
                        </div>
                    </div>

                </div>
                <?php if ($id > 0) { ?>
                    <hr>
                    <div class="panel-body">

                        <table class="table table-bordered datatable" id="table-4">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>&nbsp;</th>
                                    <th>Brand Name</th>                                                       
                                    <th>Actions</th>

                                </tr>
                            </thead>
                            <?php if (sizeof($brandlist) > 0): ?>
                                <tbody>
                                    <?php
                                    $num = 1;
                                    foreach ($brandlist as $data):
                                        ?>
                                        <tr class="odd gradeX">
                                            <td style="width: 15px;"><?php echo $num++; ?></td>
                                    <input type="hidden" name="iden" value="<?php echo $id; ?>">
                                    <td style="width: 15px;"><input type="checkbox" name="clientid[]" value="<?php echo $data->client_id ?>"></td>
                                    <td><?php echo $data->brand_name; ?></td>
                                    <td>
                                        <a href="#" onclick="javascript: removebrand(<?php echo $data->brand_id; ?>);" class="btn btn-default btn-sm btn-icon icon-left">
                                            <i class="entypo-erase"></i>
                                            Remove
                                        </a>

                                        <a href="<?php echo base_url("client/editbrandclient/" . $data->brand_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                            <i class="entypo-erase"></i>
                                            Edit
                                        </a>
                                    </td>
                                    </tr>		
                                <?php endforeach; ?>
                                </tbody>
    <?php endif; ?>
                            <tfoot>
                                <tr>    
                                    <th>No.</th>
                                    <th>&nbsp;</th>
                                    <th>Brand Name</th>                                                    
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
<?php } ?>

            </form>

        </div>


    </div>
</div>
<div id="ajax_responses" style="display:none;"></div>
<div id="ajax_responses2" style="display:none;"></div>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>

<script type="text/javascript">

<?php
//if($id>0){
if ($selectcountry > 0) {
    echo '$("#province").attr("disabled",false);';
    echo '$("#city").attr("disabled",false);';
}
?>


                                jQuery(document).ready(function ($)
                                {
                                    var table = $("#table-4").dataTable({
                                        "sPaginationType": "bootstrap",
                                        "oTableTools": {
                                        },
                                    });
                                    $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/addbrandclient/" . $id) ?>\'"> Add Brand</button>');
                                    $(".dataTables_wrapper select").select2({
                                        minimumResultsForSearch: -1
                                    });
                                });

                                $("#country").change(function () {
                                    $.post("<?php echo base_url(); ?>client/ajax/country", {countryid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                                        $("#ajax_responses").html(data);
                                        $("#ajax_responses").find("script").each(function () {
                                            eval($(this).text());
                                        });
                                    });
                                });

                                $("#province").change(function () {
                                    var token2 = $("#csrf_city").val()
                                    $.post("<?php echo base_url(); ?>client/ajax/province", {provinceid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>': token2}, function (data) {
                                        $("#ajax_responses2").html(data);
                                        $("#ajax_responses2").find("script").each(function () {
                                            eval($(this).text());
                                        });
                                    });
                                });


                                function removebrand(id) {

                                    var confirms = confirm("Are You Sure ?");

                                    if (confirms == true) {
                                        $.post("<?php echo base_url(); ?>client/deletebrandclient", {brand_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function () {
                                            location.href = '<?php echo base_url(); ?>client/<?php echo $mod ?>/<?php echo $id; ?>';
                                                        });
                                                    } else {
                                                        ;
                                                    }

                                                }


                                                function run() {
                                                    var x = document.getElementById("country").value;
                                                    if (x == 2) {
                                                        $(".ngumpet").hide();
                                                        $("#negara").show();
                                                    }
                                                }
                                                

                                                $(document).ready(function () {
                                                    $("#negara").hide();
                                                    $("#sek").hide();
                                                    run();
                                                    document.myform.onclick = function () {
                                                        var y = document.myform.status.value;
                                                        if (y == 2) {
                                                            $("#sek").show();
                                                        } else {
                                                            $("#sek").hide();
                                                        }
                                                    }

                                                   

                                                });



</script>

