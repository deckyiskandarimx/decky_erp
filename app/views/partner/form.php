<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>Add Partner</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('validation_partner_name'); ?>
<h1><?php echo $title; ?></h1>
<br />


<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary panel-card" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <span style="border-left: 3px solid #61bdef;"></span><span class="entypo-vcard"></span> New Partner Form
                </div>
            </div>

            <div class="panel-body">
                <form role="form" 
                      class="form-horizontal validate" 
                      method="post" 
                      action="<?php echo base_url("partner/" . $mod . "/" . $id) ?>" 
                      novalidate="novalidate">
                    <fieldset><legend>Company Information</legend>
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Company Name</label>
                        
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $name; ?>"  name="name" id="name" placeholder="Company Name" required="required">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Country</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="countrylist" id="countrylist" required="required">
                                <option value="1" <?php echo $country == 1 ? "selected='selected'":""; ?>>Indonesia</option>
                                <option value="2" <?php echo $country == 2 ? "selected='selected'":""; ?>>Other</option>
                            </select>                            
                            <input type="text" name="othercountry" value="<?php echo $other_country; ?>" class="form-control" id="othercountry" placeholder="Your Country" style="margin-top: 10px; display: <?php echo $mod == 'edit' && $country == 2 ? '':'none'; ?> ">
                        </div>
                    </div>
                        <div id="infoid" style="<?php echo $mod == "edit" && $country == 2 ? "display: none;":""; ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Province/City</label>

                            <div class="col-sm-push-10">
                                <select class="form-control" name="province" id="province" required="required" style="margin-left: 15px; float: left; width: 200px;">
                                    <option value="0">-- Select Province --</option>
                                    <?php foreach ($province as $dataprovince): ?>
                                        <option value="<?php echo $dataprovince->id; ?>" <?php echo $address_province == $dataprovince->id ? "selected='selected'" : ""; ?>><?php echo $dataprovince->province; ?></option>
                                    <?php endforeach; ?>
                                </select>

                                <select class="form-control" name="city" id="city" required="required" style="margin-left: 15px; float: left; width: 200px;">
                                    <option value="0">-- Select City --</option>
                                    <?php if($address_city > 0 && $mod == "edit"): ?>
                                        <?php foreach ($citylist as $datacityedit): ?>
                                            <option value="<?php echo $datacityedit->id; ?>" <?php echo $address_city == $datacityedit->id ? "selected='selected'" : ""; ?>><?php echo $datacityedit->city; ?></option>
                                        <?php endforeach; ?> 
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">District/Subdistrict</label>

                            <div class="col-sm-push-10">
<!--                                <select class="form-control" name="district" id="district" required="required" style="margin-left: 15px; float: left; width: 150px;">
                                    <option value="0">-- Select district --</option>
                                    <?php //foreach ($district as $datadistrict): ?> 
                                        <option value="<?php //echo $datadistrict->district_id; ?>" <?php //echo $district_cat == $datadistrict->district_id ? "selected='selected'" : ""; ?>><?php //echo $datadistrict->district_name; ?></option>
                                    <?php //endforeach; ?>
                                </select>-->
                                
                                <input type="text" name="district" value="<?php echo $district; ?>" class="form-control" id="districttext" placeholder="District" style="margin-left: 15px; float: left; width: 150px;">
                                
<!--                                <select class="form-control" name="subdistrict" id="subdistrict" required="required"  style="margin-left: 15px; float: left; width: 150px;">
                                    <option value="0">-- Select subdistrict --</option>
                                    <?php //foreach ($subdistrict as $datasubdistrict): ?>
                                        <option value="<?php //echo $datasubdistrict->subdistrict_id; ?>" <?php //cho $subdistrict_cat == $datasubdistrict->subdistrict_id ? "selected='selected'" : ""; ?>><?php //echo $datasubdistrict->subdistrict_name; ?></option>
                                    <?php //endforeach; ?>
                                </select>-->
                                
                                <input type="text" name="subdistrict" value="<?php echo $subdistrict; ?>" class="form-control" id="subdistricttext" placeholder="Sub District" style="margin-left: 15px; float: left; width: 150px;">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Street</label>

                            <div class="col-sm-5">
                                <textarea class="form-control autogrow" name="street" id="street" placeholder="Street" required="required" rows="4"><?php echo $address_street; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Block/Kavling</label>

                            <div class="col-sm-push-10">
                                <input type="text" class="form-control" value="<?php echo $blok; ?>"  name="blok" id="blok" placeholder="Block" maxlength="5" style="margin-left: 15px; margin-right: 10px; width: 70px; float: left;"> &nbsp;
<!--                                 <input type="text" class="form-control" value="<?php //echo $homenumber; ?>"  name="homenumber" id="homenumber" placeholder="number" maxlength="5" style=" margin-right: 10px; width: 70px; float: left; font-size: smaller;"> &nbsp;                                 
                                 <input type="text" class="form-control" value="<?php //echo $rt; ?>"  name="rt" id="rt" placeholder="RT" maxlength="3" style=" margin-left: 15px; margin-right: 10px; width: 100px; float: left;"> &nbsp;
                                <input type="text" class="form-control" value="<?php //echo $rw; ?>"  name="rw" id="rw" placeholder="RW" maxlength="3" style=" margin-right: 10px; width: 70px; float: left;"> &nbsp; -->
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Building Number</label>
                            <div class="col-sm-push-10"><input type="text" class="form-control" value="<?php echo $homenumber; ?>"  name="homenumber" id="homenumber" placeholder="number" maxlength="5" style="margin-left: 15px; margin-right: 10px; width: 100px; float: left;"> &nbsp;                                
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">RT/RW</label>
                            <input type="text" class="form-control" value="<?php echo $rt; ?>"  name="rt" id="rt" placeholder="RT" maxlength="3" style=" margin-left: 15px; margin-right: 10px; width: 70px; float: left;"> &nbsp;
                            <input type="text" class="form-control" value="<?php echo $rw; ?>"  name="rw" id="rw" placeholder="RW" maxlength="3" style=" margin-right: 10px; width: 70px; float: left;"> &nbsp;
                        </div>
                        

                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Zip Code</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $zip; ?>"  name="zip" id="zip" placeholder="Postal Code" maxlength="5" required="required" style="width: 80px;">
                            </div>
                        </div>
                    </div>
                    <div id="infoother" style="<?php echo $mod == "edit" && $country == 2 ? "display: block;":" display: none; "; ?>">
                       
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Street</label>

                            <div class="col-sm-5">
                                <textarea class="form-control autogrow" name="streetot" id="streetot" placeholder="Street" required="required" rows="4"><?php echo $other_street; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Zip Code</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $other_zip; ?>"  name="zipot" id="zipot" placeholder="Postal Code" maxlength="5" required="required" style="width: 80px;">
                            </div>
                        </div>
                    </div>
                    </fieldset>
                    <fieldset><legend>Finance Information</legend>
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Account Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $account_name; ?>"  name="accountname" id="accountname" placeholder="Account Name" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Bank Account</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $bank_acc; ?>"  name="bankacc" id="bankacc" placeholder="Bank Account" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Bank Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $bank_name; ?>"  name="bankname" id="bankname" placeholder="Bank Name" required="required">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">SWIFT ID</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $swiftid; ?>"  name="swiftid" id="swiftid" placeholder="Swift Id" required="required" style="width: 150px;">
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">NPWP</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $npwp; ?>"  name="npwp" id="npwp" placeholder="NPWP Number" maxlength="20" required="required">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Alamat NPWP</label>
                        <div class="col-sm-5">
                                <div class="radio">
                                        <label>
                                            <input type="radio" value="1"  name="npwpaddress" id="npwpaddress" onclick="npwpaddr(this.value);" <?php if($mod == "edit") { echo $address_npwp == 1  ? "checked='checked'":""; } else { echo 'checked="checked"'; }  ?>> Similar With Company Address
                                        </label>
                                </div>
                                <div class="radio">
                                        <label>
                                                <input type="radio" value="2"  name="npwpaddress" id="npwpaddress" onclick="npwpaddr(this.value);" <?php if($mod == "edit") { echo $address_npwp == 2  ? "checked='checked'":""; } else { echo ''; }  ?>> Other
                                        </label>
                                </div>
                            
                        </div>
                       
                    </div>
                    <div id="infonpwp" style="<?php if($mod=="edit"){ echo $address_npwp == 2 ? "display: block; ":"display: none; "; }else{ echo "display: none;"; } ?>">
                        <div class="form-group">
                                <label class="col-sm-3 control-label">Country</label>

                                <div class="col-sm-5">
                                    <select class="form-control" name="countrylistnpwp" id="countrylistnpwp" required="required">                                        
                                        <option value="1" <?php echo $country_npwp == 1 ? "selected='selected'":""; ?>>Indonesia</option>
                                        <option value="2" <?php echo $country_npwp == 2 ? "selected='selected'":""; ?>>Other</option>
                                    </select>

                                    <input type="text" name="othercountrynpwp" class="form-control" id="othercountrynpwp" value="<?php echo $country_other_npwp; ?>" placeholder="Your Country" style="margin-top: 10px; <?php if($mod == "edit"){ echo $country_npwp == 2 ? "display: block;":"display: none;"; }else{ echo 'display: none;'; } ?>">
                                </div>
                        </div>
                        <div id="infoidnpwp" style="<?php echo $mod == "edit" && $country_npwp == 2 ? "display: none;":""; ?>">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Province/City</label>

                                <div class="col-sm-push-10">
                                    <select class="form-control" name="provincenpwp" id="provincenpwp" required="required" style="margin-left: 15px; float: left; width: 200px;">
                                        <option value="0">-- Select Province --</option>
                                        <?php foreach ($province as $dataprovince): ?>
                                            <option value="<?php echo $dataprovince->id; ?>" <?php echo $province_npwp == $dataprovince->id ? "selected='selected'" : ""; ?>><?php echo $dataprovince->province; ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                    <select class="form-control" name="citynpwp" id="citynpwp" required="required" style="margin-left: 15px; float: left; width: 200px;">
                                        <option value="0">-- Select City --</option>
                                        <?php if($citylist2 > 0 && $mod == "edit"): ?>
                                            <?php foreach ($citylist2 as $datacityedit): ?>
                                                <option value="<?php echo $datacityedit->id; ?>" <?php echo $city_npwp == $datacityedit->id ? "selected='selected'" : ""; ?>><?php echo $datacityedit->city; ?></option>
                                            <?php endforeach; ?> 
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">District/Subdistrict</label>

                                <div class="col-sm-push-10">
<!--                                    <select class="form-control" name="districtnpwp" id="districtnpwp" required="required" style="margin-left: 15px; float: left; width: 150px;">
                                        <option value="0">-- Select district --</option>
                                        <?php //foreach ($district as $datadistrict): ?> 
                                            <option value="<?php //echo $datadistrict->district_id; ?>" <?php //echo $district_cat == $datadistrict->district_id ? "selected='selected'" : ""; ?>><?php //echo $datadistrict->district_name; ?></option>
                                        <?php //endforeach; ?>
                                    </select>-->
                                    <input type="text" name="districtnpwp" value="<?php echo $districtnpwp; ?>" class="form-control" id="districtnpwptext" placeholder="District" style="margin-left: 15px; float: left; width: 150px; ">

<!--                                    <select class="form-control" name="subdistrictnpwp" id="subdistrictnpwp" required="required"  style="margin-left: 15px; float: left; width: 150px;">
                                        <option value="0">-- Select subdistrict --</option>
                                        <?php //foreach ($subdistrict as $datasubdistrict): ?>
                                            <option value="<?php //echo $datasubdistrict->subdistrict_id; ?>" <?php //echo $subdistrict_cat == $datasubdistrict->subdistrict_id ? "selected='selected'" : ""; ?>><?php //echo $datasubdistrict->subdistrict_name; ?></option>
                                        <?php //endforeach; ?>
                                    </select>-->
                                    
                                    <input type="text" name="subdistrictnpwp" value="<?php echo $subdistrictnpwp; ?>" class="form-control" id="subdistrictnpwptext" placeholder="Sub District" style="margin-left: 15px; float: left; width: 150px;">
                                </div>
                            </div>
                            
                            


                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Street</label>

                                <div class="col-sm-5">
                                    <textarea class="form-control autogrow" name="streetnpwp" id="streetnpwp" placeholder="Street" required="required" rows="4"><?php echo $street_npwp; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Block/Kavling</label>

                                <div class="col-sm-push-10">
                                    <input type="text" class="form-control" value="<?php echo $blok_npwp; ?>"  name="bloknpwp" id="bloknpwp" placeholder="Block" maxlength="5" style="margin-left: 15px; margin-right: 10px; width: 70px; float: left;"> &nbsp;
                                    <!-- <input type="text" class="form-control" value="<?php //echo $homenumber_npwp; ?>"  name="homenumbernpwp" id="homenumbernpwp" placeholder="number" maxlength="5" style=" margin-right: 10px; width: 70px; float: left; font-size: smaller;"> &nbsp;
                                    <input type="text" class="form-control" value="<?php //echo $rt_npwp; ?>"  name="rtnpwp" id="rtnpwp" placeholder="RT" maxlength="3" style=" margin-right: 10px; width: 70px; float: left;"> &nbsp;
                                    <input type="text" class="form-control" value="<?php //echo $rw_npwp; ?>"  name="rwnpwp" id="rwnpwp" placeholder="RW" maxlength="3" style=" margin-right: 10px; width: 70px; float: left;"> &nbsp;
                                    -->
                                </div>
                            </div>
                            
                            <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Building Number</label>
                            <div class="col-sm-push-10"><input type="text" class="form-control" value="<?php echo $homenumber_npwp; ?>"  name="homenumbernpwp" id="homenumbernpwp" placeholder="Building Number" maxlength="5" style="margin-left: 15px; margin-right: 10px; width: 100px; float: left;"> &nbsp;                                
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">RT/RW</label>
                            <input type="text" class="form-control" value="<?php echo $rt_npwp; ?>"  name="rtnpwp" id="rtnpwp" placeholder="RT" maxlength="3" style=" margin-left: 15px; margin-right: 10px; width: 70px; float: left;"> &nbsp;
                            <input type="text" class="form-control" value="<?php echo $rw_npwp; ?>"  name="rwnpwp" id="rwnpwp" placeholder="RW" maxlength="3" style=" margin-right: 10px; width: 70px; float: left;"> &nbsp;
                        </div>

                        <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Zip Code</label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="<?php echo $zip_npwp; ?>"  name="zipnpwp" id="zipnpwp" placeholder="Zip" maxlength="5" required="required" style="width: 80px;">
                                </div>
                        </div>
                        </div>
                        <div id="infoothernpwp"  style="<?php echo $mod == "edit" && $country_npwp == 2 ? "display: block;":" display: none; "; ?>">
                            
                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Street</label>

                                <div class="col-sm-5">
                                    <textarea class="form-control autogrow" name="streetnpwpot" id="streetnpwpot" placeholder="Street" required="required" rows="4"><?php echo $otherstreet_npwp; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Zip Code</label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="<?php echo $otherzip_npwp; ?>"  name="zipnpwpot" id="zipnpwpot" placeholder="Zip" maxlength="5" required="required" style="width: 80px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    </fieldset>
                    <fieldset><legend>Partner PIC Information</legend>                    
                        
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Sales Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $s_name; ?>"  name="s_name" id="s_name" placeholder="Full Name" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $s_phone; ?>"  name="s_phone" id="s_phone" placeholder="Phone Number" maxlength="12" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Email address</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $s_email; ?>"  name="s_email" id="s_email" placeholder="Email address" data-mask="email" required="required">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset><legend>Finance PIC Information</legend> 
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Finance/PIC Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $o_name; ?>"  name="o_name" id="o_name" placeholder="Full Name" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $o_phone; ?>"  name="o_phone" id="o_phone" placeholder="Phone Number" maxlength="12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Email address</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $o_email; ?>"  name="o_email" id="o_email" placeholder="Email address" data-mask="email" required="required">
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="idhidden" value="<?php echo $id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>partner';">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="ajax_responses" style="display:none;"></div>
<div id="ajax_responses2" style="display:none;"></div>


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>

<script type="text/javascript">

                                $('.datepicker_start').datepicker({
                                    format: 'dd/mm/yyyy',
                                    startDate: '+0d'
                                });

                                $('.datepicker_end').datepicker({
                                    format: 'dd/mm/yyyy',
                                    startDate: '+0d',
                                });

                                jQuery(document).ready(function ($)
                                {
                                    var table = $("#table-4").dataTable({
                                        "sPaginationType": "bootstrap",
                                        "oTableTools": {
                                        },
                                    });
                                    //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/addbrandclient/" . $id) ?>\'"> Add Brand</button>');                
                                    $(".dataTables_wrapper select").select2({
                                        minimumResultsForSearch: -1
                                    });
                                });

                                $("#province").change(function () {
                                    var token2 = $("#xyztoken").val();
                                    $.post("<?php echo base_url(); ?>partner/ajax/province", {provinceid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>': token2}, function (data) {
                                        $("#ajax_responses2").html(data);
                                        $("#ajax_responses2").find("script").each(function () {
                                            eval($(this).text());
                                        });
                                    });
                                });
                                
                                
                                $("#countrylist").change(function(){
                                    if(this.value == 2){
                                        $("#othercountry").css("display","block");
                                        $("#infoother").css("display","block");
                                        $("#infoid").css("display","none");
                                    }else{
                                        $("#othercountry").css("display","none");
                                        $("#infoother").css("display","none");
                                        $("#infoid").css("display","block");
                                    }
                                });
                                
                                
    function  npwpaddr(selected){
        
        if(selected==1){
            $("#infonpwp").css("display","none");
        }else if(selected==2){
            $("#infonpwp").css("display","block");
        }
        
    }
    
    $("#countrylistnpwp").change(function(){
        if(this.value == 2){
            $("#othercountrynpwp").css("display","block");
            $("#infoothernpwp").css("display","block");
            $("#infoidnpwp").css("display","none");
        }else{
            $("#othercountrynpwp").css("display","none");
            $("#infoothernpwp").css("display","none");
            $("#infoidnpwp").css("display","block");
        }
        });

$("#provincenpwp").change(function () {
    var token2 = $("#xyztoken").val();
    $.post("<?php echo base_url(); ?>partner/ajax/provincenpwp", {provinceid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>': token2}, function (data) {
        $("#ajax_responses2").html(data);
        $("#ajax_responses2").find("script").each(function () {
            eval($(this).text());
        });
    });
});



</script>
