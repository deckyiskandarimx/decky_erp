<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('partner');?>">Partner</a>
	</li>
	<li class="active">
		<strong>List</strong>
	</li>
</ol>
<h1><?php echo $title; ?></h1>
<br />
<table class="table table-bordered datatable" id="table-4">
	<thead>
		<tr>
			<th nowrap="true">No.</th>
			<th>&nbsp;</th>
			<th>Product</th>
			<th>Partner</th>
			<th>Price</th>
			<th>Discount</th>
			<th>Approval Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<?php if(sizeof($pp_rel) > 0): ?>
	<tbody>
		<?php  $num = 1; foreach ($pp_rel as $data):?>
		<tr class="odd gradeX">
			<td style="width: 15px;"><?php echo $num++;?></td>
			<td style="width: 15px;"><input type="checkbox" name="pp_id[]" value="<?php echo $data->pp_id?>"></td>
			<td><?php echo $data->name;?></td>
			<td><?php echo $data->partner;?></td>
			<td><?php echo number_format($data->price);?></td>
			<td><?php echo $data->discount;?>%</td>
			<td><?php echo $data->docstatusname;?></td>
			<td>
				<?php if($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07" ){ ?>
				<a href="<?php echo base_url("partner/edit_relation/".$data->pp_id)?>" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>Edit
				</a>
				<?php }?>
				<?php if($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08"){ ?>
				<a href="<?php echo base_url("partner/actpartner/".$data->pp_id."/".$data->status)?>" class="btn <?php echo $data->status == 0 ? "btn-green":"btn-red";?> btn-sm btn-icon icon-left">
					<i class="<?php echo $data->status == 0 ? "entypo-check":"entypo-cancel";?>"></i><?php echo $data->status == 0 ? "Activate":"Deactivate";?>
				</a>
				<?php }?>
			</td> 
		</tr>		
		<?php endforeach; ?>
	</tbody>
	<?php endif; ?>
	<tfoot>
		<tr>
			<th nowrap="true">No.</th>
			<th>&nbsp;</th>
			<th>Product</th>
			<th>Partner</th>
			<th>Price</th>
			<th>Discount</th>
			<th>Approval Status</th>
			<th>Actions</th>
		</tr>
	</tfoot>
</table>

<link rel="stylesheet" href="<?php echo assets;?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets;?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets;?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets;?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets;?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets;?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets;?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets;?>js/select2/select2.min.js"></script>

<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		var table = $("#table-4").dataTable({
			"sPaginationType": "bootstrap",
			"oTableTools": {
			},
		});
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("partner/add_relation")?>\'"> Add Relation</button>');                
        $(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
</script>