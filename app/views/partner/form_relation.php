<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('partner');?>">Partner</a>
	</li>
	<li class="active">
		<strong>Add Partner</strong>
	</li>
</ol>
<h1><?php echo $title;?></h1>
<br />


<div style="color: red">
<?php
if (validation_errors()) {
    echo validation_errors();
}
echo $msg;
?>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add Relation
				</div>
			</div>
			
			<div class="panel-body">
				<form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("partner/".$mod."/".$id)?>">
                    <div class="form-group">
						<label for="field-3" class="col-sm-3 control-label">Relation</label>
						<div class="col-sm-5">
							<select class="form-control" name="product" id="product" required="required">
	                            <option value="0">-- Product --</option>
								<?php foreach ($product as $data): ?>
									<option value="<?php echo $data->product_id; ?>" <?php echo $product_sel == $data->product_id ? "selected='selected'":"";?>><?php echo $data->name; ?></option>
								<?php endforeach;?>
							</select>
							<br />
							<select class="form-control" name="partner" id="partner" required="required">
	                            <option value="0">-- Partner --</option>
								<?php foreach ($partner as $data): ?>
									<option value="<?php echo $data->partner_id; ?>" <?php echo $partner_sel == $data->partner_id ? "selected='selected'":"";?>><?php echo $data->name; ?></option>
								<?php endforeach;?>
							</select>
							<br />
							<select class="form-control" name="partnerstatus" id="partnerstatus" required="required">
	                            <option value="0">-- Partner Status --</option>
								<?php foreach ($partnerstatus as $data): ?>
									<option value="<?php echo $data->id; ?>" <?php echo $partnerstat_sel == $data->id ? "selected='selected'":"";?>><?php echo $data->name; ?></option>
								<?php endforeach;?>
							</select>
							<br />
							<select class="form-control" name="partnertype" id="partnertype" required="required">
	                            <option value="0">-- Partner Type --</option>
								<?php foreach ($partnertype as $data): ?>
									<option value="<?php echo $data->id; ?>" <?php echo $partnertype_sel == $data->id ? "selected='selected'":"";?>><?php echo $data->name; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
                    <div class="form-group">
						<label for="field-3" class="col-sm-3 control-label">Price</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" value="<?php echo $amount; ?>"  name="amount" id="amount" placeholder="Amount" required="required">
							<br />
							<input type="text" class="form-control" value="<?php echo $discount; ?>"  name="discount" id="discount" placeholder="Discount" required="required">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
	                        <input type="hidden" name="pp_id" value="<?php echo $pp_id;?>">
	                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							<button type="submit" class="btn btn-default">Save</button>
	                        <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url()?>partner/relation';">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>