<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>Add Partner</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('validation_partner_name'); ?>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Item Quotation
                </div>


            </div>
            <form role="form" 
                  class="form-horizontal form-groups-bordered" 
                  method="post" 
                  action="<?php echo base_url("campaign/" . $mod . "/" . $idquotation) ?>">
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Product Partner</label>

                        <div class="col-sm-5">
                            <select class="form-control" 
                                    name="ppid" 
                                    id="ppid" 
                                    required="required">
                                <option value="0">-- select Product Partner --</option>
                                <?php foreach ($pp_rel as $datapp): ?>
                                    <option value="<?php echo $datapp->pp_id; ?>" <?php echo $ppid == $datapp->pp_id ? "selected='selected'" : ""; ?>><?php echo $datapp->name; ?> - <?php echo $datapp->partner; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" 
                               class="col-sm-3 control-label">Product Price</label>
                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $price; ?>"  
                                   name="price" 
                                   id="price" 
                                   placeholder="Price" 
                                   readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Selling</label>
                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $selling_price; ?>"  
                                   name="sell" 
                                   id="sellingprice" 
                                   placeholder="Discount" 
                                   readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Discount</label>
                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $discount; ?>"  
                                   name="discount" 
                                   id="discount" 
                                   placeholder="Discount" 
                                   readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Amount</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $amount; ?>"  name="amount" id="amount" placeholder="Amount" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Target</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $target; ?>"  name="target" id="field-1" placeholder="Target" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Unit</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $unit; ?>"  name="unit" id="field-1" placeholder="Unit" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Province</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="province" id="province" required="required">
                                <option value="0">-- Select Province --</option>
                                <?php foreach ($province as $dataprovince): ?>
                                    <option value="<?php echo $dataprovince->id; ?>" <?php echo $selectprovince == $dataprovince->id ? "selected='selected'" : ""; ?>><?php echo $dataprovince->province; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">City</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="city" id="city" required="required">
                                <option value="0">-- Select City --</option>
                                <?php foreach ($city as $datacity): ?>
                                    <option value="<?php echo $datacity->id; ?>" <?php echo $selectcity == $datacity->id ? "selected='selected'" : ""; ?>><?php echo $datacity->city; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Area</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $area; ?>"  name="area" id="field-1" placeholder="Area" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Start Date</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker_start" value="<?php echo $startdate; ?>" name="startdate" placeholder="Pick Start Date" style="cursor: pointer;" data-start-date="-2d" data-end-date="+1w" readonly="readonly" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">End Date</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker_end" value="<?php echo $enddate; ?>" name="enddate" placeholder="Pick End Date" style="cursor: pointer;" data-start-date="-2d" data-end-date="+1w" readonly="readonly" required="required">
                        </div>
                    </div>

                    <hr/>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="idhidden" value="<?php echo $id; ?>">
                            <input type="hidden" name="idquotation" value="<?php echo $idquotation; ?>">
                            <input type="text" value="<?php echo $idquotation > 0 ? $tokenstart : ""; ?>" id="csrf_city" style="display: none;">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>campaign/quotation';">Back To Index</button>
                        </div>
                    </div>

                    <hr/>

                    <div class="panel-body">

                        <table class="table table-bordered datatable" id="table-4">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>&nbsp;</th>
                                    <th>Product Partner</th>        
                                    <th>Price</th>       
                                    <th>Discount</th>       
                                    <th>Amount</th>       
                                    <th>Target</th>       
                                    <th>Unit</th>
                                    <th>Province</th>
                                    <th>City</th>
                                    <th>Area</th>
                                    <th>Start Date</th>     
                                    <th>End Date</th>       
                                    <th>Actions</th>

                                </tr>
                            </thead>
                            <?php if (sizeof($listitem) > 0): ?>
                                <tbody>
                                    <?php
                                    $num = 1;
                                    foreach ($listitem as $data):
                                        ?>
                                        <tr class="odd gradeX">
                                            <td style="width: 15px;"><?php echo $num++; ?></td>
                                            <td style="width: 15px;"><input type="checkbox" name="clientid[]" value="<?php echo $data->client_id ?>"></td>
                                            <td><?php echo $data->name; ?></td>
                                            <td><?php echo $data->price; ?></td>
                                            <td><?php echo $data->discount; ?></td>
                                            <td><?php echo $data->amount; ?></td>
                                            <td><?php echo $data->target; ?></td>
                                            <td><?php echo $data->unit; ?></td>
                                            <td><?php echo $data->province; ?></td>
                                            <td><?php echo $data->city; ?></td>
                                            <td><?php echo $data->area; ?></td>
                                            <td><?php echo $data->start_date; ?></td>
                                            <td><?php echo $data->end_date; ?></td>
                                            <td>
                                                <a href="#" onclick="javascript: removeitem(<?php echo $data->item_id; ?>);" class="btn btn-default btn-sm btn-icon icon-left">
                                                    <i class="entypo-erase"></i>
                                                    Remove
                                                </a>    
                                            </td>
                                        </tr>		
                                    <?php endforeach; ?>
                                </tbody>
                            <?php endif; ?>
                            <tfoot>
                                <tr>    
                                    <th>No.</th>
                                    <th>&nbsp;</th>
                                    <th>Product Partner</th>        
                                    <th>Price</th>       
                                    <th>Discount</th>       
                                    <th>Amount</th>       
                                    <th>Target</th>       
                                    <th>Unit</th>
                                    <th>Province</th>
                                    <th>City</th>
                                    <th>Area</th>
                                    <th>Start Date</th>     
                                    <th>End Date</th>       
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>

                </div>


            </form>

        </div>


    </div>
</div>
<div id="ajax_responses" style="display:none;"></div>
<div id="ajax_responses2" style="display:none;"></div>


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>

<script type="text/javascript">


                                            $('.datepicker_start').datepicker({
                                                format: 'dd/mm/yyyy',
                                                startDate: '+0d'
                                            });

                                            $('.datepicker_end').datepicker({
                                                format: 'dd/mm/yyyy',
                                                startDate: '+0d',
                                            });

                                            jQuery(document).ready(function ($)
                                            {
                                                var table = $("#table-4").dataTable({
                                                    "sPaginationType": "bootstrap",
                                                    "oTableTools": {
                                                    },
                                                });
                                                //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/addbrandclient/" . $id) ?>\'"> Add Brand</button>');                
                                                $(".dataTables_wrapper select").select2({
                                                    minimumResultsForSearch: -1
                                                });
                                            });



                                            $("#province").change(function () {
                                                var token2 = $("#csrf_city").val();
                                                $.post("<?php echo base_url(); ?>client/ajax/province", {provinceid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>': token2}, function (data) {
                                                    $("#ajax_responses2").html(data);
                                                    $("#ajax_responses2").find("script").each(function () {
                                                        eval($(this).text());
                                                    });
                                                });
                                            });

                                            $("#ppid").change(function () {
                                                var token2 = $("#csrf_city").val();
                                                $.post("<?php echo base_url(); ?>campaign/ajax/ppid", {ppid: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>': token2}, function (data) {
                                                    $("#ajax_responses2").html(data);
                                                    $("#ajax_responses2").find("script").each(function () {
                                                        eval($(this).text());
                                                    });
                                                });

                                            });

                                            function removeitem(id) {

                                                var confirms = confirm("Are You Sure ?");

                                                if (confirms == true) {
                                                    $.post("<?php echo base_url(); ?>campaign/ajax/removeitem", {item_id: id, quotid: <?php echo $idquotation; ?>, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                                                        var dataresult = $.parseJSON(data);
                                                        location.href = '<?php echo base_url(); ?>campaign/additemquotation/' + dataresult.quotid;
                                                    });
                                                } else {
                                                    ;
                                                }

                                            }

</script>

