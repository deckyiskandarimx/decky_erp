<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('add_partner'); ?>
<?php echo $this->session->flashdata('edit_partner'); ?>
<?php echo $this->session->flashdata('add_partner_account_alert'); ?>
<?php echo $this->session->flashdata('edit_partner_account_alert'); ?>
<?php echo $this->session->flashdata('delete_partner_account_alert'); ?>
<h1><?php echo $title; ?></h1>
<br />
<table class="table table-striped table-hover datatable" id="table-4">
    <thead>
        <tr>
            <th style="text-align: center">#</th>
            <!-- <th>&nbsp;</th> -->
            <th>Company</th>
            <!-- <th>Address</th> -->
            <!-- <th>Approval Status</th> -->
            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
            <th style="text-align: center;">Actions</th>
            <?php } else{ echo "<th>&nbsp;</th>"; } ?>
        </tr>
    </thead>
    <?php if (sizeof($partner) > 0): ?>
        <tbody>
            <?php $num = 1;
            foreach ($partner as $data): ?>
                <tr class="odd gradeX">
                    <td align="center"><?php echo $num++; ?></td>
                    <!-- <td style="width: 15px;"><input type="checkbox" name="userid[]" value="<?php // echo $data->partner_id ?>"></td> -->
                    <td width="50%"><?php echo $data->name; ?></td>
                    <!-- <td><?php // echo $data->address_street == ""? $data->other_street.", ".$data->other_country: $data->address_street; ?></td> -->
                    <!-- <td><?php // echo $data->docstatusname; ?></td> -->
                    <td align="center">
                        <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                            <a href="<?php echo base_url("partner/edit/" . $data->partner_id) ?>" class="btn btn-default btn-sm" data-toggle="tooltip" title="Edit Partner">
                                <span class="entypo-pencil"></span>
                            </a>
                            <a href="#" onclick="removeitem(<?php echo $data->partner_id; ?>)" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Partner">
                                <span class="entypo-cancel"></span>
                            </a>
                            <a href="<?php echo base_url("partner/actpartner/" . $data->partner_id . "/" . $data->status) ?>" class=" hidden btn <?php echo $data->status == 1 ? "btn-green" : "btn-red"; ?> btn-sm">
                                <span class="<?php echo $data->status == 1 ? "entypo-check" : "entypo-cancel"; ?>"></i=span><?php echo $data->status == 1 ? "Deactivate" : "Activate"; ?>
                            </a>
                            <a href="<?php echo base_url("partner/add_partner_account/" . $data->partner_id) ?>" class="btn btn-default btn-sm hidden" data-toggle="tooltip" title="Add Account">
                                <span class="entypo-user-add"></span>
                            </a>
                            <a href="<?php echo base_url("partner/manage_partner_account/" . $data->partner_id) ?>" class="btn btn-default btn-icon-users btn-sm" data-toggle="tooltip" title="Manage Partner Accounts">
                                <span class="entypo-users"></span>
                            </a>
                            <a href="<?php echo base_url("partner/getproducts/" . $data->partner_id) ?>" class="btn btn-blue btn-sm" data-toggle="tooltip" title="Manage Product Partner">
                                <span class="entypo-archive"></span>
                            </a>

                        <?php } ?>
                    </td>
                </tr>
        <?php endforeach; ?>
        </tbody>
<?php endif; ?>
    <tfoot>
        <tr>
            <th style="text-align: center">#</th>
            <!-- <th>&nbsp;</th> -->
            <th>Company</th>
            <!-- <th>NPWP</th> -->
            <!-- <th>Approval Status</th> -->
            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
            <th style="text-align: center;">Actions</th>
            <?php }else{ echo "<th>&nbsp;</th>"; } ?>
        </tr>
    </tfoot>
</table>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>

<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-vcard" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("partner/add") ?>\'"> Add Partner</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });




    function removeitem(id) {

        var confirms = confirm("Are You Sure ?");

        if (confirms == true) {
            $.post("<?php echo base_url(); ?>partner/ajax/removeitem", {item_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                var dataresult = $.parseJSON(data);
                location.href = '<?php echo base_url(); ?>partner/index';
            });
        } else {
            ;
        }

    }

</script>
