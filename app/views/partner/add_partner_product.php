<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>Add Partner Product</strong>
    </li>
</ol>
<h1><?php echo $title; ?></h1>
<?php echo $this->session->flashdata('add_partner_product_alert'); ?>

<div>
    <?php
        if (validation_errors()) {
            echo validation_errors();
        }
        echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <h4 class="product-header"><span class="entypo-vcard imx-blue" style="font-size: 20px;"></span><?=$partner_name?>&nbsp;&nbsp;<span class="entypo-progress-3 imx-grey" style="font-size: 20px;"></span><?=$partner_id?></h4>
    </div>

    <div class="col-md-12">
        <div class="panel panel-primary panel-card">
            <div class="panel-heading">
                <div class="panel-title">
                    <span style="border-left: 3px solid #31708F;"></span><span class="entypo-archive"></span>New Partner Product Form
                </div>
            </div>

            <div class="panel-body" style="padding-left: 250px; padding-right: 250px;">
                <form class="form-horizontal validate" method="post" action="<?php echo base_url("partner/insert_partner_product");?>">
                    <div class="form-group">
                        <label for="product_id" class="col-sm-3 control-label">Product</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="product_id" id="product_id">
                                <option value="0" <?php echo set_select('product_id', 0, TRUE); ?>>- Select Product -</option>
                                <?php foreach($products as $product): ?>
                                    <option value="<?=$product->product_id;?>" <?php echo set_select('product_id', $product->product_id); ?> ><?=$product->product_name;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="partner_type" class="col-sm-3 control-label">Partnership Type</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="partner_type" id="partner_type">
                                <option value="2" <?php echo set_select('partner_type', 2, TRUE); ?>>Revenue Share</option>
                                <option value="1" <?php echo set_select('partner_type', 1); ?> >Fixed Price</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="price" class="col-sm-3 control-label">Price (IDR)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo set_value('price'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="discount" class="col-sm-3 control-label">Discount (%)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="discount" id="discount" placeholder="Discount" value="<?php echo set_value('discount'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-3 control-label">Status</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="status" id="status">
                                <option value="0" <?php echo set_select('status', 0, TRUE); ?>>Inactive</option>
                                <option value="1" <?php echo set_select('status', 1); ?>>Active</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                         <label for="telco_unit" class="col-sm-3 control-label">Telco Unit</label>
                         
                         <div class="col-sm-3">
                            <select class="form-control" name="telco_unit" id="telco_unit">
                                <option value="Indosat" <?php echo set_select('telco_unit', 'Indosat', TRUE); ?>>Indosat</option>
                                <option value="XL" <?php echo set_select('telco_unit', 'XL'); ?>>XL</option>
                                <option value="Telkomsel" <?php echo set_select('telco_unit', 'Telkomsel'); ?>>Telkomsel</option>
                                <option value="Three" <?php echo set_select('telco_unit', 'Three'); ?>>Three</option>
                                <option value="Smartfren" <?php echo set_select('telco_unit', 'Smartfren'); ?>>Smartfren</option>
                                <option value="Other" <?php echo set_select('telco_unit', 'Other'); ?>>Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="base_cost" class="col-sm-3 control-label">Base Cost (IDR)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="base_cost" id="base_cost" placeholder="Base Cost" value="<?php echo set_value('base_cost'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="floor_price" class="col-sm-3 control-label">Floor Price (IDR)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="floor_price" id="floor_price" placeholder="Floor Price" value="<?php echo set_value('floor_price'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="isat_rev_share_percent" class="col-sm-3 control-label">Indosat Revenue Share Percent (%)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="isat_rev_share_percent" id="isat_rev_share_percent" placeholder="Indosat Revenue Share Percent" value="<?php echo set_value('isat_rev_share_percent'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="isat_rev_share_fixed" class="col-sm-3 control-label">Indosat Revenue Share Fixed (IDR)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="isat_rev_share_fixed" id="isat_rev_share_fixed" placeholder="Indosat Revenue Share Fixed" value="<?php echo set_value('isat_rev_share_fixed'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="third_party_rev_share_percent" class="col-sm-3 control-label">Third Party Revenue Share Percent (%)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="third_party_rev_share_percent" id="third_party_rev_share_percent" placeholder="Third Party Revenue Share Percent" value="<?php echo set_value('third_party_rev_share_percent'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="third_party_rev_share_fixed" class="col-sm-3 control-label">Third Party Revenue Share Fixed (IDR)</label>
                         
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="third_party_rev_share_fixed" id="third_party_rev_share_fixed" placeholder="Third Party Revenue Share Fixed" value="<?php echo set_value('third_party_rev_share_fixed'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="partner_id" value="<?=$partner_id;?>">
                            <input type="hidden" name="partner_name" value="<?=$partner_name;?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-success"><span class="entypo-floppy"></span> Save</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url("partner/getproducts/" . $partner_id);?>'"><span class="entypo-cancel-squared"></span> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>