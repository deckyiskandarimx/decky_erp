<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>Add Partner Account</strong>
    </li>
</ol>
<h1><?php echo $title; ?></h1>
<?php echo $this->session->flashdata('add_partner_account_alert'); ?>

<div>
    <?php
        if (validation_errors()) {
            echo validation_errors();
        }
        echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <h4 class="product-header"><span class="entypo-vcard imx-blue" style="font-size: 20px;"></span><?=$partner_name?>&nbsp;&nbsp;<span class="entypo-progress-3 imx-grey" style="font-size: 20px;"></span><?=$partner_id?></h4>
    </div>
    <div class="col-md-12">
        <div class="panel panel-primary panel-card" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <span style="border-left: 3px solid #31708F;"></span><span class="entypo-doc-text"></span>New Partner Account Form
                </div>
            </div>

            <div class="panel-body" style="padding-left: 250px; padding-right: 250px;">
                <form class="form-horizontal validate" method="post" action="<?php echo base_url("partner/insert_partner_account");?>">
                    <div class="form-group">
                         <label for="name" class="col-sm-3 control-label">Name</label>
                         
                         <div class="col-sm-5">
                             <input type="text" class="form-control" name="name" id="name" placeholder="Account Name" value="<?php echo set_value('name'); ?>">
                         </div>
                    </div>

                    <div class="form-group">
                         <label for="position" class="col-sm-3 control-label">Position</label>
                         
                         <div class="col-sm-5">
                             <input type="text" class="form-control" name="position" id="position" placeholder="Position" value="<?php echo set_value('position'); ?>">
                         </div>
                    </div> 

                    <div class="form-group">
                         <label for="email" class="col-sm-3 control-label">Email</label>
                         
                         <div class="col-sm-5">
                             <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
                         </div>
                    </div> 

                    <div class="form-group">
                         <label for="phone" class="col-sm-3 control-label">Phone Number</label>
                         
                         <div class="col-sm-5">
                             <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo set_value('phone'); ?>">
                         </div>
                    </div>

                    <?php
                        $manage_partner_account_href = base_url() . "/partner/manage_partner_account/" . $partner_id;
                    ?>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="partner_id" value="<?=$partner_id;?>">
                            <input type="hidden" name="partner_name" value="<?=$partner_name;?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-success"><span class="entypo-floppy"></span> Save</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?=$manage_partner_account_href;?>';"><span class="entypo-cancel-squared"></span> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>