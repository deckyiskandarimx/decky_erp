<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>Manage Partner Account</strong>
        <a class="hidden"> href="<?php echo base_url() . "/partner/manage_partner_account/" . $partner_id; ?>"><strong>Manage Partner Accounts</strong></a>
    </li>
</ol>
<h1><?php echo $title; ?></h1>
<?php echo $this->session->flashdata('manage_partner_account_alert'); ?>
<?php echo $this->session->flashdata('add_partner_account_alert'); ?>
<?php echo $this->session->flashdata('edit_partner_account_alert'); ?>
<?php echo $this->session->flashdata('delete_partner_account_alert'); ?>
<?php echo $this->session->flashdata('edit_partner_pic_alert'); ?>

<div>
    <?php
        if (validation_errors()) {
            echo validation_errors();
        }
        echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12 hidden">
        <h4 class="product-header"><span class="entypo-vcard imx-blue" style="font-size: 20px;"></span><?=$partner_name?>&nbsp;&nbsp;<span class="entypo-progress-3 imx-grey" style="font-size: 20px;"></span><?=$partner_id?></h4>
    </div>

    <div class="col-md-12">
        <div class="panel panel-primary panel-card" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <span style="border-left: 3px solid #31708F;"></span><span class="entypo-users"></span><?=$partner_name?> Account List
                </div>
                <a style="margin-top: 5px; margin-right: 5px;" href="<?php echo base_url("partner/add_partner_account/" . $partner_id) ?>" class="btn btn-info pull-right" data-toggle="tooltip" data-placement="left" title="Add New Account"><span class="entypo entypo-plus"></span><span class="entypo entypo-user"></span></a>
            </div>
            <div class="panel-body" style="padding-left: 10px; padding-right: 10px;">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="vcard">
                            <p>
                                <strong><span style="font-size: 16px"><?=$sales_pic_name?></span></strong><br />
                                <span style="font-size: 14px"><?=$sales_pic_position?></span>
                            </p>
                            <p>
                                <span class="entypo entypo-mail"></span> <?=$sales_pic_email?><br />
                                <span class="entypo entypo-phone"></span> <?=$sales_pic_phone?>
                            </p>
                            <a href="<?php echo base_url("partner/edit_partner_pic/sales/" . $partner_id) ?>" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title="Edit PIC Account"><span class="entypo entypo-pencil"></span></a>
                            <button type="button" class="btn btn-default hidden" data-toggle="modal" title="Delete Account" data-target="#delete-partner-account-modal" data-id="<?=$partner_account->id;?>" data-name="<?=$partner_account->name;?>" data-href="<?=base_url("partner/delete_partner_account/" . $partner_account->id);?>"><span class="entypo entypo-cancel"></span></button>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="vcard">
                            <p>
                                <strong><span style="font-size: 16px"><?=$finance_pic_name?></span></strong><br />
                                <span style="font-size: 14px"><?=$finance_pic_position?></span>
                            </p>
                            <p>
                                <span class="entypo entypo-mail"></span> <?=$finance_pic_email?><br />
                                <span class="entypo entypo-phone"></span> <?=$finance_pic_phone?>
                            </p>
                            <a href="<?php echo base_url("partner/edit_partner_pic/finance/" . $partner_id) ?>" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title="Edit PIC Account"><span class="entypo entypo-pencil"></span></a>
                            <button type="button" class="btn btn-default hidden" data-toggle="modal" title="Delete Account" data-target="#delete-partner-account-modal" data-id="<?=$partner_account->id;?>" data-name="<?=$partner_account->name;?>" data-href="<?=base_url("partner/delete_partner_account/" . $partner_account->id);?>"><span class="entypo entypo-cancel"></span></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php foreach($partner_accounts as $partner_account): ?>
                    <div class="col-sm-3">
                        <div class="vcard">
                            <p>
                                <strong><span style="font-size: 16px"><?=$partner_account->name?></span></strong><br />
                                <span style="font-size: 14px"><?=$partner_account->position?></span>
                            </p>
                            <p>
                                <span class="entypo entypo-mail"></span> <?=$partner_account->email?><br />
                                <span class="entypo entypo-phone"></span> <?=$partner_account->phone?>
                            </p>
                            <a href="<?php echo base_url("partner/edit_partner_account/" . $partner_account->id) ?>" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title="Edit Account"><span class="entypo entypo-pencil"></span></a>
                            <button type="button" class="btn btn-default" data-toggle="modal" title="Delete Account" data-target="#delete-partner-account-modal" data-id="<?=$partner_account->id;?>" data-name="<?=$partner_account->name;?>" data-href="<?=base_url("partner/delete_partner_account/" . $partner_account->id);?>"><span class="entypo entypo-cancel"></span></button>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>