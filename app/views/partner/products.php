<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>Products</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('add_partner'); ?>
<?php echo $this->session->flashdata('edit_partner'); ?>
<?php echo $this->session->flashdata('add_partner_product_alert'); ?>
<?php echo $this->session->flashdata('edit_partner_product_alert'); ?>
<?php echo $this->session->flashdata('delete_partner_product_alert'); ?>
<h1><?php echo $title; ?></h1>
<div class="row">
    <div class="col-md-12">
        <h4 class="product-header"><span class="entypo-vcard imx-blue" style="font-size: 20px;"></span><?=$partner_name?>&nbsp;&nbsp;<span class="entypo-progress-3 imx-grey" style="font-size: 20px;"></span><?=$id?></h4>
    </div>
</div>
<table class="table table-hover datatable" id="table-4">
    <thead>
        <tr>
            <th>#</th>
            <!-- <th>&nbsp;</th> -->
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Product Family</th>
            <th style="text-align:center">Revenue Share</th>
            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
                <th style="text-align:center;">Actions</th>
            <?php }else{ echo "<th>&nbsp;</th>"; } ?>
        </tr>
    </thead>
    <?php if (sizeof($prodpart) > 0): ?>
        <tbody>
            <?php $num = 1;
            foreach ($prodpart as $data): ?>
                <tr class="odd gradeX">
                    <td style="width: 15px;"><?php echo $num++; ?></td>
                    <!-- <td style="width: 15px;"><input type="checkbox" name="userid[]" value="<?php // echo $data->product_id ?>"></td> -->
                    <td><?php echo $data->code; ?></td>
                    <td><?php echo $data->name; ?></td>
                    <td><?php echo $data->category; ?></td>
                    <td>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="text-align:center" colspan="2">Indosat</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center">Percent (%)</th>
                                    <th style="text-align:center">Fixed (IDR)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center"><?=isset($data->isat_rev_share_percent) ? $data->isat_rev_share_percent : 0;?></td>
                                    <td align="center"><?=isset($data->isat_rev_share_fixed) ? $data->isat_rev_share_fixed : 0;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="text-align:center" colspan="2">Third Party</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center">Percent (%)</th>
                                    <th style="text-align:center">Fixed (IDR)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center"><?=isset($data->third_party_rev_share_percent) ? $data->third_party_rev_share_percent : 0;?></td>
                                    <td align="center"><?=isset($data->third_party_rev_share_fixed) ? $data->third_party_rev_share_fixed : 0;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="text-align:center">Base Cost (IDR)</th>
                                    <th style="text-align:center">Floor Price (IDR)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center"><?=isset($data->base_cost) ? $data->base_cost : 0;?></td>
                                    <td align="center"><?=isset($data->floor_price) ? $data->floor_price : 0;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <br />                        
                    </td>
                    <td align="center">
                        <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                            <a href="#" class="btn btn-default btn-sm btn-icon icon-left hidden" onclick="modalopenedit(<?php echo $data->pp_id; ?>)">
                                <i class="entypo-pencil"></i>Edit
                            </a>
                            &nbsp;
                            <a href="#" onclick="removeitem(<?php echo $data->pp_id; ?>)" class="btn btn-danger btn-sm btn-icon icon-left hidden">
                                <i class="entypo-pencil"></i>Delete
                            </a>
                            &nbsp;
                            <a href="#" onclick="activatepp(<?php echo $data->pp_id.",".$data->status; ?>)" class="btn <?php echo $data->status == 1 ? "btn-red" : "btn-green"; ?> btn-sm btn-icon icon-left hidden">
                                <i class="<?php echo $data->status == 1 ? "entypo-check" : "entypo-cancel"; ?>"></i><?php echo $data->status == 1 ? "Deactivate" : "Activate"; ?>
                            </a>                            
                            <a href="<?php echo base_url("partner/edit_partner_product/" . $data->pp_id) ?>" class="btn btn-default" data-toggle="tooltip" title="Edit Product"><span class="entypo entypo-pencil"></span></a>
                            <button type="button" class="btn btn-default" data-toggle="modal" title="Delete Product" data-target="#delete-partner-product-modal" data-id="<?=$data->pp_id;?>" data-name="<?=$data->name;?>" data-href="<?=base_url("partner/delete_partner_product/" . $data->pp_id);?>"><span class="entypo entypo-cancel"></span></button>
                        <?php } ?>
                    </td> 
                </tr>		
        <?php endforeach; ?>
        </tbody>
<?php endif; ?>
    <tfoot>
        <tr>    
            <th>#</th>
            <!-- <th>&nbsp;</th> -->
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Product Family</th>
            <th>Revenue Share</th>
            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
                <th style="text-align: center;">Actions</th>
            <?php }else{ echo "<th>&nbsp;</th>"; } ?> 
        </tr>
    </tfoot>
</table>
<div id="modalproducts">
    <div class="modal-dialog">
            <div class="modal-content">

                    <div class="modal-header">
                            <button type="button" class="close" aria-hidden="true" onclick="closingmodal();">&times;</button>
                            <h4 class="modal-title">Form Product</h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url("partner/saveproducts"); ?>" method="post" id="propartform">
                                <div class="row">                            
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Product family</label>

                                                <div class="col-sm-5">
                                                    <select class="form-control" name="family" id="family" required="required" onchange="familyselect(this.value)">
                                                        <option value="0">-- Select Family --</option>
                                                        <?php foreach ($family as $datafamily): ?>
                                                        <option value="<?php echo $datafamily->product_family; ?>"><?php echo strtoupper($datafamily->product_family); ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>

                                </div>
                                <div class="row">
				                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Product Name</label>

                                            <div class="col-sm-5">
                                                <select class="form-control" name="product" id="productlist" required="required" onchange="productselect(this.value)">
                                                    <option value="0">-- Select Product --</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Product Code</label>

                                                <div class="col-sm-5">
                                                    <input type="text" name="code" id="code" readonly="readonly" style="padding-left: 15px;">
                                                </div>

                                            </div>
                                        </div>
                                </div>
                            
                                <div class="row">
                                        <div class="col-md-20">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Business Model</label>

                                                <div class="col-sm-5">
                                                    <input type="radio" name="bm" id="bm" value="1" onclick="getprice(this.value)"> Revenue Sharing &nbsp;&nbsp;
                                                    <input type="radio" name="bm" id="bm" value="2" onclick="getprice(this.value)"> Fixed Price
                                                </div>

                                            </div>
                                        </div>
                                </div>
                                
                            <div class="row" id="sharep" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">% Share</label>

                                                <div class="col-sm-5">
                                                    <input type="text" name="pshare" id="pshare" style="padding-left: 15px;">
                                                </div>

                                            </div>
                                        </div>
                                </div>
                            
                                <div class="row" id="pricep" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Price (IDR)</label>

                                                <div class="col-sm-5">
                                                    <input type="text" name="price" id="price" style="padding-left: 15px;">
                                                </div>

                                            </div>
                                        </div>
                                </div>
                            
                         </form>
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" name="tokenxyz" id="tokenxyz" value="<?php echo $this->security->get_csrf_hash(); ?>" style="display: none;">                        
                        <input type="hidden" name="tokenxyz2" id="tokenxyz2" value="<?php echo $this->security->get_csrf_hash(); ?>" style="display: block;">   
                        <input type="hidden" name="idpartner" id="idpartner" value="<?php echo $id; ?>">
                        <input type="hidden" name="idpp" id="idpp" value="">
                        <button type="button" class="btn btn-default" onclick="closingmodal();">Closes</button>
                            <button type="button" class="btn btn-info" id="savingall" onclick="savingall();">Save changes</button>
                    </div>
            </div>
    </div>
</div>    
<div id="jsproduk" style="display: none;"></div>
<div id="jsproduk2" style="display: none;"></div>
<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>

<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    $("#modalproducts").hide();
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-vcard hidden" style="margin-left: 30px;" onclick="modalopen();"> Add Products</button>');
        $("div.dataTables_length").append('<a href="<?php echo base_url("partner/add_partner_product/" . $id) ?>" class="btn btn-default" style="margin-left: 20px;" data-toggle="tooltip" title="Add Partner Product"><span class="entypo entypo-plus"></span><span class="entypo entypo-archive"></span></a>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
    
    
    function modalopen(){
        
        $("#modal-1").html($("#modalproducts").html());
        jQuery('#modal-1').modal('show');
        
    }
    
    function modalopenedit(id){
        
        var token = $("#tokenxyz").val();
        
        $("#modal-1").html($("#modalproducts").html());
        $.post("<?php echo base_url(); ?>partner/ajax/getdatapropart", {item_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {
            //var dataresult = $.parseJSON(data);
            $("#jsproduk").html(data);
            //$("#modal-1").find('input[id="tokenxyz2"]').val(); 
        });
        jQuery('#modal-1').modal('show');
        
    }
    
    function closingmodal(){
        
         $('#modal-1').modal('hide');
         location.href='<?php echo base_url("partner/getproducts/".$id."");?>';
        
    }
    
    function removeitem(id) {

        var confirms = confirm("Are You Sure ?");

        if (confirms == true) {
            var token = $("#tokenxyz").val();
            $.post("<?php echo base_url(); ?>partner/ajax/productremoveitem", {item_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {                
                 location.href = '<?php echo base_url(); ?>partner/getproducts/<?php echo $id; ?>';
            });
        } else {
            ;
        }

    }
    
    
    function activatepp(id,st){
    
        var token = $("#tokenxyz").val();
        
        $.post("<?php echo base_url(); ?>partner/ajax/activatepp", {item_id: id, st: st, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {                
              location.href = '<?php echo base_url(); ?>partner/getproducts/<?php echo $id; ?>';
        });
    }
    
    function familyselect(id){
        
        var token2 = $("#tokenxyz2").val();
        
        $.post("<?php echo base_url(); ?>partner/ajax/productbyid", {item_id: encodeURIComponent(id), '<?php echo $this->security->get_csrf_token_name(); ?>': token2}, function (data) {
                $("#jsproduk").html(data);
        });
    
    }
    
    function productselect(id){
    
        var token = $("#tokenxyz").val();
        $.post("<?php echo base_url(); ?>partner/ajax/codebyid", {item_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {
                $("#jsproduk2").html(data);
        });
    
    }
    
    function getprice(id){
        //alert('You clicked radio!' + id);
        if(id == 1){
            $("#modal-1").find('div[id="sharep"]').css("display","block");
            $("#modal-1").find('div[id="pricep"]').css("display","none");
            $("input#price").attr("readonly",true);
            $("input#pshare").attr("readonly",false);
        }else if(id == 2){
            $("#modal-1").find('div[id="sharep"]').css("display","none");
            $("#modal-1").find('div[id="pricep"]').css("display","block");
            $("input#price").attr("readonly",false);
            $("input#pshare").attr("readonly",true);
        }
    }    
    
    function savingall(){
        
        var token = $("#tokenxyz").val();
        
        
        var product = $("#modal-1").find('select[id="productlist"]').val();
        var idpartner = $("#modal-1").find('input[id="idpartner"]').val(); 
        var bm = $("#modal-1").find('input[name=bm]:checked', '#propartform').val(); //$('input#bm').val(); 
        var price = $("#modal-1").find('input[id="price"]').val();  
        var pshare = $("#modal-1").find('input[id="pshare"]').val(); 
        var idpp = $("#modal-1").find('input[id="idpp"]').val(); 
        //alert(bm);
        
        $.post("<?php echo base_url("partner/saveproducts"); ?>", {product: product, idpartner: idpartner, bm: bm, price: price, pshare: pshare, idpp: idpp, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {
                parent.location.href="<?php echo base_url("partner/getproducts/"); ?>/"+idpartner;
                Query('#modal-1').modal('hide');
        });
        
    }
    
    
    
</script>