<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/client_payment'); ?>">client payment</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/invform'); ?>">Invoice Form</a>
    </li>
    <li class="active">
        <strong>Invoice Form</strong>
    </li>
</ol>

<h1><?php echo $title; ?></h1>
<br />

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Invoice Form
                </div>
            </div>
            
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("payment/saveinvoicegeneral") ?>">
                <div class="panel-body">

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Invoice No.</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $invoice_number; ?>" name="invoice_number" id="invoice_number" placeholder="Invoice Number" <?php echo Payment::getpaymentpaid($io_id_hidden) > 0 ? "readonly='readonly'":""; ?>>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Invoice Date.</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control <?php echo Payment::getpaymentpaid($io_id_hidden) > 0 ? "":"pilihtanggal"; ?>" value="<?php echo $invoice_date; ?>" name="invoice_date" id="invoice_date" placeholder="Invoice Date" <?php echo Payment::getpaymentpaid($io_id_hidden) > 0 ? "readonly='readonly'":""; ?>>
                        </div>
                    </div>
                    
                    <div class="form-group">
                    
                        <div class="col-sm-5">
                            <input type="hidden" name="io_id_hidden" id="io_id_hidden" value="<?php echo $io_id_hidden; ?>">
                            <input type="hidden" name="idpinv" id="idpinv" value="<?php echo $idpinv; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="submit" class="btn btn-default" value="Save" <?php echo Payment::getpaymentpaid($io_id_hidden) > 0 ? "disabled='disabled'":""; ?>>
                            <?php if($idpinv > 0): ?>
                            <input type="button" class="btn btn-danger" id="isicampaign"  value="Show Campaign Item" <?php echo $idpinv=="" ? "disabled='disabled'": ""; ?>>
                            <input type="button" class="btn btn-danger" id="isicampaignhide"  value="Hide Campaign Item" <?php echo $idpinv=="" ? "disabled='disabled'": ""; ?>>
                            <?php endif; ?>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>payment/client_payment';">Cancel</button>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>
<?php if($idpinv > 0): ?>
<div class="row" id="citem_tab">
    <div class="col-md-12">
        
        <div class="panel panel-primary" data-collapsed="0">
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("payment/savedetailpaymentinv") ?>">
            <div class="panel-heading">
                <div class="panel-title">
                    Campaign Item
                </div>
            </div>
            
            <div class="panel-body">
                <table class="table table-bordered datatable" id="table-4">
                    <thead>
                            <tr>
                                    <th>No.</th>                                                            
                                    <th>Item</th>        
                                    <th>Price</th>    
                                    <th>Discount(%)</th>    
                                    <th>Amount</th>    
                                    <th>Total</th>       
                                    <th>Amount Invoice</th>       
                                    <th>Report Document</th>

                            </tr>
                    </thead>
                    <?php //if(sizeof($listio) > 0): ?>
                    <tbody>
                        <?php  $num = 1; $amountprince = 0; foreach ($ppinv_item as $data):   
                            //$discounts = ($data->discount/100)*$data->price; //echo "===>".$data->price;
                            $discounts = 100-$data->discount;
                            $tdisk = ($discounts/100)*$data->price*$data->amount;
                            //echo "===>".$data->price."==>".$data->amount."==>".$tdisk."==>".$data->discount;
                            ?>
                            <tr class="odd gradeX">
                                    <td style="width: 15px;"><?php echo $num++;?></td>                                                            
                                    <td><?php echo $data->name;?></td>    
                                    <td><?php echo number_format($data->price);?></td>    
                                    <td><?php echo number_format($data->discount);?></td>    
                                    <td><?php echo number_format($data->amount);?></td>    
                                    <td><?php echo number_format($tdisk); ?></td>
                                    <td>
                                        <?php if(Payment::cekpaymenttype($io_id_hidden) == "Postpaid") { ?>
                                                <input type="text" name="amount_invoice[<?php echo $data->payinvo_item_id; ?>]" class="ttl_pym" value="<?php echo $data->amount_invoice == 0 ? 0: $data->amount_invoice; ?>" <?php echo Payment::getpaymentpaid($io_id_hidden) > 0 ? "readonly='readonly'":""; ?>></td>                                    
                                        <?php }else{ ?>
                                                <input type="text" name="amount_invoice[<?php echo $data->payinvo_item_id; ?>]" class="ttl_pym" value="<?php echo $data->amount_invoice == 0 ? $tdisk: $data->amount_invoice; ?>" <?php echo Payment::getpaymentpaid($io_id_hidden) > 0 ? "readonly='readonly'":""; ?>></td>                                    
                                        <?php } ?>
                                    <td>
                                        <?php 
                                        if($data->amount_invoice > 0){
                                            if($data->file == ""):?>
                                        <a href="<?php echo base_url("payment/uploadreportitem/".$data->payinvo_item_id."/".$io_id_hidden."/".$idpinv); ?>"  class="btn btn-default btn-sm btn-icon icon-left">
                                                    <i class="entypo-db-shape"></i>
                                                    Upload Report
                                        </a>    
                                        <?php else :?>
                                        <a href='<?php echo base_url("assets/invoicereport/".$data->file) ?>'>Download File</a>
                                        <?php endif; }?>
                                    </td>
                            </tr>		
                        <?php $amountprince = $amountprince + ($tdisk); 
                             
                        endforeach; ?>
                    </tbody>
                    <?php //endif; ?>
                    <tfoot>
                            <tr>    
                                    <th>No.</th>                                                            
                                    <th>Item</th>    
                                    <th>Price</th>    
                                    <th>Discount(%)</th>
                                    <th>Amount</th>    
                                    <th>Total</th>       
                                    <th>Amount Invoice</th>       
                                    <th>Actions</th>
                            </tr>
                    </tfoot>
            </table>
                
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Total.</label>

                    <div class="col-sm-5">
                        <?php 
                            if(Payment::cekpaymenttype($io_id_hidden) == "Postpaid"){
                                if($invoice_total == 0){
                                    $totaly = 0;
                                }else{
                                    $totaly = $invoice_total;
                                }
                            }else{
                                if($invoice_total == 0){
                                    $totaly = $amountprince;
                                }else{
                                    $totaly = $invoice_total;
                                }                                
                            }
                        ?>
                        <input type="text" class="form-control"  name="total" id="total" readonly="readonly" value="<?php echo $totaly; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Tax PPN.</label>
                    <div class="col-sm-5">
                        <?php 
                            if(Payment::cekpaymenttype($io_id_hidden) == "Postpaid"){
                                if($invoice_tax == 0){
                                    $taxtotaly = 0;
                                }else{
                                    $taxtotaly = $invoice_tax;
                                }
                            }else{
                                if($invoice_tax == 0){
                                    $taxtotaly = (10/100)*$amountprince;
                                }else{
                                    $taxtotaly = $invoice_tax;
                                }                                
                            }
                        ?>
                        <input type="text" class="form-control" name="tax" id="tax" readonly="readonly" value="<?php echo $taxtotaly; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Variance</label>
                    <div class="col-sm-5">
                        <?php                         
                                if(Payment::cekpaymenttype($io_id_hidden) > "Postpaid"){
                                    if($taxtotaly > 0){
                                        $var = $invoice_variance == 0 ? 0:$invoice_variance;
                                    }else{
                                        $var = $invoice_variance == 0 ? $amountprince:$invoice_variance;
                                    }
                                }else{
                                    $var = 0;
                                }
                         ?>
                        <input type="text" class="form-control" value="<?php echo $var; ?>" name="variance" id="variance" readonly="readonly">
                    </div>
                </div>
                
                <div class="form-group">
                    
                    <div class="col-sm-5">
                        <input type="hidden" name="io_id_hidden" id="io_id_hidden" value="<?php echo $io_id_hidden; ?>">
                        <input type="hidden" name="hidden_quotation_amount" id="hidden_quotation_amount" value="<?php echo $amountprince; ?>">
                        <input type="hidden" name="idpinv" id="idpinv" value="<?php echo $idpinv; ?>">
                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="submit" class="btn btn-default" value="Save" <?php echo Payment::getpaymentpaid($io_id_hidden) > 0 ? "disabled='disabled'":""; ?>>
                        <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>payment/client_payment';">Back To Client Payment</button>
                    </div>
                </div>
                
            </div>
            </form>
        </div>
    </div>
</div>

<?php endif;?>


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">

$("#citem_tab").show();



$("#isicampaign").hide();

$("#isicampaign").click(function(){
    $("#citem_tab").show();
    $("#isicampaignhide").show();
    $("#isicampaign").hide();
});

$("#isicampaignhide").click(function(){
    $("#citem_tab").hide();
    $("#isicampaignhide").hide();
    $("#isicampaign").show();
});

jQuery(document).ready(function ($)
{
    var table = $("#table-4").dataTable({
        "sPaginationType": "bootstrap",
        "oTableTools": {
        },
    });
    //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="showmodal();"> Add Item</button>');
    $(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });
    
    
});

$(".ttl_pym").keyup(function(){
    var ttl = $("#variance").val();
    var totaldb = "<?php echo $amountprince; ?>";
    var sum = 0;
    $('.ttl_pym').each(function(){
        sum += parseFloat(this.value);
    });
    var ttlpy = parseInt(totaldb) - parseInt(sum);
    var taxm = 10/100;
    if(sum > 0){
        $("#total").attr("value", sum);
        $("#variance").attr("value", ttlpy);
        $("#tax").attr("value", taxm*sum);
    }else{
        $("#total").attr("value", 0);
        $("#variance").attr("value", <?php echo $amountprince; ?>);
    }
});

function showmodal() {
    
    location.href='<?php echo base_url("payment/paymentitem/".$io_id_hidden."/".$idpinv);?>';

}



</script>