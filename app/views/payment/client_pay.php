<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/client_payment'); ?>">Client Payment</a>
    </li>    
    <li class="active">
        <strong>list</strong>
    </li>
</ol>


<div id="notif">
    <?php echo $this->session->flashdata('close_pay'); ?>
</div>
<h3><?php echo ucfirst($title); ?></h3>
<br />

<form action="<?php echo base_url("payment/io/"); ?>" method="post" id="formcategory">
    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>quotation number</th>			
                <th>campaign name</th>			
                <th>IO</th>			
<!--                <th>io number</th>-->
<!--                <th>Partner</th>-->
<!--                <th>Produk</th>-->
                <th>action</th>
                <th>Status</th>
            </tr>
        </thead>

        <?php if (sizeof($io) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($io as $data):
                                
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px; vertical-align: text-bottom;"><?php echo $num++; ?></td>
                        <td style="width: 15px; vertical-align: text-bottom;"><?php echo '<input type="checkbox" name="io_id[]" value="'.$data->io_id.'">'; ?></td>
                        <td style="width: 250px; vertical-align: text-bottom;">
                            <?php echo $data->quotation_number; ?> 
                        </td>
                        <td style="width: 250px; vertical-align: text-bottom;"><?php echo $data->campaign_name; ?></td>
                        <td style="width: 550px; vertical-align: text-bottom;">
                             <?php if($data->package_id > 0){ ?>                                  
                                <table style="width: 100%; font-size: small; border-style: inherit; border-color: inherit; padding: initial;" border="1">    
                                    <thead style="background-color: inherit;">                                                                                                            
                                        <th style="width: 50%; text-align: center;">Package</th>      
                                    </thead>
                                    <tbody>                                             
                                        <?php $nod = 1; foreach (Payment::packagedetailcpay($data->package_id)["io"] as $pcpay2):?>
                                        <tr>
                                            <td style="vertical-align: top; text-align: center;"><?php echo $pcpay2->package_name; ?></td> 
                                        </tr>
                                        <?php endforeach;?>                                   
                                    </tbody>
                                </table>
                             <?php }else{ ?>
                                <table style="width: 100%; font-size: small; border-style: inherit; border-color: inherit; padding: initial;" border="1">    
                                    <thead style="background-color: inherit;">                                                                                                            
                                        <th style="width: 50%; text-align: center;">Product</th>                                
                                        <th style="text-align: center;">Partner</th>
                                    </thead>
                                    <tbody>                                             
                                        <?php $nod = 1; foreach (Payment::productdetailcpay($data->quotation_id)["io"] as $pcpay):?>
                                        <tr>
                                            <td style="vertical-align: top; text-align: center;"><?php echo $pcpay->product_name; ?></td> 
                                            <td style="vertical-align: top; text-align: center;"><?php echo $pcpay->partner_name; ?></td>
                                        </tr>
                                        <?php endforeach;?>                                   
                                    </tbody>
                                </table>     
                             <?php } ?>
                        </td> 
<!--                        <td>
                            <a href="<?php //echo base_url("payment/detail/" . $data->io_id) ?>">
                                <?php //echo $data->io_number; ?>
                            </a>
                        </td>-->
<!--                        <td>
                            <a href="<?php //echo base_url("payment/download_invoice/" . $data->id_invoice) ?>">
                                <?php //echo $data->name_file; ?>
                            </a>
                        </td>-->
<!--                        <td>
                            <?php //echo $data->partner_name; ?>
                        </td>-->
<!--                        <td style="<?php //echo $sessarray[$num-1] == $data->quotation_id ? "border-bottom: 0px; border-top: 0px;":"border-bottom: 0px;"; ?>">
                            <?php //echo $data->product_name; ?>                            
                        </td>-->
                        <td style="width: 250px; vertical-align: text-bottom;">
                            <?php  if(($this->session->userdata("account_type") == "AC04" || 
                                    $this->session->userdata("account_type") == "AC05" || 
                                    $this->session->userdata("account_type") == "AC08") ){ ?>                            
                                    <?php if($data->package_id == ""):  ?>      
                                        <a href="<?php echo Payment::getpaymentinvo($data->quotation_id) ?>" class="btn btn-default btn-sm btn-icon icon-left" style="text-align: center;">
                                            <i class="entypo-pencil"></i>
                                            Invoice 
                                        </a>&nbsp;&nbsp;
                                        <a href="<?php echo base_url("Payment/generateinvoice/".$data->quotation_id); ?>" class="btn btn-blue btn-sm btn-icon icon-left" target="_blank" style="text-align: center;">
                                            <i class="entypo-pencil"></i>
                                            Download
                                        </a>&nbsp;&nbsp;
                                        <?php if(Payment::getpaymentpaid($data->quotation_id) == 0): ?>
                                        <a href="<?php echo base_url("payment/markpaid/" . $data->quotation_id) ?>" onclick="javascript:location.href='<?php echo base_url("payment/markpaid/".$data->quotation_id); ?>'"
                                                class="btn btn-default btn-sm btn-icon icon-left">
                                                 <i class="entypo-pencil"></i>
                                                 Mark Paid
                                         </a>&nbsp;&nbsp;
                                         <?php endif;?>
                                     <?php else:?>
                                        <a href="<?php echo base_url("Payment/generateinvoicepackage/".$data->quotation_id); ?>" class="btn btn-blue btn-sm btn-icon icon-left" target="_blank" style="text-align: center;">
                                            <i class="entypo-pencil"></i>
                                            Download Invoice Package
                                        </a>     &nbsp;&nbsp;
                                        <?php if(Payment::getpaymentpaid($data->quotation_id) == 0): ?>
                                        <a href="<?php echo base_url("payment/markpaidpackage/" . $data->quotation_id) ?>" onclick="javascript:location.href='<?php echo base_url("payment/markpaidpackage/".$data->quotation_id); ?>'"
                                                    class="btn btn-default btn-sm btn-icon icon-left">
                                             <i class="entypo-pencil"></i>
                                             Mark Paid
                                         </a>&nbsp;&nbsp;
                                        <?php endif; ?>
                                     <?php endif;?>
                            <?php } ?>

                        </td>
                        <td style="vertical-align: text-bottom;">
                            <?php 
                                if(Payment::getpaymentpaid($data->quotation_id) > 0){
                                    echo "PAID";
                                }else{
                                    if(Payment::getstatuspayment($data->quotation_id) > 0):  
                                        echo "Invoice Sent";
                                    else :
                                       echo "Invoicing Process"; 
                                    endif;
                                }
                            ?>
                        </td>
                    </tr>                    
                <?php  endforeach; ?>
            </tbody>
        <?php endif; ?>

        <tfoot>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>quotation number</th>			
                <th>campaign name</th>		
                <th>IO</th>			
<!--                <th>io number</th>-->
<!--                <th>Produk</th>-->
<!--                <th>Partner</th>-->
                <th>action</th>
                <th>status</th>
            </tr>
        </tfoot>
    </table>

</form> 

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
                                jQuery(document).ready(function ($)
                                {
                                    var table = $("#table-4").dataTable({
                                        "sPaginationType": "bootstrap",
                                        "oTableTools": {
                                        }
                                    });
                                    //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("payment/proses") ?>\'"> Proses Payment</button>');
                                    $(".dataTables_wrapper select").select2({
                                        minimumResultsForSearch: -1
                                    });
                                });



</script>