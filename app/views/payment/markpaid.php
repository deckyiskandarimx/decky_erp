<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/'); ?>">payment</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/uploadreportitem'); ?>">Report Item</a>
    </li>
    <li class="active">
        <strong>upload</strong>
    </li>
</ol>


<h1><?php echo $title; ?></h1>
<br />

<div class="panel-heading">
    <div class="panel-title">
    </div>
</div>
<html>
    <head>
        <title>Upload Report Item</title>
    </head>
    <body>

        <?php echo $error; ?>

        <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("payment/".$action); ?>">
            Are you sure you want to mark this campaign as paid ?
        <br/><br/>
        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                            
        <input type="hidden" name="quotation_id" id="quotation_id" value="<?php echo $quotation_id; ?>">
        <input type="submit" value="YES" />
        <input type="button" value="NO" onclick="location.href='<?php echo base_url("payment/client_payment"); ?>'" />
        </form>

</body>
</html>
