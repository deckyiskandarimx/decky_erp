<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/client_payment'); ?>">client payment</a>
    </li>
    <li class="active">
        <strong>detail</strong>
    </li>
</ol>

<h2><?php echo $title; ?></h2>
<br />

<div class="panel-heading">

    <div class="panel-title">
        Form Quotation
    </div>


</div>

<form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("payment/" . $mod . "/" . $id) ?>">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary" data-collapsed="0">

                <div class="panel-body">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Io Number</label>

                        <div class="col-sm-5">
                            <input type ="text" 
                                   class="form-control" 
                                   value="<?php echo $io_number; ?>" 
                                   name="io_number" id="field-1" 
                                   placeholder="Io Number" 
                                   disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Campaign Name</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $campaign_name; ?>" 
                                   name="campaign_name" id="field-2" 
                                   placeholder="Campaign Name" 
                                   disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Campaign Item</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $description; ?>" 
                                   name="campaign_item" id="field-2" 
                                   placeholder="Campaign Item" 
                                   disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Total</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $total; ?>" 
                                   name="total" id="field-3" 
                                   placeholder="Total" 
                                   disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Tax</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $tax; ?>" 
                                   name="total" id="field-4" 
                                   placeholder="Tax" 
                                   disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Grand Total</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $grand_total; ?>" 
                                   name="total" id="field-5" 
                                   placeholder="Grand Total" 
                                   disabled="disabled">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Created on</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $created_on; ?>" 
                                   name="total" id="field-5" 
                                   placeholder="created on" 
                                   disabled="disabled">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Created by</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $fullname; ?>" 
                                   name="total" id="field-5" 
                                   placeholder="created by" 
                                   disabled="disabled">
                        </div>
                    </div>
                    
