<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/'); ?>">payment</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/invform'); ?>">Invoice Item Form</a>
    </li>
    <li class="active">
        <strong>Invoice Item Form</strong>
    </li>
</ol>

<h1><?php echo $title; ?></h1>
<br />

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Invoice Item Form
                </div>
            </div>
            
            <form action="<?php echo base_url("payment/saveqitem"); ?>" method="post" name="formmodal1">
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Quotation Item</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="pp_id" id="pp_id" style="width: 350px;">                                                        
                                    <option value="0">-- select Quotation Item --</option>
                                    <?php foreach ($item as $datapp): ?>
                                        <option value="<?php echo $datapp->item_id; ?>"><?php echo $datapp->name; ?> - <?php echo $datapp->partner; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="io_id_hidden" id="io_id_hidden" value="<?php echo $io_id_hidden; ?>">
                            <input type="hidden" name="idpinv" id="idpinv" value="<?php echo $idpinv; ?>">
                    <input type="hidden" id="xyztokenxxxx" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                  
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:closemodal();">Close</button>
                    <button type="submit" class="btn btn-info" onclick="javascript:refdraw();">Select</button>
                </div>
            </form>
        </div>
    </div>
</div>

