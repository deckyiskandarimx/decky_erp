<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/'); ?>">Payment</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/'); ?>">io</a>
    </li>
    <li class="active">
        <strong>list</strong>
    </li>
</ol>


<div id="notif">
    <h5><font color="green"><?php echo $this->session->flashdata('tax'); ?></font></h5>
    <h5><font color="green"><?php echo $this->session->flashdata('sudah'); ?></font></h5>
    <h5><font color="green"><?php echo $this->session->flashdata('update'); ?></font></h5>
    <h5><font color="blue"><?php echo $this->session->flashdata('approved'); ?></font></h5>
    <h5><font color="red"><?php echo $this->session->flashdata('rejected'); ?></font></h5>
    <?php echo $this->session->flashdata('close_pay'); ?>
    <?php echo $this->session->flashdata('sukses_upload_invoice'); ?>
    <?php echo $this->session->flashdata('gagal_upload_invoice'); ?>
</div>
<h2><?php echo $title; ?></h2>
<br />

<form action="<?php echo base_url("payment/io/"); ?>" method="post" id="formcategory">
    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Quotation number</th>			
                <th>campaign name</th>
                <th>Io Number</th>
                <th>campaign item</th>
                <th>Partner</th>
                <th>Status</th>
                <th>Transfer Amount</th>
                <th>Invoice</th>
                <th>action</th>
            </tr>
        </thead>

        <?php if (sizeof($io) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($io as $data):
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td style="width: 15px;"><input type="checkbox" name="io_id[]" value="<?php echo $data->io_id ?>"></td>
                        <td>
                            <?php echo $data->quotation_number; ?>
                        </td>

                        <td>
                            <?php echo $data->campaign_name; ?>
                        </td>

                        <td>
                            <?php echo $data->io_number; ?>
                        </td>

                        <td>
                            <?php echo $data->product_name; ?>
                        </td>

                        <td>
                            <?php echo $data->partner_name; ?>
                        </td>

                        <td>
                            <?php echo $data->io_status; ?>
                        </td>
                        
                        <td style="text-align: right;">
                            <?php echo number_format($data->transfer_amount); ?>
                        </td>
                        
                        <td>
                            <a href="<?php echo base_url("payment/download_invoice/" . $data->invoice_number) ?>">
                                <?php echo $data->invoice_number; ?>
                            </a>
                        </td>
                        <td>
                            <?php
                            
                            if (Payment::cekio($data->io_id) > 0 && ($this->session->userdata("account_type") == "AC04" || $this->session->userdata("account_type") == "AC08")) {
                                if ($data->paid_status == 2) {
                                    ?>
                                    <a href="<?php echo base_url("payment/close_pay/" . $data->io_id) ?>" 
                                       onclick="return confirmClose();"
                                       class="btn btn-default btn-sm btn-icon icon-left"
                                       disabled>
                                        <i class="entypo-pencil"></i>
                                        Proses Payment
                                    </a>
                                <?php } else {
                                    ?>
                                    <a href="<?php echo base_url("payment/close_pay/" . $data->io_id) ?>" 
                                       onclick="return confirmClose();"
                                       class="btn btn-default btn-sm btn-icon icon-left"
                                       >
                                        <i class="entypo-pencil"></i>
                                        Proses Payment
                                    </a><?php
                                }
                                ?>

                            <?php } ?>

                            <script type="text/javascript">
                                function confirmClose() {
                                    return confirm('Are you sure you want to mark this invoice as paid ?');
                                }
                            </script>
                            <?php if ($data->io_status == "paid" || $data->io_status == 'invoiced') { ?>
                                <a href="<?php echo base_url("payment/proses/" . $data->io_id) ?>" 
                                   class="btn btn-default btn-sm btn-icon icon-left"
                                   id="buttonproses"
                                   disabled>
                                    <i class="entypo-pencil"></i>
                                    add invoice
                                </a>
                            <?php } else {
                                ?><a href="<?php echo base_url("payment/proses/" . $data->io_id) ?>" 
                                   class="btn btn-default btn-sm btn-icon icon-left"
                                   id="buttonproses"
                                   >
                                    <i class="entypo-pencil"></i>
                                    add invoice
                                </a>
                                <?php
                            }
                            ?>


                        </td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>    

        <tfoot>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Quotation number</th>			
                <th>campaign name</th>
                <th>Io Number</th>
                <th>campaign item</th>
                <th>Partner</th>
                <th>Status</th>
                <th>Transfer Amount</th>
                <th>Invoice</th>
                <th>action</th>
            </tr>
        </tfoot>
    </table>

</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
                        jQuery(document).ready(function ($)
                        {
                            var table = $("#table-4").dataTable({
                                "sPaginationType": "bootstrap",
                                "oTableTools": {
                                }
                            });
                            //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("payment/proses") ?>\'"> Proses Payment</button>');
                            $(".dataTables_wrapper select").select2({
                                minimumResultsForSearch: -1
                            });
                        });



</script>