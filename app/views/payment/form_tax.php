<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/client_payment'); ?>">client payment</a>
    </li>

    <li class="active">
        <strong>tax upload</strong>
    </li>
</ol>

<div id="notif">

</div>
<?php echo $this->session->flashdata('upload_tax_document'); ?>
<?php echo $this->session->flashdata('hapus_tax_document'); ?>
<h3><?php echo $title; ?></h3>
<br />

<div class="panel-heading">
    <div class="panel-title">
    </div>
</div>

<html>
    <head>

    </head>
    <body>

        <?php echo $error; ?>

        <?php echo form_open_multipart(base_url('/payment/tax_do_upload/')); ?>
        <input type="file" name="userfile" size="20" />
        <br/>

        <input type="submit" value="upload" />

    </form>
    <br>
    <br>
    <h4>
        List Tax Document no : <?php echo $this->session->userdata("io_number") ?>
    </h4>
    <form action="<?php echo base_url("payment/io/"); ?>" method="post" id="formcategory">
        <table class="table table-bordered datatable" id="table-4">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>&nbsp;</th>
                    <th>name file</th>
                    <th>uploaded by</th>			
                    <th>action</th>
                </tr>
            </thead>

            <?php if (sizeof($tax_) > 0): ?>
                <tbody>
                    <?php
                    $num = 1;
                    foreach ($tax_ as $data):
                        ?>
                        <tr class="odd gradeX">
                            <td style="width: 15px;"><?php echo $num++; ?></td>
                            <td style="width: 15px;"><input type="checkbox" name="io_id[]" value="<?php echo $data->io_id ?>"></td>
                            <td>
                                <a href="<?php echo base_url("payment/download_tax/" . $data->id_tax) ?>">
                                    <?php echo $data->name_file; ?></a>
                                </a>
                            </td>
                            <td>
                                <?php echo $data->fullname; ?>
                            </td>
                            <td>
                                <a href = "<?php echo base_url("payment/remove_tax/" . $data->id_tax) ?>" 
                                   onclick="return confirmDelete();"
                                   class = "btn btn-default btn-sm btn-icon icon-left" >
                                    <i class = "entypo-pencil"></i>
                                    Remove
                                </a>
                                <script type="text/javascript">
                                    function confirmDelete() {
                                        return confirm('Are you sure you want to delete this invoice?');
                                    }
                                </script>


                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>

            <tfoot>
                <tr>
                    <th>No.</th>
                    <th>&nbsp;</th>
                    <th>name file</th>
                    <th>uploaded by</th>			
                    <th>action</th>
                </tr>
            </tfoot>
        </table>

    </form>

    <link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
    <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
    <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

    <!-- Bottom Scripts -->

    <script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
    <script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
    <script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
    <script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
    <script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
    <script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



    <div id="ajax_responses" style="display:none;"></div>

    <script type="text/javascript">
                                    jQuery(document).ready(function ($)
                                    {
                                        var table = $("#table-4").dataTable({
                                            "sPaginationType": "bootstrap",
                                            "oTableTools": {
                                            }
                                        });
                                        //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("payment/proses") ?>\'"> Proses Payment</button>');
                                        $(".dataTables_wrapper select").select2({
                                            minimumResultsForSearch: -1
                                        });
                                    });



    </script>


</body>
</html>