<script type = "text/javascript" >
    function startCalc() {
        interval = setInterval("calc()", 1)
    }
    function calc() {
        one = document.autoSumForm.amount.value;
        two = document.autoSumForm.ppn.value;
        three = document.autoSumForm.tax.value;
        document.autoSumForm.transfer.value = (one * 1) + (two * 1) - (three * 1);
    }
    function stopCalc() {
        clearInterval(interval)
    }
</script> 
<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />


<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>
<?php echo $this->session->flashdata('same_invoice_num'); ?>
<?php echo $this->session->flashdata('approve_invoice'); ?>
<?php echo $this->session->flashdata('hapus_invoice'); ?>
<?php echo $this->session->flashdata('sukses_upload_invoice'); ?>
<?php echo $this->session->flashdata('gagal_upload_invoice'); ?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" 
             data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Form Invoice
                </div>


            </div>

            <div class="panel-body">
                <form role="form"
                      name="autoSumForm"
                      class="form-horizontal form-groups-bordered" 
                      method="post" 
                      action="<?php echo base_url("payment/do_upload/".$io_id) ?>"  
                      enctype="multipart/form-data">

                    <input type="hidden" 
                           name="io_id"
                           value="<?php echo $io_id ; ?>">
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Invoice Number</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   name="invoice_num" 
                                   id="field-1" 
                                   value="<?php echo $invoice_number; ?>"
                                   placeholder="Invoice Number" 
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Invoice Date</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control datepicker"  
                                   name="invoice_date"
                                   placeholder="Invoice Date"
                                   readonly="readonly" 
                                   value="<?php echo $invoice_date; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Amount</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control"
                                   name="amount" 
                                   id="amon" 
                                   onFocus="startCalc();" onBlur="stopCalc();"
                                   placeholder="Amount"
                                   value="<?php echo $amount; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Tax Ppn</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control"
                                   name="ppn" 
                                   id="ppn"
                                   onFocus="startCalc();" onBlur="stopCalc();"
                                   placeholder="Tax Ppn" 
                                   value="<?php echo $tax_ppn; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Holding Tax</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control"
                                   name="tax" 
                                   onFocus="startCalc();" onBlur="stopCalc();"
                                   id="field-1" 
                                   placeholder="Holding Tax" 
                                   value="<?php echo $holding_tax; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Transfer Amount</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control"
                                   name="transfer" 
                                   id="field-1"
                                   value=""
                                   placeholder="Transfer Amount" 
                                   value="<?php echo $transfer_amount; ?>"
                                   disabled
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Adjusted</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control"
                                   name="adjusted" 
                                   id="field-1" 
                                   value="<?php echo $adjusted; ?>"
                                   placeholder="Adjusted" 
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">file</label>

                        <div class="col-sm-5"
                             style="border:0px;">
                            <input type="file"  
                                   name="userfile" 
                                   id="field-1" 
                                   required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="idhidden" value="<?php echo $io_id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-default">Save</button>    
                            <button type="button"    
                                    class="btn btn-default"     
                                    onclick="javascript: location.href = '<?php echo base_url('payment'); ?>';">Cancel
                            </button>
                            
                        </div>
                    </div>
                </form>

            </div>

        </div>

        <link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
        <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
        <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

        <!-- Bottom Scripts -->

        <script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
        <script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
        <script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
        <script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
        <script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
        <script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
        <script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
        <script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>



        <div id="ajax_responses" style="display:none;"></div>

        <script type="text/javascript">
    
                                        $('.datepicker').datepicker({
                                            format: 'yyyy-mm-dd',
                                            //startDate: '+0d'
                                        });

                                        jQuery(document).ready(function ($)
                                        {
                                            var table = $("#table-4").dataTable({
                                                "sPaginationType": "bootstrap",
                                                "oTableTools": {
                                                },
                                            });
<?php //if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") {                   ?>
                                            //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("product/add_partner") ?>\'"> Add Partner</button>');
<?php //}                   ?>
                                            $(".dataTables_wrapper select").select2({
                                                minimumResultsForSearch: -1
                                            });
                                        });
                                        function chstate(id, st) {


                                            var c = confirm("Are you sure?");
                                            if (c == true) {
                                                $.post("<?php echo base_url(); ?>user/ajax/chstate", {user_id: id, status: st, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                                                    location.href = '<?php echo base_url() ?>user';
                                                });
                                            } else {
                                                ;
                                            }

                                        }

                                        function simpan(id) {

                                            if (id == 1) {
                                                var label = "Suspend";
                                            } else {
                                                var label = "Activate";
                                            }

                                            $("#formcategory").submit(function () {
                                                return confirm('Are you Sure you want to ' + label + ' this product category?');
                                            })
                                        }


        </script>
    </div>
</div>