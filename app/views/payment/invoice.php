<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/'); ?>">payment</a>
    </li>  
    <li class="active">
        <strong>proses</strong>
    </li>
</ol>

<div id="notif">
</div>


<h3><?php echo $title; ?></h3>
<br />

<div class="panel-heading">
    <?php echo $this->session->flashdata('approve_invoice'); ?>
    <?php echo $this->session->flashdata('hapus_invoice'); ?>
    <?php echo $this->session->flashdata('sukses_upload_invoice'); ?>
    <?php echo $this->session->flashdata('gagal_upload_invoice'); ?>
    <div class="panel-title">
        Form Io
    </div>


</div>

<form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("payment/" . $mod . "/" . $id) ?>">

    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Invoice Name</th>
                <th>Uploaded by</th>
                <th>status</th>
                <th>Actions</th>

            </tr>
        </thead>
        <?php if (sizeof($invoice) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($invoice as $data):
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td style="width: 15px;"><input type="checkbox" name="invoice_id[]" value="<?php echo $data->id_invoice ?>"></td>
                        <td>
                            <a href="<?php echo base_url("payment/download_invoice/" . $data->id_invoice) ?>">
                                <?php echo $data->name_file; ?>
                            </a>
                        </td>
                        <td><?php echo $data->fullname; ?></td>
                        <td><?php echo $data->name; ?></td>
                        <td>
                            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
                            <a href = "<?php echo base_url("payment/approval/" . $data->id_invoice) ?>" 
                               onclick="return confirmApprove();"
                               class = "btn btn-default btn-sm btn-icon icon-left">
                                <i class = "entypo-pencil"></i>
                                Approve
                            </a>
                            <?php } ?>
                            <script type="text/javascript">
                                function confirmApprove() {
                                    return confirm('Are you sure you want to approve this invoice?');
                                }
                            </script>

                            <a href = "<?php echo base_url("payment/remove_invoice/" . $data->id_invoice) ?>" 
                               onclick="return confirmDelete();"
                               class = "btn btn-default btn-sm btn-icon icon-left" >

                                <i class = "entypo-pencil"></i>
                                Remove
                            </a>
                            <script type="text/javascript">
                                function confirmDelete() {
                                    return confirm('Are you sure you want to delete this invoice?');
                                }
                            </script>

                        </td>
                    </tr>		
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>    
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Invoice Name</th>
                <th>Uploaded by</th>
                <th>status</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

    <div id="ajax_responses" style="display:none;"></div>
    <div id="ajax_responses2" style="display:none;"></div>


    <link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
    <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
    <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


    <script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
    <script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
    <script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
    <script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
    <script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
    <script src="<?php echo assets; ?>js/select2/select2.min.js"></script>


    <script type="text/javascript">


                                jQuery(document).ready(function ($)
                                {
                                    var table = $("#table-4").dataTable({
                                        "sPaginationType": "bootstrap",
                                        "oTableTools": {
                                        },
                                    });
                                    $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("payment/upload/" . $data->io_id) ?>\'"> Add Invoice</button>');
                                    $(".dataTables_wrapper select").select2({
                                        minimumResultsForSearch: -1
                                    });
                                });



    </script>