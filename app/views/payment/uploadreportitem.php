<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/'); ?>">payment</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/uploadreportitem'); ?>">Report Item</a>
    </li>
    <li class="active">
        <strong>upload</strong>
    </li>
</ol>


<h1><?php echo $title; ?></h1>
<br />

<div class="panel-heading">
    <div class="panel-title">
    </div>
</div>
<html>
    <head>
        <title>Upload Report Item</title>
    </head>
    <body>

        <?php echo $error; ?>

        <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("payment/uploadreportitem/".$payinv_item_id); ?>"  enctype="multipart/form-data">

        <input type="file" name="reportitem" size="20" />
        <br/>
        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                    
        <input type="hidden" name="payinv_item_id" id="payinv_item_id" value="<?php echo $payinv_item_id; ?>">
        <input type="hidden" name="idqitem" id="idqitem" value="<?php echo $idqitem; ?>">
        <input type="hidden" name="payinv_id" id="payinv_id" value="<?php echo $payinv_id; ?>">
        <input type="submit" value="upload" />

        </form>

</body>
</html>