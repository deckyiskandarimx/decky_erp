
<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
			
			<a href="index.html" class="logo">
				<img src="<?php echo base_url();?>assets/images/logo-imx.png" width="80" alt="" />
			</a>
			
			<p class="description"><strong>Enterprise Resource Planning</strong></p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>                
	</div>
        <?php if($msg=="success"):?>
        <div class="afterchange">
            <h4 style="color: #E30808; text-align: center;">Please Login with your new password account</h4>
        </div>
        <?php endif;?>
	<div class="login-form">
		
		<div class="login-content">
                        
                    
			
			<div class="form-login-error">
				<h3>Invalid login</h3>
<!--				<p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>-->
			</div>
			
                    <form method="post" role="form" id="form_login">
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" />
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
					</div>
				
				</div>
				
				<div class="form-group">
                                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login
					</button>
				</div>
				
			</form>
			
			
			<div class="login-bottom-links">
				
				<a href="<?php echo base_url('auth/forget');?>" class="link">Forgot your password?</a>
				
				<br />
				
				<a href="#">ToS</a>  - <a href="#">Privacy Policy</a>
				
			</div>
			
		</div>
		
	</div>
	
</div>
