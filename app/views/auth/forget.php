<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
			
			<a href="index.html" class="logo">
				<img src="<?php echo base_url();?>assets/images/logo@2x.png" width="120" alt="" />
			</a>
			
			<p class="description">Enter your email, and we will send the reset link.</p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
            
		<div class="login-content">
                    <div style="color: red">
                    <?php
                            if (validation_errors()) {
                                echo validation_errors();
                            }
                    ?>
                    </div>
			
                    <form method="post" role="form" id="form_forgot_password" action="<?php echo base_url()?>auth/forget">
				<?php if($send != ""): ?>
				<div class="">
<!--					<i class="entypo-check"></i>-->
					<h4 style="color: red;">Reset email has been sent.</h4>
<!--					<p>Please check your email, reset password link will expire in 24 hours.</p>-->
				</div>
                                <?php endif;?>
				<div class="form-steps">
					
					<div class="step current" id="step-1">
					
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="entypo-mail"></i>
								</div>
								
								<input type="text" class="form-control" name="email" id="email" placeholder="Email" data-mask="email" autocomplete="off" />
							</div>
						</div>
						
						<div class="form-group">
                                                    <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							<button type="submit" name="send" class="btn btn-info btn-block btn-login">
								Submit<i class="entypo-right-open-mini"></i>
							</button>
						</div>
					
					</div>
					
				</div>
				
			</form>
			
			
			<div class="login-bottom-links">
				
                            <a href="<?php echo base_url()?>" class="link">
					<i class="entypo-lock"></i>
					Return to Login Page
				</a>
				
				<br />
				
				<a href="#">ToS</a>  - <a href="#">Privacy Policy</a>
				
			</div>
			
		</div>
		
	</div>
	
</div>
