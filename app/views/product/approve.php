<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />


<div style="color: red">
                            <?php
                                    if (validation_errors()) {
                                        echo validation_errors();
                                    }
                                    echo $msg;
                            ?>
                            </div>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Form Product
				</div>
				
				
			</div>
			
			<div class="panel-body">
                            
                            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("product/".$mod."/".$id)?>">
	
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Name</label>
						
						<div class="col-sm-5">
                                                    <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="field-1" placeholder="Product Name" required="required">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label class="col-sm-3 control-label">Category</label>
						
						<div class="col-sm-5">
                                                    <select class="form-control" name="category" id="category" required="required">
                                                        <option value="0">-- select category --</option>
                                                            <?php foreach ($category as $data): ?>
                                                                <option value="<?php echo $data->id; ?>" <?php echo $selectcat == $data->id ? "selected='selected'":"";?>><?php echo $data->name; ?></option>
                                                             <?php endforeach;?>
							</select>
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-3" class="col-sm-3 control-label">Selling Price</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" value="<?php echo $selling_price; ?>"  name="selling_price" id="field-1" placeholder="selling price" required="required">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-3" class="col-sm-3 control-label">Description</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" value="<?php echo $description; ?>"  name="description" id="field-1" placeholder="description" required="required">
						</div>
					</div>
                                        
                                        <div class="form-group">
						<label class="col-sm-3 control-label">Status</label>
						
						<div class="col-sm-5">
                                                    <select class="form-control" name="status" id="status" required="required">                                                        
                                                            <?php foreach ($statuselect as $datastatus): ?>
                                                                <option value="<?php echo $datastatus->status; ?>" <?php echo $statusproduct == $datastatus->status ? "selected='selected'":"";?>><?php echo $datastatus->name; ?></option>
                                                             <?php endforeach;?>
							</select>
						</div>
					</div>
                                    	
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
                                                    <input type="hidden" name="idhidden" value="<?php echo $id;?>">
                                                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							<button type="submit" class="btn btn-default">Save</button>
                                                        <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url()?>product/category';">Cancel</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

