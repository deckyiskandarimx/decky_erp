<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />


<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" 
             data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Form Product
                </div>


            </div>

            <div class="panel-body">

                <form role="form" 
                      class="form-horizontal form-groups-bordered" 
                      method="post" 
                      action="<?php echo base_url("product/" . $mod . "/" . $id) ?>">

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Name</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $name; ?>" 
                                   name="name" 
                                   id="field-1" 
                                   placeholder="Product Name" 
                                   required="required"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category</label>

                        <div class="col-sm-5">
                            <select class="form-control" 
                                    name="category" id="category" 
                                    required="required">
                                <option value="0">-- select category --</option>
                                <?php foreach ($category as $data): ?>
                                    <option value="<?php echo $data->id; ?>" <?php echo $selectcat == $data->id ? "selected='selected'" : ""; ?>><?php echo $data->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Selling Price</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $selling_price; ?>"  
                                   name="selling_price" 
                                   id="field-1" 
                                   placeholder="selling price" 
                                   required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Bearer Cost</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control"
                                   value="<?php echo $bearer_cost; ?>"  
                                   name="bearer_cost" 
                                   id="field-1" 
                                   placeholder="Bearer Cost" 
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Partner</label>

                        <div class="col-sm-5">
                            <select class="form-control" 
                                    name="partner" 
                                    id="partner" 
                                    >
                                <option value="0">-- Partner --</option>
                                <?php foreach ($partner as $data): ?>
                                    <option value="<?php echo $data->partner_id; ?>" <?php echo $partner_sel == $data->partner_id ? "selected='selected'" : ""; ?>><?php echo $data->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Description</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $description; ?>"  
                                   name="description" 
                                   id="field-1" 
                                   placeholder="description" 
                                   required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label"></label>

                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" 
                                       name="flag" 
                                       id="status" 
                                       value="1"
                                       <?php
                                       if ($flag == 1) {
                                           ?>checked
                                           <?php
                                       }
                                       ?>
                                       > Isat Product
                            </label>
                            <label class="radio-inline">
                                <input type="radio" 
                                       name="flag" 
                                       id="status" 
                                       value="2"
                                       <?php
                                       if ($flag == 2) {
                                           ?>checked
                                           <?php
                                       }
                                       ?>
                                       > KLOC Product
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="idhidden" value="<?php echo $product_id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="button"    
                                    class="btn btn-default"     
                                    onclick="javascript: location.href = '<?php echo base_url('product'); ?>';">Cancel
                            </button>
                            <button type="submit" class="btn btn-default">Save</button>    
                        </div>
                    </div>
                </form>

            </div>

        </div>

        <link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
        <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
        <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

        <!-- Bottom Scripts -->

        <script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
        <script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
        <script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
        <script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
        <script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
        <script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
        <script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



        <div id="ajax_responses" style="display:none;"></div>

        <script type="text/javascript">
                                        jQuery(document).ready(function ($)
                                        {
                                            var table = $("#table-4").dataTable({
                                                "sPaginationType": "bootstrap",
                                                "oTableTools": {
                                                },
                                            });
<?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                                                $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("product/add_partner") ?>\'"> Add Partner</button>');
<?php } ?>
                                            $(".dataTables_wrapper select").select2({
                                                minimumResultsForSearch: -1
                                            });
                                        });

                                        function chstate(id, st) {


                                            var c = confirm("Are you sure?");
                                            if (c == true) {
                                                $.post("<?php echo base_url(); ?>user/ajax/chstate", {user_id: id, status: st, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                                                    location.href = '<?php echo base_url() ?>user';
                                                });
                                            } else {
                                                ;
                                            }

                                        }

                                        function simpan(id) {

                                            if (id == 1) {
                                                var label = "Suspend";
                                            } else {
                                                var label = "Activate";
                                            }

                                            $("#formcategory").submit(function () {
                                                return confirm('Are you Sure you want to ' + label + ' this product category?');
                                            })
                                        }


        </script>
    </div>
</div>

