<?php echo $this->session->flashdata('sukses_edit'); ?>
<?php echo $this->session->flashdata('sukses_add_product'); ?>   
<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />     
<form action="<?php echo base_url("product/apprvbylist/"); ?>" method="post" id="formcategory">
    <table class="table table-bordered datatable" id="table-4">
        <thead>    
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Category</th>
                <th>Description</th>
                <th>Selling Price</th>
                <th>Bearer Cost</th>
                <th>Actions</th>

            </tr>
        </thead>
        <?php if (sizeof($product) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($product as $data):
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td style="width: 15px;"><input type="checkbox" name="userid[]" value="<?php echo $data->id ?>"></td>
                        <td><?php echo $data->name; ?></td>
                        <td><?php echo $data->katname; ?></td>
                        <td><?php echo $data->description; ?></td>
                        <?php if ($data->selling_price != 0) {
                            ?><td>Rp. <?php echo number_format($data->selling_price); ?></td> 
                        <?php } else {
                            ?><td> - </td>
                        <?php } ?>
                            
                        <?php if ($data->bearer_cost != 0) {
                            ?><td>Rp. <?php echo number_format($data->bearer_cost); ?></td> 
                        <?php } else {
                            ?><td> - </td>
                        <?php } ?>
                        <td>
                            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                                <a href="<?php echo base_url("product/edit/" . $data->product_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                    <i class="entypo-pencil"></i>Edit
                                </a>        
                            <?php } ?>
                            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                                <a href="<?php echo base_url("product/partner_rel/" . $data->product_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                    <i class="entypo-pencil"></i>Partner
                                </a>        
                            <?php } ?>
                            <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
                                <a href="<?php echo base_url("product/actproduct/" . $data->product_id . "/" . $data->status) ?>" class="btn <?php echo $data->status == 1 ? "btn-green" : "btn-red"; ?> btn-sm btn-icon icon-left">
                                    <i class="<?php echo $data->status == 1 ? "entypo-check" : "entypo-cancel"; ?>"></i><?php echo $data->status == 1 ? "Activate" : "Deactivate"; ?>
                                </a>
                            <?php } ?>

                        </td> 
                    </tr>		
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>    
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Category</th>
                <th>Description</th>
                <th>Selling Price</th>  
                <th>Bearer Cost</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
<?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
            $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("product/add") ?>\'"> Add Product</button>');
<?php } ?>
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

    function chstate(id, st) {


        var c = confirm("Are you sure?");
        if (c == true) {
            $.post("<?php echo base_url(); ?>user/ajax/chstate", {user_id: id, status: st, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                location.href = '<?php echo base_url() ?>user';
            });
        } else {
            ;
        }

    }

    function simpan(id) {

        if (id == 1) {
            var label = "Suspend";
        } else {
            var label = "Activate";
        }

        $("#formcategory").submit(function () {
            return confirm('Are you Sure you want to ' + label + ' this product category?');
        })
    }


</script>