<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Package</a>
    </li>
    <li class="active">
        <strong>Subscriber</strong>
    </li>
</ol>

<h1><?php echo $title; ?></h1>
<br />
<table class="table table-bordered datatable" id="table-4">
    <thead>
        <tr>
            <th>No.</th>
            <th>&nbsp;</th>
            <th>Subscriber</th>
            <th>Quotation</th>
            <th>Subscriber Date</th>
            <th>Current Usage</th>
            <th>Unused Quota</th>
            <th>Action</th>
        </tr>
    </thead>
    <?php if (sizeof($prodpart) > 0): ?>
        <tbody>
            <?php $num = 1;
            foreach ($prodpart as $data): ?>
                <tr class="odd gradeX">
                    <td style="width: 15px;"><?php echo $num++; ?></td>
                    <td style="width: 15px;"><input type="checkbox" name="userid[]" value="<?php echo $data->product_id ?>"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td> 
                </tr>		
        <?php endforeach; ?>
        </tbody>
<?php endif; ?>
    <tfoot>
        <tr>    
            <th>No.</th>
            <th>&nbsp;</th>
            <th>Subscriber</th>
            <th>Quotation</th>
            <th>Subscriber Date</th>
            <th>Current Usage</th>
            <th>Unused Quota</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>

<div id="jsproduk" style="display: none;"></div>
<div id="jsproduk2" style="display: none;"></div>
<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>

<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    $("#modalproducts").hide();
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
    
    
    function modalopen(){
        
        $("#modal-1").html($("#modalproducts").html());
        jQuery('#modal-1').modal('show');
        
    }
    
    function modalopenedit(id){
        
        var token = $("#tokenxyz").val();
        
        $("#modal-1").html($("#modalproducts").html());
        $.post("<?php echo base_url(); ?>partner/ajax/getdatapropart", {item_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {
            //var dataresult = $.parseJSON(data);
            $("#jsproduk").html(data);
        });
        jQuery('#modal-1').modal('show');
        
    }
    
    
    function removeitem(id) {

        var confirms = confirm("Are You Sure ?");

        if (confirms == true) {
            var token = $("#tokenxyz").val();
            $.post("<?php echo base_url(); ?>partner/ajax/productremoveitem", {item_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {                
                 location.href = '<?php echo base_url(); ?>partner/getproducts/<?php echo $id; ?>';
            });
        } else {
            ;
        }

    }
    
    
    function activatepp(id,st){
    
        var token = $("#tokenxyz").val();
        
        $.post("<?php echo base_url(); ?>partner/ajax/activatepp", {item_id: id, st: st, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {                
              location.href = '<?php echo base_url(); ?>partner/getproducts/<?php echo $id; ?>';
        });
    }
    
    function familyselect(id){
        
        var token = $("#tokenxyz").val();
        
        $.post("<?php echo base_url(); ?>partner/ajax/productbyid", {item_id: encodeURIComponent(id), '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {
                $("#jsproduk").html(data);
        });
    
    }
    
    function productselect(id){
    
        var token = $("#tokenxyz").val();
        $.post("<?php echo base_url(); ?>partner/ajax/codebyid", {item_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {
                $("#jsproduk2").html(data);
        });
    
    }
    
    function getprice(id){
        //alert('You clicked radio!' + id);
        if(id == 1){
            $("input#price").attr("readonly",true);
            $("input#pshare").attr("readonly",false);
        }else if(id == 2){
            $("input#price").attr("readonly",false);
            $("input#pshare").attr("readonly",true);
        }
    }    
    
    function savingall(){
        
        var token = $("#tokenxyz").val();
        
        
        var product = $("#modal-1").find('select[id="productlist"]').val();
        var idpartner = $("#modal-1").find('input[id="idpartner"]').val(); 
        var bm = $("#modal-1").find('input[name=bm]:checked', '#propartform').val(); //$('input#bm').val(); 
        var price = $("#modal-1").find('input[id="price"]').val();  
        var pshare = $("#modal-1").find('input[id="pshare"]').val(); 
        var idpp = $("#modal-1").find('input[id="idpp"]').val(); 
        //alert(bm);
        
        $.post("<?php echo base_url("partner/saveproducts"); ?>", {product: product, idpartner: idpartner, bm: bm, price: price, pshare: pshare, idpp: idpp, '<?php echo $this->security->get_csrf_token_name(); ?>': token}, function (data) {
                parent.location.href="<?php echo base_url("partner/getproducts/"); ?>/"+idpartner;
                Query('#modal-1').modal('hide');
        });
        
    }
    
    
    
</script>