<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product/listproduct'); ?>">Product</a>
    </li>
    <li class="active">
        <strong>Add Product</strong>
    </li>
</ol>
<!-- <?php // echo $this->session->flashdata('validation_partner_name'); ?> -->
<h1><?php echo $title; ?></h1>
<br />

<?php echo $this->session->flashdata('add_product_alert'); ?>

<div>
    <?php
        if (validation_errors()) {
            echo validation_errors();
        }
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary panel-card" data-collapsed="0" >
            <div class="panel-heading">
                <div class="panel-title">
                    <span style="border-left: 3px solid #31708F;"></span><span class="entypo-doc-text"></span>New Product Input
                </div>
            </div>
            <div class="panel-body" style="padding-left: 250px; padding-right: 250px;">
                <form action="<?php echo base_url("product/insert_product"); ?>" method="post" class="form-horizontal validate">
                    <div class="form-group">
                        <label for="product_code" class="col-sm-2 control-label">Code</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="product_code" id="product_code" value="<?php echo set_value('product_code'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product_name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="product_name" id="product_name" value="<?php echo set_value('product_name'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product_family" class="col-sm-2 control-label">Family</label>

                        <div class="col-sm-3">
                            <select class="form-control" name="product_family" id="product_family">
                                <option value="NONE" <?php echo set_select('product_family', 'NONE', TRUE); ?>>NONE</option>
                                <option value="Message Base Ads" <?php echo set_select('product_family', 'Message Base Ads'); ?>>Message Base Ads</option>
                                <option value="LBA" <?php echo set_select('product_family', 'LBA'); ?>>LBA</option>
                                <option value="SMS BULK" <?php echo set_select('product_family', 'SMS BULK'); ?>>SMS BULK</option>
                                <option value="Voice Ads" <?php echo set_select('product_family', 'Voice Ads'); ?>>Voice Ads</option>
                                <option value="DISPLAY" <?php echo set_select('product_family', 'DISPLAY'); ?>>DISPLAY</option>
                                <option value="DIGITAL REWARD" <?php echo set_select('product_family', 'DIGITAL REWARD'); ?>>DIGITAL REWARD</option>
                                <option value="DATA PROFILLING" <?php echo set_select('product_family', 'DATA PROFILLING'); ?>>DATA PROFILLING</option>
                                <option value="BANNER" <?php echo set_select('product_family', 'BANNER'); ?>>BANNER</option>
                                <option value="PACKAGE" <?php echo set_select('product_family', 'PACKAGE'); ?>>PACKAGE</option>
                                <option value="OTHER" <?php echo set_select('product_family', 'OTHER'); ?>>OTHER</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product_description" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" name="product_description" id="product_description" value="<?php echo set_value('product_description'); ?>"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product_tariff_idr_total" class="col-sm-2 control-label">Price (IDR)</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="product_tariff_idr_total" id="product_tariff_idr_total" value="<?php echo set_value('product_tariff_idr_total'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Status</label>

                        <div class="col-sm-3">
                            <select class="form-control" name="status" id="status">
                                <option value="0" <?php echo set_select('status', 0, TRUE); ?>>Inactive</option>
                                <option value="1" <?php echo set_select('status', 1); ?>>Active</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="minimum_quantity" class="col-sm-2 control-label">Minimum Quantity</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="minimum_quantity" id="minimum_quantity" value="<?php echo set_value('minimum_quantity'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="minimum_budget" class="col-sm-2 control-label">Minimum Budget</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="minimum_budget" id="minimum_budget" value="<?php echo set_value('minimum_budget'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sla" class="col-sm-2 control-label">SLA</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sla" id="sla" value="<?php echo set_value('sla'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sf_pricebook_id" class="col-sm-2 control-label"><span class="salesforce-blue">Salesforce</span> Pricebook Id</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sf_pricebook_id" id="sf_pricebook_id" value="01s28000004nANd">
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 10px; ">
                        <div class="col-sm-12 text-center">                            
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                
                            <button type="submit" class="btn btn-success"><span class="entypo-floppy"></span> Save</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>product/listproduct';"><span class="entypo-cancel-squared"></span> Cancel</button>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>