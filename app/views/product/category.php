<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product/category'); ?>">Product Category</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('edit_category'); ?> 
<?php echo $this->session->flashdata('add_cat'); ?> 
<h1><?php echo $title; ?></h1>
<br />
<form action="<?php echo base_url("product/apprvcategorybylist/"); ?>" method="post" id="formcategory">
    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>

            </tr>
        </thead>
        <?php if (sizeof($category) > 0): ?>
            <tbody>
                <?php $num = 1;
                foreach ($category as $data): ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td style="width: 15px;"><input type="checkbox" name="userid[]" value="<?php echo $data->id ?>"></td>
                        <td><?php echo $data->prodname; ?></td>
                        <td><?php echo $data->description; ?></td>
                        <td>
        <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                                <a href="<?php echo base_url("product/editcategory/" . $data->id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                    <i class="entypo-pencil"></i>
                                    Edit
                                </a>                               
                            <?php } ?>
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                        
        <?php //if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
                <!--                <button type="submit" name="datacategory[]" id="datacategory"  class="btn btn-default btn-sm btn-icon icon-left" value="<?php echo $data->id . "_" . $data->status; ?>" onclick="simpan(<?php echo $data->status; ?>);">
                                    <i class="entypo-pencil"></i>
                                <?php //echo $data->status_desc; ?>
                                </button>
                -->
                            <?php// } ?>
        <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
                                <a href="<?php echo base_url("product/actcategory/" . $data->id . "/" . $data->status) ?>" class="btn <?php echo $data->status == 1 ? "btn-green" : "btn-red"; ?> btn-sm btn-icon icon-left">
                                    <i class="<?php echo $data->status == 1 ? "entypo-check" : "entypo-cancel"; ?>"></i><?php echo $data->status == 1 ? "Activate" : "Deactivate"; ?>
                                </a>
        <?php } ?>
                        </td>
                    </tr>		
            <?php endforeach; ?>
            </tbody>
<?php endif; ?>
        <tfoot>
            <tr>    
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
                        jQuery(document).ready(function ($)
                        {
                            var table = $("#table-4").dataTable({
                                "sPaginationType": "bootstrap",
                                "oTableTools": {
                                },
                            });
                            $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("product/addcategory") ?>\'"> Add Product Category</button>');
                            $(".dataTables_wrapper select").select2({
                                minimumResultsForSearch: -1
                            });
                        });


                        function simpan(id) {

                            if (id == 1) {
                                var label = "Suspend";
                            } else {
                                var label = "Activate";
                            }

                            $("#formcategory").submit(function () {
                                return confirm('Are you Sure you want to ' + label + ' this product category?');
                            })
                        }

</script>