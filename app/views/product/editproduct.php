<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product/listproduct'); ?>">list Product</a>
    </li>
    <li class="active">
        <strong>Edit Product</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('validation_partner_name'); ?>
<h1><?php echo $title; ?></h1>
<br />


<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo $title; ?>
                </div>
            </div>
            <div class="panel-body">
                <form role="form" action="<?php echo base_url("product/savedetailsproduct/".$product_id); ?>" method="post" class="form-horizontal validate">
                    
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Product Code</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $code; ?>"  name="code" id="code" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Product Name</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $name; ?>"  name="name" id="name" readonly="readonly">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Product Family</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $family; ?>"  name="family" id="family" readonly="readonly">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Product Description</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $desc; ?>"  name="desc" id="desc" readonly="readonly">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Product Tariff</label>

                            <div class="col-sm-5">
                                <div style="float: left; padding-top: 8px;"><b>IDR</b></div>
                                <input type="text" class="form-control" value="<?php echo $tariff_idr; ?>"  name="tariff_idr" id="tariff_idr" style="float: left; width: 150px; margin-left: 10px;" disabled="disabled">
                                <div style="float: left; margin-left: 120px; padding-top: 8px;"><b>Tariff Status</b></div>
                                <select class="form-control" name="statusidr" id="statusidr" style="float: left; width: 150px; margin-left: 10px;" disabled="disabled">
                                    <option value="0" <?php echo $statusidr == 0 ? "selected='selected'":""; ?>>Inactive</option>
                                    <option value="1" <?php echo $statusidr == 1 ? "selected='selected'":""; ?>>Active</option>
                                </select>  
                            </div>
                            
                            
                        </div>
                    
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">&nbsp</label>

                            <div class="col-sm-5">
                                <div style="float: left; padding-top: 8px;"><b>USD</b></div>
                                <input type="text" class="form-control" value="<?php echo $tariff_usd; ?>"  name="tariff_usd" id="tariff_usd" style="float: left; width: 150px; margin-left: 10px;"  disabled="disabled">
                                <div style="float: left; margin-left: 120px; padding-top: 8px;"><b>Tariff Status</b></div>
                                <select class="form-control" name="statususd" id="statusidr" style="float: left; width: 150px; margin-left: 10px;"  disabled="disabled">
                                    <option value="0" <?php echo $statususd == 0 ? "selected='selected'":""; ?>>Inactive</option>
                                    <option value="1" <?php echo $statususd == 1 ? "selected='selected'":""; ?>>Activer</option>
                                </select>  
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Status</label>

                            <div class="col-sm-5">
                                <select class="form-control" name="status" id="status" style="width: 150px;" disabled="disabled">
                                    <option value="0" <?php echo $status == 0 ? "selected='selected'":""; ?>>Inactive</option>
                                    <option value="1" <?php echo $status == 1 ? "selected='selected'":""; ?>>Active</option>
                                </select>      
                            </div>
                        </div>
                        
                        <fieldset style="border: 3px;">
                            <legend>Billing and Periode</legend>
                            
                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Base Price</label>

                                <div class="col-sm-5">
                                    <div style="float: left; padding-top: 8px;"><b>IDR</b></div>
                                    <input type="text" class="form-control" value="<?php echo $best_price; ?>"  name="best_price" id="best_price" style="float: left; width: 150px; margin-left: 10px;">                                
                                </div>                            
                            </div>
                            
                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label"><input type="checkbox" name="bearer_cost" value="1" id="bearer_cost" <?php echo $bearer_cost == 1 ? "checked='checked'":""; ?>>&nbsp; Bearer Cost</label>

                                <div class="col-sm-5">
                                    &nbsp;
                                </div>                            
                            </div>
                            
                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Partner</label>

                                <div class="col-sm-5">                                    
                                    <select class="form-control" name="partner_id" id="partner_id" style="width: 150px; margin-left: 20px;">
                                        <option value="0">-- Select Partner --</option>
                                        <?php foreach($partner as $datapartner): ?>
                                                <option value="<?php echo $datapartner->partner_id; ?>" <?php echo $partner_id == $datapartner->partner_id ? "selected='selected'":""; ?>><?php echo $datapartner->name; ?></option>
                                        <?php endforeach; ?>
                                    </select> 
                                </div>                            
                            </div>
                            
                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label">Cost Per Unit</label>

                                <div class="col-sm-5">
                                    <div style="float: left; padding-top: 8px;"><b>IDR</b></div>
                                    <input type="text" class="form-control" value="<?php echo $cost_per_unit; ?>"  name="cost_per_unit" id="cost_per_unit" style="float: left; width: 150px; margin-left: 10px;">                                    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="field-3" class="col-sm-3 control-label"><input type="checkbox" name="isindosat_product" id="isindosat_product" value="1" <?php echo $isindosat_product == 1 ? "checked='checked'":""; ?>>&nbsp; Indosat Product</label>

                                <div class="col-sm-5">       
                                    <div style="float: left; padding-top: 8px;"><input type="checkbox" name="iskloc_product" id="iskloc_product" value="1" <?php echo $iskloc_product == 1 ? "checked='checked'":""; ?>>&nbsp; Kloc Product</div>
                                    
                                </div>
                            </div>
                            
                        </fieldset>
                    
                        <div class="form-group" style="margin-top: 10px; ">
                            <div class="col-sm-offset-3 col-sm-5">                            
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
                                <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                
                                <button type="submit" class="btn btn-default" disabled>Save</button>
                                <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>product/listproduct';">Cancel</button>
                            </div> 
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>