<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product/listproduct'); ?>">Product</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('sukses_add_package'); ?>  
<?php echo $this->session->flashdata('edit_package'); ?>  
<?php echo $this->session->flashdata('syncproduct_alert'); ?>
<?php echo $this->session->flashdata('add_product_alert'); ?>

<h1><?php echo $title; ?></h1>
<br />

    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Product Family</th>
<!--                <th>Status</th>          -->
                <th>Pricebook Id</th>
                <th>Actions</th>
            </tr>
        </thead>
        <?php if (sizeof($product) > 0): ?>
            <tbody>
                <?php $num = 1;
                foreach ($product as $data): ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td><?php echo $data->product_code; ?></td>
                        <td><?php echo $data->product_name; ?></td>
                        <td><?php echo $data->product_family; ?></td>
<!--                        <td><?php //echo $data->status == 0 ? "Inactive":"Active"; ?></td>-->
                        <td><?php echo $data->sf_pricebook_id; ?></td>
                        <td align="center">
                           <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                            <a href="<?php echo base_url("product/editproduct/" . $data->product_id) ?>" class="btn btn-default btn-sm hidden">
                                <i class="glyphicon glyphicon-pencil"></i> Edit
                            </a>
                            <a href="<?php echo base_url("product/syncproduct/" . $data->product_id) ?>" class="btn btn-info btn-sm" data-toggle="tooltip" title="Synchronize to Salesforce"><i class="glyphicon glyphicon-refresh"></i></a>
                            <a href="<?php echo base_url("product/flashproduct")?>" class="hidden btn btn-warning btn-sm"><i class="glyphicon glyphicon-flash"></i> Flash</a>
                            <a href="<?php echo base_url("product/hitcurl")?>" class="hidden btn btn-warning btn-sm"><i class="glyphicon glyphicon-flash"></i> Hit</a>
                            <a href="<?php echo base_url("product/get_salesforce_token_response")?>" class="hidden btn btn-warning btn-sm"><i class="glyphicon glyphicon-magnet"></i> Token</a>
                            <a href="<?php echo base_url("product/manage_product_partner/" . $data->product_id)?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Manage Partners"><i class="glyphicon glyphicon-tower"></i></a>
                        <?php } ?>
                        </td>
                    </tr>		
            <?php endforeach; ?>
            </tbody>
<?php endif; ?>
        <tfoot>
            <tr>    
                <th>No.</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Product Family</th>
<!--                <th>Status</th>          -->
                <th>Pricebook Id</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>



<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-list-add" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("product/importproduct/unpack") ?>\'"> Import</button> <a href="<?php echo assets; ?>template/sample_products.xlsx" class="btn btn-white entypo-docs">Sample</a> <button type="button" class="btn btn-success entypo-plus" onclick="location.href=\'<?php echo base_url("product/add_product") ?>\'"> Add</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });


    function simpan(id) {

        if (id == 1) {
            var label = "Suspend";
        } else {
            var label = "Activate";
        }

        $("#formcategory").submit(function () {
            return confirm('Are you Sure you want to ' + label + ' this product category?');
        })
    }

</script>