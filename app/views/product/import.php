<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('partner'); ?>">Partner</a>
    </li>
    <li class="active">
        <strong>Add Partner</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('validation_partner_name'); ?>
<h1><?php echo $title; ?></h1>
<br />


<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Import Product
                </div>
            </div>
            <div class="panel-body">
                <form role="form" action="<?php echo base_url("product/importing"); ?>" method="post" enctype="multipart/form-data">
                    <fieldset><legend>Import File XLS</legend>
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">File Upload</label>

                            <div class="col-sm-5">
                                <input type="file" class="form-control" value="<?php echo $name; ?>"  name="fileproduct" id="fileproduct" placeholder="File XLS" required="required">
                            </div>
                        </div>                        
                    </fieldset>
                    <div class="form-group" style="margin-top: 10px; ">
                        <div class="col-sm-offset-3 col-sm-5">                            
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="isproduct" value="<?php echo $isproduct == "unpack" ? true:false; ?>">
                            <button type="submit" class="btn btn-default">Upload</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>product/package';">Cancel</button>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>