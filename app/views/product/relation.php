<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />


<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>
<div class="panel panel-primary" data-collapsed="0">
    <div class="panel-heading">
        <?php echo $this->session->flashdata('sukses_add_partner'); ?>
        <?php echo $this->session->flashdata('sukses_edit_partner'); ?>
        <?php echo $this->session->flashdata('sukses_hapus_partner'); ?>
         <?php echo $this->session->flashdata('gagal_hapus_partner'); ?>
        <?php echo $this->session->flashdata('sukses_approve_partner'); ?>
        <div class="panel-title">
            List Partner
        </div>


    </div>

    <div class="panel-body">
        <h3>list partner of <?php echo $product_name; ?></h3>
        <form action="<?php echo base_url("product/apprvbylist/"); ?>" method="post" id="formcategory">
            <table class="table table-bordered datatable" id="table-4">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>&nbsp;</th>
                        <th>Partner</th>
                        <th>Partner Type</th>
                        <th>Fixed Price(idr)</th>
                        <th>Revenue Share(%)</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <?php if (sizeof($product_partner_rel) > 0): ?>
                    <tbody>
                        <?php
                        $num = 1;
                        foreach ($product_partner_rel as $data):
                            ?>
                            <tr class="odd gradeX">
                                <td style="width: 15px;"><?php echo $num++; ?></td>
                                <td style="width: 15px;"><input type="checkbox" name="userid[]" value="<?php echo $data->id ?>"></td>
                                <td><?php echo $data->partner_name; ?></td>
                                <td><?php echo $data->partner_type; ?></td>

                                <?php if ($data->price != 0) {
                                    ?><td>Rp. <?php echo number_format($data->price); ?></td> 
                                <?php } else {
                                    ?><td> - </td>
                                <?php } ?>

                                <?php if ($data->discount != 0) {
                                    ?><td><?php echo $data->discount; ?>%</td> 
                                <?php } else {
                                    ?><td> - </td>
                                <?php } ?>
                                <td><?php echo $data->doc_status; ?></td>
                                <td>          
                                    <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                                        <a href="<?php echo base_url("product/edit_relation/" . $data->pp_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                            <i class="entypo-pencil"></i>Edit  
                                        </a>
                                    <?php } ?>

                                    <?php if ($this->session->userdata("account_type") == "AC04" || $this->session->userdata("account_type") == "AC08") { ?>
                                        <a href = "<?php echo base_url("product/remove_partner/" . $data->pp_id) ?>" 
                                           onclick="return confirmDelete();"
                                           class = "btn btn-default btn-sm btn-icon icon-left" >

                                            <i class = "entypo-pencil"></i>
                                            Remove
                                        </a>
                                    <?php } ?>

                                    <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08") { ?>
                                        <a href="<?php echo base_url("product/actrelation/" . $data->pp_id . "/" . $data->status) ?>" class="btn <?php echo $data->status == 1 ? "btn-green" : "btn-red"; ?> btn-sm btn-icon icon-left">
                                            <i class="<?php echo $data->status == 1 ? "entypo-check" : "entypo-cancel"; ?>"></i><?php echo $data->status == 1 ? "Activate" : "Deactivate"; ?>
                                        </a>
                                    <?php } ?>

                                    <?php if ($this->session->userdata("account_type") == "AC06") { ?>
                                        <a href = "<?php echo base_url("product/approve_partner/" . $data->pp_id) ?>" 
                                           onclick="return confirmApprove();"
                                           class = "btn btn-default btn-sm btn-icon icon-left" >

                                            <i class = "entypo-pencil"></i>
                                            Approve
                                        </a>
                                    <?php } ?>

                                    <script type="text/javascript">
                                        function confirmDelete() {
                                            return confirm('Are you sure you want to delete this product relation?');
                                        }

                                        function confirmApprove() {
                                            return confirm('Are you sure you want to approve this product relation?');
                                        }
                                    </script>
                                </td> 
                            </tr>		
                        <?php endforeach; ?>
                    </tbody>
                <?php endif; ?>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>&nbsp;</th>
                        <th>Partner</th>
                        <th>Partner Type</th>
                        <th>Fixed Price(idr)</th>
                        <th>Revenue Share(%)</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </form>

        <link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
        <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
        <link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

        <!-- Bottom Scripts -->

        <script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
        <script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
        <script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
        <script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
        <script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
        <script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
        <script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



        <div id="ajax_responses" style="display:none;"></div>

        <script type="text/javascript">
                                jQuery(document).ready(function ($)
                                {
                                    var table = $("#table-4").dataTable({
                                        "sPaginationType": "bootstrap",
                                        "oTableTools": {
                                        }
                                    });
<?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                                        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("product/add_relation/" . $product_id); ?>\'"> Add Relation</button>');
<?php } ?>
                                    $(".dataTables_wrapper select").select2({
                                        minimumResultsForSearch: -1
                                    });
                                });

                                function chstate(id, st) {


                                    var c = confirm("Are you sure?");
                                    if (c == true) {
                                        $.post("<?php echo base_url(); ?>user/ajax/chstate", {user_id: id, status: st, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                                            location.href = '<?php echo base_url() ?>user';
                                        });
                                    } else {
                                        ;
                                    }

                                }

                                function simpan(id) {

                                    if (id == 1) {
                                        var label = "Suspend";
                                    } else {
                                        var label = "Activate";
                                    }

                                    $("#formcategory").submit(function () {
                                        return confirm('Are you Sure you want to ' + label + ' this product category?');
                                    })
                                }


        </script>
    </div>
</div>
