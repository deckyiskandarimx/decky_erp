<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product/package'); ?>">Package</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('sukses_add_package'); ?>  
<?php echo $this->session->flashdata('edit_package'); ?>  
<h1><?php echo $title; ?></h1>
<br />
<form action="<?php echo base_url("product/apprvpackagebylist/"); ?>" method="post" id="formpacakge">
    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Product Code</th>
                <th>Package Name</th>
                <th>Tariff IDR</th>
                <th>Status</th>
                <th>Tariff USD</th>
                <th>Status</th>                
                <th>Actions</th>
            </tr>
        </thead>
        <?php if (sizeof($package) > 0): ?>
            <tbody>
                <?php $num = 1;
                foreach ($package as $data): ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td style="width: 15px;">&nbsp;</td>
                        <td><?php echo $data->product_code; ?></td>
                        <td><?php echo $data->package_name; ?></td>
                        <td><?php echo $data->product_tariff_idr_total > 0 ? number_format($data->product_tariff_idr_total):"0,00"; ?></td>
                        <td><?php echo $data->product_tariff_idr_status == 1 ?"Active":"Not Active"; ?></td>
                        <td><?php echo $data->product_tariff_usd_total > 0 ? number_format($data->product_tariff_usd_total):"0,00"; ?></td>
                        <td><?php echo $data->product_tariff_usd_status == 1 ?"Active":"Not Active"; ?></td>
                        <td>
                           <?php if ($this->session->userdata("account_type") == "AC06" || $this->session->userdata("account_type") == "AC08" || $this->session->userdata("account_type") == "AC07") { ?>
                            <a href="<?php echo base_url("product/packagedetails/" . $data->package_id) ?>" class="btn btn-default btn-sm">
                                Details
                            </a>
                            &nbsp;
                            <a href="<?php echo base_url("product/packagesubs/" . $data->package_id) ?>" class="btn btn-gold btn-sm">
                                Subscriber
                            </a>
                        <?php } ?>
                        </td>
                    </tr>		
            <?php endforeach; ?>
            </tbody>
<?php endif; ?>
        <tfoot>
            <tr>    
                <th>No.</th>
                <th>&nbsp;</th>
                <th>Product Code</th>
                <th>Package Name</th>
                <th>Tariff IDR</th>
                <th>Status</th>
                <th>Tariff USD</th>
                <th>Status</th>                
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("product/importproduct") ?>\'"> Import Package</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });


    function simpan(id) {

        if (id == 1) {
            var label = "Suspend";
        } else {
            var label = "Activate";
        }

        $("#formcategory").submit(function () {
            return confirm('Are you Sure you want to ' + label + ' this product category?');
        })
    }

</script>