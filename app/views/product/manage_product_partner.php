<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product/listproduct'); ?>">Product</a>
    </li>
    <li class="active">
        <strong>Manage Product Partner</strong>
    </li>
</ol>
<!-- <?php // echo $this->session->flashdata('validation_partner_name'); ?> -->
<h1><?php echo $title; ?></h1>

<?php echo $this->session->flashdata('add_product_partner_alert'); ?>
<?php echo $this->session->flashdata('delete_product_partner_alert'); ?>

<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    // echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <h4 class="product-header"><span class="entypo-tag imx-blue" style="font-size: 20px;"></span><?=$product_name?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="entypo-progress-3 imx-grey" style="font-size: 20px;"></span><?=$product_code?></h4>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary panel-card" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <span style="border-left: 3px solid #31708F;"></span><span class="entypo-list"></span> Existing Partner
                </div>
            </div>
            <div class="panel-body">
                <form action="<?php echo base_url("product/delete_product_partner_rel/" . $product_id); ?>" method="post" class="form-horizontal validate">
                    <div class="form-group <?php echo(!$is_indosat ? 'hidden' : '');?>">
                        <div class="col-md-12 hidden">
                            <p class="form-control-static">Indosat</p>
                        </div>

                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners_r[]" value="11">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_indosat.png" alt="Logo Indosat" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$is_telkomsel ? 'hidden' : '');?>">
                        <div class="col-md-12 hidden">
                            <p class="form-control-static">Telkomsel</p>
                        </div>

                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners_r[]" value="20">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_telkomsel.png" alt="Logo Telkomsel" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$is_xl ? 'hidden' : '');?>">
                        <div class="col-md-12 hidden">
                            <p class="form-control-static">XL</p>
                        </div>

                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners_r[]" value="10">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_xl.png" alt="Logo XL" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$is_3 ? 'hidden' : '');?>">
                        <div class="col-md-12 hidden">
                            <p class="form-control-static">3</p>
                        </div>

                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners_r[]" value="26">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_three.png" alt="Logo Three" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$is_smartfren ? 'hidden' : '');?>">
                        <div class="col-md-12 hidden">
                            <p class="form-control-static">Smartfren</p>
                        </div>

                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners_r[]" value="27">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_smartfren.png" alt="Logo Smartfren" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$is_smatoo ? 'hidden' : '');?>">
                        <div class="col-md-12 hidden">
                            <p class="form-control-static">Smaato</p>
                        </div>

                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners_r[]" value="23">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_smaato.png" alt="Logo Smaato" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$is_other ? 'hidden' : '');?>">
                        <div class="col-md-12 hidden">
                            <p class="form-control-static">Other</p>
                        </div>

                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners_r[]" value="22"><span style="font-size: 22px;">&nbsp;&nbsp;Other</span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo($has_partner ? 'hidden' : '');?>">
                        <div class="col-md-12">
                            <p style="border-left: 2px solid rgba(172,24,23,1); padding-left: 5px;" class="form-control-static">This product has no partners.</p>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$has_partner ? 'hidden' : '');?>" style="margin-top: 10px; ">
                        <div class="col-sm-12 text-center">                            
                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                
                            <button type="submit" class="btn btn-danger" data-toggle="tooltip" title="Remove Selected Partner"><span class="entypo-minus"></span></button>
                            <button type="button" class="btn btn-default hidden" onclick="javascript: location.href = '<?php echo base_url() ?>product/listproduct';">Cancel</button>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-primary panel-card" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <span style="border-left: 3px solid #3C763D;"></span><span class="entypo-list-add"></span> Add Partner
                </div>
            </div>
            <div class="panel-body">
                <form role="form" action="<?php echo base_url("product/insert_product_partner_rel/" . $product_id); ?>" method="post" class="form-horizontal validate">
                    
                    <div class="form-group hidden">
                        <label for="field-3" class="col-sm-3 control-label">Product Code</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $product_code; ?>"  name="code" id="code" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group <?php echo($is_indosat ? 'hidden' : '');?>">
                        <div class="checkbox">
                            <label class="col-sm-6">
                                <input type="checkbox" name="partners[]" value="11">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_indosat.png" alt="Logo Indosat" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo($is_telkomsel ? 'hidden' : '');?>">
                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners[]" value="20">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_telkomsel.png" alt="Logo Telkomsel" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo($is_xl ? 'hidden' : '');?>">
                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners[]" value="10">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_xl.png" alt="Logo XL" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo($is_3 ? 'hidden' : '');?>">
                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners[]" value="26">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_three.png" alt="Logo Three" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo($is_smartfren ? 'hidden' : '');?>">
                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners[]" value="27">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_smartfren.png" alt="Logo Smartfren" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo($is_smatoo ? 'hidden' : '');?>">
                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners[]" value="23">
                                <img class="img-responsive" src="<?=base_url()?>/assets/images/logo_smaato.png" alt="Logo Smaato" width="100">
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo($is_other ? 'hidden' : '');?>">
                        <div class="checkbox">
                            <label class="col-sm-3">
                                <input type="checkbox" name="partners[]" value="22"><span style="font-size: 22px;">&nbsp;&nbsp;Other</span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group <?php echo(!$max_partner ? 'hidden' : '');?>">
                        <div class="col-md-12">
                            <p style="border-left: 2px solid rgba(172,24,23,1); padding-left: 5px;" class="form-control-static">All partners has been selected.</p>
                        </div>
                    </div>
                
                    <div class="form-group <?php echo($max_partner ? 'hidden' : '');?>" style="margin-top: 10px; ">
                        <div class="col-sm-12 text-center">                            
                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                
                            <button type="submit" class="btn btn-success" data-toggle="tooltip" title="Add Selected Partner"><span class="entypo-plus"></span></button>
                            <button type="button" class="btn btn-default hidden" onclick="javascript: location.href = '<?php echo base_url() ?>product/listproduct';">Cancel</button>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>