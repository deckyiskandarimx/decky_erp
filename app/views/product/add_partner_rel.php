<head>   
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>   

</head>   

<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product'); ?>">product</a>
    </li>
    <li class="active">
        <strong>Add Partner</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('validation_partner_id'); ?>
<?php echo $this->session->flashdata('validation_partner'); ?>
<?php echo $this->session->flashdata('validation_partner_type'); ?>
<h1><?php echo $title; ?></h1>
<br />


<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" 
             data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Add Relation
                </div>
            </div>

            <div class="panel-body">
                <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("product/add_relation/" . $product_id) ?>">
                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Relation</label>
                        <div class="col-sm-5">    
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $product_name; ?>" 
                                   class=""  
                                   name="product"
                                   disabled>
                            <br />
                            <select class="form-control" 
                                    name="partner" id="partner" 
                                    required>
                                <option value="0">-- Partner --</option>
                                <?php foreach ($partner as $data): ?>
                                    <option value="<?php echo $data->partner_id; ?>" <?php echo $partner_sel == $data->partner_id ? "selected='selected'" : ""; ?>><?php echo $data->name; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <br />

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#partnertype').change(function () {
                                        if ($('#partnertype').val() === '1') {
                                            $('#fixprice').show();
                                            $('#rev').hide();
                                            $('#value').show();
                                        } else {
                                            $('#fixprice').hide();
                                            $('#rev').show();
                                            $('#value').show();
                                        }
                                    });
                                });

                            </script>

                            <select class="form-control" 
                                    name="partnertype" 
                                    id="partnertype" 
                                    required>
                                <option value="0">-- Partner Type --</option>
                                <?php foreach ($partnertype as $data): ?>
                                    <option value="<?php echo $data->id; ?>" <?php echo $partnertype_sel == $data->id ? "selected='selected'" : ""; ?>><?php echo $data->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group"
                         id="valuediv1"
                         name="valuediv1"
                         >
                        <label for="field-3"
                               name="fixprice"
                               id="fixprice"
                               class="col-sm-3 control-label"
                               style="display: none;">
                            Fixed Price (idr)
                        </label>
                        <label for="field-3"
                               name="rev"
                               id="rev"
                               class="col-sm-3 control-label"
                               style="display: none;">
                            Revenue Share (%)
                        </label>
                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $value; ?>"  
                                   name="value" 
                                   id="value" 
                                   style="display: none;"
                                   placeholder="value" >
                            <br>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" 
                                   name="pp_id" 
                                   value="<?php echo $pp_id; ?>"
                                   >
                            <input type="hidden" 
                                   id="xyztoken" 
                                   name="<?php echo $this->security->get_csrf_token_name(); ?>" 
                                   value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="button" 
                                    class="btn btn-default" 
                                    onclick="javascript: location.href = '<?php echo base_url() ?>product/';">Cancel</button>
                            <input type="submit" 
                                   value="save" 
                                   class="btn btn-default">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>   