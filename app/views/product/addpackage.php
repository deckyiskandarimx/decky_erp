<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('product'); ?>">Product</a>
    </li>
    <li>
        <a href="<?php echo base_url('product/package'); ?>">Package</a>
    </li>
    <li class="active">
        <strong>Create Package</strong>
    </li>
</ol>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Form Package
                </div>


            </div>

            <div class="panel-body">
                <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("product/" . $mod . "/" . $id) ?>">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="package_name" placeholder="Package Name" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-5">
                            <textarea class="form-control autogrow" name="description" id="description" placeholder="Description" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 48px;" required="required"><?php echo $description; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Price</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $price; ?>"  name="price" id="price" placeholder="Amount" data-mask="fdecimal" maxlength="10" required="required">
                        </div>
                    </div>
                    <?php
                    /*
                      <div class="form-group">
                      <label for="field-3" class="col-sm-3 control-label">Period</label>
                      <div class="col-sm-5 input-group">
                      <span class="input-group-addon"><i class="entypo-calendar"></i></span><input type="text" class="form-control" value="<?php echo $start_date; ?>"  name="start_date" id="start_date" data-mask="date" placeholder="Start Date" required="required">
                      <span class="input-group-addon"><i class="entypo-calendar"></i></span><input type="text" class="form-control" value="<?php echo $end_date; ?>"  name="end_date" id="end_date" data-mask="date" placeholder="End Date" required="required">
                      </div>
                      </div>

                      <div class="form-group">
                      <label for="field-3" class="col-sm-3 control-label">Product</label>
                      <div class="col-sm-3">
                      <select class="form-control" name="product_partner" id="product_partner" required="required">
                      <option value="0">-- Product --</option>
                      <?php foreach ($product_partner as $data): ?>
                      <option value="<?php echo $data->id; ?>" <?php echo $productpartner_sel == $data->id ? "selected='selected'":"";?>><?php echo $data->name; ?></option>
                      <?php endforeach;?>
                      </select>
                      </div>
                      <div class="col-sm-2">
                      <input type="text" class="form-control" value="<?php echo $amount; ?>"  name="amount" id="amount" placeholder="Amount" data-mask="fdecimal" maxlength="10" required="required">
                      </div>
                      </div>
                     */
                    ?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="idhidden" value="<?php echo $id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>product/package';">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

