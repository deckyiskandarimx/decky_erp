<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />


<div style="color: red">
                            <?php
                                    if (validation_errors()) {
                                        echo validation_errors();
                                    }
                                    echo urldecode($msg);
                            ?>
                            </div>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Form Change Password
				</div>
				
				
			</div>
			
			<div class="panel-body">
                            
                            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("user/changepswd")?>">
	
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Old Password</label>
						
						<div class="col-sm-5">
                                                    <input type="password" class="form-control" name="oldpassword" id="field-1" placeholder="Old Password" required="required" autocomplete="off">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">New Password</label>
						
						<div class="col-sm-5">
                                                    <input type="password" class="form-control" name="password" id="field-1" placeholder="New Password" required="required" autocomplete="off">
						</div>
					</div>
                                        
                                        <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Confirm New Password</label>
						
						<div class="col-sm-5">
                                                    <input type="password" class="form-control" name="password2" id="field-1" placeholder="New Password Again" required="required" autocomplete="off">
						</div>
					</div>
                                    	
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">                                                    
                                                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							<button type="submit" class="btn btn-default">Save</button>
                                                        <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url("main")?>';">Cancel</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

