<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('user'); ?>">User</a>
    </li>
    <li class="active">
        <strong>Form</strong>
    </li>
</ol>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Form User
                </div>


            </div>

            <div class="panel-body">

                <form role="form" 
                      class="form-horizontal form-groups-bordered" 
                      method="post" 
                      action="<?php echo base_url("user/store/" . $idx) ?>">

                    <div class="form-group">
                        <label for="field-1" 
                               class="col-sm-3 control-label">
                            Username
                        </label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $uname; ?>" 
                                   name="uname" id="field-1" 
                                   placeholder="Email" <?php echo $idx > 0 ? 'disabled="disabled"' : ''; ?> 
                                   required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" 
                               class="col-sm-3 control-label">
                            Fullname
                        </label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $fname; ?>"  
                                   name="fname" 
                                   id="field-1" 
                                   placeholder="Fullname" 
                                   required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" 
                               class="col-sm-3 control-label">
                            NIK
                        </label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $nik; ?>"  
                                   name="nik" 
                                   id="field-1" 
                                   placeholder="NIK" 
                                   required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" 
                               class="col-sm-3 control-label">Mobile Phone</label>

                        <div class="col-sm-5">   
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $mobile; ?>"  
                                   name="mobile" id="field-1" 
                                   placeholder="mobile" 
                                   required="required">
                        </div>   
                    </div>

                    <div class="form-group">
                        <label for="field-3" 
                               class="col-sm-3 control-label">
                            initial
                        </label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $initial; ?>"  
                                   name="initial" 
                                   id="field-1" 
                                   placeholder="Initial" 
                                   required="required">
                        </div>  
                    </div>    

                    <div class="form-group">
                        <label class="col-sm-3 control-label">User Type</label>

                        <div class="col-sm-5">
                            <select class="form-control" 
                                    name="utype" 
                                    id="selecttype" 
                                    required="required">
                                <option value="0">-- select type --</option>
                                <?php foreach ($type as $usertype): ?>
                                    <option value="<?php echo $usertype->user_type; ?>" <?php echo $utype == $usertype->user_type ? "selected='selected'" : ""; ?>><?php echo $usertype->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Parent</label>
                        <div class="col-sm-5">
                            <select class="form-control" 
                                    name="parent" 
                                    id="parent" 
                                    disabled="disabled" <?php echo $parent == 0 ? "disabled='disabled'" : ""; ?>>             
                                <?php echo $parent; ?>
                            </select>
                        </div>
                    </div>
                    <?php if ($idx > 0 && ($this->router->fetch_method() == "edit" || $this->router->fetch_method() == "store")): ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status</label>

                            <div class="col-sm-5">
                                <select class="form-control" 
                                        name="uactivate" 
                                        id="selectstatus" 
                                        required="required">
                                    <option value="0">-- select status --</option>
                                    <?php foreach ($uactivate as $state): ?>
                                        <option value="<?php echo $state->status; ?>" <?php echo $ustatus == $state->status ? "selected='selected'" : ""; ?>><?php echo $state->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" 
                                   name="id_hidden" 
                                   id="id_hidden" 
                                   value="<?php echo $idx; ?>"> 
                            <input type="hidden" 
                                   id="xyztoken" 
                                   name="<?php echo $this->security->get_csrf_token_name(); ?>" 
                                   value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" 
                                    class="btn btn-default" <?php echo ($this->router->fetch_method() == "edit" && $idrecorddb == 0 ) ? "disabled='disabled'" : ""; ?>>Save</button>
                            <button type="button" 
                                    class="btn btn-default" 
                                    onclick="javascript: location.href = '<?php echo base_url("user") ?>';">Cancel</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
<div id="ajax_responses" style="display:none;"></div>
<script type="text/javascript">
    $("#selecttype").change(function () {
        $.post("<?php echo base_url("user/ajax/getparent"); ?>", {type_id: this.value, selfid: $("#id_hidden").val(), '<?php echo $this->security->get_csrf_token_name(); ?>': $("#xyztoken").val()}, function (data) {
            $("#ajax_responses").html(data);
            $("#ajax_responses").find("script").each(function () {
                eval($(this).text());
            });
        });
    });

<?php
if (!empty($parent)): echo "$('#parent').attr('disabled',false)";
endif;
?>

</script>

