<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('user');?>">User</a>
	</li>
	<li class="active">
		<strong>List</strong>
	</li>
</ol>
<h1><?php echo $title; ?></h1>
<br />

<table class="table table-bordered datatable" id="table-4">
	<thead>
		<tr>
                        <th>No.</th>
                        <th>&nbsp;</th>
						<th>Username</th>
						<th>Fullname</th>
                        <th>NIK</th>
                        <th>Mobile Phone</th>
                        <th>Initial</th>
                        <th>Line Manager</th>
						<th>Type</th>
                        <th>Status</th>
                        <th>Actions</th>
			
		</tr>
	</thead>
	<tbody>
            <?php $num = 1; foreach ($userdata as $user):?>
		<tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++;?></td>
                        <td style="width: 15px;"><input type="checkbox" name="userid[]" value="<?php echo $user->user_id?>"></td>
						<td><?php echo $user->username;?></td>
						<td><?php echo $user->fullname;?></td>
                        <td><?php echo $user->emp_id;?></td>
                        <td><?php echo $user->mobile_phone;?></td>
                        <td><?php echo $user->initial;?></td>
                        <td><?php echo $user->manager;?></td>
						<td><?php echo $user->user_type;?></td>
                        <td><?php echo $user->status;?></td>
                        <td>
                            <?php if($this->session->userdata("account_type") == "AC08"): ?>
                            <a href="<?php echo base_url("user/edit/".$user->user_id)?>"  class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					Edit
				</a>
                            <?php endif;?>
			</td>
		</tr>		
            <?php endforeach;?>
	</tbody>
	<tfoot>
		<tr>    
                        <th>No.</th>
                        <th>&nbsp;</th>
			<th>Username</th>
			<th>Fullname</th>
                        <th>NIK</th>
                        <th>Mobile Phone</th>
                        <th>Initial</th>
                        <th>Line Manager</th>
			<th>Type</th>
                        <th>Status</th>
                        <th>Actions</th>
		</tr>
	</tfoot>
</table>

        <link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">

	<!-- Bottom Scripts -->
	
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/TableTools.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="assets/js/datatables/lodash.min.js"></script>
	<script src="assets/js/datatables/responsive/js/datatables.responsive.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		var table = $("#table-4").dataTable({
			"sPaginationType": "bootstrap",
			"oTableTools": {
			},
			
		});
                 <?php if($this->session->userdata("account_type") == "AC08"): ?>
                $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url()?>user/add\'"> Add User</button>');                
                <?php endif; ?>
                $(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
        
        function chstate(id,st){
            
           
            var c = confirm("Are you sure?");
            if(c == true){
                $.post("<?php echo base_url();?>user/ajax/chstate",{user_id: id, status:st,'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'}, function(data){  
                    location.href='<?php echo base_url()?>user';  
                });
            }else{;}
            
        }
        
		
</script>