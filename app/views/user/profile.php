<h3><span class="label label-warning"><?php echo $title; ?></span> </h3>
<br />


<div style="color: red">
                            <?php
                                    if (validation_errors()) {
                                        echo validation_errors();
                                    }
                                    echo urldecode($msg);
                            ?>
                            </div>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Form Edit Profile
				</div>
				
				
			</div>
			
			<div class="panel-body">
                            
                            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("user/profile")?>">
	
                                        <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Username</label>
						
						<div class="col-sm-5">
                                                    <input type="text" class="form-control" name="username" id="field-1" disabled="disabled" value="<?php echo $profile->username; ?>">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Fullname</label>
						
						<div class="col-sm-5">
                                                    <input type="text" class="form-control" name="fullname" id="field-1"  value="<?php echo $profile->fullname; ?>">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">NIK</label>
						
						<div class="col-sm-5">
                                                    <input type="text" class="form-control" name="nik" id="field-1"  value="<?php echo $profile->emp_id; ?>">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Type</label>
						
						<div class="col-sm-5">
                                                    <input type="text" class="form-control" name="parent" disabled="disabled" id="field-1"  value="<?php echo $profile->role; ?>">
						</div>
					</div>
                                
                                        <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Parent</label>
						
						<div class="col-sm-5">
                                                    <input type="text" class="form-control" name="parent" disabled="disabled" id="field-1"  value="<?php echo $profile->atasan == "" ? "n/a": $profile->atasan; ?>">
						</div>
					</div>
					
                                    	
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">                                                    
                                                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							<button type="submit" class="btn btn-default">Save</button>
                                                        <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url("main")?>';">Cancel</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

