<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/client_payment'); ?>">client payment</a>
    </li>
    <li>
        <a href="<?php echo base_url('payment/invform'); ?>">Invoice Form</a>
    </li>
    <li class="active">
        <strong>Invoice Form</strong>
    </li>
</ol>

<h1><?php echo $title; ?></h1>
<br />

<div class="row">
    <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("campaign/termpayment/".$qid) ?>">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Term Payment 
                </div>
            </div>
            
                <div class="panel-body">
                    <?php for($x=1;$x<=$jmltp;$x++): ?>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Term <?php echo $x; ?></label>
                        <div class="col-sm-1" style="float: left;">
                            <input type="text" class="form-control pilihtanggal" value="<?php echo Campaign::reformatdate($start_date[$x-1]); ?>" name="start_date[<?php echo $x; ?>]" id="invoice_date" placeholder="Start Date">                            
                        </div>
                        <div class="col-sm-1">                            
                            <input type="text" class="form-control pilihtanggal" value="<?php echo Campaign::reformatdate($end_date[$x-1]); ?>" name="end_date[<?php echo $x; ?>]" id="invoice_date" placeholder="End Date">
                        </div>
                        <div class="col-sm-2">                            
                            <input type="text" class="form-control amount" value="<?php echo $payment_amount[$x-1] == 0 ? 0:$payment_amount[$x-1];  ?>" name="amount[<?php echo $x; ?>]" id="amount" placeholder="Amount">
                            <input type="hidden" name="idtp[<?php echo $x; ?>]" id="idtp" value="<?php echo $idtp[$x-1]; ?>">
                        </div>
                    </div>
                    <?php endfor; ?>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Selisih</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" value="<?php echo number_format($selisih); ?>" name="total" id="total" disabled="disabled">                            
                        </div>
                        <div class="col-sm-2">
                            Total <b><?php echo number_format($total); ?>.00</b>
                        </div>
                    </div>
                </div>
            
        </div>
        <div class="form-group">
                    
                        <div class="col-sm-5">
                            <input type="hidden" name="qid" id="qid" value="<?php echo $qid; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="submit" class="btn btn-default" value="Save" id="save" disabled="disabled">                            
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url("campaign/quotation") ?>';">Cancel</button>
                        </div>
                    </div> </form> 
    </div>
    
   
</div> 


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>

<script type="text/javascript">

    $(".amount").keyup(function (){
       
        var totaldb = "<?php echo $total; ?>";
        var sum = 0;
        $('.amount').each(function(){
            sum += parseFloat(this.value);
        });
        var ttlpy = parseInt(totaldb) - parseInt(sum);
        if(sum > 0){            
            $("#total").attr("value", ttlpy);
        }else{
            $("#total").attr("value", 0);
        }
        
        if ($("#total").val() == 0){
            $("#save").attr("disabled", false);
        }else{
            $("#save").attr("disabled", true);
        }
    });
    
    if ($("#total").val() == 0){
        $("#save").attr("disabled", false);
    }else{
        $("#save").attr("disabled", true); 
    }
    
    

</script>
    