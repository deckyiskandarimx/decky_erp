<style>
    #remfile{
        text-decoration: underline;
        color: #0071f3;
    }
    
    #remfile, a:visited{
        color: #0071f3;
    }
    
    #files{
        text-decoration: none;
        color: #0071f3;
    }
    
    #files, a:visited{
        color: #ff0000;
    }
    
    #files:hover{
        text-decoration: underline;
    }
</style>
<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('campaign/quotation');?>">Campaign</a>
	</li>
        
        <li>
		<a href="<?php echo base_url('campaign/quotation');?>">Quotation</a>
	</li>
	<li class="active">
		<strong>Form Upload Quotation</strong>
	</li>
</ol>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
                            <?php
                                    if (validation_errors()) {
                                        echo validation_errors();
                                    }
                                    echo $msg;
                            ?>
                            </div>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
                                           Upload Quotation <?php echo $label; ?>
				</div>
				
				
			</div>
			<form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("campaign/uploadquotation/".$quotation_id)?>"  enctype="multipart/form-data">
			<div class="panel-body">
                                <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">File</label>

                                        <div class="col-sm-5">
                                            <?php if($filename == ""){ ?>
                                            <input type="file" name="userfile" id="userfile">
                                            <?php }else{ echo $download_label; ?>
                                            
                                            <?php } ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <input type="hidden" name="idhidden" value="<?php echo $quotation_id;?>">
                                                 <?php if($filename == ""){ ?>
                                                    <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                    
                                                    <button type="submit" class="btn btn-default">Upload</button>
                                                    <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url("campaign/quotation")?>';">Cancel</button>
                                                     <?php }else{ ?>
                                                    <button type="button" class="btn btn-default" onclick="javascript: location.href='<?php echo base_url("campaign/quotation")?>';">Back</button>
                                                     <?php } ?>
                                            </div>
                                    </div>
                        </div>
                            
                            </form>
		
		</div>
            
	
	</div>
</div>



<link rel="stylesheet" href="<?php echo assets;?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2.css">


<script src="<?php echo assets;?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets;?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets;?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets;?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets;?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets;?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets;?>js/select2/select2.min.js"></script>


<script type="text/javascript">
    
  
    function remfile(id){
        
       var xs = confirm("Are You Sure ?");
       
       if(xs == true){
        
                $.post("<?php echo base_url();?>campaign/removeupload",{idquotation: id, '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'}, function(data){  
                   
                        location.href='<?php echo base_url("campaign/uploadquotation/")?>/'+data;
                });
        }else{;}
    }
    
      
    
</script>

