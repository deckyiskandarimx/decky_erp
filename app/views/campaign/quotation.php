<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('campaign/quotation'); ?>">Campaign</a>
    </li>
    <li>
        <a href="<?php echo base_url('campaign/quotation'); ?>">Quotation</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<div id="itemnull"></div>
<?php echo $this->session->flashdata('quotation_np'); ?>
<?php echo $this->session->flashdata('quotation_p'); ?>
<?php echo $this->session->flashdata('synccampaign_alert'); ?>
<h1><?php echo $title; ?></h1>
<br />

<form action="<?php echo base_url("campaign/quotation/"); ?>" method="post" id="formcategory">
    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>No.</th>
<!--                        <th>&nbsp;</th>-->
                <th>Quotation Number</th>			
                <th>Campaign Name</th>		
                <th>Package Name</th>		
                <th>Quotation Date</th>			
<!--                        <th>Agency</th>-->
                <th>Advertiser</th>		
<!--                        <th>Brand Name</th>			-->
                <th>Payment Type</th>			
                <th>Total</th>			
                <th>Status</th>			
                <th>Actions</th>

            </tr>
        </thead>
        <?php if (sizeof($quotation) > 0): ?>
            <tbody>
                <?php $num = 1;
                foreach ($quotation as $data):
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
        <!--                        <td style="width: 15px;"><input type="checkbox" name="quotationid[]" value="<?php //echo $data->quotation_id  ?>"></td>-->
                        <td><?php echo $data->quotation_number; ?></td>
                        <td><?php echo $data->campaign_name; ?></td>
                        <td><?php echo $data->package_name; ?></td>
                        <td><?php echo date("d-m-Y", strtotime($data->quotation_date)); ?></td>
                        <!--<td><?php //echo $data->agency_name;  ?></td>-->
                        <td><?php echo $data->advertiser_name; ?></td>
                        <!--<td><?php //echo $data->brand_name;  ?></td>-->
                        <td><?php echo $data->payment_type; ?></td>
                        <!-- <td>Rp. <?php //echo number_format($data->total); ?></td> -->
                        <td><?php echo $data->package_id == 0 ? number_format(Campaign::totalitemquotation($data->quotation_id)) : number_format($data->total); ?></td>
                        <td>
                            <?php
                                $status = $data->status;
                                if($status == 'Approved') {
                                    $label_type = 'info';
                                } else if($status == 'Done') {
                                    $label_type = 'success';
                                } else if($status == 'Draft') {
                                    $label_type = 'warning';
                                } else {
                                    $label_type = 'default';
                                }
                            ?>
                            <span class="label label-<?=$label_type?>">
                                <?php echo $status; ?>
                            </span>
                        </td>
                        <td>

        <?php if ($data->status != "Canceled" && $data->status != "Approved") { ?>
                                <!-- <a href="<?php echo base_url("campaign/editquotation/" . $data->quotation_id) ?>" class="btn btn-default btn-sm btn-icon icon-left"><i class="entypo-pencil"></i>View</a>  
                                &nbsp;
                                <?php if ($data->package_id == "" && $data->status != "Expired") { ?>
                                    <a href="<?php echo base_url("campaign/additemquotation/" . $data->quotation_id) ?>" class="btn btn-default btn-sm btn-icon icon-left"><i class="entypo-basket"></i>Add Item</a>  
                                <?php } ?>
                                &nbsp; -->
                            <?php } ?>
                            <?php if ($data->status != "Canceled") { ?>
                                <!-- <a href="javascript:;" onclick="showmodal(<?php echo $data->quotation_id; ?>);" class="btn btn-default btn-sm btn-icon icon-left"><i class="entypo-alert"></i>Process</a>  
                            <?php } ?>
                            <?php if ($data->status == "Expired" && $data->created_by == $this->session->userdata("user_id")) { ?>
                                <a href="javascript:;" onclick="copyquotation(<?php echo $data->quotation_id; ?>);" class="btn btn-default btn-sm btn-icon icon-left"><i class="entypo-export"></i>Copy</a>  
                            <?php } ?>
                            &nbsp; -->
        <?php if ($data->status != "Draft" && $data->status != "Canceled" && $data->status != "Expired") { ?>
                                <a href="<?php echo base_url("campaign/exportpdf/?quotation_id=" . $data->quotation_id) ?>" target="_blank" class="btn btn-default btn-sm btn-icon icon-left">
                                    <i class="entypo-print"></i>
                                    Export
                                </a> 
                                <!-- &nbsp;
                                <a href="<?php echo base_url("campaign/uploadquotation/" . $data->quotation_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                    <i class="entypo-upload"></i>
                                    Upload Quotation
                                </a> -->

                                <?php if(($status != "Approved") && ($status != "Rejected")) { ?>
                                <a href="<?php echo base_url("campaign/synccampaign/" . $data->quotation_id) ?>" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-refresh"></i> Sync</a>
                                <a href="<?php echo base_url("campaign/hitcurl") ?>" class="hidden btn btn-info btn-sm"><i class="glyphicon glyphicon-refresh"></i> Hit</a>
                                <?php } ?>

                            <?php } ?>
        <?php if ($data->signed_quotation_file != "" && $data->package_id == "") { ?>
                                <!-- <a href="<?php echo base_url("campaign/viewio/" . $data->quotation_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                    <i class="entypo-attach"></i>
                                    View IO
                                </a> --> 
        <?php } ?>
                                <?php if($data->package_name != "" && $data->termpayment > 0){ ?>
                                    <!-- <a href="<?php echo base_url("campaign/termpayment/" . $data->quotation_id) ?>" class="btn btn-default btn-sm btn-icon icon-left">
                                        <i class="entypo-upload"></i>
                                        Term Payment
                                    </a> --> 
                                <?php } ?>
                        </td>
                    </tr>		
            <?php endforeach; ?>
            </tbody>
<?php endif; ?>
        <tfoot>
            <tr>    
                <th>No.</th>
<!--                        <th>&nbsp;</th>-->
                <th>Quotation Number</th>			
                <th>Campaign Name</th>		
                <th>Package Name</th>		
                <th>Quotation Date</th>			
<!--                        <th>Agency</th>-->
                <th>Advertiser</th>		
                <!--<th>Brand Name</th>-->			
                <th>Payment Type</th>			
                <th>Total</th>			
                <th>Status</th>			
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

</form>

<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>



<div id="ajax_responses" style="display:none;"></div>

<div id="modalindex" style="display: none;">    
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="javascript:closemodal();">&times;</button>
                <h4 class="modal-title">Process Quotation</h4>
            </div>
            <form action="<?php echo base_url("campaign/setstatus"); ?>" method="post" name="formmodal1">
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Set Status</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="status_id" id="statusselect">                                                        
                                    <?php foreach ($status as $data): ?>
                                        <option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option> 
<?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="xyztoken2" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <input type="hidden" id="xyztokennext" name="<?php echo $this->security->get_csrf_token_name(); ?>">
                    <input type="hidden" name="idquotationhidden" id="idquotationhidden">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:closemodal();">Close</button>
                    <button type="submit" class="btn btn-info">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="ajax_responses2" style="display:none;"></div>
<div id="tokenhidden" style="display:none;"><?php echo $this->security->get_csrf_hash(); ?></div>

<script type="text/javascript">
                                    jQuery(document).ready(function ($)
                                    {
                                        var table = $("#table-4").dataTable({
                                            "sPaginationType": "bootstrap",
                                            "oTableTools": {
                                            },
                                        });
                                        
                                        $(".dataTables_wrapper select").select2({
                                            minimumResultsForSearch: -1
                                        });
                                    });

                                    function showmodal(id) {
                                        var token3 = $("#xyztoken2").val();
                                        
                                        $("#modal-1").html($("#modalindex").html());
                                        $.post("<?php echo base_url(); ?>campaign/ajax/modal", {quotation_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': token3}, function (data) {
                                            var dataresult = $.parseJSON(data);
                                            if(dataresult.pid > 0){
                                                $("#modal-1").find('input[id="xyztoken2"]').val(dataresult.newtoken);                                            
                                                $("#modal-1").find('input[id="xyztokennext"]').val(dataresult.newtoken);    
                                                $("#modal-1").find('input[name="idquotationhidden"]').val(dataresult.qid);
                                                $('#statusselect option[value="' + dataresult.status + '"]').attr('selected', 'selected');
                                                $('#modal-1').modal('show');
                                            }else{
                                                if(dataresult.item > 0){
                                                    $("#modal-1").find('input[id="xyztoken2"]').val(dataresult.newtoken);                                            
                                                    $("#modal-1").find('input[id="xyztokennext"]').val(dataresult.newtoken);    
                                                    $("#modal-1").find('input[name="idquotationhidden"]').val(dataresult.qid);
                                                    $('#statusselect option[value="' + dataresult.status + '"]').attr('selected', 'selected');
                                                    $('#modal-1').modal('show');
                                                }else{
                                                    $("#itemnull").html('<div class="alert alert-danger"><strong>Failed!&nbsp;</strong>Please Add Item First.</div>');
                                                }
                                            }
                                        });
                                        
                                        
                                       
                                    }

                                    function savestatus() {
                                        var tokenhidden = $("#tokenhidden").html();
                                        var idhidden = $("#modal-1").find('input[id="idquotationhidden"]').val();      //$("#idquotationhidden").val();
                                        var statusval = $("#modal-1").find('select[name="statusselect"]').val();
                                        $.post("<?php echo base_url(); ?>campaign/ajax/setstatus", {quotation_id: idhidden, '<?php echo $this->security->get_csrf_token_name(); ?>': tokenhidden, status_id: statusval}, function (data) {
                                            $('#modal-1').modal('hide');                                           
                                            parent.window.location.reload();
                                        });                                        
                                    }
                                    
                                    function closemodal(){
                                        var tokenhidden3 = $("#modal-1").find('input[id="xyztokennext"]').val();
                                        parent.window.location.reload();
                                    }


                                    function copyquotation(qid) {

                                        var token4 = $("#xyztoken2").val();
                                        var c = confirm("Are You Sure?");
                                        if (c == true) {
                                            $.post("<?php echo base_url("campaign/ajax/copyquoation/"); ?>", {quotation_id: qid, '<?php echo $this->security->get_csrf_token_name(); ?>': token4}, function (data) {
                                                location.href = '<?php echo base_url("campaign/editquotation"); ?>/' + data;
                                            });
                                        } else {
                                            ;
                                        }

                                    }

</script>
