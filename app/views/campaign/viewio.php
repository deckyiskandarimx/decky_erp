<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('campaign/quotation');?>">Campaign</a>
	</li>
        <li>
		<a href="<?php echo base_url('campaign/quotation');?>">Quotation</a>
	</li>
	<li class="active">
		<strong>Form Add Items</strong>
	</li>
</ol>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
                            <?php
                                    if (validation_errors()) {
                                        echo validation_errors();
                                    }
                                    echo $msg;
                            ?>
                            </div>

<div class="row">
	<div class="col-md-12">
            <form action="<?php echo base_url("campaign/saveallitem"); ?>" method="post" id="formio">
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					IO Item
				</div>
				
				
			</div>
                            <div class="panel-body">
                                            
                                            <table class="table table-bordered datatable" id="table-4">
                                            <thead>
                                                    <tr>
                                                            <th>No.</th>                                                            
                                                            <th>Ref Number</th>    
                                                            <th>Product Name</th>
                                                            <th>Partner Name</th>
                                                            <th>Partner Type</th>
                                                            <th>Amount</th>
                                                            <th>Price</th>
                                                            <th>Discount (%)</th>
                                                            <th>Total(+PPN)</th>
                                                            <th>Start Date</th>       
                                                            <th>End Date</th>     

                                                    </tr>
                                            </thead>
                                            <?php if(sizeof($listio) > 0): ?>
                                            <tbody>
                                                <?php  $num = 1; foreach ($listio as $data):?>
                                                    <tr class="odd gradeX">
                                                            <td style="width: 15px;"><?php echo $num++;?></td>                                                            
                                                            <td><input type="text" name="ref_number[<?php echo $data->io_id; ?>]" id="ref_number_<?php echo $data->io_id; ?>" value="<?php echo $data->reference_id; ?>" class="large"></td>                                                            
                                                            <td><?php echo $data->product_name; ?></td>        
                                                            <td><?php echo $data->name; ?></td>     
                                                            <td><?php echo $data->type == 2 ? "Fixed Price":"Rev.Share"; ?></td>     
                                                            <td><?php echo $data->amount; ?></td>     
                                                            <td><?php echo (int)$data->priced; ?></td>     
                                                            <td><?php echo $data->disc; ?>%</td>     
                                                            <td><?php echo number_format((((100- $data->disc)/100)*($data->priced)*$data->amount) * 1.1); ?></td>
                                                            <td><?php echo date("d-m-Y", strtotime($data->start_date));?></td>
                                                            <td><?php echo date("d-m-Y", strtotime($data->end_date));?></td>    
                                                    </tr>		
                                                <?php endforeach; ?>
                                            </tbody>
                                            <?php endif; ?>
                                            <tfoot>
                                                    <tr>    
                                                            <th>No.</th>                                                            
                                                            <th>Ref Number</th>    
                                                            <th>Product Name</th>
                                                            <th>Partner Name</th>
                                                            <th>Partner Type</th>
                                                            <th>Amount</th>
                                                            <th>Price</th>
                                                            <th>Discount (%)</th>
                                                            <th>Total(+PPN)</th>
                                                            <th>Start Date</th>       
                                                            <th>End Date</th>     
                                                    </tr>
                                            </tfoot>
                                    </table>
                                        
                                        <input type="hidden" id="quotation_id" name="quotation_id" value="<?php echo $quotation_id; ?>">
                                        <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                          <a href="#" onclick="$('#formio').submit();" class="btn btn-red btn-sm btn-icon icon-left">
                                                    <i class="entypo-db-shape"></i>
                                                    SAVE ALL
                                            </a>   
                                        </div>
                    
                            
                            </div>
            </form>
                            
		</div>
                
                
	</div>
</div>
<div id="ajax_responses" style="display:none;"></div>
<div id="ajax_responses2" style="display:none;"></div>


<link rel="stylesheet" href="<?php echo assets;?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2.css">


<script src="<?php echo assets;?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets;?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets;?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets;?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets;?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets;?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets;?>js/select2/select2.min.js"></script>
<script src="<?php echo assets;?>js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
   
   
        jQuery(document).ready(function($)
	{
		var table = $("#table-4").dataTable({
			"sPaginationType": "bootstrap",
			"oTableTools": {
			},
			
		});
                //$("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/addbrandclient/".$id)?>\'"> Add Brand</button>');                
                $(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
        
        
  
        function saveio(ioid){
            
            var refnum = $("#ref_number_"+ioid).val();
            $.post("<?php echo base_url("campaign/ajax/saveio");?>",{refnumber: refnum, ioid: ioid, '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'}, function(data){            
                location.href='<?php echo base_url("campaign/viewio/".$quotation_id);?>'
            });
            
        }
  
</script>

