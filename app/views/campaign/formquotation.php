<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('campaign'); ?>">Campaign</a>
    </li>
    <li>
        <a href="<?php echo base_url('campaign/quotation'); ?>">Quotation</a>
    </li>
    <li class="active">
        <strong>Form</strong>
    </li>
</ol>
<?php echo $this->session->flashdata('val_qno'); ?>
<h1><?php echo $title; ?></h1>
<br />
<div style="color: red">
    <?php
    if (validation_errors()) {
        echo validation_errors();
    }
    echo $msg;
    ?>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    Quotation
                </div>


            </div>
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url("campaign/" . $mod . "/" . $id) ?>">
                <div class="panel-body">

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Quotation No.</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   value="<?php echo $qno; ?>" 
                                   name="qno" id="field-1" 
                                   placeholder="Quotation Number">
                        </div>
                    </div>
                    <!--<script type="text/javascript">
                        $(document).ready(function () {
                            $('#package').change(function () {
                                if ($('#package').val() === '0') {
                                    $('#unedited').show();
                                    $('#campaign').hide();
                                } else {
                                    $('#unedited').hide();
                                    $('#campaign').show();
                                }
                            });
                        });-->

                    </script>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Package</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="package" id="package"  <?php echo sizeof($package) == 0 || $jumlah_item > 0 ? "disabled='disabled'" : ""; ?>> 
                                <option value="0">-- Select Package --</option>
                                <?php foreach ($package as $datapackage): ?>
                                    <option value="<?php echo $datapackage->package_id; ?>" <?php echo $datapackage->package_id == $packageselect  ? "selected='selected'" : ""; ?>><?php echo $datapackage->package_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="form-group" id="term" style="display: <?php echo $packageselect > 0 ? 'block': 'none'; ?>">
                        <label class="col-sm-3 control-label">Term Payment</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="termpayment" id="termpayment"> 
                                <option value="0">-- Select Term --</option>
                                <?php for($x=1;$x<=12;$x++): ?>
                                    <option value="<?php echo $x; ?>" <?php echo $termpayment == $x  ? "selected='selected'" : ""; ?>><?php echo $x; ?> Month</option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Quotation Date</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" value="<?php echo $qdate; ?>" name="qdate">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Advertiser</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="advertiser" id="advertiser">
                                <option value="0">-- select advertiser --</option>
                                <?php foreach ($client as $data): ?>
                                    <option value="<?php echo $data->client_id; ?>" <?php echo $advertiserid == $data->client_id ? "selected='selected'" : ""; ?>><?php echo $data->client_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Business Category</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $busscat; ?>"  name="busscat" id="busscat" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Brand</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="brand" id="brand">
                                <option value="0">-- select brand --</option>
                                <?php foreach ($brandid as $databr): ?>
                                    <option value="<?php echo $databr->brand_id; ?>" <?php echo $brands == $databr->brand_id ? "selected='selected'" : ""; ?>><?php echo $databr->brand_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-3" class="col-sm-3 control-label">Campaign</label>

                        <div class="col-sm-5">
                            <input type="text" 
                                   class="form-control" 
                                   name="campaign" 
                                   value="<?php echo $campaign_name; ?>"
                                   placeholder="campaign name"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Agency</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="agency" id="agency">
                                <option value="0">-- select agency --</option>
                                <?php foreach ($client as $data): ?>
                                    <option value="<?php echo $data->client_id; ?>" <?php echo $agency == $data->client_id ? "selected='selected'" : ""; ?>><?php echo $data->client_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Payment</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="payment" id="payment">
                                <option value="0">-- select payment --</option>
                                <?php foreach ($payment as $data): ?>
                                    <option value="<?php echo $data->payment_id; ?>" <?php echo $paymentid == $data->payment_id ? "selected='selected'" : ""; ?>><?php echo $data->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <!--<div class="form-group">
                        <label class="col-sm-3 control-label">Quotation Payment Method</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="q_payment" id="q_payment"  <?php echo sizeof($full_payment) == 0 ? "disabled='disabled'" : ""; ?>>                                                          
                    <?php foreach ($full_payment as $data): ?>
                                                        <option value="<?php echo $data->id; ?>" <?php echo $fpaymentselect == $data->id ? "selected='selected'" : ""; ?>><?php echo $data->name; ?></option>
                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>-->

                    <?php if ($status_quotation == 3) { ?>
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Cancel Reason</label>

                            <div class="col-sm-5">
                                <textarea class="form-control" name="reason" disabled="disabled"><?php echo $cancel_reason; ?></textarea>
                            </div>
                        </div>
                    <?php } ?>
                    <hr/>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <input type="hidden" name="idhidden" value="<?php echo $id; ?>">
                            <input type="hidden" id="xyztoken" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <?php if ($status_quotation != 3) { ?>
                                <button type="submit" class="btn btn-default">Save</button>
                            <?php } ?>
                            <button type="button" class="btn btn-default" onclick="javascript: location.href = '<?php echo base_url() ?>campaign/quotation';">Cancel</button>
                        </div>
                    </div>

                </div>


            </form>

        </div>


    </div>
</div>
<div id="ajax_responses" style="display:none;"></div>
<div id="ajax_responses2" style="display:none;"></div>


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">


<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>
<script src="<?php echo assets; ?>js/bootstrap-datepicker.js"></script>

<script type="text/javascript">


                                $('.datepicker').datepicker({
                                    format: 'dd/mm/yyyy',
                                })

                                jQuery(document).ready(function ($)
                                {
                                    var table = $("#table-4").dataTable({
                                        "sPaginationType": "bootstrap",
                                        "oTableTools": {
                                        },
                                    });
                                    $("div.dataTables_length").append('<button type="button" class="btn btn-white entypo-drive" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("client/addbrandclient/" . $id) ?>\'"> Add Brand</button>');
                                    $(".dataTables_wrapper select").select2({
                                        minimumResultsForSearch: -1
                                    });
                                });

                                function removebrand(id) {

                                    var confirms = confirm("Are You Sure ?");

                                    if (confirms == true) {
                                        $.post("<?php echo base_url(); ?>client/deletebrandclient", {brand_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function () {
                                            location.href = '<?php echo base_url(); ?>client/addbrandlist/<?php echo $id; ?>';
                                                        });
                                                    } else {
                                                        ;
                                                    }

                                                }


                                                $("#advertiser").change(function () {
                                                    $.post("<?php echo base_url("campaign/ajax/quotation"); ?>", {idclient: this.value, '<?php echo $this->security->get_csrf_token_name(); ?>': $("#xyztoken").val()}, function (data) {
                                                        $("#ajax_responses").html(data);
                                                        $("#ajax_responses").find("script").each(function () {
                                                            eval($(this).text());
                                                        });
                                                    });
                                                });
                                                
                                                $("#package").change(function () {
                                                    if(this.value > 0){
                                                        $("#term").css("display","block");
                                                    }else{
                                                        $("#term").css("display","none");
                                                    }
                                                });

</script>

