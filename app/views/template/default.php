<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Neon Admin Panel" />
        <meta name="author" content="" />

        <title><?php echo $title; ?></title>


        <link rel="stylesheet" href="<?php echo assets; ?>js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
        <link rel="stylesheet" href="<?php echo assets; ?>css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
        <link rel="stylesheet" href="<?php echo assets; ?>css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo assets; ?>css/neon-core.css">
        <link rel="stylesheet" href="<?php echo assets; ?>css/neon-theme.css">
        <link rel="stylesheet" href="<?php echo assets; ?>css/neon-forms.css">
        <link rel="stylesheet" href="<?php echo assets; ?>css/custom.css">
        <link rel="stylesheet" href="<?php echo assets; ?>css/skins/white.css">
        <link rel="stylesheet" href="<?php echo assets; ?>css/azalia.css">
        <link rel="shortcut icon" href="<?=assets?>/images/favicon.ico">


        <script src="<?php echo assets; ?>js/jquery-1.11.0.min.js"></script>
        <script src="<?php echo assets; ?>js/jquery.cookie.js"></script>

        <!--[if lt IE 9]><script src="<?php echo assets; ?>js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            .btn, .alert, .label {
                border-radius: 0;
            }   
        </style>
    </head>
    <body class="page-body" data-url="http://neon.dev">

        <div class="page-container sidebar-collapsed"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	

            <div class="sidebar-menu">


                <header class="logo-env">

                    <!-- logo -->
                    <div class="logo">
                        <a href="<?=base_url()?>">
                            <img src="<?php echo assets; ?>images/logo-imx.png" width="80" alt="" />
                        </a>
                    </div>

                    <!-- logo collapse icon -->

                    <div class="sidebar-collapse">
                        <a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>



                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="sidebar-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>

                </header>






                <ul id="main-menu" class="">
                    <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                    <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                    <!-- Search Bar -->
<!--                    <li id="search">
                        <form method="get" action="">
                            <input type="text" name="q" class="search-input" placeholder="Search something..."/>
                            <button type="submit">
                                <i class="entypo-search"></i>
                            </button>
                        </form>
                    </li>-->
                    <?php
                    $c = $this->router->fetch_class();
                    $m = $this->router->fetch_method();
                    ?>
                    <?php ///if($this->session->userdata("account_type") == "AC08"):  ?>
                    <li <?php
                    if ($c == "user") {
                        echo 'class="opened active"';
                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-tools"></i>
                            <span>Administrator</span>
                        </a>
                        <ul>
                            <li <?php
                            if ($c == "user" && ($m == "index" || $m == "add" || $m == "edit")) {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("user") ?>">
                                    <span>User Management</span>
                                </a>
                            </li>
                        </ul>
                    </li>
					<li <?php
                    if ($c == "client") {
                        echo 'class="opened active"';
                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-heart"></i>
                            <span>Client</span>
                        </a>
                        <ul>
                            <li <?php
                            if ($c == "client" && $m == "account") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("client/account") ?>">
                                    <span>View Account</span>
                                </a>
                            </li>
                            <li <?php
                            if ($c == "client" && $m == "contact") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("client/contact") ?>">
                                    <span>View Contact</span>
                                </a>
                            </li>
							<li <?php
                            if ($c == "client" && $m == "export") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("client/export") ?>">
                                    <span>Export CSV</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php //endif;   ?>
                    <li <?php
                    if ($c == "product") {
                        echo 'class="opened active"';
                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-archive"></i>
                            <span>Product</span>
                        </a>
                        <ul>
                            <li <?php
                            if ($c == "product" && ($m == "listproduct" || $m == "add" || $m == "edit")) {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("product/listproduct") ?>">
                                    <span>Product Management</span>
                                </a>
                            </li>
                            <!-- <li <?php
//                            if ($c == "product" && ($m == "category" || $m == "addcategory" || $m == "editcategory")) {
//                                echo 'class="active"';
//                            }
                            ?>>
                                <a href="<?php //echo base_url("product/category") ?>">
                                    <span>Prod. Category Management</span>
                                </a>
                            </li> -->

                            <li <?php
                            if ($c == "product" && ($m == "package" || $m == "addpackage" || $m == "editpackage")) {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("product/package") ?>">
                                    <span>Package Management</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li <?php
                    if ($c == "partner") {
                        echo 'class="opened active"';
                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-users"></i>
                            <span>Partners</span>
                        </a>
                        <ul>
                            <li <?php
                            if ($c == "partner" && ($m == "index" || $m == "add" || $m == "edit")) {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("partner") ?>">
                                    <span>Partner Management</span>
                                </a>
                            </li>

                            <!--<li <?php //if ($c == "partner" && ($m == "category" || $m == "addcategory" || $m == "editcategory")) {
                                //echo 'class="active"'; } 
                            ?>
                                <a href="<?php //echo base_url("partner/relation")  ?>">
                                    <span>Partner Product Relation</span>
                                </a>
                            </li>
                            -->
                        </ul>
                    </li>

                    <!-- <li <?php
//                    if ($c == "client") {
//                        echo 'class="opened active"';
//                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-tag"></i>
                            <span>Client</span>
                        </a>
                        <ul>
                            <li <?php
//                            if ($c == "client" && ($m == "index" || $m == "add" || $m == "edit" || $m == "addbrandlist")) {
//                                echo 'class="active"';
//                            }
                            ?>>
                                <a href="<?php //echo base_url("client") ?>">
                                    <span>Client Management</span>
                                </a>
                            </li>

                            <li <?php
                            /*if ($c == "client" && ($m == "category" || $m == "addcategory" || $m == "editcategory")) {
                                echo 'class="active"';
                            }*/
                            ?>>
                                <a href="<?php //echo base_url("client/category") ?>">
                                    <span>Business Category</span>
                                </a>
                            </li>


                        </ul>
                    </li> -->

                    <li <?php
                    if ($c == "campaign") {
                        echo 'class="opened active"';
                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-sound"></i>
                            <span>Campaign</span>
                        </a>
                        <ul>
                            <li <?php
                            if ($c == "campaign" && ($m == "quotation" || $m == "add" || $m == "edit" || $m == "addbrandlist")) {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("campaign/quotation") ?>">
                                    <span>Quotation</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li <?php
                    if ($c == "payment") {
                        echo 'class="opened active"';
                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-paypal"></i>
                            <span>Payment</span>
                        </a>
                        <ul>
                            <li <?php
                            if ($c == "payment" && ($m == "index" || $m == "add" || $m == "edit" || $m == "addbrandlist")) {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("payment/") ?>">
                                    <span>Partner Payment Management</span>
                                </a>
                            </li>

                            <li <?php
                            if ($c == "payment" && ($m == "index" || $m == "add" || $m == "edit" || $m == "addbrandlist")) {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("payment/client_payment") ?>">
                                    <span>Client Payment</span>
                                </a>
                            </li>


                        </ul>
                    </li>
                    <li <?php
                    if ($c == "report") {
                        echo 'class="opened active"';
                    }
                    ?>>
                        <a href="#">
                            <i class="entypo-chart-line"></i>
                            <span>Reports</span>
                        </a>
                        <ul>
                            <li <?php
                            if ($c == "report" && $m == "ar") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("report/ar") ?>">
                                    <span>Accounts Receivable</span>
                                </a>
                            </li>
                            <li <?php
                            if ($c == "report" && $m == "ap") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("report/ap") ?>">
                                    <span>Accounts Payable</span>
                                </a>
                            </li>
                            
                            <li <?php
                            if ($c == "report" && $m == "revenue") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url("report/rev") ?>">
                                    <span>Revenue</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                </ul>

            </div>	
            <div class="main-content">

                <?php require_once 'accountinfo.php'; ?>            
                <?php echo $body; ?>
                <br />

                <!-- lets do some work here... --><!-- Footer -->
                <footer class="main">
                    &copy; <?=date('Y')?> <strong>Indonesia Mobile Exchange</strong> - Enterprise Resource Planning
                </footer>
            </div>


        </div>

        <div id="delete-partner-account-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close hidden" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>This partner account will be deleted from partner account list. Are You sure?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="<?=base_url("partner/delete_partner_account")?>" class="btn btn-default">Yes</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="delete-partner-account-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close hidden" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>This partner account will be deleted from partner account list. Are You sure?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="<?=base_url("partner/delete_partner_account")?>" class="btn btn-default">Yes</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="delete-partner-product-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close hidden" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>This partner product will be deleted from partner product list. Are You sure?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="<?=base_url("partner/delete_partner_product")?>" class="btn btn-default">Yes</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Bottom Scripts -->
        <script src="<?php echo assets; ?>js/gsap/main-gsap.js"></script>
        <script src="<?php echo assets; ?>js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
        <script src="<?php echo assets; ?>js/bootstrap.js"></script>
        <script src="<?php echo assets; ?>js/joinable.js"></script>        
        <script src="<?php echo assets; ?>js/resizeable.js"></script>
        <script src="<?php echo assets; ?>js/neon-api.js"></script>
        <script src="<?php echo assets; ?>js/jquery-ui/js/jquery.inputmask.bundle.min.js"></script>
        <script src="<?php echo assets; ?>js/neon-custom.js"></script>
        <script src="<?php echo assets; ?>js/neon-demo.js"></script>

        <script>
            $(document).ready(function() {
                $("#delete-partner-account-modal").on("show.bs.modal", function (event) {
                    var button = $(event.relatedTarget);
                    var partner_account_id = button.data("id");
                    var partner_account_name = button.data("name");
                    var delete_partner_href = button.data("href");

                    var modal = $(this);
                    modal.find('.modal-body p').text(partner_account_name + ' will be deleted from partner account list. Are you sure?');
                    // var href = modal.find('.modal-footer a').attr('href');
                    // href = href + '/' + partner_account_id;
                    modal.find('.modal-footer a').attr('href', delete_partner_href);
                });

                $("#delete-partner-product-modal").on("show.bs.modal", function (event) {
                    var button = $(event.relatedTarget);
                    var partner_product_id = button.data("id");
                    var partner_product_name = button.data("name");
                    var delete_partner_href = button.data("href");

                    var modal = $(this);
                    modal.find('.modal-body p').text(partner_product_name + ' will be deleted from partner product list. Are you sure?');
                    // href = modal.find('.modal-footer a').attr('href');
                    // href = href + '/' + partner_product_id;
                    modal.find('.modal-footer a').attr('href', delete_partner_href);
                });
            });
        </script>

        <div class="modal fade" id="modal-1">

        </div>
    </body>
</html>
<script type="text/javascript">
$('.pilihtanggal').datepicker({
    format: 'dd-mm-yyyy',
});
</script>
