<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* setting zona waktu */
date_default_timezone_set('Asia/Jakarta');
$this->fpdf->FPDF("L","cm","A4");

// kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
$this->fpdf->SetMargins(1,1,1);
/* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
di footer, nanti kita akan membuat page number dengan format : number page / total page
*/
$this->fpdf->AliasNbPages();  

// AddPage merupakan fungsi untuk membuat halaman baru
$this->fpdf->AddPage();

// Setting Font : String Family, String Style, Font size
$this->fpdf->SetFont('Arial','B',12);

/* Kita akan membuat header dari halaman pdf yang kita buat
————– Header Halaman dimulai dari baris ini —————————–
*/
$this->fpdf->Cell(5,0.5,'PT. Portal Bursa Digital',0,0,'L');
$this->fpdf->Cell(22,0.5,'Insertion Order',0,0,'R'); 

// fungsi Ln untuk membuat baris baru
$this->fpdf->Ln(1);
$this->fpdf->SetFont('Arial','',8);

// fill cell
$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(255,255,255); 
$this->fpdf->Cell(5,0.3,' DIPESAN DARI',0,0,'L', true);

// back to default
$this->fpdf->SetTextColor(0,0,0); 
$this->fpdf->SetFillColor(255,255,255); 
$this->fpdf->Cell(17,0.3,'IO No. :',0,0,'R'); 
$this->fpdf->Cell(5,0.3,$header->io_number,0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Cell(5,0.3,$item[0]->name,0,0,'L', true);
$this->fpdf->Cell(17,0.3,'Tanggal IO :',0,0,'R'); 
$this->fpdf->Cell(5,0.3,$header->io_date,0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Cell(5,0.3,$item[0]->address_street,0,0,'L', true);
$this->fpdf->Cell(17,0.3,'Reff No.:',0,0,'R'); 
$this->fpdf->Cell(5,0.3,$header->quotation_number,0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Cell(5,0.3,$item[0]->address_city.', '.$item[0]->address_zip,0,0,'L', true);
$this->fpdf->Cell(17,0.3,'Advertiser :',0,0,'R'); 
$this->fpdf->Cell(5,0.3,$header->advertiser,0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Cell(5,0.3,'-',0,0,'L', true);
$this->fpdf->Cell(17,0.3,'Kategori Bisnis :',0,0,'R'); 
$this->fpdf->Cell(5,0.3,'',0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Cell(5,0.3,$item[0]->npwp,0,0,'L', true);
$this->fpdf->Cell(17,0.3,'Brand :',0,0,'R'); 
$this->fpdf->Cell(5,0.3,$header->brand_name,0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Cell(5,0.3,'',0,0,'L', true);
$this->fpdf->Cell(17,0.3,'Campaign :',0,0,'R'); 
$this->fpdf->Cell(5,0.3,$header->campaign,0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Cell(5,0.3,'',0,0,'L', true);
$this->fpdf->Cell(17,0.3,'Agent :',0,0,'R'); 
$this->fpdf->Cell(5,0.3,$header->agency,0,0,'L'); 
$this->fpdf->Ln();

$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','BU',7);
$this->fpdf->Cell(5.6,0.5,"NOMOR KONTAK",0,0,'L');
$this->fpdf->SetX(11);
$this->fpdf->Cell(23,0.5,'ADOPS KONTAK',0);
$this->fpdf->SetX(21);
$this->fpdf->Cell(20,0.5,"SALES KONTAK",0);

$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',8);
$this->fpdf->Cell(7,0.5,"Nama",1);
$this->fpdf->SetX(3);
$this->fpdf->Cell(14,0.5,": ".$item[0]->sales_name,0);
$this->fpdf->SetX(11);
$this->fpdf->Cell(7,0.5,'Nama',1);
$this->fpdf->SetX(13);
$this->fpdf->Cell(14,0.5,": ".$item[0]->adops_name,0);
$this->fpdf->SetX(21);
$this->fpdf->Cell(7,0.5,'Nama',1);
$this->fpdf->SetX(23);
$this->fpdf->Cell(14,0.5,": ".$header->sales_name,0);

$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',8);
$this->fpdf->Cell(7,0.5,"Email","BLR");
$this->fpdf->SetX(3);
$this->fpdf->Cell(14,0.5,": ".$item[0]->sales_email,0);
$this->fpdf->SetX(11);
$this->fpdf->Cell(7,0.5,'Email',"BLR");
$this->fpdf->SetX(13);
$this->fpdf->Cell(14,0.5,": ".$item[0]->adops_email,0);
$this->fpdf->SetX(21);
$this->fpdf->Cell(7,0.5,'Email',"BLR");
$this->fpdf->SetX(23);
$this->fpdf->Cell(14,0.5,": ".$header->sales_email,0);

$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',8);
$this->fpdf->Cell(7,0.5,"Phone","BLR");
$this->fpdf->SetX(3);
$this->fpdf->Cell(14,0.5,": ".$item[0]->sales_phone,0);
$this->fpdf->SetX(11);
$this->fpdf->Cell(7,0.5,'Phone',"BLR");
$this->fpdf->SetX(13);
$this->fpdf->Cell(14,0.5,": ".$item[0]->adops_phone,0);
$this->fpdf->SetX(21);
$this->fpdf->Cell(7,0.5,'Phone',"BLR");
$this->fpdf->SetX(23);
$this->fpdf->Cell(14,0.5,": ".$header->sales_phone,0);

$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(255,255,255); 

$this->fpdf->Ln(1);
$this->fpdf->SetFont('Arial','',6);
$this->fpdf->Cell(4,0.5,"Item",'TLB',0,'C', true);
$this->fpdf->Cell(9,0.5,"Deskripsi",'TLB',0,'C', true); 
$this->fpdf->Cell(4,0.5,"Tanggal Tayang",'TLB',0,'C', true); 
$this->fpdf->Cell(1.5,1,"Harga",'TL',0,'C', true);
$this->fpdf->Cell(2,1,"Kuota/Impresi/Klik",'TL',0,'C', true);
$this->fpdf->Cell(1.5,1,"Total Budget",'TL',0,'C', true);
$this->fpdf->Cell(2,1,"Budget Harian",'TL',0,'C', true);
$this->fpdf->Cell(1,1,"Diskon",'TL',0,'C', true);
$this->fpdf->Cell(2,1,"Total NET(IDR)",'TLR',0,'C', true); 
$this->fpdf->Ln(); 


$this->fpdf->Cell(2,-0.5,"Tipe",'L',0,'C', true);
$this->fpdf->Cell(2,-0.5,"Product",'L',0,'C',true);
$this->fpdf->Cell(3,-0.5,"Target",'L',0,'C', true);
$this->fpdf->Cell(1.5,-0.5,"Unit",'L',0,'C', true);
 
$this->fpdf->Cell(2.5,-0.5,"Kota",'L',0,'C', true);

$this->fpdf->Cell(2,-0.5,"Area",'L',0,'C', true); 
$this->fpdf->Cell(1.5,-0.5,"Mulai",'L',0,'C', true);
$this->fpdf->Cell(1.5,-0.5,"Akhir",'L',0,'C', true);
$this->fpdf->Cell(1,-0.5,"Hari",'LR',0,'C', true);
$this->fpdf->Ln(0.01); 

$no = 1;
$this->fpdf->SetTextColor(0,0,0); 
$this->fpdf->SetFillColor(255,255,255); 
$this->fpdf->SetFont('Arial','',6);
foreach ($item as $list){
    
    $this->fpdf->Cell(2,0.5,$list->type,"L",0,'L');
    $this->fpdf->Cell(2,0.5,$list->product,"L",0,'L');
    $this->fpdf->Cell(3,0.5,$list->target,"L",0,'L');
    $this->fpdf->Cell(1.5,0.5,$list->unit,"L",0,'L');
    $this->fpdf->Cell(2.5,0.5,$list->city,"L",0,'L');
    $this->fpdf->Cell(2,0.5,$list->area,"L",0,'L');
    $this->fpdf->Cell(1.5,0.5,date("d-M-Y", strtotime($list->start_date)),"L",0,'R');
    $this->fpdf->Cell(1.5,0.5,date("d-M-Y", strtotime($list->end_date)),"L",0,'R');
    
    $date1 = new DateTime($list->start_date);
    $date2 = new DateTime($list->end_date);
    $diff = $date2->diff($date1)->format("%a");
    
    $this->fpdf->Cell(1,0.5,$diff,"L",0,'C');
    $this->fpdf->Cell(1.5,0.5,  number_format($list->price),"L",0,'R');
    $this->fpdf->Cell(2,0.5,  number_format($list->amount),"L",0,'R');
    $this->fpdf->Cell(1.5,0.5,  number_format($list->price*$list->amount),"L",0,'R');
    $m21 = ($list->price*$list->amount);
    $this->fpdf->Cell(2,0.5,  $diff>0 ? number_format($m21/$diff,2):0 ,"L",0,'R');
    $this->fpdf->Cell(1,0.5,$list->diskon."%","L",0,'R');
    $this->fpdf->Cell(2,0.5,  number_format($m21-($m21*($list->diskon/100))),"LR",0,'R');  
    $this->fpdf->Ln();
    
    $total +=$m21-($m21*($list->diskon/100));
}


$this->fpdf->Cell(22,0.5,"","TL",0,'C');
$this->fpdf->Cell(3,0.5,"TOTAL","TL",0,"R");
$this->fpdf->Cell(2,0.5,number_format($total),"TLR",0,"R");
$this->fpdf->Ln();

$this->fpdf->Cell(22,0.5,"","L",0,'C');
$this->fpdf->Cell(3,0.5,"PPN","L",0,"R");
$this->fpdf->Cell(2,0.5,number_format($total*(10/100)),"TLR",0,"R");
$this->fpdf->Ln();

$this->fpdf->Cell(22,0.5,"","LBR",0,'C');
$this->fpdf->Cell(3,0.5,"GRAND TOTAL","B",0,"R");
$this->fpdf->Cell(2,0.5,number_format(($total*(10/100))+$total),1,0,"R");
$this->fpdf->Ln(1);

$this->fpdf->Cell(2,0.3,"1. Campaign tidak bisa carry over",0,'L');

$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(255,255,255); 

$this->fpdf->Ln(1); 
$this->fpdf->Cell(27,0.5,"Tanda tangan","TLR",0,'L',true);
$this->fpdf->SetTextColor(0,0,0);
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.3,"","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(12,0.3,"PT.Portal Bursa Digital","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.2,"","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(12,0.2,"Oleh:","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(15,1,"","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(12,1,"","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.3,"","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(12,0.3,"Nama","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(14,0.3,"","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(12,0.3,"Jabatan","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.3,"","BL",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(12,0.3,"Tanggal","BR",0,'L');
$this->fpdf->Ln();

$this->fpdf->Output("Quotation.pdf","I"); 

?>

