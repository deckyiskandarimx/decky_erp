<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* setting zona waktu */
date_default_timezone_set('Asia/Jakarta');
$this->fpdf->FPDF("P","cm","A4");

// kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
$this->fpdf->SetMargins(1,1,1);
/* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
di footer, nanti kita akan membuat page number dengan format : number page / total page
*/
$this->fpdf->AliasNbPages();  

// AddPage merupakan fungsi untuk membuat halaman baru
$this->fpdf->AddPage();

/* Kita akan membuat header dari halaman pdf yang kita buat
————– Header Halaman dimulai dari baris ini —————————–
*/
$this->fpdf->SetFont('Arial','B',12);

$this->fpdf->Cell(5,0.5,'PT. Portal Bursa Digital',0,0,'L');
$this->fpdf->SetTextColor(51,102,255); 
$this->fpdf->Cell(14,0.5,'Invoice',0,0,'R'); 

$this->fpdf->SetFillColor(51,102,255); 
$this->fpdf->SetTextColor(255,255,255); 

$this->fpdf->SetFont('Arial','B',8);
$this->fpdf->Ln(1);
$this->fpdf->SetX(1);
$this->fpdf->Cell(7,0.5,'DITAGIHKAN KEPADA',0,0,'L',true);

$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(0,0,0);

$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',8);
$this->fpdf->Cell(7,0.5,$invoice->client_name,0,0,'L');


$this->fpdf->Ln();
$this->fpdf->Multicell(7,0.5,$invoice->address_street,0);
$this->fpdf->SetX(13);

$this->fpdf->Cell(2,0.5,"Invoice No ",0,0,'R'); 
$this->fpdf->Cell(5,0.5,$invoice->pinv_number,"TLR",0,'L'); 


$this->fpdf->Ln();
$this->fpdf->Cell(7,0.5,$invoice->city.', '.$invoice->province.', '.$invoice->address_zip,0,0,'L');
$this->fpdf->SetX(13);
$this->fpdf->Cell(2,0.5,"Tanggal Invoice ",0,0,'R'); 
$this->fpdf->Cell(5,0.5,date("d-M-Y", strtotime($invoice->pinv_date)),"TLR",0,'L'); 

$this->fpdf->Ln();
$this->fpdf->Cell(7,0.5,$invoice->phone,0,0,'L');
$this->fpdf->SetX(13);
$this->fpdf->Cell(2,0.5,"Jatuh Tempo ",0,0,'R'); 
$this->fpdf->Cell(5,0.5,"","TLR",0,'L'); 

$this->fpdf->Ln();
$this->fpdf->Cell(7,0.5,"NPWP : ".$invoice->npwp,0,0,'L');
$this->fpdf->SetX(13);
$this->fpdf->Cell(2,0.5,"Bulan Tagihan ",0,0,'R'); 
$this->fpdf->Cell(5,0.5,"",1,0,'L'); 

$this->fpdf->SetFillColor(51,102,255); 
$this->fpdf->SetTextColor(255,255,255); 
$this->fpdf->Ln(1);
$this->fpdf->Cell(14,0.5,'DESKRIPSI',"TLB",0,'C', true);
$this->fpdf->Cell(5,0.5,'TOTAL',1,0,'C', true);

$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(0,0,0);
$this->fpdf->SetFont('Arial','',8);

$jml = 0;


    $this->fpdf->Ln();
    $this->fpdf->Cell(14,0.5,$invoice->package_name.', ('.$invoice->product_code.')','L',0,'L');
    $this->fpdf->Cell(5,0.5,  number_format($invoice->product_tariff_idr_total),'LR',0,'R');
    $jml = $invoice->product_tariff_idr_total; 


$this->fpdf->Ln();
$this->fpdf->Cell(14,0.5,'','L',0,'L');
$this->fpdf->Cell(5,0.5,'','LR',0,'R');

$this->fpdf->Ln();
$this->fpdf->Cell(14,0.5,'','L',0,'L');
$this->fpdf->Cell(5,0.5,'','LR',0,'R');

$this->fpdf->Ln();
$this->fpdf->Cell(14,0.5,'','LB',0,'L');
$this->fpdf->Cell(5,0.5,'','LR',0,'R');

$this->fpdf->Ln();
$this->fpdf->Cell(14,0.5,'Jumlah Tagihan/Total Amount',0,0,'R');
$this->fpdf->Cell(5,0.5,number_format($jml),1,0,'R');

$this->fpdf->Ln();
$this->fpdf->Cell(14,0.5,'Dasar Pengenaan Pajak / Tax Base Value',0,0,'R');
$this->fpdf->Cell(5,0.5,number_format($jml),"LR",0,'R');

$taxx = ($jml)*0.1;

$this->fpdf->Ln();
$this->fpdf->Cell(14,0.5,'PPN / Value Added Tax',0,0,'R');
$this->fpdf->Cell(5,0.5,number_format($taxx),1,0,'R');

$gt = $taxx+$jml;
$this->fpdf->SetFont('Arial','B',8);
$this->fpdf->Ln();
$this->fpdf->Cell(14,0.5,'Total Tagihan / Total Charge',0,0,'R');
$this->fpdf->Cell(5,0.5,number_format($gt),"LRB",0,'R');

$this->fpdf->SetFont('Arial','BI',8);
$this->fpdf->Ln();
$this->fpdf->Cell(19,1,  Payment::terbilang($gt),0,0,'R');

$this->fpdf->SetFont('Arial','',8);
$this->fpdf->SetFillColor(51,102,255); 
$this->fpdf->SetTextColor(255,255,255);
$this->fpdf->Ln(1);
$this->fpdf->Cell(6,0.5,'Pembayaran dialamatkan kepada','RLT',0,'L', true);
$this->fpdf->Ln();
$this->fpdf->Cell(6,0.5,'Payment should be addressed to','RLB',0,'L', true);
$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(0,0,0);
$this->fpdf->Ln();
$this->fpdf->Cell(6,0.5,'Bank Mandiri KCP Jakarta Gedung Indosat','RL',0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(6,0.5,'Jl. Medan Merdeka Barat no 21','RL',0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(6,0.5,'','RL',0,'L');
$this->fpdf->SetFont('Arial','BI',9);
$this->fpdf->Ln();
$this->fpdf->Cell(6,0.5,'Rek No (IDR) 1210008098984','LR',0,'L');
$this->fpdf->SetX(13);
$this->fpdf->Cell(6,0.5,'Prasetya Abimanyu','B',0,'C');
$this->fpdf->Ln();
$this->fpdf->Cell(6,0.5,'a/n PT Portal Bursa Digital','LRB',0,'L');
$this->fpdf->SetX(13);
$this->fpdf->Cell(6,0.5,'Financial Controller',0,0,'C');

$this->fpdf->Output("Quotation.pdf","I"); 

?>

