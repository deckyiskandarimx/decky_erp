<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('report'); ?>">Reports</a>
    </li>
    <li>
        <a href="<?php echo base_url('report/rev'); ?>">Revenue</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<h1><?php echo $title; ?></h1>
<br />

<table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>no. </th>                
                <th>Campaign Number</th>
                <th>Campaign Name</th>
                <th>IO Number</th>
                <th>Invoice Number</th>
                <th>Invoice Date</th>	
                <th>Payment Status</th>
                <th>Payment Date</th>			                
                <th>Cost Base</th>			                
                <th>Third Party Cost</th>
                <th>Rev Share Indosat</th>
                <th>COGS</th>
                <th>Gross Margin (rev nett)</th>
            </tr>
        </thead>
        <?php if (sizeof($datarev) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($datarev as $data):
                    
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td> 
                        <td><?php echo $data->campaign_name; ?></td>  
                        <td><?php echo $data->quotation_number; ?></td>  
                        <td><?php echo $data->io_number; ?></td>  
                        <td style="vertical-align: text-top;"><?php echo $data->number; ?></td>
                        <td style="vertical-align: text-top;"><?php echo $data->invdate; ?></td>
                        <td><?php echo $data->paid == 1 ? "Paid" : "Not Paid"; ?></td>        
                        <td><?php echo date("d-m-Y", strtotime($data->paid_date)); ?></td>        
                        <td><?php echo number_format($data->bearer_costdb); ?></td>        
                        <td><?php echo number_format($data->partner_share); ?></td>     
                        <td><?php echo number_format($data->shareisat); ?></td>        
                        <td><?php echo number_format($data->cogs); ?></td>                                                         
                        <td><?php echo number_format((int)$data->total-(int)$data->cogs); ?></td>      
                    </tr>		
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>
                <th>no. </th>       
                <th>Campaign Number</th>
                <th>Campaign Name</th>
                <th>IO Number</th>
                <th>Invoice Number</th>
                <th>Invoice Date</th>	
                <th>Payment Status</th>
                <th>Payment Date</th>			                
                <th>Cost Base</th>			                
                <th>Third Party Cost</th>
                <th>Rev Share Indosat</th>
                <th>COGS</th>
                <th>Gross Margin (rev nett)</th>
            </tr>
        </tfoot>    
    </table>


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-red entypo-download" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("report/revdownload") ?>\'"> Download Report</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>