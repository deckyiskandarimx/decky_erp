<ol class="breadcrumb bc-3">
	<li>
		<a href="<?php echo base_url('/');?>"><i class="entypo-home"></i>Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('report');?>">Reports</a>
	</li>
        <li>
		<a href="<?php echo base_url('report/ar');?>">Accounts Receivable</a>
	</li>
	<li class="active">
		<strong>List</strong>
	</li>
</ol>
<h1><?php echo $title; ?></h1>
<br />

<form action="<?php echo base_url("report/"); ?>" method="post" id="formcategory">
<table class="table table-bordered datatable" id="table-4">
	<thead>
		<tr>
            <th>No.</th>
            <th>Quotation #</th>			
            <th>Campaign Name</th>			
            <th>Detail Item</th>
            <th>Quotation Amount</th>
            <th>Invoiced Amount</th>
            <th>Variance</th>
            <th>Aging Days</th>
            <th>Aging</th>
            <th>Status</th>
		</tr>
	</thead>
        <?php if(sizeof($quotation) > 0): ?>
	<tbody>
            <?php  $num = 1; foreach ($quotation as $data):?>
		<tr class="odd gradeX">
                        <td style="width: 15px; vertical-align: top;"><?php echo $num++;?></td>
			<td style="width: 50px; vertical-align: top;"><?php echo $data->quotation_number;?></td>
                        <td style="width: 100px; vertical-align: top;"><?php echo $data->campaign_name;?></td>
                        <td style="width: 500px; vertical-align: text-bottom;">
                            
                            <table style="width: 100%; font-size: small; border-style: inherit; border-color: inherit; padding: initial;" border="1">    
                                <thead style="background-color: inherit;">                                                                                                                                                                           
                                    <th style="width: 15%;">IO Number</th>                                
                                    <th style="width: 40%;">Product</th>    
                                    <th style="width: 30%;">Partner</th>                                    
                                </thead>
                                <tbody>                                    
                                        <?php $nod = 1; foreach (Report::productdetailcpay($data->quotation_id) as $pcpay):
                                           
                                            ?>
                                        <tr>   
                                            <td style="vertical-align: top;"><?php echo $pcpay->io_number; ?></td> 
                                            <td style="vertical-align: top;"><?php echo $pcpay->product_name; ?></td> 
                                            <td style="vertical-align: top;"><?php echo $pcpay->partner_name; ?></td>                                            
                                        </tr>
                                        <?php 
                                        
                                        endforeach;?>
                                    
                                </tbody>
                            </table>
                           
                        </td> 
                        <td style="width: 20px; vertical-align: top; text-align: right;"><?php echo number_format(Report::getquotationtotal($data->quotation_id)); //number_format($data->total_quotation); ?></td>
                        <td style="width: 20px; vertical-align: top; text-align: right;"><?php echo number_format($data->pinv_total); ?></td>
                        <td style="width: 20px; vertical-align: top; text-align: right;"><?php echo number_format($data->total_quotation-$data->pinv_total); ?></td>
                        <td style="width: 20px; vertical-align: top;"><?php echo $data->total_quotation == 0 ? 0 : Report::agingdays($data->quotation_id); ?></td>
                        <td style="width: 20px; vertical-align: top;">
                            <?php 
                                
                                  if (Report::agingdays($data->quotation_id) < 30) {
                                      echo "0 - 29";
                                  }  else if(Report::agingdays($data->quotation_id) >= 30 && Report::agingdays($data->quotation_id) <= 60){
                                      echo "30 - 60";
                                  } else if(Report::agingdays($data->quotation_id) >= 61 && Report::agingdays($data->quotation_id) <= 90){
                                      echo "61 - 90";
                                  } else if(Report::agingdays($data->quotation_id) > 90 && $data->total_quotation != 0){
                                      echo "> 90";
                                  } 

                            ?>                        
                        </td>
                        <td style="width: 20px; vertical-align: top;">
                            <?php 
                                if(Report::getpaymentpaid($data->quotation_id) > 0){
                                    echo "PAID";
                                }else{
                                    if(Report::getstatuspayment($data->quotation_id) > 0):  
                                        echo "Invoice Sent";
                                    else :
                                       echo "Invoicing Process"; 
                                    endif;
                                }
                            ?>
                        </td>
		</tr>		
            <?php endforeach; ?>
	</tbody>
        <?php endif; ?>
</table>

</form>

        <link rel="stylesheet" href="<?php echo assets;?>js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?php echo assets;?>js/select2/select2.css">

	<!-- Bottom Scripts -->
	
	<script src="<?php echo assets;?>js/jquery.dataTables.min.js"></script>
	<script src="<?php echo assets;?>js/datatables/TableTools.min.js"></script>
	<script src="<?php echo assets;?>js/dataTables.bootstrap.js"></script>
	<script src="<?php echo assets;?>js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="<?php echo assets;?>js/datatables/lodash.min.js"></script>
	<script src="<?php echo assets;?>js/datatables/responsive/js/datatables.responsive.js"></script>
	<script src="<?php echo assets;?>js/select2/select2.min.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		var table = $("#table-4").dataTable({
			"sPaginationType": "bootstrap",
			"oTableTools": {
			},
			
		});
                
        $("div.dataTables_length").append('<button type="button" class="btn btn-red entypo-download" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("report/ardownload2") ?>\'"> Download Report</button>');
        $(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1 
		});
	});
</script>