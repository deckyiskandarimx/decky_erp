<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url('/'); ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo base_url('report'); ?>">Reports</a>
    </li>
    <li>
        <a href="<?php echo base_url('report/ap'); ?>">Accounts Payable</a>
    </li>
    <li class="active">
        <strong>List</strong>
    </li>
</ol>
<h1><?php echo $title; ?></h1>
<br />

<form action="<?php echo base_url("report/"); ?>" method="post" id="formcategory">
    <table class="table table-bordered datatable" id="table-4">
        <thead>
            <tr>
                <th>no. </th>
                <th>&nbsp;</th>
                <th>Quotation Number</th>
                <th>Campaign Name</th>	
                <th>IO Number</th>
                <th>Product</th>			                
                <th>Partner</th>			                
                <th>IO Amount</th>
                <th>Invoice Amount</th>
                <th>Adjustment</th>
                <th>Status</th>
                <th>Aging (days)</th>
                <th>Aging (category)</th>
                <th>Sales</th>
            </tr>
        </thead>
        <?php if (sizeof($io_report) > 0): ?>
            <tbody>
                <?php
                $num = 1;
                foreach ($io_report as $data):
                    
                    $ioamount = Report::getIOAmount($data->campaign_item_id);
                    $transferamount= $data->transfer_amount;
                    $adj = (int)$ioamount - (int)$transferamount;
                    ?>
                    <tr class="odd gradeX">
                        <td style="width: 15px;"><?php echo $num++; ?></td>
                        <td style="width: 15px; vertical-align: text-top;"><input type="checkbox" name="io_id[]" value="<?php echo $data->io_id ?>"></td>
                        <td style="vertical-align: text-top;"><?php echo $data->quotation_number; ?></td>
                        <td style="vertical-align: text-top;"><?php echo $data->campaign_name; ?></td>
                        <td><?php echo $data->io_number; ?></td>        
                        <td><?php echo $data->product_name; ?></td>        
                        <td><?php echo $data->partner_name; ?></td>        
                        <td><?php echo number_format($ioamount); ?></td>        
                        <td><?php echo $transferamount==0? "": number_format($transferamount); ?></td>                                
                        <td><?php echo number_format($adj); ?></td>      
                        <td style="vertical-align: text-top;">
                            <?php
                            /* if ($data->paid_status == 2) {
                                $status = 'paid';
                            } else {
                                $status = 'unpaid';
                            }
                            echo $status; */
							
							if(Report::getpaymentpaid($data->quotation_id) > 0){
                                    echo "PAID";
                                }else{
                                    if(Report::getstatuspayment($data->quotation_id) > 0):  
                                        echo "Invoice Sent";
                                    else :
                                       echo "Invoicing Process"; 
                                    endif;
                                }
                            ?>
                        </td>
                        <td style="vertical-align: text-top;">
                            <?php
                            $invdate = $data->invoice_date;
                            $paiddate = $data->paid_date;
                            $nowdate = date("Y-m-d");
                            
                            if ($data->paid_status == 2) {
                                
                                $dt1 = strtotime($invdate);
                                $dt2 = strtotime($paiddate);
                                $diff = abs($dt2-$dt1);
                                $selisih = $diff/86400;
                                
                                
                                
                            } else {
                                
                                $dt1 = strtotime($invdate);
                                $dt2 = strtotime($nowdate);
                                $diff = abs($dt2-$dt1);
                                $selisih = $diff/86400;
                                
                            }
                            echo $transferamount==0 ? "":$selisih." Day(s)";
                            ?> 
                        </td>
                        <td style="vertical-align: text-top;">
                            <?php
                            if($selisih < 30){
                                $agingkat = "0 - 29";
                            }else if($selisih >= 30 && $selisih <= 59 ){
                                $agingkat = "30 - 59";
                            }else if($selisih >= 60 && $selisih <= 90 ){
                                $agingkat = "60 - 90";
                            }else if($selisih > 90){
                                $agingkat = "90";
                            }
                            
                            echo $transferamount==0? "": $agingkat;
                            ?>
                        </td>
                        <td style="vertical-align: text-top;"><?php echo $data->fullname ;?></td>
                    </tr>		
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>
                <th>no. </th>
                <th>&nbsp;</th>
                <th>Quotation Number</th>
                <th>Campaign Name</th>	
                <th>IO Number</th>
                <th>Product</th>			                
                <th>Partner</th>		
                <th>IO Amount</th>
                <th>Invoice Amount</th>
                <th>Adjustment</th>
                <th>Status</th>
                <th>Aging (days)</th>
                <th>Aging (category)</th>
                <th>Sales</th>
            </tr>
        </tfoot>    
    </table>

</form>


<link rel="stylesheet" href="<?php echo assets; ?>js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo assets; ?>js/select2/select2.css">

<!-- Bottom Scripts -->

<script src="<?php echo assets; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/TableTools.min.js"></script>
<script src="<?php echo assets; ?>js/dataTables.bootstrap.js"></script>
<script src="<?php echo assets; ?>js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo assets; ?>js/datatables/lodash.min.js"></script>
<script src="<?php echo assets; ?>js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo assets; ?>js/select2/select2.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "oTableTools": {
            },
        });
        $("div.dataTables_length").append('<button type="button" class="btn btn-red entypo-download" style="margin-left: 30px;" onclick="location.href=\'<?php echo base_url("report/apdownload") ?>\'"> Download Report</button>');
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>