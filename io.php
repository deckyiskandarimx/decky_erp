<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* setting zona waktu */
date_default_timezone_set('Asia/Jakarta');
$this->fpdf->FPDF("L","cm","A4");

// kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
$this->fpdf->SetMargins(1,1,1);
/* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
di footer, nanti kita akan membuat page number dengan format : number page / total page
*/
$this->fpdf->AliasNbPages();  

// AddPage merupakan fungsi untuk membuat halaman baru
$this->fpdf->AddPage();

// Setting Font : String Family, String Style, Font size
$this->fpdf->SetFont('Times','B',8);

/* Kita akan membuat header dari halaman pdf yang kita buat
————– Header Halaman dimulai dari baris ini —————————–
*/
$this->fpdf->Cell(5.5,0.5,'PT. PORTAL BURSA DIGITAL',0,0,'C');
$this->fpdf->Cell(29,0.5,'QUOTATION',0,0,'C'); 

// fungsi Ln untuk membuat baris baru
$this->fpdf->Ln(1);

$this->fpdf->SetFont('Times','U',8);
$this->fpdf->Cell(4.8,0.5,'DITAGIHKAN KEPADA',0,0,'C');
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(20);
$this->fpdf->SetFont('Times','B',8);
$this->fpdf->Cell(23,0.5,'Q.No.',0);
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(23);
$this->fpdf->Cell(20,0.5,": ".$quotation->quotation_number,0);

$this->fpdf->Ln();
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,$quotation->agency_name,0,0,'L');
$this->fpdf->SetX(20);
$this->fpdf->SetFont('Times','B',8);
$this->fpdf->Cell(23,0.5,'Tanggal Q',0);
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(23);
$this->fpdf->Cell(20,0.5,": ".$quotation->quotation_date,0);

$this->fpdf->Ln();
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,$quotation->address_street,0,0,'L');
$this->fpdf->SetX(20);
$this->fpdf->SetFont('Times','B',8);
$this->fpdf->Cell(23,0.5,'Advertiser',0);
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(23);
$this->fpdf->Cell(20,0.5,": ".$quotation->advertiser_name,0);

$this->fpdf->Ln();
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,$quotation->city_agency.", ".$quotation->agency_zip,0,0,'L');
$this->fpdf->SetX(20);
$this->fpdf->SetFont('Times','B',8);
$this->fpdf->Cell(23,0.5,'Kategori Bisnis',0);
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(23);
$this->fpdf->Cell(20,0.5,": ".$quotation->adv_category,0);

$this->fpdf->Ln();
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,$quotation->agency_phone,0,0,'L');
$this->fpdf->SetX(20);
$this->fpdf->SetFont('Times','B',8);
$this->fpdf->Cell(23,0.5,'Brand',0);
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(23);
$this->fpdf->Cell(20,0.5,": ".$quotation->brand_name,0);

$this->fpdf->Ln();
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,$quotation->agency_npwp,0,0,'L');
$this->fpdf->SetX(20);
$this->fpdf->SetFont('Times','B',8);
$this->fpdf->Cell(23,0.5,'Campaign',0);
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(23);
$this->fpdf->Cell(20,0.5,": ".$quotation->campaign_name,0);

$this->fpdf->Ln();
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,'',0,0,'L');
$this->fpdf->SetX(20);
$this->fpdf->SetFont('Times','B',8);
$this->fpdf->Cell(23,0.5,'Payment Type',0);
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(23);
$this->fpdf->Cell(20,0.5,": ".$quotation->payment_type,0);

$this->fpdf->Ln();
$this->fpdf->SetX(1.8);
$this->fpdf->SetFont('Times','BU',8);
$this->fpdf->Cell(5.6,0.5,"NOMOR KONTAK",0,0,'L');
$this->fpdf->SetX(10);
$this->fpdf->Cell(23,0.5,'ADOPS KONTAK',0);
$this->fpdf->SetX(20);
$this->fpdf->Cell(20,0.5,"SALES KONTAK",0);

$this->fpdf->Ln();
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,"Nama",1);
$this->fpdf->SetX(3);
$this->fpdf->Cell(14,0.5,": ".$quotation->agency_finance_name,0);
$this->fpdf->SetX(10);
$this->fpdf->Cell(8,0.5,'Nama',1);
$this->fpdf->SetX(12);
$this->fpdf->Cell(14,0.5,": ".$quotation->agency_media_name,0);
$this->fpdf->SetX(20);
$this->fpdf->Cell(8,0.5,'Nama',1);
$this->fpdf->SetX(22);
$this->fpdf->Cell(14,0.5,": ".$quotation->sales_name,0);

$this->fpdf->Ln();
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,"Email",1);
$this->fpdf->SetX(3);
$this->fpdf->Cell(14,0.5,": ".$quotation->agency_finance_email,0);
$this->fpdf->SetX(10);
$this->fpdf->Cell(8,0.5,'Email',1);
$this->fpdf->SetX(12);
$this->fpdf->Cell(14,0.5,": ".$quotation->agency_media_email,0);
$this->fpdf->SetX(20);
$this->fpdf->Cell(8,0.5,'Email',1);
$this->fpdf->SetX(22);
$this->fpdf->Cell(14,0.5,": ".$quotation->sales_email,0);

$this->fpdf->Ln();
$this->fpdf->SetFont('Times','',8);
$this->fpdf->SetX(1.8);
$this->fpdf->Cell(5.6,0.5,"Phone",1);
$this->fpdf->SetX(3);
$this->fpdf->Cell(14,0.5,": ".$quotation->agency_finance_phone,0);
$this->fpdf->SetX(10);
$this->fpdf->Cell(8,0.5,'Phone',1);
$this->fpdf->SetX(12);
$this->fpdf->Cell(14,0.5,": ".$quotation->agency_media_phone,0);
$this->fpdf->SetX(20);
$this->fpdf->Cell(8,0.5,'Phone',1);
$this->fpdf->SetX(22);
$this->fpdf->Cell(14,0.5,": ".$quotation->sales_phone,0);

$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(255,255,255); 


$this->fpdf->Ln(1);
$this->fpdf->SetFont('Times','B',6);
$this->fpdf->SetX(1);
$this->fpdf->Cell(4,0.5,"ITEM",1,0,'C', true);
$this->fpdf->SetX(5);
$this->fpdf->Cell(10.5,0.5,"DESKRIPSI",1,0,'C', true); 
$this->fpdf->SetX(15.5);
$this->fpdf->Cell(4,0.5,"TANGGAL TAYANG",1,0,'C', true); 
$this->fpdf->Cell(1.5,1,"Harga",1,0,'C', true);
$this->fpdf->Cell(2,1,"Kuota/Impresi/Klik",1,0,'C', true);
$this->fpdf->Cell(1.5,1,"Total Budget",1,0,'C', true);
$this->fpdf->Cell(2,1,"Budget Harian",1,0,'C', true);
$this->fpdf->Cell(1,1,"Diskon",1,0,'C', true);
$this->fpdf->Cell(2,1,"Total NET(IDR)",1,0,'C', true); 
$this->fpdf->Ln(); 


$this->fpdf->Cell(2,-0.5,"Tipe",1,0,'C', true);
$this->fpdf->Cell(2,-0.5,"Product",1,0,'C',true);
$this->fpdf->Cell(3,-0.5,"Target",1,0,'C', true);
$this->fpdf->Cell(2.5,-0.5,"Unit",1,0,'C', true);
 
$this->fpdf->Cell(2.5,-0.5,"Kota",1,0,'C', true);

$this->fpdf->Cell(2.5,-0.5,"Area",1,0,'C', true); 
$this->fpdf->Cell(1.5,-0.5,"Mulai",1,0,'C', true);
$this->fpdf->Cell(1.5,-0.5,"Akhir",1,0,'C', true);
$this->fpdf->Cell(1,-0.5,"Hari",1,0,'C', true);
$this->fpdf->Ln(0.01); 

$no = 1;
$this->fpdf->SetTextColor(0,0,0); 
$this->fpdf->SetFillColor(255,255,255); 
$this->fpdf->SetFont('Times','',6);
foreach ($listitem as $list){
    
    $this->fpdf->Cell(2,0.5,$list->type,1,0,'L');
    $this->fpdf->Cell(2,0.5,$list->product,1,0,'L');
    $this->fpdf->Cell(3,0.5,$list->target,1,0,'L');
    $this->fpdf->Cell(2.5,0.5,$list->unit,1,0,'L');
    $this->fpdf->Cell(2.5,0.5,$list->city,1,0,'L');
    $this->fpdf->Cell(2.5,0.5,$list->area,1,0,'L');
    $this->fpdf->Cell(1.5,0.5,date("d-M-Y", strtotime($list->start_date)),1,0,'R');
    $this->fpdf->Cell(1.5,0.5,date("d-M-Y", strtotime($list->end_date)),1,0,'R');
    
    $date1 = new DateTime($list->start_date);
    $date2 = new DateTime($list->end_date);
    $diff = $date2->diff($date1)->format("%a");
    
    $this->fpdf->Cell(1,0.5,$diff,1,0,'C');
    $this->fpdf->Cell(1.5,0.5,  number_format($list->price),1,0,'R');
    $this->fpdf->Cell(2,0.5,  number_format($list->amount),1,0,'R');
    $this->fpdf->Cell(1.5,0.5,  number_format($list->price*$list->amount),1,0,'R');
    $m21 = ($list->price*$list->amount);
    $this->fpdf->Cell(2,0.5,  $diff>0 ? $m21/$diff:0 ,1,0,'R');
    $this->fpdf->Cell(1,0.5,$list->diskon."%",1,0,'R');
    $this->fpdf->Cell(2,0.5,  number_format($m21-($m21*($list->diskon/100))),1,0,'R');  
    $this->fpdf->Ln();
    
    $total +=$m21-($m21*($list->diskon/100));
}


$this->fpdf->Cell(2,0.5,"",1,0,'C');
$this->fpdf->Cell(2,0.5,"",1,0,'C');
$this->fpdf->Cell(3,0.5,"",1,0,'C');
$this->fpdf->Cell(2.5,0.5,"",1,0,'C');
$this->fpdf->Cell(2.5,0.5,"",1,0,'C');
$this->fpdf->Cell(2.5,0.5,"",1,0,'C');
$this->fpdf->Cell(1.5,0.5,"",1,0,'C');
$this->fpdf->Cell(1.5,0.5,"",1,0,'C');
$this->fpdf->Cell(1,0.5,"",1,0,'C');
$this->fpdf->Cell(1.5,0.5,"",1,0,'C');
$this->fpdf->Cell(2,0.5,"",1,0,'C');
$this->fpdf->Cell(1.5,0.5,"",1,0,'C');
$this->fpdf->Cell(2,0.5,"",1,0,'C');
$this->fpdf->Cell(1,0.5,"",1,0,'C');
$this->fpdf->Cell(2,0.5,"",1,0,'C');
$this->fpdf->Ln();

$this->fpdf->Cell(2,0.5,"","TL",0,'C');
$this->fpdf->Cell(2,0.5,"","T",0,'C');
$this->fpdf->Cell(3,0.5,"","T",0,'C');
$this->fpdf->Cell(2.5,0.5,"","T",0,'C');
$this->fpdf->Cell(2.5,0.5,"","T",0,'C');
$this->fpdf->Cell(2.5,0.5,"","T",0,'C');
$this->fpdf->Cell(1.5,0.5,"","T",0,'C');
$this->fpdf->Cell(1.5,0.5,"","T",0,'C');
$this->fpdf->Cell(1,0.5,"","T",0,'C');
$this->fpdf->Cell(1.5,0.5,"","T",0,'C');
$this->fpdf->Cell(2,0.5,"","T",0,'C');
$this->fpdf->Cell(1.5,0.5,"","T",0,'C');
$this->fpdf->Cell(2,0.5,"","LT",0,'C');
$this->fpdf->Cell(1,0.5,"Total","T",0,"R");
$this->fpdf->Cell(2,0.5,number_format($total),1,0,"R");
$this->fpdf->Ln();

$this->fpdf->Cell(2,0.5,"","L",0,'C');
$this->fpdf->Cell(2,0.5,"",0,'C');
$this->fpdf->Cell(3,0.5,"",0,'C');
$this->fpdf->Cell(2.5,0.5,"",0,'C');
$this->fpdf->Cell(2.5,0.5,"",0,'C');
$this->fpdf->Cell(2.5,0.5,"",0,'C');
$this->fpdf->Cell(1.5,0.5,"",0,'C');
$this->fpdf->Cell(1.5,0.5,"",0,'C');
$this->fpdf->Cell(1,0.5,"",0,'C');
$this->fpdf->Cell(1.5,0.5,"",0,'C');
$this->fpdf->Cell(2,0.5,"",0,'C');
$this->fpdf->Cell(1.5,0.5,"",0,'C');
$this->fpdf->Cell(2,0.5,"","L",0,'C');
$this->fpdf->Cell(1,0.5,"PPN","R",0,"R");
$this->fpdf->Cell(2,0.5,number_format($total*(10/100)),1,0,"R");
$this->fpdf->Ln();

$this->fpdf->Cell(2,0.5,"","LB",0,'C');
$this->fpdf->Cell(2,0.5,"","B",0,'C');
$this->fpdf->Cell(3,0.5,"","B",0,'C');
$this->fpdf->Cell(2.5,0.5,"","B",0,'C');
$this->fpdf->Cell(2.5,0.5,"","B",0,'C');
$this->fpdf->Cell(2.5,0.5,"","B",0,'C');
$this->fpdf->Cell(1.5,0.5,"","B",0,'C');
$this->fpdf->Cell(1.5,0.5,"","B",0,'C');
$this->fpdf->Cell(1,0.5,"","B",0,'C');
$this->fpdf->Cell(1.5,0.5,"","B",0,'C');
$this->fpdf->Cell(2,0.5,"","B",0,'C');
$this->fpdf->Cell(1.5,0.5,"","B",0,'C');
$this->fpdf->Cell(2,0.5,"","BL",0,'C');
$this->fpdf->Cell(1,0.5,"GRAND TOTAL","BR",0,"R");
$this->fpdf->Cell(2,0.5,number_format(($total*(10/100))+$total),1,0,"R");
$this->fpdf->Ln(1);

$this->fpdf->Cell(2,0.3,"1. Paket ini hanya berlaku 1 minggu dari tanggal quotation",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(2,0.3,"2. Pembayaran harus dilakukan 30 hari setelah invoice diterbitkan",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(2,0.3,"3. Materi campaign sudah harus diterima minimal h-2 (hari kerja, hari jum'at maksimal jam 12 siang",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(2,0.3,"4. Detail konten terlampir",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(2,0.3,"5. Silahkan melakukan pembayaran",0,'L'); 

$this->fpdf->Ln();
$this->fpdf->SetX(5);
$this->fpdf->Cell(10,0.3,"Account : PT Portal Bursa Digital",0,'L'); 

$this->fpdf->Ln();
$this->fpdf->SetX(5);
$this->fpdf->Cell(10,0.3,"Bank",0,'L'); 
$this->fpdf->SetX(10);
$this->fpdf->Cell(10,0.3,"Bank Mandiri cabang PT. Indosat",0,'L'); 

$this->fpdf->Ln();
$this->fpdf->SetX(5);
$this->fpdf->Cell(10,0.3,"Account",0,'L'); 
$this->fpdf->SetX(10);
$this->fpdf->Cell(10,0.3,"121.0008098984 (Rek IDR)",0,'L'); 

$this->fpdf->Ln();
$this->fpdf->SetX(5);
$this->fpdf->Cell(10,0.3,"",0,'L'); 
$this->fpdf->SetX(10);
$this->fpdf->Cell(10,0.3,"121.0003089897 (Rek USD (Rek IDR)",0,'L'); 

$this->fpdf->Ln();
$this->fpdf->Cell(2,0.3,"6. Pembatalan campaign akan dikenakan cancelation Fee sebagai berikuta",0,'L'); 
$this->fpdf->Ln();
$this->fpdf->SetX(1);
$this->fpdf->Cell(2,0.3,"H-7 : 50% dari total budget yang tertera di Quotation",0,'L'); 
$this->fpdf->Ln();
$this->fpdf->SetX(1);
$this->fpdf->Cell(2,0.3,"H-3 : 75% dari total budget yang tertera di Quotation",0,'L'); 
$this->fpdf->Ln();
$this->fpdf->SetX(1);
$this->fpdf->Cell(2,0.3,"Hari H : 100% dari total budget yang tertera di Quotation",0,'L'); 

$this->fpdf->SetFillColor(64,64,64); 
$this->fpdf->SetTextColor(255,255,255); 

$this->fpdf->Ln(1); 

$this->fpdf->Cell(28.5,0.2,"Tanda tangan","TLR",0,'L',true);
$this->fpdf->SetTextColor(0,0,0); 
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.2,"Liquid Thread","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(13.5,0.2,"PT.Portal Bursa Digital","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.5,"Oleh:","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(13.5,0.5,"Oleh:","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.3,"Nama","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(13.5,0.3,"Nama","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(14,0.3,"Jabatan","L",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(13.5,0.3,"Jabatan","R",0,'L');
$this->fpdf->Ln();
$this->fpdf->Cell(15,0.3,"Tanggal","BL",0,'L');
$this->fpdf->SetX(16);
$this->fpdf->Cell(13.5,0.3,"Tanggal","BR",0,'L');
$this->fpdf->Ln();

$this->fpdf->Output("Quotation.pdf","I"); 

?>

