<div class="row">
	
	<!-- Profile Info and Notifications -->
	<div class="col-md-6 col-sm-8 clearfix">
		
		<ul class="user-info pull-left pull-none-xsm">
		
						<!-- Profile Info -->
			<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
				
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<img src="<?php echo assets; ?>images/avatar.png" alt="" class="img-circle" width="44" style="" />
                                        <?php echo $this->session->userdata("fullname"); ?>&nbsp;&nbsp;&nbsp;<span class="label label-danger"><?php echo $this->session->userdata("role"); ?></span>
                                        <!--<h5 style="font-weight: bold; margin-top: 0px; float: left;">Role : <?php //echo $this->session->userdata("role"); ?></h5>-->
				</a>
                                
				<ul class="dropdown-menu">
					
					<!-- Reverse Caret -->
					<li class="caret"></li>
					
					<!-- Profile sub-links -->
					<li>
                                            <a href="<?php echo base_url("user/profile"); ?>">
							<i class="entypo-user"></i>
							Edit Profile
						</a>
					</li>
					
					<li>
						<a href="<?php echo base_url("user/changepswd");?>">
							<i class="entypo-cog"></i>
							Change Password
						</a>
					</li>
					
					<li>
                                            <a href="<?php echo base_url("auth/logout");?>">
							<i class="entypo-logout"></i>
							Logout
						</a>
					</li>
				</ul>
			</li>
		
		</ul>
				
		
	
	</div>
	
	
	<!-- Raw Links -->
	
	
</div>

<hr />