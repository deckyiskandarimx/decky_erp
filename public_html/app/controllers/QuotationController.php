<?php

class QuotationController extends ControllerNew {

    public function initialize() {
        $this->tag->setTitle('Quotation');
        parent::initialize();
    }

    public function indexAction() {

//get data list quotation
        $dataquotation = Quotation::find(array("order" => "quotation_id"));
        
        $var = array(
            'active_menu' => 'quotation',
            'sub_menu' => 'list_quotation',
            'page_title' => 'Quotation',
            'dataquotation' => $dataquotation,
            'sub_title' => 'quotation list & management',
            'extra' => array(
                'css' => array(
                    'external/metronic/assets/global/plugins/datatables/datatables.min.css',
                    'external/metronic/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'
                ),
                'js' => array(
                    'external/metronic/assets/global/plugins/datatables/datatables.min.js',
                    'external/metronic/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'
                ),
            )
        );

        $this->view->setVar('var', $var);
    }

    public function step_1Action($parent_id = null) {
        //$get = $this->request->getQuery('create');
        /* if ($get !== 'create') {
          $this->session->remove('step_value');
          } */
        $post = $this->request->getPost();
        $auth = $this->session->get("auth");
        $step_value = $this->session->get('step_value');
        $step2 = $step_value['step2'];

        if ($post) {
            if ($this->request->hasFiles() == TRUE) {
                $file_mo = array();
                foreach ($this->request->getUploadedFiles() as $file => $files) {
                    $name_file = str_replace(' ', '', $files->getName());
                    $name = $name_file . date("dmy") . rand(1, 99) . "-" . $file;
                    $path = strtolower("upload/mo/" . $name);
                    $files->moveTo($path);

                    array_push($file_mo, $name);
                }
                $this->session->set('file_mo', $file_mo);
            }
            
            $step = array(
                'step1' => true,
                'step2' => false,
                'step3' => false,
            );

            $step_value = array(
                'step1' => $post,
                'step2' => $step2,
                'step3' => ''
            );

            $this->session->set("step", $step);
            $this->session->set("step_value", $step_value);

            return $this->response->redirect('quotation/step_2/' . $parent_id);
        }

//get data for revision
        if ($parent_id != 0) {
            $dataquotationrev = Quotation::findFirst("quotation_id = $parent_id ");
        }

        $data_mo = QuotationMo::find("flag = 1");

//get data for dropdown           
        $dataclientbrand = ClientBrand::getClientbrand();
        $client = Client::find(array("order" => "client_id"));
        $user = UsersErp::find("user_type = 'AC03'");
        $payment_type = PaymentType::find(array("order" => "payment_id"));
//

        $var = array(
            'active_menu' => 'quotation',
            'datasummaries' => $data_summaries,
            'parent_id' => $parent_id,
            'sub_menu' => 'add_quotation',
            'page_title' => 'Quotation',
            'data_mo' => $data_mo,
            'user' => $user,
            'dataquotationrev' => $dataquotationrev,
            'sub_title' => 'quotation list & management',
            'client' => $client,
            'databrand' => $dataclientbrand,
            'payment_type' => $payment_type,
            'active_step' => 'step1',
            'extra' => array(
                'css' => array(
                    'external/metronic/assets/global/plugins/select2/css/select2.min.css',
                    'external/metronic/assets/global/plugins/select2/css/select2-bootstrap.min.css',
                    'external/metronic/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                    'external/metronic/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
                ),
                'js' => array(
                    'external/metronic/assets/global/plugins/select2/js/select2.full.min.js',
                    'external/metronic/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                    'external/metronic/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
                    'external/metronic/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
                    'external/metronic/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
                ),
            )
        );

        $this->view->setVar('var', $var);
    }

    function removeFilebyName($name) {
        $path = "upload/mo/" . $name;
        $directory = realpath($path);
        unlink($directory);
    }

    function getdatabrandAction() {
        $this->view->disable();

        $post = $this->request->getPost();

        if ($post) {
            $pp_id = $post['product_partner'];

            $productPartner = ProductPartner::getProductPartnerByID($pp_id);

            echo json_encode($productPartner);
        }
    }

    public function step_2Action($parent_id = null) {
        $post = $this->request->getPost();
        $sess = $this->session->get('step_value');
        $files = $sess['file'];

//get data summaries
        $sessionstep1 = $this->session->get('step_value');
        $step1 = $sessionstep1['step1'];
        $summaries = $this->session->get('step_value');
        $data_advertiser_id = $summaries['step1']['client'];
        $data_brand_id = $summaries['step1']['brand'];
        $data_sales_id = $summaries['step1']['sales'];
        $data_campaign_name = $summaries['step1']['campaign_name'];

        $summ_brand = Brand::findFirst("brand_id = $data_brand_id");
        $summ_advertiser = Client::findFirst("client_id = $data_advertiser_id");
        $summ_sales = UsersErp::findFirst("user_id = $data_sales_id");

        $data_summaries = array(
            'advertiser' => $summ_advertiser->client_name,
            'brand' => $summ_brand->brand_name,
            'sales' => $summ_sales->fullname,
            'campaign_name' => $data_campaign_name,
        );

        $this->session->set("data_summaries", $data_summaries);


        if ($post) {
            $step = array(
                'step1' => false,
                'step2' => true,
                'step3' => false,
            );

            $step_value = array(
                'step1' => $step1,
                'step2' => $post,
                'step3' => ''
            );


            $this->session->set("step", $step);
            $this->session->set("step_value", $step_value);

            return $this->response->redirect('quotation/step_3/' . $parent_id);
        }

        if ($parent_id != 0) {
            $dataquotationitemrev = QuotationItem::find("quotation_id = $parent_id ");
        }

//get data for dropdown
        $data_product_partner_rel = ProductPartner::getDataBrand();
        $province = Province::find(array("order" => "id"));
        $city = City::find(array("order" => "id"));
//

        $var = array(
            'active_menu' => 'quotation',
            'sub_menu' => 'add_quotation',
            'page_title' => 'Quotation',
            'parent_id' => $parent_id,
            'product_partner' => $data_product_partner_rel,
            'dataquotationitemrev' => $dataquotationitemrev,
            'province' => $province,
            'city' => $city,
            'sub_title' => 'quotation list & management',
            'active_step' => 'step2',
            'extra' => array(
                'css' => array(
                    'external/metronic/assets/global/plugins/select2/css/select2.min.css',
                    'external/metronic/assets/global/plugins/select2/css/select2-bootstrap.min.css',
                    'external/metronic/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                    'external/metronic/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
                ),
                'js' => array(
                    'external/metronic/assets/global/plugins/select2/js/select2.full.min.js',
                    'external/metronic/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                    'external/metronic/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
                    'external/metronic/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
                    'external/metronic/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
                ),
            )
        );

        $this->view->setVar('var', $var);
    }

    function getdataproductpartnerAction() {
        $this->view->disable();

        $post = $this->request->getPost();

        if ($post) {
            $pp_id = $post['product_partner'];

            $productPartner = ProductPartner::getProductPartnerByID($pp_id);

            echo json_encode($productPartner);
        }
    }

    public function getdatacityAction() {
        $this->view->disable();

        $post = $this->request->getPost();

//if ($post) 
//{
        $province_id = $post['province'];

        $datacity = City::find("province = $province_id");

        foreach ($datacity as $city) {
            $data[] = array(
                'id_city' => $city->id,
                'city' => $city->city,
            );
        }

//var_dump($data);
//var_dump($datacity);
        echo json_encode($data);
//}	
    }

    public function step_3Action($parent_id = null) {

        $post = $this->request->getPost();

        $session_step = $this->session->get('step_value');
        $session_step1 = $session_step['step1'];
        $session_step2 = $session_step['step2'];

        $agency_id = $session_step1['advertiser'];
        $agency = Client::findFirst("client_id = $agency_id ");

        $payment_id = $session_step1['payment_type'];
        $payment = PaymentType::findFirst("payment_id = $payment_id");

        $length = sizeof($session_step2['product_partner']);

        if ($post) {
//$step = $this->session->get('step');
//$step['step3'] = true;
//$this->session->set("step", $step);

            return $this->response->redirect('quotation');
        }

        $var = array(
            'active_menu' => 'quotation',
            'sub_menu' => 'add_quotation',
            'page_title' => 'Quotation',
            'agency' => $agency,
            'parent_id' => $parent_id,
            'payment' => $payment,
            'sub_title' => 'quotation list & management',
            'active_step' => 'step3',
            'extra' => array()
        );

        $this->view->setVar('var', $var);
    }

    public function viewAction($id_quotation = 0) {

        $dataquotation = Quotation::findFirst("quotation_id = $id_quotation");

        $datarevisiquotation = Quotation::find("parent_id = $id_quotation");

        $dataquotationitem = QuotationItem::getItembyQuoId($id_quotation);
        $dataquotationstatus = QuotationStatus::find();

        $post = $this->request->getPost();

        if ($post) {
            $status_update = $post['status'];
            $reason = $post['reason'];

            $updatestatus = Quotation::findFirst("quotation_id = $id_quotation");
            $updatestatus->status = $status_update;
            if ($status_update == 3) {
                $updatestatus->cancel_reason = $reason;
            }

            if (!$updatestatus->update()) {
                foreach ($updatestatus->getMessages() as $message) {
                    var_dump($message);
                    exit;
                }
            } else {
                $this->flash->success('<strong>Well done!</strong> You update Quotation Status.');
                return $this->response->redirect('quotation');
            }
        }
        $var = array(
            'active_menu' => 'quotation',
            'sub_menu' => 'add_quotation',
            'page_title' => 'Quotation',
            'sub_title' => 'quotation list & management',
            'data_quotation' => $dataquotation,
            'dataquotationitem' => $dataquotationitem,
            'dataquotationstatus' => $dataquotationstatus,
            'datarevisi' => $datarevisiquotation,
            'extra' => array(
                'css' => array(),
                'js' => array(
                    'external/metronic/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
                    'external/metronic/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
                ),
            )
        );

        $this->view->setVar('var', $var);
    }

    public function finishAction($parent_id = null) {
        $file = $this->session->get('file_mo');
        $length_files = sizeof($file);
        //exit ;

        $datasession = $this->session->get('step_value');
        $dataquotation = $datasession['step1'];
        $dataquotationitem = $datasession['step2'];

//insert tabel_quotation		
        $quotationinsert = new Quotation();
        $quotationinsert->quotation_number = $dataquotation['quotation_number'];
        $quotationinsert->quotation_date = $dataquotation['quotation_date'];
        $quotationinsert->agency_id = $dataquotation['client'];
        $quotationinsert->advertiser_id = $dataquotation['advertiser'];
        $quotationinsert->brand_id = $dataquotation['brand'];
        $quotationinsert->campaign_name = $dataquotation['campaign_name'];
        $quotationinsert->payment_type = $dataquotation['payment_type'];
        $quotationinsert->total = 0;
        $quotationinsert->tax = 0;
        $quotationinsert->grand_total = 0;
        $quotationinsert->status = 1;
        $quotationinsert->created_on = date("Y-m-d H:i:s");
        $quotationinsert->created_by = $dataquotation['sales'];


        if (!$quotationinsert->create()) {
            foreach ($quotationinsert->getMessages() as $message) {
                var_dump($message);
                exit;
            }
        } else {
            $quotation_number = $dataquotation['quotation_number'];
            $data_quotation_id = Quotation::findFirst("quotation_number = '$quotation_number' ");
            $quotation_id = $data_quotation_id->quotation_id;

            $length = sizeof($dataquotationitem['product_partner']);

            for ($i = 0; $i < $length_files; $i++) {
                $quotation_mo = new QuotationMo();
                $quotation_mo->name_file = $file[$i];
                $quotation_mo->quotation_id = $quotation_id;
                $quotation_mo->create();
            }
        }

        for ($a = 0; $a < $length; $a++) {

            $quotationiteminsert = new QuotationItem();
            $quotationiteminsert->quotation_id = $quotation_id;
            $quotationiteminsert->pp_id = $dataquotationitem['product_partner'][$a];
            $quotationiteminsert->province = $dataquotationitem['province'][$a];
            $quotationiteminsert->city = $dataquotationitem['city'][$a];
            $quotationiteminsert->area = $dataquotationitem['area'][$a];
            $quotationiteminsert->start_date = $dataquotationitem['start_date'][$a];
            $quotationiteminsert->end_date = $dataquotationitem['end_date'][$a];
            $quotationiteminsert->amount = $dataquotationitem['amount'][$a];
            $quotationiteminsert->price = $dataquotationitem['product_price'][$a];
            $quotationiteminsert->discount = $dataquotationitem['discount'][$a];
            $quotationiteminsert->target = $dataquotationitem['target'][$a];
            $quotationiteminsert->unit = $dataquotationitem['unit'][$a];

            if (!$quotationiteminsert->create()) {
                foreach ($quotationiteminsert->getMessages() as $message) {
                    var_dump($message);
                    exit;
                }
            }
        }

        if (sizeof($parent_id) != 0) {

            $quotationinsertp_id = Quotation::findFirst("quotation_id = $quotation_id ");
            $quotationinsertp_id->parent_id = $parent_id;

            $quotationinsertp_id->update();
        } else {

            $quotationinsertp_id = Quotation::findFirst("quotation_id = $quotation_id ");
            $quotationinsertp_id->parent_id = 0;

            $quotationinsertp_id->update();
        }


        $this->flash->success('<strong>Well done!</strong> You successfully create Quotation and item.');
        $this->session->remove('file_mo');
        $this->session->remove('step_value');
        return $this->response->redirect('quotation');
    }

    public function removestepAction() {
        $step = array(
            'step1' => false,
            'step2' => false,
            'step3' => false,
        );

        $this->session->set("step", $step);

        return $this->response->redirect('quotation/');
    }

    public function addproductAction() {
        $this->view->disable();
        $province = Province::find(
                        array(
                            "order" => "id"
                        )
        );

        $sql1 = "select ProductPartner.pp_id,ProductPartner.price, Product.name as product, Partner.name as partner, DocStatus.name from ProductPartner
                 join DocStatus on (DocStatus.status = ProductPartner.doc_status) 
                 join Product on (Product.product_id = ProductPartner.product_id)
                 join Partner on (Partner.partner_id = ProductPartner.partner_id)
                 where ProductPartner.doc_status = 2 and 
                 ProductPartner.status = 1";

        $query = $this->modelsManager->createQuery($sql1);
        $data_product_partner_rel = $query->execute();

        $city = City::find(
                        array(
                            "order" => "id"
                        )
        );
        $post = $this->request->getPost();

        if ($post) {
            $length = $post['data_length'];


            $html = '<div class="row-element margin-top-15" id="row-element-' . ($length + 1) . '"> 
					<div class="row"> 
						<div class="col-md-6"> 
							<div class="form-group"> 
				            <label class="col-left col-md-9"> 
				                Product Partner 
				                <span class="required-info">*</span> 
				            </label> 
				            				 							
				            <select id="product-partner" name="product_partner[]" class="form-control input-select2 product-partner" data-id="' . ($length + 1) . '"> 
				                <option value="0">--Choose Product Partner--</option>';
            if (sizeof($data_product_partner_rel) > 0) {
                foreach ($data_product_partner_rel as $data) {
                    $html .= '
										<option value="' . $data->pp_id . '">' . $data->product . " - " . $data->partner . '</option>
												';
                }
            }
            $html .='</select> 
				        </div> 
						</div>	 
						<div class="col-md-6"> 
							<div class="row"> 
								<div class="col-md-4"> 
									<div class="form-group"> 
				    				<label>Product Price <span class="required-info">*</span></label> 
				                    <div> 
				                    	<input readonly="readonly" type="text" name="product_price[]" id="product-price-' . ($length + 1) . '" class="form-control input-sm" placeholder="Product Price">                                                       
				                    </div> 
				                </div> 
								</div>	 
								<div class="col-md-4"> 
									<div class="form-group"> 
				    				<label>Selling <span class="required-info">*</span></label> 
				                    <div> 
				                    	<input readonly="readonly" type="text" name="product_selling[]" id="product-selling-' . ($length + 1) . '" class="form-control input-sm" placeholder="Selling">                                                       
				                    </div> 
				                </div> 
								</div>	 
								<div class="col-md-4"> 
									<div class="form-group"> 
				    				<label>Discount <span class="required-info">*</span></label> 
				                    <div> 
				                    	<input readonly="readonly" type="text" name="discount[]" id="discount-' . ($length + 1) . '" class="form-control input-sm" placeholder="Discount">                                                       
				                    </div> 
				                </div> 
								</div>	 
								
								
							</div>	 
						</div>	 		       					
					</div> 	
					
					<div class="row"> 
						<div class="col-md-6"> 
							<div class="row"> 
								<div class="col-md-6"> 
									<div class="form-group"> 
				    				<label>Amount <span class="required-info">*</span></label> 
				                    <div> 
				                    	<input type="text" name="amount[]" id="amount' . ($length + 1) . '" class="form-control input-sm" placeholder="Amount">                                                       
				                    </div> 
				                </div> 
								</div>	 
								<div class="col-md-6"> 
									<div class="form-group"> 
				    				<label>Unit <span class="required-info">*</span></label> 
				                    <div> 
				                    	<input type="text" name="unit[]" id="unit' . ($length + 1) . '" class="form-control input-sm" placeholder="Unit">                                                       
				                    </div> 
				                </div> 
								</div>	 		       								
							</div>	 
						</div> 
						<div class="col-md-6"> 
							<div class="form-group"> 
							<label>Target <span class="required-info">*</span></label> 
				            <div> 
				            	<input type="text" name="target[]" id="target' . ($length + 1) . '" class="form-control input-sm" placeholder="Target">                                                       
				            </div> 
				        </div> 
						</div>	 	
					</div> 
					
					<div class="row"> 
						<div class="col-md-6"> 
							<div class="row"> 
								<div class="col-md-6"> 
									<div class="form-group"> 
				                    <label class="col-left col-md-9"> 
				                        Province 
				                        <span class="required-info">*</span> 
				                    </label> 
				                    				 							
				                    <select id="province' . ($length + 1) . '" name="province[]" class="form-control input-select2 province" data-id="' . ($length + 1) . '"> 
				                        <option value="">--Choose Province--</option>';
            if (sizeof($province) > 0) {
                foreach ($province as $data) {
                    $html .= '
													<option value="' . $data->id . '">' . $data->province . '</option>
												';
                }
            }
            $html .='</select>';
            $html .='</div> 
								</div> 
								<div class="col-md-6"> 
									<div class="form-group"> 
				                    <label class="col-left col-md-9"> 
				                        City 
				                        <span class="required-info">*</span> 
				                    </label>		 	                                    				 						
				                    <select disabled="disabled" id="city-' . ($length + 1) . '" name="city[]" class="form-control input-select2"> 
				                        <option value="">--Choose City--</option> 
				                       	
				                    </select> 
				                </div> 
								</div>	 	
							</div>		 
						</div>	 
						<div class="col-md-6"> 
							<div class="form-group"> 
							<label>Area <span class="required-info">*</span></label> 
				            <div> 
				            	<input type="text" name="area[]" id="area' . ($length + 1) . '" class="form-control input-sm" placeholder="Area">                                                       
				            </div> 
				        </div>	 
						</div>	 		       					
					</div>	 
					
					<div class="row"> 
						<div class="col-md-6"> 
							<div class="form-group"> 
							<label>Start Date <span class="required-info">*</span></label> 
				            <div> 
				            	<input type="text" name="start_date[]" id="start-date' . ($length + 1) . '" class="form-control input-sm date-picker" placeholder="Start Date">                                                       
				            </div> 
				        </div> 
						</div> 
						<div class="col-md-6"> 
							<div class="form-group"> 
							<label>End Date <span class="required-info">*</span></label> 
				            <div> 
				            	<input type="text" name="end_date[]" id="end-date' . ($length + 1) . '" class="form-control input-sm date-picker" placeholder="End Date">                                                       
				            </div> 
				        </div> 
						</div>	 	       					
					</div>		 
					
					<div class="row"> 
						<div class="col-md-12 align-right"> 
							<a href="javascript:;" class="btn green-haze add-product" data-id="' . ($length + 1) . '"><i class="fa fa-plus"></i> Product</a> 
							<a href="javascript:;" class="btn red remove-product" data-id="' . ($length + 1) . '"><i class="fa fa-minus"></i> Product</a> 							
						</div>	 
					</div>	 
				</div>';

            $data = array(
                'html' => $html
            );

            echo json_encode($data);
        }
    }

}
